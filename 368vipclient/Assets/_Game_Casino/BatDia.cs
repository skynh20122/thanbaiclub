﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BatDia : MonoBehaviour
{
    public GameObject bat;
    public Image[] vis;
    public Sprite[] baucuaVisSpite;
    public Sprite[] xocdiaVisSpite;
    public Sprite[] taixiuVisSprite;
    public Text txtCoolDown;
    public Image imgCoolDown;

    Animator batAnim;
    public Vector3 originPos;
    bool isOpen;
    long DOTweenId;

    public CanvasGroup canvasBat;

    void OnEnable()
    {
        batAnim = GetComponent<Animator>();
        originPos = transform.position;
        isOpen = false;
        DOTweenId = System.DateTime.Now.Ticks;
        if (IGUIM_Casino.instance != null)
            vis[3].gameObject.SetActive(IGUIM_Casino.instance.casinoMode == CasinoMode.XOCDIA);

        if (canvasBat == null)
            canvasBat = gameObject.GetComponent<CanvasGroup>();
    }
    private void OnDisable()
    {
        DOTween.Kill(DOTweenId);
        transform.DOKill();
    }

    public void SetTime(float time)
    {
        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
            SetCanvasBat(true);
           
        }
        if (time <= 0)
        {
            txtCoolDown.gameObject.SetActive(false);
            imgCoolDown.gameObject.SetActive(false);
        }
        else
        {
            txtCoolDown.gameObject.SetActive(true);
            imgCoolDown.gameObject.SetActive(true);
            DOTween.Kill(DOTweenId);
            DOVirtual.Float(time, 0, time, (x) =>
            {
                if(imgCoolDown != null)
                    imgCoolDown.fillAmount = x / time;
                txtCoolDown.text = x.ToString("0");
            }).SetEase(Ease.Linear).SetId(DOTweenId);
        }
    }
    public void StartShake()
    {
        if (OGUIM.currentLobby.id == (int)LobbyId.XOCDIA || OGUIM.currentLobby.id == (int)LobbyId.XOCDIA_OLD)
            UIManager.PlaySound("shake3", 5.5f);
        SetCanvasBat(IGUIM_Casino.instance.casinoMode != CasinoMode.TAIXIU);
        if (originPos == Vector3.zero)
            originPos = transform.position;
        transform.position = originPos;
        transform.DOKill();
        transform.DOShakePosition(30, 30, 15, 0, true).SetEase(Ease.Linear).SetDelay(isOpen ? 1 : 0);
        Close();
    }
    public void StopShake()
    {
        //SetCanvasBat(true);
       
        if (originPos == Vector3.zero)
            originPos = transform.position;
        transform.DOKill();
        transform.DOMove(originPos, 0.5f);
    }
    public void Open()
    {
        if(OGUIM.currentLobby.id == (int)LobbyId.XOCDIA  || OGUIM.currentLobby.id == (int)LobbyId.XOCDIA_OLD)
            UIManager.PlaySound("bell");
        SetCanvasBat(IGUIM_Casino.instance.casinoMode != CasinoMode.TAIXIU);

        StopShake();
        if (!isOpen && batAnim != null)
            batAnim.SetBool("isOpen", true);
        isOpen = true;
    }
    public void Close()
    {
        //SetCanvasBat(true);

        if (isOpen && batAnim != null)
            batAnim.SetBool("isOpen", false);
        isOpen = false;
    }
    public void SetVis(List<int> visData)
    {
        for (int i = 0; i < vis.Length; i++)
        {
            if (i >= visData.Count)
                return;
            int index = visData[i];
            if (IGUIM_Casino.instance.casinoMode == CasinoMode.XOCDIA)
            {
                vis[i].sprite = xocdiaVisSpite[index];
            }
            else if (IGUIM_Casino.instance.casinoMode == CasinoMode.BAUCUA)
            {
                vis[i].sprite = baucuaVisSpite[index - 1];
            }
            else
            {
                vis[i].sprite = taixiuVisSprite[index - 1];
            }
        }
    }

    public void SetCanvasBat(bool isShow)
    {
        if (canvasBat == null)
            return;

        if (isShow)
            canvasBat.alpha = 1;
        else
            canvasBat.alpha = 0;
    }
}
