﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class RoulltetGameManager : MonoBehaviour
{

    public static RoulltetGameManager instance { get; set; }
    public GameObject ObjRoullet;
    public ChipSprite prefabChip;
    public RoulletWheelView WheelView;

    public Transform PosSpawnChip;
    public Transform PosWinChip;
    public TextEffext textEfxPrefab;

    public Text TxtTotalMoneyBet;
    public List<RoulletPotNumber> LstPotView;
    public List<Image> LstSpriteChipBet;
    public int CurrentBet;
    [SerializeField]
    private GameObject chipTransform;
    [SerializeField]
    private PlayerMoneyView goldView;
    [SerializeField]
    private AvatarView avataview;

    public RoulletHistoryListView HistoryListView;

    private int[] lstBet = new int[] { 500, 10000, 50000, 500000 };
    //private Dictionary<int, PotBet> LstPotBet;
    private List<PotBet> LstPotBet;
    private List<ChipSprite> LstChipOnBoard;
    private Sprite CurSpriteChipBet;
    private List<CasinoVi> history;

    private int[] lstNumbetRed = new int[] { 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36 };
    private int[] lstNumbetBlack = new int[] { 2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35 };

    private int totalBet;
    private bool isSpinning;
    private bool musicCheck =false;

    private void Awake()
    {
        instance = this;

        musicCheck = UIManager.instance.musicToggle.isOn;
        if (musicCheck)
            UIManager.instance.MuteMusic();
    }
    // Use this for initialization
    void Start()
    {
        LstChipOnBoard = new List<ChipSprite>();
        LstPotBet = new List<PotBet>();
        history = new List<CasinoVi>();
        totalBet = 0;

        SetGameName();

        SellectCurrentBet(0);

        //OGUIM.instance.ShowButtonChatInGame(false);
        OGUIM.instance.mainLobiesCanvas.Hide();

        avataview.FillData(OGUIM.me);
        //goldView.FillData(MoneyType.Gold, OGUIM.me.gold);
    }

    public void SetGameName()
    {
        OGUIM.instance.lobbyName.text = "ROULLET";
        OGUIM.instance.roomBetValue.text = "";
    }

    public void updateMoneyView()
    {
        avataview.FillData(OGUIM.me);
    }
    public void SellectCurrentBet(int id)
    {
        CurrentBet = lstBet[id];
        CurSpriteChipBet = LstSpriteChipBet[id].sprite;
        //PosSpawnChip = LstSpriteChipBet[id].transform;
    }

    private bool CheckMoneyUser()
    {
        if (OGUIM.me.gold < (totalBet +CurrentBet))
        {
            if (OGUIM.Toast != null)
                OGUIM.Toast.ShowNotification("Bạn không đủ tiền đặt cược.");
            return true;
        }
        return false;
    }

    bool isClickBet = false;
    public void OnClickBetRoullet(int pot)
    {
        if (isClickBet)
        {
            isClickBet = false;
            return;
        }
        if (isSpinning)
        {
            return;
        }
        isClickBet = true;
        if (CheckMoneyUser())
        {
            return;
        }
        var potbet = new PotBet();
        potbet.type = RoulletPotType.MOT_SO;
        potbet.bet = CurrentBet;
        potbet.pot = new List<int>();
        potbet.pot.Add(pot);
        LstPotBet.Add(potbet);
        totalBet += CurrentBet;

        UpdateTotalBet();
    }

    public void OnClickBetPotType(int potType)
    {
        if (isClickBet)
        {
            isClickBet = false;
            return;
        }
        if(isSpinning)
        {
            return;
        }
        isClickBet = true;
        if (CheckMoneyUser()) return;

        var potbet = new PotBet();
        potbet.type = potType;
        potbet.bet = CurrentBet;
        potbet.pot = new List<int>();
        //potbet.pot.Add(pot);

        LstPotBet.Add(potbet);

        totalBet += CurrentBet;
        UpdateTotalBet();
    }

    public void OnClickBetRoullet(string numbers)
    {
        if (isClickBet)
        {
            isClickBet = false;
            return;
        }
        if (isSpinning)
        {
            return;
        }
        isClickBet = true;
        if (CheckMoneyUser())
        {
            return;
        }

        var numArr = numbers.Split(',');

        var typeBet = RoulletPotType.HAI_SO;
        if(numArr.Length == 4)
            typeBet = RoulletPotType.BON_SO;

        var potbet = new PotBet();
        potbet.type = typeBet;
        potbet.bet = CurrentBet;
        potbet.pot = GetListNumber(numArr);
        LstPotBet.Add(potbet);

        totalBet += CurrentBet;
        UpdateTotalBet();
    }

    private List<int> GetListNumber(string[] numbers)
    {
        var lst = new List<int>();
        for (int i = 0; i < numbers.Length; i++)
        {
            lst.Add(int.Parse(numbers[i]));
        }
        return lst;
    }

    public void OnClickBetRoulletRow(int row)
    {
        if (isClickBet)
        {
            isClickBet = false;
            return;
        }
        if (isSpinning)
        {
            return;
        }
        isClickBet = true;
        if (CheckMoneyUser()) return;

        var bet = new PotBet();
        bet.type = RoulletPotType.COT_12_SO;
        bet.bet = CurrentBet;
        bet.pot = GetLstNumberCol((Roullet_Col)row);
        LstPotBet.Add(bet);

        totalBet += CurrentBet;
        UpdateTotalBet();
    }

    public void OnClickBetRoulletGroup(int grp)
    {
        if (isClickBet)
        {
            isClickBet = false;
            return;
        }
        if (isSpinning)
        {
            return;
        }
        isClickBet = true;
        if (CheckMoneyUser()) return;

        var bet = new PotBet();
        bet.type = RoulletPotType.NHOM_12_SO;
        bet.bet = CurrentBet;
        bet.pot = GetLstNumberGroup((Roullet_Group)grp);
        LstPotBet.Add(bet);

        totalBet += CurrentBet;
        UpdateTotalBet();
    }

    bool isSpawn = false;
    public void SpawnChipBet(Transform posBet)
    {
        if (isSpawn)
        {
            isSpawn = false;
            return;
        }
        if (isSpinning)
        {

            return;
        }
        isSpawn = true;
        //Debug.Log("???? ");
        if (OGUIM.me.gold < CurrentBet + totalBet) return;
        var x = UnityEngine.Random.Range(-0.05f, 0.05f);
        var y = UnityEngine.Random.Range(-0.05f, 0.05f);
        Vector3 fromPos = PosSpawnChip.position;
        Vector3 toPos = posBet.position + new Vector3(x, y);

        var chip = Instantiate(prefabChip) as ChipSprite;
        chip.transform.SetParent(chipTransform.transform, false);
        //chip.ResetTransform();
        chip.transform.localScale = Vector3.one*1.25f;
        chip.transform.position = fromPos;
        chip.gameObject.SetActive(true);
        chip.SetSprite(CurSpriteChipBet);
        chip.SetPosition(toPos, true);
        //chip.userId = fakeUserFrom >= -1 ? fakeUserFrom : fromUser;
        //chip.pot = toPot;
        LstChipOnBoard.Add(chip);
        var temp = posBet.GetComponent<PotInfo>();
        temp.chipInPot.Add(chip.transform);

        UILogView.Log("Spwan Chip roullet");
        UILogView.Log("Spwan Chip roullet : " + CurSpriteChipBet.name);
    }
    bool isClickSpin = false;
    public void OnClickSpin()
    {
        if (LstPotBet.Count <= 0) 
        {
            if (OGUIM.Toast != null)
                OGUIM.Toast.ShowNotification("Bạn chưa đặt cửa nào.");
            return;
        }
        if (isSpinning)
        {
            return;
        }
        if (isClickSpin)
        {
            return;
        }
        isClickSpin = true;
        isSpinning = true;

        BuildWarpHelper.ROULLET_Spin(LstPotBet.ToList(), () =>
        {
            isClickSpin = false;
            UILogView.Log("Spin is timeout");
            //isSpining = false;
            OGUIM.Toast.Hide();
            isSpinning = false;
        });
    }

    public void OnClickCancelBet()
    {
        if (isSpinning)
        {

            return;
        }
        ResetBet();
    }

    private void ResetBet()
    {
        LstPotBet.Clear();
        totalBet = 0;
        UpdateTotalBet();
        ClearAllChip();
    }

    private void ClearAllChip()
    {
        for (int i = 0; i < LstChipOnBoard.Count; i++)
        {
            //Destroy(LstChipOnBoard[i].gameObject);
            if (LstChipOnBoard[i] != null)
                LstChipOnBoard[i].Recycle();
        }

        LstChipOnBoard.Clear();
    }

    private void UpdateTotalBet()
    {
        TxtTotalMoneyBet.text = Utils.GetMoneyFormat(totalBet);
    }

    private void AddListenner()
    {
        if (WarpClient.wc != null)
        {
            //WarpClient.wc.OnRoomStateChanged += _wc_OnRoomStateChanged;
            WarpClient.wc.OnStartSpinRoullet += OnStartSpinRoullet;
            //WarpClient.wc.OnSlotStartFailed += _wc_OnSlotStartFailed;
        }
    }

    private void RemoveListenner()
    {
        if (WarpClient.wc != null)
        {
            //WarpClient.wc.OnRoomStateChanged += _wc_OnRoomStateChanged;
            WarpClient.wc.OnStartSpinRoullet -= OnStartSpinRoullet;
            //WarpClient.wc.OnSlotStartFailed += _wc_OnSlotStartFailed;
        }
    }

    private void OnStartSpinRoullet(JSONObject data)
    {
        Debug.Log(" OnStartSpinRoullet: "+ data.ToString());
        var number = (int)data["result"]["vi"]["face"].n;

        var chip = (int)data["result"]["winChips"].n;
        var userCoin = (int)data["gold"].n;
        var chipChange = (int)data["result"]["chipchange"].n;
        Debug.Log(" result " + number + "   " + chip + "     " + userCoin);
        OGUIM.me.gold = userCoin;
        WheelView.StartSpin(number, () =>
        {
            var gr = CheckGroupWin(number);
            var col = CheckColWin(number);
            var isDown = IsHalfDownTable(number);
            var isRed = IsRedNumber(number);
            var isEven = IsEvenNumber(number);

            CasinoVi vi = new CasinoVi();
            vi.mainPot = number;
            vi.point = isRed ? 0 : 1;
            history.Add(vi);

            SetHistory();
                
            for (int i = 0; i < LstPotView.Count; i++)
            {
                var pot = LstPotView[i];
                var temp = pot.GetComponent<PotInfo>();
                if (number == 0)
                {
                    if (pot.NumbetPot == 0)
                    {
                        pot.ShowFxWin();
                        TakeChipWin(temp.chipInPot);
                    }
                    else
                    {
                        pot.HideFxWin();
                        TakeChipLose(temp.chipInPot);
                    }
                }
                else
                {
                    if (pot.NumbetPot < 0)
                    {
                        var gpwin = pot.CheckGroup(gr);
                        var colwin = pot.CheckCol(col);
                        var downwin = pot.IsHalfDownTable();
                        var redwin = pot.IsRedColorPot();
                        var evenwin = pot.IsEvenNumPot();
                        if (pot.NumbetPot == -5)
                        {
                            if (evenwin == isEven)
                            {
                                pot.ShowFxWin();
                                TakeChipWin(temp.chipInPot);
                            }
                            else
                            {
                                pot.HideFxWin();
                                TakeChipLose(temp.chipInPot);
                            }
                        }

                        if (pot.NumbetPot == -3)
                        {
                            if (downwin == isDown)
                            {
                                pot.ShowFxWin();
                                TakeChipWin(temp.chipInPot);
                            }
                            else
                            {
                                pot.HideFxWin();
                                TakeChipLose(temp.chipInPot);
                            }
                        }
                        if (pot.NumbetPot == -4)
                        {
                            if (redwin == isRed)
                            {
                                pot.ShowFxWin();
                                TakeChipWin(temp.chipInPot);
                            }
                            else
                            {
                                pot.HideFxWin();
                                TakeChipLose(temp.chipInPot);
                            }
                        }
                        if (pot.NumbetPot >= -2)
                        {
                            if (gpwin || colwin)
                            {
                                pot.ShowFxWin();
                                TakeChipWin(temp.chipInPot);
                            }
                            else
                            {
                                pot.HideFxWin();
                                TakeChipLose(temp.chipInPot);
                            }
                        }
                    }
                    else
                    {
                        if (pot.NumbetPot == number)
                        {
                            pot.ShowFxWin();
                            TakeChipWin(temp.chipInPot);
                        }
                        else
                        {
                            pot.HideFxWin();
                            TakeChipLose(temp.chipInPot);
                        }
                    }
                }
            }

            //if(chip > 0)
            //{
            //    var toastStrs = new List<string>();
            //    UIManager.PlaySound("winchip");
            //    toastStrs.Add(Ultility.CoinToString(chip) + " Chip");
            //    SpawnTextEfx(toastStrs[0], Vector3.zero);
            //}
            if(chipChange != 0)
            {
                var sound = chipChange > 0 ? "winchip" : "slow_down";
                var toastStrs = new List<string>();
                UIManager.PlaySound(sound);
                toastStrs.Add(Ultility.CoinToString(chipChange) + " " + GameBase.moneyGold.name);
                SpawnTextEfx(toastStrs[0], Vector3.zero);
            }
            //Debug.Log("????");
            OGUIM.me.gold = userCoin;
            OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, userCoin);
            avataview.moneyView.FillData(MoneyType.Gold, userCoin);
            //goldView.FillData(MoneyType.Gold, userCoin);
            DelayResetTable();
        });
    }

    private void DelayResetTable()
    {
        isClickSpin = false;
        //HideAllFxWin();
        DOVirtual.DelayedCall(3f, () =>
        {
            //isClickSpin = false;
            //OnClickCancelBet();
            HideAllFxWin();
            isSpinning = false;
            ResetBet();
        });
    }

    private void HideAllFxWin()
    {
        for (int i = 0; i < LstPotView.Count; i++)
        {
            LstPotView[i].HideFxWin();
        }
    }

    private void TakeChipWin(List<Transform> lstChip)
    {
        for (int i = 0; i < lstChip.Count;i++)
        {
            if (lstChip[i] != null)
            {
                var chip = lstChip[i].GetComponent<ChipSprite>();
                chip.SetPosition(PosWinChip.position, true, destroyOnComplete: true);
            }
        }
    }

    private void TakeChipLose(List<Transform> lstChip)
    {
        for (int i = 0; i < lstChip.Count; i++)
        {
            //var chip = lstChip[i].GetComponent<ChipSprite>();
            //chip.SetPosition(PosWinChip.position, true, destroyOnComplete: true);
            if (lstChip[i] != null)
                lstChip[i].gameObject.Recycle();
        }
    }

    private Roullet_Group CheckGroupWin(int num)
    {
        if (GetLstNumberGroup(Roullet_Group.Group1).Contains(num))
            return Roullet_Group.Group1;
        if (GetLstNumberGroup(Roullet_Group.Group2).Contains(num))
            return Roullet_Group.Group2;
        if (GetLstNumberGroup(Roullet_Group.Group3).Contains(num))
            return Roullet_Group.Group3;

        return Roullet_Group.Group1;
    }

    private Roullet_Col CheckColWin(int num)
    {
        if (GetLstNumberCol(Roullet_Col.Col1).Contains(num))
            return Roullet_Col.Col1;
        if (GetLstNumberCol(Roullet_Col.Col2).Contains(num))
            return Roullet_Col.Col2;
        if (GetLstNumberCol(Roullet_Col.Col3).Contains(num))
            return Roullet_Col.Col3;

        return Roullet_Col.Col1;
    }

    private bool IsHalfDownTable(int num)
    {
        return num <= 18;
    }

    private bool IsRedNumber(int num)
    {
        return lstNumbetRed.Contains(num);
    }

    private bool IsEvenNumber(int num)
    {
        return num % 2 == 0;
    }

    private void OnEnable()
    {
        AddListenner();
    }

    private void OnDisable()
    {
        RemoveListenner();

        //OGUIM.instance.ShowButtonChatInGame(true);
        if (musicCheck)
        {
            UIManager.instance.musicToggle.isOn = true;
            UIManager.instance.UpdateMusicState();
        }
    }

    public void SpawnTextEfx(string str, Vector3 pos)
    {
        var te = textEfxPrefab.Spawn();
        te.gameObject.transform.SetParent(transform, false);
        te.gameObject.transform.position = pos;
        te.gameObject.transform.localScale = Vector3.one;

        te.SetData(str.ToUpper(), 0.5f, 3f);
    }

    private List<int> GetLstNumberCol(Roullet_Col row)
    {
        switch(row)
        {
            case Roullet_Col.Col1:
                return new List<int>() { 1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34 };

            case Roullet_Col.Col2:
                return new List<int>() { 2, 5, 8, 11, 14, 17, 20, 23, 26, 29, 32, 35 };

            case Roullet_Col.Col3:
                return new List<int>() { 3, 6, 9, 12, 15, 18, 21, 24, 27, 30, 33, 36 };
        }
        return new List<int>();
    }

    private List<int> GetLstNumberGroup(Roullet_Group group)
    {
        switch (group)
        {
            case Roullet_Group.Group1:
                return new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

            case Roullet_Group.Group2:
                return new List<int>() { 13, 14,15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };

            case Roullet_Group.Group3:
                return new List<int>() { 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36 };
        }
        return new List<int>();
    }

    private void SetHistory()
    {
        HistoryListView.Get_Data(true);
        HistoryListView.listData = history;
        HistoryListView.FillData();
        DOVirtual.DelayedCall(0.5f, () =>
        {
            HistoryListView.uiListView.ScrollVerticalToRight();
        });
    }

    public void ShowUserHistory()
    {
        OGUIM.instance.popupHistory.Show(0);
        //OGUIM.instance.popupHistory.ShowOnly(0);
    }
}

[Serializable]
public class RoulletData
{
    public List<PotBet> bets;
}

[Serializable]
public class PotBet
{
    public List<int> pot;
    public int bet;
    public int type;
}

public class RoulletPotType
{
    public const int ERROR = -1;
    public const int MOT_SO = 0;
    public const int HAI_SO = 1;
    public const int BA_SO = 2;
    public const int BON_SO = 3;
    public const int SAU_SO = 4;
    public const int HANG_12_SO = 5;
    public const int COT_12_SO = 6;
    public const int NHOM_12_SO = 7;
    public const int RED = 8;
    public const int BLACK = 9;
    public const int EVEN = 10;
    public const int ODD = 11;
    public const int NUA_BAN_1 = 12;
    public const int NUA_BAN_2 = 13;

    public static string[] name = { "MOT_SO", "HAI_SO", "BA_SO", "BON_SO", "SAU_SO", "HANG_12_SO", "COT_12_SO", "NHOM_12_SO", "RED", "BLACK", "EVEN", "ODD", "NUA_BAN_1", "NUA_BAN_2" };
}

public enum Roullet_Col
{
    Col1 =0,
    Col2,
    Col3,
}

public enum Roullet_Group
{
    Group1=0,
    Group2,
    Group3,
}
