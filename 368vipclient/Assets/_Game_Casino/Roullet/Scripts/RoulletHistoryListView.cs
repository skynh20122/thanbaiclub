﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoulletHistoryListView : MonoBehaviour {

    public UIListView uiListView;
    public int items = 10;
    public List<CasinoVi> listData = new List<CasinoVi>();
    private List<RoulletItemHistoryView> listView = new List<RoulletItemHistoryView>();


    public void Get_Data(bool reload)
    {
        if (reload)
        {
            uiListView.ClearList();
            listData = new List<CasinoVi>();
            listView = new List<RoulletItemHistoryView>();
        }
    }

    public void FillData()
    {
        if (listData != null && listData.Any())
        {
            int count = 0;
            foreach (var i in listData)
            {
                try
                {
                    var ui = uiListView.GetUIView<RoulletItemHistoryView>(uiListView.GetDetailView());
                    if (count < listData.Count - 1)
                        ui.image.SetAlpha(0.8f);
                    //if (count % 2 != 0)
                    //    ui.GetComponent<Image>().color = new Color32(0, 0, 0, 0);

                    if (ui.FillData(i))
                        listView.Add(ui);

                    count++;
                }
                catch (System.Exception ex)
                {
                    Debug.Log("FillData: " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        OGUIM.Toast.Hide();
    }
}
