﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoulletItemHistoryView : MonoBehaviour {

    public Color[] spriteVis;
    public Image image;
    public Text txtNumber;
    public bool FillData(CasinoVi vi)
    {
        try
        {
            if(vi.mainPot == 0)
            {
                vi.point = 2;
            }
            //image.color = spriteVis[vi.point];
            txtNumber.text = vi.mainPot.ToString();
            txtNumber.color = spriteVis[vi.point];
        }
        catch (System.Exception ex)
        {
            Debug.Log("RoulletItemHistoryView FillData: " + ex.Message + " " + ex.StackTrace);
            return false;
        }

        return true;
    }
}
