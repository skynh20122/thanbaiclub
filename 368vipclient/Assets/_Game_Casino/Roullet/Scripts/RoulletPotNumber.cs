﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoulletPotNumber : MonoBehaviour {

    public PotInfo PotNumber;
    public int NumbetPot;
    public Roullet_Col ColPot;
    public Roullet_Group GroupPot;
    public bool IsHalfDown;
    public bool IsRedColor;
    public bool IsEvenPot;
    public GameObject ObjFx;

	// Use this for initialization
	void Start () {
        FindObj();
	}

    void FindObj()
    {
        if (PotNumber == null)
            PotNumber = gameObject.GetComponent<PotInfo>();

        if (ObjFx == null)
            ObjFx = gameObject.transform.Find("Win efx").gameObject;
    }

    public void ShowFxWin()
    {
        FindObj();
        ObjFx.SetActive(true);
    }

    public void HideFxWin()
    {
        FindObj();
        ObjFx.SetActive(false);
    }

    public bool CheckGroup(Roullet_Group gp)
    {
        if (NumbetPot != -1) return false;
        return GroupPot == gp;
    }

    public bool CheckCol(Roullet_Col col)
    {
        if (NumbetPot != -2) return false;
        return ColPot == col;
    }

    public bool IsHalfDownTable()
    {
        if (NumbetPot != -3) return false;
        return IsHalfDown;
    }

    public bool IsRedColorPot()
    {
        if (NumbetPot != -4) return false;
        return IsRedColor;
    }

    public bool IsEvenNumPot()
    {
        if (NumbetPot != -5) return false;
        return IsEvenPot;
    }
}
