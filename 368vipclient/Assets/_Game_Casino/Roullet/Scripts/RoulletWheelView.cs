﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RoulletWheelView : MonoBehaviour
{
    public GameObject ObjBgBall;
    public GameObject ObjBall;
    public GameObject ObjWheel;

    public Transform PosResult;
    public Transform PosBeginSpin;
    public Vector3 beginPosition;

    public int NumberCirBegin = 15;
    public float TimeBegin = 3f;

    public int NumberCirFast = 80;//30
    public float TimeFast = 6f;//3

    public int NumberCirAfter = 5;
    public float TimeAfter = 2f;

    public float TimeLast = 2f;

    private float[] LstEulerNumber = new float[] { 0f, 136f,302f,20f,322f,175f,263f,58f,204f,97f,185f,224f,39f
        ,244f,116f,341f,155f,283f,78f,332f,126f,312f,88f,194f,165f,293f,10f,254f,49f,68f,214f,107f,351f,146f,273f,30f,234f};
    //public List<RoulletSlotNumber> LstNumberWheel;

    private int Number;
    private void Awake()
    {
        beginPosition = transform.localPosition;
    }

    public void StartSpin(int number, Action onComplete)
    {
        ObjBgBall.SetActive(false);
        ObjBgBall.transform.localEulerAngles = ObjWheel.transform.localEulerAngles;
        ObjBall.transform.localPosition = PosBeginSpin.transform.localPosition;

        DOVirtual.DelayedCall(TimeFast * 2 / 3, () =>
        {
            ObjBgBall.SetActive(true);
            MoveBeginToResult(null);
        });

        //SpinNormal(NumberCirBegin, TimeBegin, () =>
        //{
        //    SpinNormal(NumberCirFast, TimeFast, () =>
        //    {
        //        SpinSlow(() =>
        //        {
        //            SpinFinal(number, () =>
        //            {
        //                 if (onComplete != null)
        //                     onComplete();
        //            });
        //        });
        //    });
        //});
        MoveToCenter(() =>
        {
            SpinNormal(NumberCirFast, TimeFast, () =>
            {
                SpinFinal(number, () =>
                {
                    DOVirtual.DelayedCall(2f, () =>
                    {
                        MoveToBegin(() =>
                        {
                            if (onComplete != null)
                                onComplete();
                        });
                    });

                });
            });
        });
    }


    public void MoveBeginToResult(Action onComplete)
    {
        ObjBall.transform.DOLocalMove(PosResult.localPosition, 2f).OnComplete(() =>
        {
            if (onComplete != null)
                onComplete();
        });
    }

    public void MoveResultToBegin(Action onComplete)
    {
        ObjBall.transform.DOLocalMove(PosBeginSpin.localPosition, 2f).OnComplete(() =>
        {
            if (onComplete != null)
                onComplete();
        });
    }

    public void MoveToCenter(Action onComplete)
    {
        var center = new Vector3(0, 0, 0);
        gameObject.transform.DOLocalMove(center, 1f).OnComplete(() =>
         {
             gameObject.transform.DOScale(2.5f, 1f).OnComplete(() =>
              {
                  if (onComplete != null)
                  {
                      onComplete();
                  }
              });
         });
    }

    public void MoveToBegin(Action onComplete)
    {
        gameObject.transform.DOScale(1f, 0.7f).OnComplete(() =>
        {
            gameObject.transform.DOLocalMove(beginPosition, 0.5f).OnComplete(() =>
             {
                 if (onComplete != null)
                     onComplete();
             });
        });
    }

    public void SpinNormal(int numCir, float time, Action onComplete)
    {
        Vector3 cir = new Vector3(0, 0, 360 * numCir + 100);
        Vector3 cirBg = new Vector3(0, 0, -(360 * numCir + 100));
        ObjWheel.transform.DOLocalRotate(cir, time, RotateMode.WorldAxisAdd)
        .OnComplete(() =>
        {
            //if (onComplete != null)
            //onComplete();
        }).SetEase(Ease.OutBack);
        ObjBgBall.transform.DOLocalRotate(cirBg, time, RotateMode.WorldAxisAdd)
        .OnComplete(() =>
        {
            if (onComplete != null)
                onComplete();
        }).SetEase(Ease.OutBack);
    }

    private void SpinSlow(Action onComplete)
    {
        Vector3 cir = new Vector3(0, 0, 360 * NumberCirAfter);
        Vector3 cirBg = new Vector3(0, 0, -(360 * NumberCirAfter));
        ObjWheel.transform.DOLocalRotate(cir, TimeAfter, RotateMode.WorldAxisAdd)
                .OnComplete(() =>
                {
                    //if (onComplete != null)
                    //onComplete();
                });
        ObjBgBall.transform.DOLocalRotate(cirBg, TimeAfter, RotateMode.WorldAxisAdd)
                .OnComplete(() =>
                {
                    if (onComplete != null)
                        onComplete();
                });
    }

    private void SpinFinal(int number, Action onComplete)
    {
        var curEulerWheel = ObjWheel.transform.eulerAngles;
        var desBall = new Vector3(0, 0, LstEulerNumber[number]);
        ObjBgBall.transform.localEulerAngles = ObjWheel.transform.localEulerAngles;
        //SpinSlow(() =>
        //{
        //    ObjWheel.transform.DOLocalRotate(curEulerWheel, TimeAfter, RotateMode.FastBeyond360)
        //        .OnComplete(() =>
        //        {
        //        });
        //});
        ObjBgBall.transform.DOLocalRotate(desBall, TimeLast, RotateMode.WorldAxisAdd)
                 .OnComplete(() =>
        {
            if (onComplete != null)
                onComplete();
        });
    }
}
