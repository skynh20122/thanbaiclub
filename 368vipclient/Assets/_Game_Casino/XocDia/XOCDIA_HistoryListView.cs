﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using UnityEngine.UI;

public class XOCDIA_HistoryListView : MonoBehaviour {
    private const string chanFormat = "<color=#000000>CHẴN:</color>";
    private const string leFormat = "<color=#FFFFFF>LẺ:</color>";

    public Text HisChanTxt, HisLeTxt;
    public UIListView uiListView;
    public int items = 10;
    public List<CasinoVi> listData = new List<CasinoVi>();
    private List<XOCDIA_HistoryItemView> listView = new List<XOCDIA_HistoryItemView>();
    private bool isShow = false;
    public GameObject fromPos;
    public GameObject toPos;
    private void Awake()
    {
        
    }
    public void Get_Data(bool reload)
    {
        if (reload)
        {
            uiListView.ClearList();
            listData = new List<CasinoVi>();
            listView = new List<XOCDIA_HistoryItemView>();
        }

    }

    public void FillData()
    {
        HisChanTxt.text = chanFormat + "\r\n" + listData.Count(x => x.mainPot % 2 == 1);
        HisLeTxt.text = leFormat + "\r\n" + listData.Count(x => x.mainPot % 2 == 0);
        if (listData != null && listData.Any())
        {
            int count = 0;
            foreach (var i in listData)
            {
                try
                {
                    var ui = uiListView.GetUIView<XOCDIA_HistoryItemView>(uiListView.GetDetailView());
                    if (count < listData.Count - 1)
                    {
                        foreach(var image in ui.images)
                        {
                            image.SetAlpha(0.7f);
                        }
                    }
                        
                    //if (count % 2 != 0)
                    //    ui.GetComponent<Image>().color = new Color32(0, 0, 0, 0);

                    if (ui.FillData(i))
                        listView.Add(ui);

                    count++;
                }
                catch (System.Exception ex)
                {
                    Debug.Log("FillData: " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        OGUIM.Toast.Hide();
    }

    public void ShowHistory()
    {
        if (!isShow)
        {
            transform.DOMove(toPos.transform.position, 0.5f).OnComplete(() => {
                isShow = true;
                DOVirtual.DelayedCall(3f, () => {
                    if (isShow)
                        transform.DOMove(fromPos.transform.position, 1f).OnComplete(() => {
                            isShow = false;
                        });
                });
            });
        }
        else
        {
            transform.DOMove(fromPos.transform.position, 1f).OnComplete(() => {
                isShow = false;
            });
        }
    }
}
