﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class XOCDIA_HistoryItemView : MonoBehaviour {
    public Sprite[] spriteVis;
    public Image[] images;
    public bool FillData(CasinoVi i)
    {
        Debug.Log("Casino Vi : " +JsonUtility.ToJson(i));
        try
        {
            if(images != null && images.Length > 0)
            {
                for(int x = 0;x < images.Length; x++)
                {
                    images[x].sprite = spriteVis[i.face[x]];
                }
            }
           //image.sprite = spriteVis[i.face.Count(x => x == 1)];
        }
        catch (System.Exception ex)
        {
            Debug.Log("XOCDIA_HistoryItemView FillData: " + ex.Message + " " + ex.StackTrace);
            return false;
        }

        return true;
    }
}
