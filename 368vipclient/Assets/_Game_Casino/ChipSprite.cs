﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;

public class ChipSprite : MonoBehaviour {
    public static readonly string chipTweenId = "chipTweenId";
    RectTransform rect;
    public int userId;
    public int pot;
    public Image imgChip;
    public List<Sprite> LstSpriteChip;

    public void SetPosition(Vector3 position, bool isAnimate, float delay = 0, bool destroyOnComplete = false)
    {
        rect = GetComponent<RectTransform>();

        var time = 0.5f;
        rect.DOKill();
        if (isAnimate)
        {
            rect.DOMove(position, time).SetDelay(delay).OnComplete(() =>
            {
                if (destroyOnComplete)
                    gameObject.Recycle();
            }).SetEase(Ease.Linear).SetId(chipTweenId);
        }
        else
        {
            transform.position = position;
        }
    }
    public void SetSprite(Sprite sp)
    {
        if (imgChip == null)
            imgChip = gameObject.GetComponent<Image>();

        if (imgChip == null) return;

        imgChip.sprite = sp;
    }

    public void SetSprite(int id)
    {
        if (imgChip == null)
            imgChip = gameObject.GetComponent<Image>();

        if (imgChip == null || LstSpriteChip == null || LstSpriteChip.Count == 0) return;

        if (id >= LstSpriteChip.Count) return;

        imgChip.sprite = LstSpriteChip[id];
    }
}
