﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;

public class TaiXiuGameManager : CasinoGameManager
{
    public static TaiXiuGameManager Api { get { return _api; } }
    private static TaiXiuGameManager _api;
    private WaitForSeconds timeWait = new WaitForSeconds(2);
    private void Start()
    {
        _api = this;
    }


    private void OnEnable()
    {
        //historyViewTraditional.gameObject.SetActive(false);
        //GameController.Api.OnActionServerResponse -= OnServerResponse;
        //GameController.Api.OnActionServerResponse += OnServerResponse;

        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnTaiXiuClearBet += OnClearBet;
            WarpClient.wc.OnSetBetList += OnSetBetList;
            //WarpClient.wc.OnGetLobbyInfo += Wc_OnGetLobbyInfo;
        }

    }

    private void OnDisable()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnTaiXiuClearBet -= OnClearBet;
            WarpClient.wc.OnSetBetList -= OnSetBetList;
            //WarpClient.wc.OnGetLobbyInfo += Wc_OnGetLobbyInfo;
        }
    }

    public override void _wc_OnEndMatch(byte[] payLoadBytes)
    {
        base._wc_OnEndMatch(payLoadBytes);
        Debug.Log("overide on endmatch tãiiu");
        IGUIM_Casino.SetTime(0);
        var data = ZenMessagePack.DeserializeObject<CasinoTurnData>(payLoadBytes, WarpContentTypeCode.MESSAGE_PACK);
        if (data != null)
        {
            if (data.vi != null)
            {
                //set vi
                if (data.vi.faces != null && data.vi.faces.Any())
                    IGUIM_Casino.SetVis(data.vi.faces);//======================chua co

                //IGUIM_Casino.AddHistory(data.vi);
                IGUIM_Casino.instance.PlayAnimationDice(() =>
                {

                    IGUIM_Casino.instance.SetResultDice(data.vi.faces);
                    
                    StartCoroutine(EndedMatch(data));

                });
                //mo bat
                IGUIM_Casino.Open();
                IGUIM_Casino.instance.HideBatDia();
                //IGUIM_Casino.SetButtonActive("CASINO_huycuoc_btn", false);
            }
        }
    }

    IEnumerator EndedMatch(CasinoTurnData data)
    {
        Debug.Log("On end macth Tài Xỉu :" + JsonUtility.ToJson(data));
        yield return new WaitForSeconds(2);
        IGUIM_Casino.instance.HideBatDia();
        IGUIM_Casino.instance.ShowGoDice();
        //lay tien tu cua thua
        IGUIM_Casino.TakeChipToHostTaiXiu(data.vi.results.ToArray());
        IGUIM_Casino.SetResultTaiXiu(data.vi.results.ToArray());

        yield return timeWait;

        if (data.users != null)
        {
            var listUsers = new List<UserData>();
            listUsers.Add(data.user);
            IGUIM_Casino.SetUsers(listUsers);
        }

        for (int i = 0; i < data.vi.results.Count; i++)
        {
            if (data.vi.results[i] > 0)
            {
                IGUIM_Casino.AddChipFromHost(i);
            }
        }

        var chipWin = data.chipChange;
        if (chipWin != 0)
        {
            var pos = Vector3.zero;
            //IGUIM_Casino.SpawnTextEfx(chipchange > 0 ? "Thắng" : "Thua", pos, chipchange > 0);
            var str = Ultility.CoinToString(chipWin);// + " " + OGUIM.currentMoney.name;
            IGUIM_Casino.SpawnTextEfx(str, pos);
            UIManager.PlaySound("winchip");
        }
        IGUIM_Casino.instance.meMoneyView.FillData(MoneyType.Gold, data.gold);
        yield return timeWait;

        if (data.users != null && OGUIM.currentLobby.id == (int)LobbyId.TAIXIU2)
        {
            for (int i = 0; i < data.users.Count; i++)
            {
                var playersOnBoard = IGUIM_Casino.GetPlayersOnBoard();
                if (playersOnBoard.ContainsKey(data.users[i].id))
                {
                    var chipchange = data.users[i].chipChange;
                    if (chipchange != 0)
                    {
                        var pos = playersOnBoard[data.users[i].id].avatarView.imageAvatar.transform.position;
                        IGUIM_Casino.SpawnTextEfx("", pos, chipchange > 0);

                        if (chipchange != 0)
                        {
                            var str = Ultility.CoinToString(playersOnBoard[data.users[i].id].userData.chipChange);// + " " + OGUIM.currentMoney.name;
                            IGUIM_Casino.SpawnTextEfx(str, playersOnBoard[data.users[i].id].avatarView.imageAvatar.transform.position + Vector3.down * 0.75f, chipchange > 0);
                        }

                    }
                }
            }
        }

        if (data.user != null && OGUIM.currentLobby.id == (int)LobbyId.XOCDIA_OLD)
        {
            var playersOnBoard = IGUIM_Casino.GetPlayersOnBoard();
            if (playersOnBoard.ContainsKey(data.user.id))
            {
                var chipchange = data.user.chipChange;
                if (chipchange != 0)
                {
                    var pos = playersOnBoard[data.user.id].avatarView.imageAvatar.transform.position;
                    IGUIM_Casino.SpawnTextEfx("", pos, chipchange > 0);

                    if (chipchange != 0)
                    {
                        var str = Ultility.CoinToString(data.user.chipChange);// + " " + OGUIM.currentMoney.name;
                        IGUIM_Casino.SpawnTextEfx(str, playersOnBoard[data.user.id].avatarView.imageAvatar.transform.position + Vector3.down * 0.75f, chipchange > 0);
                    }

                }
            }
        }



        var gold = data.gold;
        OGUIM.me.gold = gold;
        OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, gold);
        //tien bay ve user
        IGUIM_Casino.TakeChipToUser(true, data.vi.mainPot, data.vi.smallPot);
        IGUIM_Casino.instance.UpdateTotalBet(0);
        IGUIM_Casino.ClearUserBet(OGUIM.me.id);
    }

    public void OnSetBet(CasinoTurnData casinoData)
    {
        if (OGUIM.currentLobby.id == (int)LobbyId.TAIXIU2)
        {
            UILogView.Log("Casino on setbet : " + JsonUtility.ToJson(casinoData));
            Debug.Log("Tài Xỉu Post :" + casinoData.pot);
            IGUIM_Casino.instance.UpdateTotalBet(casinoData.total);
            IGUIM_Casino.SpawnChipSprite(OGUIM.me.id, casinoData.pot);
            //IGUIM_Casino.SetButtonActive("CASINO_huycuoc_btn", true);
        }

    }
    public void OnSetBetList(CasinoTurnData dataTurn)
    {
        Debug.Log("Onlist bet :" + JsonUtility.ToJson(dataTurn));
    }

    public void OnClearBet(byte[] payloads)
    {
        Debug.Log("On clear bet Tài Xỉu ");
        IGUIM_Casino.instance.UpdateTotalBet(0);
        IGUIM_Casino.ClearUserBet(OGUIM.me.id);
        //IGUIM_Casino.SetButtonActive("CASINO_huycuoc_btn", false);
    }
}
