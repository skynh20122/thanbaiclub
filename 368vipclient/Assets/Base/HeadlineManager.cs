﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Linq;

public class HeadlineManager : MonoBehaviour
{
    public Text contentTxt;
    public RectTransform rect;
    public int defaultMaxTextLength = 500;
    public float defaultTotalTime = 50;

    float originX = 1000;
    float totalTime = 40f;
    bool isRunning = false;
    float beginX, endX, currX, elapsedTime;
    int currentContentIndex = 0;

    List<Message> messages = new List<Message>();

    void OnEnable()
    {
        isRunning = true;
        Reset();
        Invoke("onLoaded", 1);
    }

    void OnDisable()
    {
        isRunning = false;
    }

    // Use this for initialization
    void Start()
    {
        WarpClient.wc.OnGetSystemMessagesDone += Wc_OnGetSystemMessagesDone;
        WarpClient.wc.OnNewHeadLine += Wc_OnNewHeadLine;
    }
    // Update is called once per frame
    void Update()
    {
        if (isRunning)
        {
            endX = -rect.sizeDelta.x;
            var pos = rect.anchoredPosition;
            if (elapsedTime < totalTime)
            {
                elapsedTime += Time.deltaTime;
                var step = elapsedTime / totalTime;
                var x = Mathf.Lerp(beginX, endX, step);
                pos.x = x;
            }
            else
            {
                NextText();
                elapsedTime = 0;
                beginX = originX;
                pos.x = originX;
            }
            rect.anchoredPosition = pos;
        }
    }

    private void Show()
    {
        this.gameObject.SetActive(true);
    }

    private void Hide()
    {

        this.gameObject.SetActive(false);

    }

    private void Reset()
    {
        endX = 0;
        currX = 0;
        elapsedTime = 0;
        beginX = originX;
        rect.anchoredPosition = Vector2.right * originX;
    }

    private void NextText(int index = -1)
    {
        if (!messages.Any())
            return;

        if (index == -1)
        {
            var special = messages.FindIndex(x => x.typeMessage == 1);
            if (special == -1)
            {
                currentContentIndex = (currentContentIndex + 1) % messages.Count;
            }
            else
                currentContentIndex = special;
            contentTxt.text = messages[currentContentIndex].content;
        }
        else
        {
            currentContentIndex = index % messages.Count;
            contentTxt.text = messages[currentContentIndex].content;
        }
        if (messages[currentContentIndex].content != null)
        {
            int len = messages[currentContentIndex].content.Length;
            totalTime = Mathf.Max(len, 300) * defaultTotalTime / defaultMaxTextLength;
        }

        if (messages[currentContentIndex].typeMessage == 1)
            messages.RemoveAt(currentContentIndex);
    }

    private void onLoaded()
    {
        //loin success
        if (WarpClient.wc != null && WarpClient.wc.SessionId != 0)
            WarpRequest.GetSystemMessage(SystemMessageType.HEADLINE_MESSAGE);
    }

    private void Wc_OnGetSystemMessagesDone(WarpResponseResultCode status, List<Message> data)
    {
        if (data != null)
        {
            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].senderId == 0 && string.IsNullOrEmpty(data[i].senderName) && data[i].type == (int)SystemMessageType.HEADLINE_MESSAGE)
                {
                    var mes = data[i];
                    mes.typeMessage = 0;
                    messages.Add(mes);
                }
            }
            NextText(0);
        }
    }

    private void Wc_OnNewHeadLine(HeadLineMessage data)
    {
        Message mess = new Message();
        mess.typeMessage = 1;
        mess.content = data.message;
        messages.Add(mess);
        //NextText(-1);
    }
}
