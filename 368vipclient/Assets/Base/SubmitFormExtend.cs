﻿using UnityEngine;
using UnityEngine.UI;

public class SubmitFormExtend : MonoBehaviour
{
    #region ValidateCard
    public static bool ValidateCard(InputField seriInputField, InputField pinInputField, Text statusLabel)
    {
        if (seriInputField == null)
            Debug.Log("seriInputField not found");
        if (pinInputField == null)
            Debug.Log("pinInputField not found");
        if (statusLabel == null)
            Debug.Log("statusLabel not found");
       

        if (!ValidateCard(seriInputField, statusLabel, "Seri thẻ", false))
            return false;
        if (!ValidateCard(pinInputField, statusLabel, "Mã thẻ", false))
            return false;
     
        return true;
    }



    public static bool ValidateCard(InputField inputField, Text statusLabel, string name, bool autoFocus)
    {
        string status = "";

        status = inputField.ValidateEmpty(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            statusLabel.text = status;
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateLength(name, 6, 30, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            statusLabel.text = status;
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        statusLabel.text = "";
        return true;
    }
    #endregion

    #region ValidateLogin
    public static bool ValidateLogin(InputField userNameInputField, InputField passwordInputField, bool autoFocus)
    {
        if (userNameInputField == null)
            Debug.Log("userNameInputField not found");
        if (passwordInputField == null)
            Debug.Log("passwordInputField not found");

        if (!ValidateUserName(userNameInputField, "Tên tài khoản", autoFocus))
            return false;
        if (!ValidatePassWord(passwordInputField, "Mật khẩu", autoFocus))
            return false;
        return true;
    }
    #endregion

    #region ValidateRegister
    public static bool ValidateRegister(InputField userNameInputField, InputField fullNameInputField, InputField passwordInputField, InputField phoneNumber,InputField verifyCode, bool autoFocus)
    {
        if (userNameInputField == null)
            Debug.Log("userNameInputField not found");
        //if (fullNameInputField == null)
        //    Debug.Log("passwordInputField not found");
        if (passwordInputField == null)
            Debug.Log("rePasswordInputField not found");
        if(verifyCode == null)
        {
            Debug.Log("Captcha not found");
        }

        //if (!ValidateFullName(fullNameInputField, "Tên nhân vật", autoFocus))
        //    return false;
        if (!ValidateUserName(userNameInputField, "Tên tài khoản", autoFocus))
            return false;
        if (!ValidatePassWord(passwordInputField, "Mật khẩu", autoFocus))
            return false;
        if (phoneNumber != null && !ValidatePhoneNumber(phoneNumber, "Số điện thoại", autoFocus))
            return false;
        if(verifyCode != null && !validateCaptcha(verifyCode))
        {
            return false;
        }
        return true;
    }

    public static bool validateCaptcha(InputField verifyCode)
    {
        string capcha = verifyCode.text.Trim().ToLower();
        if (!capcha.Equals(Captcha.instance.captcha.ToLower()))
        {
           
            OGUIM.Toast.Show("Mã xác nhận sai", UIToast.ToastType.Warning, 3f);
            Captcha.instance.resetCaptcha();
            return false;
        }
        
        return true;
    }

    public static bool ValidateUserName(InputField inputField, string name, bool autoFocus)
    {
        string status = "";

        status = inputField.ValidateEmpty(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateLength(name, 3, 18, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateUserName(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }
        return true;
    }

    public static bool ValidatePassWord(InputField inputField, string name, bool autoFocus)
    {
        string status = "";

        status = inputField.ValidateEmpty(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateLength(name, 6, 18, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }
        return true;
    }
    public static bool ValidateFullName(InputField fullName, string name, bool autoFocus)
    {
        string status = "";

        status = fullName.ValidateEmpty(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = fullName.ValidateLength(name, 3, 18, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = fullName.ValidateUserName(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }
        return true;
    }
    public static bool ValidateRePassWord(InputField passWord, InputField rePassWord, string name, bool autoFocus)
    {
        if (string.IsNullOrEmpty(rePassWord.text))
        {
            if (autoFocus)
                rePassWord.FocusInputField();
            OGUIM.Toast.Show(name + " không được để trống", UIToast.ToastType.Warning);
            return false;
        }

        if (rePassWord.text != passWord.text)
        {
            if (autoFocus)
                rePassWord.FocusInputField();
            OGUIM.Toast.Show(name + " không khớp", UIToast.ToastType.Warning);
            return false;
        }
        return true;
    }
    #endregion

    #region ValidateUpdateInfo
    public static bool ValidateUpdateInfo(InputField displayNameInputField, InputField phoneInputField, Text statusLabel)
    {
        if (displayNameInputField == null)
            Debug.Log("displayNameInputField not found");
        if (phoneInputField == null)
            Debug.Log("phoneInputField not found");
        if (statusLabel == null)
            Debug.Log("statusLabel not found");

        if (!ValidateDisplayName(displayNameInputField, "Tên hiển thị", false))
            return false;
        if (!ValidatePhoneNumber(phoneInputField, "Số điện thoại", false))
            return false;
        return true;
    }

    private static bool ValidateDisplayName(InputField inputField, string name, bool autoFocus)
    {
        string status = "";

        status = inputField.ValidateEmpty(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateLength(name, 6, 18, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateDisplayName(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }
        return true;
    }

    public static bool ValidatePhoneNumber(InputField inputField, string name, bool autoFocus, bool allowEmpty = true)
    {
        string status = "";
        if (string.IsNullOrEmpty(inputField.text) && allowEmpty)
        {
            return true;
        }

        status = inputField.ValidateNumb(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        status = inputField.ValidateLength(name, 8, 12, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }
        return true;
    }
    #endregion

    #region ValidateGiftCode
    public static bool ValidateString(InputField inputField, string name, bool autoFocus, int minLength = 0, int maxLength = 0)
    {
        string status = "";

        status = inputField.ValidateEmpty(name, autoFocus);
        if (!string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }


        status = inputField.ValidateLength(name, minLength, maxLength, autoFocus);
        if (minLength > 0 && maxLength > 0 && !string.IsNullOrEmpty(status))
        {
            OGUIM.Toast.Show(status, UIToast.ToastType.Warning, 3f);
            return false;
        }

        OGUIM.Toast.Hide();
        return true;
    }
    #endregion
}
