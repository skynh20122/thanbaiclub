﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatView : MonoBehaviour
{
    public int delayTimeSend = 3;
    public InputField inputChat;
    public Button buttonSendChat;
    public List<Text> iconOnButtons;
    public PlayerChatListView chatListView;

    public static string[] quickChat = new string[]
    {
        "Hello bà con",
        "Nhanh đi mấy thím",
        "Đánh thế chịu rồi",
        "Gà vãi nồi",
        "Thôi xong cmnr...",
        "Bà con ơi... hàng nóng hổi đây",
        "Giúp tại hạ hồi máu cái",
        "Chuẩn cơm mẹ nấu cmnr...",
        "Xin hộp sữa cho cháu",
        "Sao phải xoắn",
        "Các thím tuổi gì"
    };
    private DateTime lastTimeSend;

    public void Awake()
    {
    }

    public void Start()
    {
        WarpClient.wc.OnSenChatDone += Wc_OnSenChatDone;
    }

    private void Wc_OnSenChatDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            if (inputChat != null)
                inputChat.text = "";
            //buttonSendChat.interactable = true;
            lastTimeSend = DateTime.Now;
        }
        else
        {
            Debug.Log(status);
        }
    }

    //public void Update()
    //{
    //    if (gameObject.activeSelf && inputChat.isFocused)
    //    {
    //        if (!string.IsNullOrEmpty(inputChat.text))
    //        {
    //            buttonSendChat.interactable = true;
    //            if (Input.GetKeyDown(KeyCode.Return))
    //                SendChatText();
    //        }
    //        else
    //        {
    //            buttonSendChat.interactable = false;
    //        }
    //    }
    //}

    public void FillData(RootChatData data)
    {
        try
        {
            ShowAnimationChat();
            //chatListView.FillData(data);
        }
        catch (Exception ex)
        {
            UILogView.Log("PlayerChatListView FillData: " + "\n" + ex.Message + "\n" + ex.StackTrace, true);
        }
    }

    public void FillData(List<RootChatData> listData)
    {
        try
        {
            ShowAnimationChat();
            //chatListView.FillData(listData);
        }
        catch (Exception ex)
        {
            UILogView.Log("PlayerChatListView FillData: " + "\n" + ex.Message + "\n" + ex.StackTrace, true);
        }
    }


    public void SendQuickChat(int quickIndex)
    {
        if (OGUIM.instance.timeChat == 0f)
        {


            UILogView.Log("quickIndex:" + quickIndex, false);
            BuildWarpHelper.SendChat(OGUIM.currentRoom.room.id, quickChat[quickIndex], (int)ChatType.Text, null, null);
        }
        else
        {
            OGUIM.Toast.Show("Bạn chát quá nhanh ! Hãy chát chậm lại.");
        }
    }

    public void SendChatText()
    {
        if (OGUIM.instance.timeChat == 0f)
        {
            if (string.IsNullOrEmpty(inputChat.text))
            {
                var ran = UnityEngine.Random.Range(0, quickChat.Length);
                inputChat.text = quickChat[ran];
            }
            //buttonSendChat.interactable = false;
            Debug.Log("input chat : " + inputChat.text.Trim());

            BuildWarpHelper.SendChat(OGUIM.currentRoom.room.id, inputChat.text, (int)ChatType.Text, null, () => buttonSendChat.interactable = true);
        }
        else
        {
            OGUIM.Toast.Show("Bạn chát quá nhanh ! Hãy chát chậm lại.");
        }

    }
    public void SendChatTextMiniRoom()
    {
        if (string.IsNullOrEmpty(inputChat.text))
        {
            var ran = UnityEngine.Random.Range(0, quickChat.Length);
            inputChat.text = quickChat[ran];
        }
        MiniTaiXiu.instance.Chat_Send(inputChat.text);
    }


    public void SendChatMessage(string text)
    {
        if (string.IsNullOrEmpty(text))
        {
            var ran = UnityEngine.Random.Range(0, quickChat.Length);
            inputChat.text = quickChat[ran];
        }
        //buttonSendChat.interactable = false;
        BuildWarpHelper.SendChat(OGUIM.currentRoom.room.id, inputChat.text, (int)ChatType.Text, null, () => buttonSendChat.interactable = true);
    }

    public void SendChatEmotion(int emotionIndex)
    {
        if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null && OGUIM.currentRoom.room.id != 0)
        {
            //inputChat.interactable = false;
            BuildWarpHelper.SendChat(OGUIM.currentRoom.room.id, emotionIndex.ToString(), (int)ChatType.Emotion, "1", () => inputChat.interactable = true);
        }
        else
        {
            UILogView.Log("Next chat available in" + (delayTimeSend - (DateTime.Now - lastTimeSend).TotalSeconds) + " seconds");
        }
    }

    public void SendChatEmotion(string emotionName)
    {
        if (OGUIM.instance.timeChat == 0)
        {
            if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null && OGUIM.currentRoom.room.id != 0)
            {
                //inputChat.interactable = false;
                BuildWarpHelper.SendChat(OGUIM.currentRoom.room.id, emotionName, (int)ChatType.Emotion, "1", () => inputChat.interactable = true);
            }
            else
            {
                UILogView.Log("Next chat available in" + (delayTimeSend - (DateTime.Now - lastTimeSend).TotalSeconds) + " seconds");
            }
        }
        else
        {
            OGUIM.Toast.Show("Bạn chát quá nhanh ! Hãy chát chậm lại.");
        }

    }



    public void ShowAnimationChat()
    {
        if (iconOnButtons != null)
        {
            for (int i = 0; i < iconOnButtons.Count; i++)
            {
                iconOnButtons[i].color = new Color32(255, 200, 0, 255);
                iconOnButtons[i].text = "";
                int ii = i;
                DOVirtual.DelayedCall(1, () =>
                {
                    iconOnButtons[ii].color = new Color32(255, 255, 255, 255);
                    iconOnButtons[ii].text = "";
                });
            }
        }
    }
}
