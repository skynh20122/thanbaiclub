﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerChatView : MonoBehaviour
{
    public bool inListView = false;
    public Text displayName;
    public Text time;
    public Text message;
    public Image emotion;
    public GameObject chatView;
    public Color myColor;
    public Color ortherColor;
    public int delayTime;
    private string formatMe = "<color=#08B708>{0} :</color> {1}";
    private string formatUser = "<color=#FFFF00>{0} :</color> {1}";
    public bool chatTaiXiu = false;

    public int maxCharacterLength = 128;

    private RectTransform rectTransform;
    private RectTransform rectTransformParent;
    private DateTime dateTime;

    void Awake()
    {
        rectTransform = message.GetComponent<RectTransform>().parent.GetComponent<RectTransform>();
        if (inListView)
        {
            rectTransformParent = message.GetComponent<RectTransform>().parent.parent.GetComponent<RectTransform>();
        }
        else
        {
           
        }
    }
    private void Start()
    {
        
    }

    public bool FillData(RootChatData data)
    {
        try
        {
            if (data == null || string.IsNullOrEmpty(data.message))
                return false;

            if (chatTaiXiu)
            {
                if (data.userId == OGUIM.me.id)
                {
                    message.text = string.Format(formatMe, data.userName, data.message);

                    UILogView.Log(string.Format(formatMe, data.userName, data.message));
                }
                else
                {
                    message.text = string.Format(formatUser, data.userName, data.message);
                    UILogView.Log(string.Format(formatUser, data.userName, data.message));
                }

                return true;
            }

            
            if(data.type != (int)ChatType.Emotion) { 
            if (data.message.Length > maxCharacterLength)
                message.text = data.message.Substring(0, maxCharacterLength) + "...";
            else
                message.text = data.message;

            

            if (inListView)
            {
                if (data.userId == OGUIM.me.id)
                {
                    displayName.color = myColor;
                }
                else
                {
                    displayName.color = ortherColor;
                }
                displayName.text = data.userName;
                time.text = "vài giây trước";

                dateTime = DateTime.Now;

                Debug.Log(message.preferredWidth + " " + message.preferredHeight);

                var background = message.transform.parent.GetComponent<RectTransform>();
                var chatView = GetComponent<RectTransform>();
                var verticalLayoutGroup = background.GetComponent<VerticalLayoutGroup>();
                var mesHeight = message.preferredHeight + verticalLayoutGroup.padding.top + verticalLayoutGroup.padding.bottom + Math.Abs(rectTransform.offsetMax.y);
                chatView.sizeDelta = new Vector2(rectTransformParent.sizeDelta.x, mesHeight);
            }


            if (!inListView)
            {

                gameObject.SetActive(true);
                if (emotion != null)
                {
                    emotion.gameObject.SetActive(false);
                }
                if (!gameObject.activeSelf)
                    gameObject.SetActive(true);
                if (!gameObject.activeInHierarchy)
                    gameObject.SetActive(true);

                    OGUIM.instance.timeChat = delayTime;
            }
            }
            else
            {
                Debug.Log("Chat Emotion type : " + data.type);
                if (EmotionSheet.instance.resourcesDics.ContainsKey(data.message))
                {
                    emotion.sprite = EmotionSheet.instance.resourcesDics[data.message];
                    if (!emotion.gameObject.activeSelf)
                        emotion.gameObject.SetActive(true);

                    gameObject.SetActive(true);
                    if (chatView != null)
                        chatView.SetActive(false);

                    OGUIM.instance.timeChat = delayTime;
                }
            }
        }
        catch (System.Exception ex)
        {
            UILogView.Log("PlayerChatView FillData: " + "\n" + ex.Message + "\n" + ex.StackTrace, true);
            return false;
        }
        return true;
    }

    public void UpdateTime()
    {
        if (time != null && inListView)
            time.text = DateTimeConverter.ToRelativeTime(dateTime);
    }

    private void Update()
    {
        if (chatTaiXiu)
            return;
        if(OGUIM.instance.timeChat > 0)
        {
            OGUIM.instance.timeChat -= Time.deltaTime;
            if(OGUIM.instance.timeChat <= 0)
            {
                OGUIM.instance.timeChat = 0;
            }
        }

        if(OGUIM.instance.timeChat == 0)
        {
            gameObject.SetActive(false);
            chatView.SetActive(true);
        }
    }

}
