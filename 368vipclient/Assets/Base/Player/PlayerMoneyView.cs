﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMoneyView : MonoBehaviour
{
    public NumberAddEffect moneyNumber;
    public Image moneyImage;

    private UserData userData;
    public static PlayerMoneyView View;

    private void Awake()
    {
      
        View = this;
    }

    public void FillData(UserData _userData)
    {
        if (_userData == null || moneyNumber == null || moneyImage == null)
        {
            Debug.Log("_userData = NULL or moneyNumber = NULL or moneyImage = NULL");
            return;
        }

        userData = _userData;
        if (OGUIM.currentMoney.type == MoneyType.Gold)
        {
            moneyNumber.FillData(userData.gold);
            if (_userData.id == OGUIM.me.id)
                OGUIM.me.gold = userData.gold;
        }
        else
        {
            moneyNumber.FillData(userData.koin);
            if (_userData.id == OGUIM.me.id)
                OGUIM.me.koin = userData.koin;
        }
        moneyImage.sprite = OGUIM.currentMoney.image;


    }

    public void FillData(MoneyType type, long value)
    {
        if (type == MoneyType.Gold)
        {
            moneyNumber.FillData(value);
            moneyImage.sprite = GameBase.moneyGold.image;
        }
        else
        {
            moneyNumber.FillData(value);
            moneyImage.sprite = GameBase.moneyKoin.image;
        }
    }

    public void FillData(long value)
    {
        if(moneyNumber != null)
        moneyNumber.FillData(value);
    }

    public void UpdateBet(long value)
    {
        gameObject.SetActive(value > 0);
        if (OGUIM.currentMoney.type == MoneyType.Gold)
        {
            moneyNumber.FillData(value);
        }
        else
        {
            moneyNumber.FillData(value);
        }

        if (moneyImage != null && OGUIM.currentMoney.image != null)
            moneyImage.sprite = OGUIM.currentMoney.image;

      
    }

    public void ShowTopUp()
    {
#if UNITY_ANDROID || UNITY_IOS
        if (GameBase.underReview)
        {
            OGUIM.instance.popupIAP.Show();
            return;
        }
#endif
        if (GameBase.cash_in == 0)
            return;
        OGUIM.instance.Cash_In_Feature = PlayerPrefs.GetInt(Common.CASH_IN_FEATURE, 0);
        if (OGUIM.instance.Cash_In_Feature == 0)
        {
            OGUIM.instance.authPopup.Show(AuthenType.CASH_IN);
            return;
        }
        OGUIM.instance.popupTopUp.Show();
        //WarpRequest.GetLinkPolicy(GameBase.underReview);
    }
}
