﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultNumberDayView : MonoBehaviour
{

    public Text txtDay;

    public List<Text> lstTextResult;

    public void SetData(ResultDay data)
    {
        if(data.isToDay)
        {
            
        }
        else
        {
            
        }

        for (int i = 0; i < data.lstResult.Count; i++)
        {
            lstTextResult[i].text = data.lstResult[i];
        }
    }
}
