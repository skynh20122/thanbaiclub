﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameNumberController : MonoBehaviour {

    public GameObject PanelNormal;
    public GameObject PanelQuick;
    public GameObject PanelResult;
    public static GameNumberController instance { get; set; }
    public GameObject GroupBaseBtn;
    public GameObject GroupNormalBtn;
    public GameObject GroupXienBtn;
    public GameObject GroupDauDuoiBtn;
    private float timer = 0.5f;

    //public GameObject GroupInputXien;
    //public GameObject GroupInputNormal;

    //public InputField inputNormal;
    //public List<InputField> lstInputXien;
    public GameObject ObjGameXien;
    public List<GameObject> LstObjXien;
    [SerializeField]
    public List<List<Toggle>> LstNumberXien;
    public List<Toggle> LstNumXien1;
    public List<Toggle> LstNumXien2;
    public List<Toggle> LstNumXien3;
    public List<Toggle> LstNumXien4;

    public InputField inputBet;
    public Text txtRatioMoney;
    public Text txtRatioWin;

    public AvatarView CurrentPlayer;

    // Button Loai dat cuoc
    public Button BtBetDe;
    public Button BtnBetLo;
    public Button BtBetXien;
    public Button BtBetCang;
    // Button loai xien
    public Button BtXien2;
    public Button BtXien3;
    public Button BtXien4;

    public Button BtDau;
    public Button BtDuoi;

    public Text txtCurResult;
    public Button btnNextResult;
    public Button btnPreResult;
    public Text txtNextDay;
    public Text txtPreDay;

    public List<Text> lstTxtResult;
    public GameObject ObjResult;
    public GameObject ObjNoneResult;
    public UIPopupTabs popupHistory;
    public UIAnimation popupHistoryRoom;
    public HistoryListView historyViewTraditional;

    public GameObject ObjGameLo;
    public List<Toggle> LstNumberLo;

    public GameObject ObjDauDuoiBtn;
    public List<Toggle> LstNumberDauDuoi;

	private ButtonToggle btnTogBetDe;
	private ButtonToggle btnTogBetLo;
	private ButtonToggle btnTogBetXien;
    private ButtonToggle btnTogBetCang;

    private ButtonToggle btnToggleXien2;
    private ButtonToggle btnToggleXien3;
    private ButtonToggle btnToggleXien4;

    private ButtonToggle btnToggleDau;
    private ButtonToggle btnToggleDuoi;

    private UIAnimation animPanel;
    private DateTime currentDayView;

    private NumberBetType curBetType = NumberBetType.De;
    private XienType curXienType = XienType.Xien2;
    private NumberBetType curDauDuoi = NumberBetType.Dau;
    private List<int> RatioBet = new List<int>() { 0, 10000, 10000, 10000, 10000, 10000,10000 };
    private int CurRatioBet = 10000;
    private List<string> RuleWin = new List<string>()
    { Rule_De, Rule_Lo, "", "", "", Rule_Dau , Rule_Cuoi };
    private Dictionary<XienType, string> RuleXienWin = new Dictionary<XienType, string>()
    {
        {XienType.Xien2 , "Tỉ lệ trúng xiên 2 là 1 ăn 9"},
        {XienType.Xien3 , "Tỉ lệ trúng xiên 3 là 1 ăn 35"},
        {XienType.Xien4 , "Tỉ lệ trúng xiên 4 là 1 ăn 90"},
    };

    private Dictionary<string, LotteryHistory> historyResult = new Dictionary<string, LotteryHistory>();

    private const string Rule_De = "Đánh 2 số cuối của giải đặc biệt, vì dụ: giải đặc biệt 567890, nếu bạn đánh 90 thì trúng. Tỉ lệ 1 ăn 90";
    private const string Rule_Lo = "Đánh 2 số cuối của tất cả các dãy nếu về thì bạn trúng, nếu về 2 lần thì bạn được 2*3.5=x7 về " +
    "N lần thì bạn được N*3,5. Gồm các số từ 00 đến 99.";
    //private const string Rule_De = "";
    //private const string Rule_Lo = "";
    private const string Rule_Dau = "Đánh số đầu của 2 số cuối giải đặc biệt, ví dụ: giải đặc biệt 456789, nếu bạn đánh số 8 thì trúng. Tỉ lệ 1 ăn 8";
    private const string Rule_Cuoi = "Đánh số cuối của 2 số cuối giải đặc biệt, ví dụ: giải đặc biệt 456789, nếu bạn đánh số 9 thì trúng. Tỉ lệ 1 ăn 8";

    private List<string> TextNameBetType = new List<string>() { "Đề", "Lô", "Xiên 2", "Xiên 3", "Xiên 4", "Đầu", "Đuôi" };

    private void Start()
    {
        instance = this;
        FindObj();
        AddListener();
        //OGUIM.instance.chatInGameBtn.SetActive(false);
        //DisplayPlayerInfo();
        LstNumberXien = new List<List<Toggle>>();
        LstNumberXien.Add(LstNumXien1);
        LstNumberXien.Add(LstNumXien2);
        LstNumberXien.Add(LstNumXien3);
        LstNumberXien.Add(LstNumXien4);

        //GetResultDay(DateTime.Today);
        //ShowGame();
        OnClickSellectBet(curBetType);
        //inputBet.text = Utils.GetMoneyFormat(RatioBet[(int)curBetType]);
       // GetResultDay();
    }

    private void GetResultDay()
    {
        Debug.Log("Get result day :");
        BuildWarpHelper.SLOT_GetRoomResult(() =>
        {
            UILogView.Log("LOTTERY_GetSlotHistory " + "LOTTERY" + " is time out!");
        });
    }


    public void DisplayPlayerInfo()
    {
        CurrentPlayer.FillData(OGUIM.me);
        //CurrentPlayer.avatarView.FillDisplayName(OGUIM.me, true);
        UpdateMoneyUserView();
    }

    private void OnEnable()
    {
        //historyViewTraditional.gameObject.SetActive(false);
        //GameController.Api.OnActionServerResponse -= OnServerResponse;
        //GameController.Api.OnActionServerResponse += OnServerResponse;

        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnLotteryRoom += OnServerResponse;
            //WarpClient.wc.OnGetLobbyInfo += Wc_OnGetLobbyInfo;
        }

        //OGUIM.instance.ShowBackButton(false);
        //OGUIM.instance.inGameCanvas.Hide();
        //MenuTopManager.instance.setMenu(MenuType.PURPLE);
        //OGUIM.instance.mainLobiesCanvas.Hide();
    }

    private void OnDisable()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnLotteryRoom -= OnServerResponse;
            //WarpClient.wc.OnGetLobbyInfo += Wc_OnGetLobbyInfo;
        }
        //OGUIM.instance.ShowBackButton(true);
    }

    private void OnServerResponse(List<LotteryHistory> data)
    {
        Debug.Log("Xổ số : " + JsonUtility.ToJson(data) + data.Count);
        historyResult = new Dictionary<string, LotteryHistory>();

        for (int i = 0; i < data.Count; i++)
        {
            //Debug.Log("date ???  " + data[i].date);
            historyResult.Add(data[i].date, data[i]);
        }

        ShowHistoryResultDay(true);
    }

    private void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;
            if(timer<= 0)
            {
                timer = 0;
            }
        }
    }

    private bool ShowHistoryResultDay(bool isShowData)
    {
        //var curDate = currentDayView.Day

        if(historyResult.ContainsKey(currentDayView.ToShortDateString()))
        {
            if (isShowData)
            {
                ObjResult.SetActive(true);
                ObjNoneResult.SetActive(false);

                var history = historyResult[currentDayView.ToShortDateString()];
                lstTxtResult[0].text = history.x90;
                lstTxtResult[1].text = "";
                for (int i = 0; i < history.x9.Count; i++)
                {
                    if (i > 0)
                        lstTxtResult[1].text += " - ";
                    var temp = history.x9[i];
                    lstTxtResult[1].text += temp;
                }

                lstTxtResult[2].text = "";
                for (int i = 0; i < history.x3_5.Count; i++)
                {
                    if (i > 0)
                        lstTxtResult[2].text += " - ";
                    var temp = history.x3_5[i];
                    lstTxtResult[2].text += temp;
                }
            }
            return true;
        }
        else
        {
            ObjResult.SetActive(false);
            ObjNoneResult.SetActive(true);
        }

        return false;
    }

    private bool CheckDayResult(DateTime date)
    {
        if (historyResult.ContainsKey(date.ToShortDateString()))
            return true;
        return false;
    }

    public void UpdateMoneyUserView()
    {
        var userData = OGUIM.me;
        CurrentPlayer.moneyView.FillData(MoneyType.Gold, userData.gold);
    }

    private void FindObj()
    {
        if (btnToggleXien2 == null)
        {
            btnToggleXien2 = BtXien2.GetComponent<ButtonToggle>();
            if(btnToggleXien2 == null)
            {
                btnToggleXien2 = BtXien2.gameObject.AddComponent<ButtonToggle>();
            }
        }

        if (btnToggleXien3 == null)
		{
            btnToggleXien3 = BtXien3.GetComponent<ButtonToggle>();
            if (btnToggleXien3 == null)
			{
                btnToggleXien3 = BtXien3.gameObject.AddComponent<ButtonToggle>();
			}
		}

		if (btnToggleXien4 == null)
		{
            btnToggleXien4 = BtXien4.GetComponent<ButtonToggle>();
            if (btnToggleXien4 == null)
			{
                btnToggleXien4 = BtXien4.gameObject.AddComponent<ButtonToggle>();
			}
		}

        if (btnTogBetDe == null)
		{
            btnTogBetDe = BtBetDe.GetComponent<ButtonToggle>();
			if (btnTogBetDe == null)
			{
				btnTogBetDe = BtBetDe.gameObject.AddComponent<ButtonToggle>();
			}
		}

        if (btnTogBetLo == null)
		{
            btnTogBetLo = BtnBetLo.GetComponent<ButtonToggle>();
			if (btnTogBetLo == null)
			{
				btnTogBetLo = BtnBetLo.gameObject.AddComponent<ButtonToggle>();
			}
		}

        if (btnTogBetCang == null)
		{
            btnTogBetCang = BtBetCang.GetComponent<ButtonToggle>();
			if (btnTogBetCang == null)
			{
				btnTogBetCang = BtBetCang.gameObject.AddComponent<ButtonToggle>();
			}
		}

        if (btnTogBetXien == null)
		{
            btnTogBetXien = BtBetXien.GetComponent<ButtonToggle>();
			if (btnTogBetXien == null)
			{
				btnTogBetXien = BtBetXien.gameObject.AddComponent<ButtonToggle>();
			}
		}

        if (btnToggleDau == null)
        {
            btnToggleDau = BtDau.GetComponent<ButtonToggle>();
            if (btnToggleDau == null)
            {
                btnToggleDau = BtDau.gameObject.AddComponent<ButtonToggle>();
            }
        }

        if (btnToggleDuoi == null)
        {
            btnToggleDuoi = BtDuoi.GetComponent<ButtonToggle>();
            if (btnToggleDuoi == null)
            {
                btnToggleDuoi = BtDuoi.gameObject.AddComponent<ButtonToggle>();
            }
        }
        if (animPanel == null)
            animPanel = gameObject.GetComponent<UIAnimation>();
    }

	public void AddListener()
	{
        btnTogBetDe.AddListener(() => OnClickSellectBet(NumberBetType.De));
        btnTogBetLo.AddListener(() => OnClickSellectBet(NumberBetType.Lo));
        btnTogBetCang.AddListener(() => OnClickSellectBet(NumberBetType.Dau));
        btnTogBetXien.AddListener(() => OnClickSellectBet(NumberBetType.Xien));

        btnToggleXien2.AddListener(() => OnClickSellectXienType(XienType.Xien2));
        btnToggleXien3.AddListener(() => OnClickSellectXienType(XienType.Xien3));
        btnToggleXien4.AddListener(() => OnClickSellectXienType(XienType.Xien4));

        btnToggleDau.AddListener(() => OnClickSellectDauDuoi(NumberBetType.Dau));
        btnToggleDuoi.AddListener(() => OnClickSellectDauDuoi(NumberBetType.Duoi));

        //BtNormalLottery.onClick.RemoveAllListeners();
        //BtNormalLottery.onClick.AddListener(() => ShowBetNumberKind());
        //BtQuickLottery.onClick.RemoveAllListeners();
        //BtQuickLottery.onClick.AddListener(() => ShowSpinNumberKind());
	}

	public void ShowPrevDayResult()
    {
        var date = currentDayView;
        var preDay = date.AddDays(-1);
        GetResultDay(preDay);

        ShowHistoryResultDay(true);
	}

    public void ShowNextDayResult()
    {
        var date = currentDayView;
        var nextDay = date.AddDays(1);
        GetResultDay(nextDay);

        ShowHistoryResultDay(true);
    }

    #region Show Panel

    public void ShowGame()
    {
        FindObj();
        animPanel.Show(actionOnShowCompleted: () =>
        {
            //OGUIM.instance.inGameCanvas.Show();
            if(popupHistory != null)
            {
                popupHistory.Hide(null);
            }
            if(popupHistoryRoom != null)
            {
                popupHistoryRoom.Hide();
            }
            //OGUIM.instance.ShowBackButton(false);
            ShowBetNumberKind();
            ResetAll();
        });
    }

    public void HideGame()
    {
        animPanel.Hide();
    }

    public void ShowBetNumberKind()
    {
		PanelQuick.SetActive(false);
        PanelNormal.SetActive(true);
        PanelResult.SetActive(false);

        GroupBaseBtn.SetActive(false);
        GroupNormalBtn.SetActive(true);
	}

	public void ShowSpinNumberKind()
    {
        //GameService.Api.JoinRoomLotterySpin((obj) =>
        //{
        //    GameController.Api.roomLotterySpin = GameService.Api.LastJoinRoom();
        //    PanelQuick.SetActive(true);
        //    PanelNormal.SetActive(false);
        //    PanelResult.SetActive(false);
        //    GroupBaseBtn.SetActive(false);
        //    GroupNormalBtn.SetActive(false);
        //});
	}

    public void ShowResultPage()
    {
        PanelQuick.SetActive(false);
        PanelNormal.SetActive(false);
        PanelResult.SetActive(true);

        GroupBaseBtn.SetActive(true);
        GroupNormalBtn.SetActive(false);

        GetResultDay(DateTime.Today);
    }

    public void GetResultDay(DateTime today)
    {
        currentDayView = today;
        ShowTextResultDay();
    }
    #endregion
    #region Action click

    public void OnBackClick()
    {
        //if (PanelResult.activeSelf)
        //{
        //    OGUIM.GoBack();
        //}
        //else if(PanelNormal.activeSelf)
        //{
        //    ShowResultPage();
        //}
        //else if(PanelQuick.activeSelf)
        //{
        //    //GameService.Api.LeaveRoomLotterySpin();
        //    ShowResultPage();
        //}
        if(timer == 0)
        {
            timer = 0.5f;
            OGUIM.GoBack();
            //animPanel.Hide(() =>
            //{
            //    OGUIM.BackToLobies();
            //});
        }
       
    }

    public void OnClickSellectBet(NumberBetType betType)
    {
        HighlightBetType(betType);
        curBetType = betType;

		GroupXienBtn.SetActive(false);
        GroupDauDuoiBtn.SetActive(false);

  //      GroupInputXien.SetActive(false);
		//GroupInputNormal.SetActive(false);

        ShowTextRatioWin();
        ShowTextRatioBet();
        SetNumberLimitInput(2);

        ObjGameLo.SetActive(false);
        ObjDauDuoiBtn.SetActive(false);
        ObjGameXien.SetActive(false);

		switch(curBetType)
        {
            case NumberBetType.De:
                ObjGameLo.SetActive(true);
                //GroupInputNormal.SetActive(true);
                break;

            case NumberBetType.Lo:
                ObjGameLo.SetActive(true);
                //GroupInputNormal.SetActive(true);
				break;

            case NumberBetType.Dau:
                ObjDauDuoiBtn.SetActive(true);
                //GroupInputNormal.SetActive(true);
                GroupDauDuoiBtn.SetActive(true);
                SetNumberLimitInput(1);
                OnClickSellectDauDuoi(NumberBetType.Dau);
				break;

            case NumberBetType.Xien:
                ObjGameXien.SetActive(true);
				OnClickSellectXienType(XienType.Xien2);
				GroupXienBtn.SetActive(true);
                //GroupInputXien.SetActive(true);
				break;
        }

    }

    public void OnClickSellectXienType(XienType xienType)
    {
        HighlightXienType(xienType);
        curXienType = xienType;
        for (int i = 0; i < LstObjXien.Count;i++)
        {
            bool isEnable = i < (int)xienType;
            LstObjXien[i].gameObject.SetActive(isEnable);
        }
        var childGo = ObjGameXien.GetComponent<RectTransform>().GetChild(0).GetComponent<RectTransform>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(childGo);
        ShowTextRatioWin();
    }

    public void OnClickSellectDauDuoi(NumberBetType betType)
    {
        HighlightDauDuoiType(betType);
        curDauDuoi = betType;
        //for (int i = 0; i < lstInputXien.Count; i++)
        //{
        //    bool isEnable = i < (int)betType;
        //    lstInputXien[i].gameObject.SetActive(isEnable);
        //}
        //ShowTextRatioWin();
        txtRatioWin.text = RuleWin[(int)curDauDuoi];
    }

    private void SetNumberLimitInput(int numberCount)
    {
        //inputNormal.characterLimit = numberCount;
    }

    public void OnClickPlusBet()
    {
        var betPlus = RatioBet[(int)curBetType];
        var valueBet = GetValueBet() + betPlus;
        inputBet.text = Utils.GetMoneyFormat(valueBet);
    }

    public void OnClickSubBet()
    {
        var betSub = RatioBet[(int)curBetType];
        var curBet = GetValueBet();
        //if (GetValueBet() <= betSub) return;
        var valueBet = GetValueBet() - betSub;
        if (valueBet < betSub)
            valueBet = betSub;
        inputBet.text = Utils.GetMoneyFormat(valueBet);
    }

    public void OnClickShowHistory()
    {
        popupHistory.gameObject.SetActive(true);
        popupHistory.Show(0);
        historyViewTraditional.Get_DataLottery(true);
    }
    public void OnClickShowLogSpin()
    {
        //historyViewTraditional.ShowGameLoterrySpin();
    }

    public void CloseHistory()
    {
        //historyViewTraditional.HidePopup();
    }

    public void CheckNumberDe(int number)
    {
        //if(curBetType == NumberBetType.De)
        //{
        //    for (int i = 0; i < LstNumberLo.Count; i++)
        //    {
        //        LstNumberLo[i].isOn = (i == number);
        //    }
        //}
    }

    private List<int> GetLstNumberBet()
    {
        var lst = new List<int>();

        for (int i = 0; i < LstNumberLo.Count; i++)
        {
            if( LstNumberLo[i].isOn)
            {
                lst.Add(i);
            }
        }
        return lst;
    }

    #endregion

    #region send request 
    public void SendBet()
    {
        List<int> lstNumber = new List<int>();
        string nameBetType = "";
        int betType = (int)curBetType;
        if (curBetType != NumberBetType.Xien)
        {
            //if (string.IsNullOrEmpty(inputNormal.text)) return;
            //int numberBet = int.Parse(inputNormal.text);
            //lstNumber.Add(numberBet);
            if(curBetType == NumberBetType.Lo || curBetType == NumberBetType.De)
            {
                lstNumber = GetLstNumberBet();
            }
            else
            {
                for (int i = 0; i < LstNumberDauDuoi.Count; i++)
                {
                    if (LstNumberDauDuoi[i].isOn)
                    {
                        lstNumber.Add(i);
                    }
                }
            }
            nameBetType = TextNameBetType[(int)curBetType];
            if (curBetType == NumberBetType.Dau)
            {
                betType = (int)curDauDuoi;
                nameBetType = TextNameBetType[(int)curDauDuoi];
            }
        }
        else
        {
            for (int i = 0; i < LstNumberXien.Count; i++)
            {
                if (i + 1 > (int)curXienType) break;
                var lstTemp = LstNumberXien[i];
                for (int j = 0; j < lstTemp.Count; j++)
                {
                    if(lstTemp[j].isOn)
                    {
                        lstNumber.Add(j);
                    }
                }
            }
            nameBetType = TextNameBetType[(int)curXienType];
            betType = (int)curXienType;
        }
        var tempBet = inputBet.text;
        var converBet = inputBet.text.Replace(" ", "").Replace(".", "").Replace(",", "");
        if (string.IsNullOrEmpty(converBet))
        {
            if (OGUIM.Toast != null)
                OGUIM.Toast.ShowNotification("Bạn chưa đặt tiền cược.");
            return;
        }
        int valueBet = int.Parse(converBet);
        if(valueBet < 8000)
        {
            if (OGUIM.Toast != null)
                OGUIM.Toast.ShowNotification("Mức cược tối thiểu 8.000 chip, vui lòng đặt cược thêm");
            return;
        }
        if (lstNumber.Count == 0)
        {
            if (OGUIM.Toast != null)
                OGUIM.Toast.ShowNotification("Bạn chưa chọn số để cược");
            return;
        }
        if(betType >= 2 && betType <= 4)
        {
            if(lstNumber.Count != betType)
            {
                if (OGUIM.Toast != null)
                    OGUIM.Toast.ShowNotification("Bạn chưa chọn số để cược");
                return;
            }
        }
        string contentMess = "Bạn muốn đặt cược " + nameBetType + " : " + tempBet + " " + GameBase.moneyGold.name;
        OGUIM.MessengerBox.Show("Xác nhận", contentMess, button1Content: "Đồng ý", button1Action: () =>
        {
            var param = new lotteryBetRequest();
            //param.type = (int)MINIGAME.ADD_BET;
            param.betType = betType;// LO DE XIEN ??

            param.value = valueBet;
            param.numbers = lstNumber;
            WarpRequest.LotteryRequestBet(param);

            ResetAll();

        },button2Content: "Huỷ", button2Action: () =>
        {
            OGUIM.MessengerBox.Hide();
        });

    }

    public void OnLotterySetBetDone()
    {
        string contentMess = "Bạn đã đặt cược thành công. Chúc bạn may mắn.";
        OGUIM.MessengerBox.Show("", contentMess, button1Action: () =>
        {
            OGUIM.MessengerBox.Hide();
        });
    }
    #endregion

    #region view helper

    private void HighlightBetType(NumberBetType betType)
	{
        btnTogBetDe.SetImage(betType == NumberBetType.De ? Utils.BUTTON_1 : Utils.BUTTON_2);
        btnTogBetLo.SetImage(betType == NumberBetType.Lo ? Utils.BUTTON_1 : Utils.BUTTON_2);
        btnTogBetCang.SetImage(betType == NumberBetType.Dau ? Utils.BUTTON_1 : Utils.BUTTON_2);
        btnTogBetXien.SetImage(betType == NumberBetType.Xien ? Utils.BUTTON_1 : Utils.BUTTON_2);
	}

    private void HighlightXienType(XienType xienType)
	{
        btnToggleXien2.SetImage(xienType == XienType.Xien2 ? Utils.BUTTON_1 : Utils.BUTTON_2);
        btnToggleXien3.SetImage(xienType == XienType.Xien3 ? Utils.BUTTON_1 : Utils.BUTTON_2);
        btnToggleXien4.SetImage(xienType == XienType.Xien4 ? Utils.BUTTON_1 : Utils.BUTTON_2);
	}

    private void HighlightDauDuoiType(NumberBetType dauDuoiType)
    {
        btnToggleDau.SetImage(dauDuoiType == NumberBetType.Dau ? Utils.BUTTON_1 : Utils.BUTTON_2);
        btnToggleDuoi.SetImage(dauDuoiType == NumberBetType.Duoi ? Utils.BUTTON_1 : Utils.BUTTON_2);
    }

    private void DisplayResult(ResultDay day)
    {
        //resultView.SetData(day);
    }

    private void ShowTextResultDay()
    {
        var today = DateTime.Today;
        if(currentDayView.Day == today.Day && currentDayView.Month == today.Month && currentDayView.Year == today.Year)
        {
            //dang view hom nay
            btnNextResult.gameObject.SetActive(false);
            btnPreResult.gameObject.SetActive(true);
            var preDay = today.AddDays(-1);
            txtPreDay.text = preDay.ToShortDateString();
            txtCurResult.text = today.ToShortDateString();
        }
        else
        {
            
            var dateView = currentDayView;
            var preDay = dateView.AddDays(-1);
            txtPreDay.text = preDay.ToShortDateString();

            var nextDay = dateView.AddDays(1);
            txtNextDay.text = nextDay.ToShortDateString();
            txtCurResult.text = currentDayView.ToShortDateString();

            btnNextResult.gameObject.SetActive(true);
            btnPreResult.gameObject.SetActive(CheckDayResult(preDay));
        }
    }

    private void ShowTextRatioWin()
    {
        if(curBetType == NumberBetType.Xien)
        {
            txtRatioWin.text = RuleXienWin[curXienType];
        }
        else
        {
            txtRatioWin.text = RuleWin[(int)curBetType];
        }
    }
    private void ShowTextRatioBet()
    {
        //txtRatioMoney.text = LocalKey.LOTERRY_RATIO_BET.ToStringToken(Utils.GetMoneyFormat(RatioBet[(int)curBetType]));
    }

    private int GetValueBet()
    {
        int valueBet = int.Parse(inputBet.text);
        return valueBet;
    }

    public void UpdateViewInputBet()
    {
        
    }

    public void ShowResultGame()
    {
        
    }

    private void ResetAll()
    {
        for (int i = 0; i < LstNumberLo.Count; i++)
        {
            LstNumberLo[i].isOn = false;
        }

        for (int i = 0; i < LstNumberDauDuoi.Count; i++)
        {
            LstNumberDauDuoi[i].isOn = false;
        }

        for (int i = 0; i < LstNumberXien.Count; i++)
        {
            for (int j = 0; j < LstNumberXien[i].Count; j++)
            {
                LstNumberXien[i][j].isOn = false;
            }
        }

        inputBet.text = "";
    }
    #endregion

}

public enum KindNumberGame
{
	BetNumber,
	SpinNumber,
}

public enum NumberBetType
{
    De = 0,
    Lo = 1,
    Xien = 2,
    Dau = 5,
    Duoi = 6,
}

public enum XienType : int
{
    Xien2 = 2,
    Xien3 = 3,
    Xien4 = 4,
}

public class NumberResult
{
    public int Rank;
    public string Number;

    public NumberResult() {}

    public NumberResult(int rank, string number)
    {
        Rank = rank;
        Number = number;
    }

    public NumberResult(Dictionary<string,object> data)
    {
        //Rank = data.Get<int>(ServerKey.rank);
        //Number = data.Get<string>(ServerKey.number);
    }
}
