﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class XiDachGameManager : CardGameManager
{
    public const string Btn_Rut = "XIDACH_boc_btn";
    public const string Btn_Dan = "XIDACH_dan_btn";
    public const string Btn_CheckUser = "XIDACH_check_btn";

    public int pointCard;
    public int handType;
    private Text txtValueCard;
    public Dictionary<string, List<CardData>> suitesOnGame;

    public static void SpawnChipEfx(int fromId, int toId)
    {
        try
        {
            var playersOnBoard = IGUIM.GetPlayersOnBoard();

            GameObject go = IGUIM.SpawnChipEfx();
            try
            {
                var fromPos = playersOnBoard[fromId].avatarView.imageAvatar.transform.position;
                var toPos = playersOnBoard[toId].avatarView.imageAvatar.transform.position;

                go.transform.localScale = Vector3.one * 0.5f;
                go.transform.position = fromPos;
                go.transform.DOMove(toPos, 1).SetDelay(0.5f).SetEase(Ease.InOutQuart).OnComplete(() => go.Recycle());
            }
            catch (System.Exception ex)
            {
                UILogView.Log(ex.Message + " " + ex.StackTrace);
                go.Recycle();
            }
        }
        catch (System.Exception ex)
        {
            UILogView.Log("BACAY_GameManager/SpawnChipEfx throw ex: " + ex.Message + " " + ex.StackTrace);
        }
    }
    public override void AddListener()
    {
        base.AddListener();
        WarpClient.wc.OnUserGetCard += OnPlayerGetCard;
        WarpClient.wc.OnUserCheckCard += OnUserCheckCard;
        WarpClient.wc.OnCardsChanged += _wc_OnCardsChanged;
        WarpClient.wc.OnSetSubmitSuite += _wc_OnSetSubmitSuite;
        WarpClient.wc.OnShowHand += _wc_OnShowHand;
        WarpClient.wc.OnNotifyChickenBet += _wc_OnNotifyChickenBet;

        cardsEachRow = 3;
        suitesOnGame = new Dictionary<string, List<CardData>>();
        handType = 0;
        pointCard = 0;
    }

    public override void RemoveListener()
    {
        base.RemoveListener();
        WarpClient.wc.OnUserGetCard -= OnPlayerGetCard;
        WarpClient.wc.OnUserCheckCard -= OnUserCheckCard;
        WarpClient.wc.OnCardsChanged -= _wc_OnCardsChanged;
        WarpClient.wc.OnSetSubmitSuite -= _wc_OnSetSubmitSuite;
        WarpClient.wc.OnShowHand -= _wc_OnShowHand;
        WarpClient.wc.OnNotifyChickenBet -= _wc_OnNotifyChickenBet;
    }

    private void Start()
    {
        if (txtValueCard != null)
            txtValueCard.gameObject.SetActive(false);
    }

    public override void Instance_OnSizeChanged()
    {
        DOVirtual.DelayedCall(0.1f, () =>
        {
            ReorderCardOnHand();
            ChangeCardsOnBoard();
        });
    }

    private void _wc_OnShowHand(TurnData turn)
    {
        if (OGUIM.me.id == turn.userId)
        {
            UILogView.Log("Show hand");
            IGUIM.SetAllButtons(false, "PHOM_ha_btn");
        }
    }

    private void _wc_OnSetSubmitSuite(TurnData turn)
    {
        if (turn.userId == OGUIM.me.id)
        {
            IGUIM.SetAllButtons(false, "submit_btn");
        }
    }

    //move
    private void _wc_OnCardsChanged(TurnData turn)
    {
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        foreach (var user in turn.users)
        {
            if (playersOnBoard.ContainsKey(user.id))
            {
                playersOnBoard[user.id].userData.playedCards = user.playedCards;
                //playersOnBoard[user.id].userData.acquiredCards = user.acquiredCards;
            }
        }
        ChangeCardsOnBoard();
    }

    private void OnPlayerGetCard(PHOM_TurnData data)
    {
        UIManager.PlaySound("card");
        IGUIM.SetAllButtons(false);
        float time = 0.75f;
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        var currentPos = IGUIM.instance.cardOnHandTransform.position;
        if (data.card != null)
        {
            if (OGUIM.me.id == data.userId)
            {

                var go = IGUIM.CreateCard(cardPrefab, data.card, Vector3.zero);
                var card = go.GetComponent<Card>();
                float x = 0;
                float y = currentPos.y;
                card.SetCoord(4, 0);
                card.SetSortingOrder(4 + 200);
                go.transform.DOMove(new Vector3(x, y, 0), time).OnComplete(() =>
                {
                    card.SetFlip(true);
                    card.canTouch = true;
                    DOVirtual.DelayedCall(0.5f, ReorderCardOnHand);
                    ShowValueHandCard(data, false);
                });


                go.transform.DOScale(cardOnHandScale, time);
                if (cardsOnHand.ContainsKey(data.card.ToString()))
                    cardsOnHand[data.card.ToString()] = card;
                else
                    cardsOnHand.Add(data.card.ToString(), card);
            }
            else if (playersOnBoard.ContainsKey(data.userId))
            {
                var player = playersOnBoard[data.userId];
                var go = IGUIM.CreateCard(cardPrefab, data.card, Vector3.zero);
                go.transform.DOMove(player.cardView.transform.position, time);
                go.transform.DOScale(cardOnBoardScale, time).OnComplete(() =>
                {
                    Destroy(go);
                });
                player.addPlusRemainCard();
            }

            //update cardsinhold
            if (data.cardsInHold > 0)
            {
                IGUIM.instance.phomCardOnHold.setHoldText(data.cardsInHold);
            }
            else
            {
                IGUIM.instance.phomCardOnHold.subHoldCount();
            }

        }


    }

    private void _wc_OnUserGetCard(PHOM_TurnData data)
    {
        //UIManager.PlaySound("card");
        float time = 0.5f;
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        var currentPos = IGUIM.instance.cardOnHandTransform.position;
        if (data.card != null)
        {
            var go = IGUIM.CreateCard(cardPrefab, data.card, Vector3.zero);
            var card = go.GetComponent<Card>();
            float x = 0;
            float y = currentPos.y;
            card.SetCoord(4, 0);
            card.SetSortingOrder(4 + 200);
            go.transform.DOMove(new Vector3(x, y, 0), time).OnComplete(() =>
            {
                card.SetFlip(true);
                card.canTouch = true;
                DOVirtual.DelayedCall(0.5f, ReorderCardOnHand);
            });
            ShowValueHandCard(data, false);

            go.transform.DOScale(cardOnHandScale, time);
            if (cardsOnHand.ContainsKey(data.card.ToString()))
                cardsOnHand[data.card.ToString()] = card;
            else
                cardsOnHand.Add(data.card.ToString(), card);

            IGUIM.instance.phomCardOnHold.setHoldText(data.cardsInHold);
        }
    }

    //move
    public override void _wc_OnSubmitFailed()
    {
        foreach (var key in cardsOnHand.Keys)
            cardsOnHand[key].SetSelected(false);
    }

    public override void Instance_OnSetTurn(TurnData data)
    {
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        foreach (var key in playersOnBoard.Keys)
        {
            playersOnBoard[key].SetTurn(data.userId == key, interval, interval);
            if (data.userId == key)
                playersOnBoard[key].SetStatus("");
        }
        if (OGUIM.me.id == data.userId)
        {
            IGUIM.SetAllButtons(false);
            if (OGUIM.me.owner)
            {
                var btnRut = handType <= 2;
                var btnCheck = pointCard >= 16 || handType > 2;
                IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_CheckUser }, new bool[] { btnRut, btnCheck });
            }
            else
            {
                var btnRut = handType <= 2;
                var btnDan = pointCard >= 16 || handType > 2;
                IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_Dan }, new bool[] { btnRut, btnDan });

                //binh thuong
                //IGUIM.SetAllButtons(false, Btn_Rut, Btn_Dan);
            }
            // Auto-fix me.isPlayer = true if set turn to me

            Debug.Log("Is me set turn");
            OGUIM.me.isPlayer = true;
        }
        else
        {
            Debug.Log("not me is setturn" + IGUIM.instance.gameMode);
            IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_Dan, Btn_CheckUser }, new bool[] { false, false, false });
        }
        IGUIM.instance.ShowHideSlideBet(false);
    }

    public override void Instance_OnStartMatch(byte[] payLoadBytes)
    {
        base.Instance_OnStartMatch(payLoadBytes);
        suitesOnGame.Clear();
        IGUIM.instance.ShowHideSlideBet(false);
        foreach (var player in IGUIM.GetPlayersOnBoard())
        {
            player.Value.SetReady(true);
        }
        var data = ZenMessagePack.DeserializeObject<TurnData>(payLoadBytes, WarpContentTypeCode.MESSAGE_PACK);
        ShowValueHandCard(data, true);
        IGUIM.instance.phomCardOnHold.setHoldText(data.cardsInHold);
    }
    public override void Instance_OnEndMatch(byte[] payLoadBytes)
    {
        base.Instance_OnEndMatch(payLoadBytes);

        var data = ZenMessagePack.DeserializeObject<TurnData>(payLoadBytes, WarpContentTypeCode.MESSAGE_PACK);
        if (data == null)
        {
            Debug.Log(this.GetType().Name + " - Instance_OnEndMatch: data is null");
            return;
        }

        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        var owner = data.users.Where(x => x.owner == true).FirstOrDefault();
        IGUIM.UpdateRoomState(data.users.Count > 1 ? CardRoomState.WAIT : CardRoomState.STOP, 5);

        foreach (var user in data.users)
        {
            if (user.extra != null && user.extra.cards != null && user.extra.cards.Any() && user.id != OGUIM.me.id)
                Instance_OnSubmitTurn(new TurnData { userId = user.id, cards = user.extra.cards, handType = user.extra.handType, point = user.extra.point });
            if (playersOnBoard.ContainsKey(user.id))
            {
                user.isPlayer = true;
                if (GameBase.isOldVersion)
                {
                    user.isReady = false;
                    playersOnBoard[user.id].SetReady(user.isReady);
                }

                playersOnBoard[user.id].SetTurn(false);
               
                playersOnBoard[user.id].FillData(user);
                playersOnBoard[user.id].Reset();
                if (user.chipChange != 0 && user.isPlayer)
                {
                    DOVirtual.DelayedCall(1f, () =>
                    {
                        var str = Ultility.CoinToString(user.chipChange);// + " " + OGUIM.currentMoney.name;
                        IGUIM.SpawnTextEfxPoker(str, playersOnBoard[user.id].avatarView.imageAvatar.transform.position, user.chipChange > 0);
                    });

                    //spawn text thắng thua
                    if (!user.owner)
                    {
                        var tt = "";
                        if (user.chipChange > 0)
                        {
                            tt = "Thắng";
                        }
                        else
                        if (user.chipChange < 0)
                        {
                            tt = "Thua";
                        }

                        IGUIM.SpawnTextEfx(tt, playersOnBoard[user.id].avatarView.chipSpawn.transform.position, user.chipChange > 0);



                    }
                    //spawn chip 
                    if (!user.owner && owner != null)
                    {
                        Debug.Log("Id owner : " + owner.id);


                        if (user.chipChange > 0)
                        {
                            SpawnChipEfx(owner.id, user.id);
                        }

                        if (user.chipChange < 0)
                        {
                            SpawnChipEfx(user.id, owner.id);
                        }
                    }
                }
                else
                {
                    if (user.chipChange == 0 && user.isPlayer && !user.owner)
                    {
                        var str = "Hoà";
                        IGUIM.SpawnTextEfx(str, playersOnBoard[user.id].avatarView.chipSpawn.transform.position, user.chipChange > 0);
                    }
                }
            }
        }
        IGUIM.SetAllButtons(false);

        DOVirtual.DelayedCall(2f, () =>
         {
             IGUIM.SetAllButtons(false);

             IGUIM.SetButtonsActive(new string[] { "unready_btn", "ready_btn", "start_btn", "invite_btn" },
                                     new bool[] {false, !OGUIM.me.owner, OGUIM.me.owner,
                                                data.users.Count < IGUIM.instance.maxPlayerInGame && OGUIM.currentMoney.type == MoneyType.Gold });
         });
        if (!OGUIM.me.owner && !OGUIM.me.isReady && GameBase.isOldVersion)
        {
            DOVirtual.DelayedCall(2, () =>
            { OGUIM.Toast.Show("Sẵn sàng để chơi ván mới", UIToast.ToastType.Notification, 2); });
        }
    }

    public override void Instance_OnSubmitTurn(TurnData data)
    {
        

        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        float time = 0.5f;
        float distanceCard = 0.4f;
        Vector2 sizeOfCards = CalculateSizeOfCards(data.cards.Count, distanceCard, Vector3.down, cardOnBoardScale);

        var tempCards = data.cards.OrderBy(c => c.card).ToList();

        var valueOnCard = ValueCardHand(data.handType, data.point);
        float x = 0;
        float y = 0;
        var cardPos = IGUIM.instance.cardOnHandTransform.position;
        var currentPlayer = IGUIM.instance.currentUser;

        for (var i = 0; i < tempCards.Count; i++)
        {
            CardData cardData = tempCards[i];
            Card card = null;
            if (OGUIM.me.id == data.userId)
            {
                string cardStr = cardData.ToString();
                if (cardsOnHand.ContainsKey(cardStr))
                {
                    card = cardsOnHand[cardStr];
                    cardsOnHand.Remove(cardStr);

                    card.SetSortingOrder(cardsOnBoard.Count + 100);

                    x = i * distanceCard - (sizeOfCards.x * 0.5f - 0.5f);
                    y = cardPos.y;
                    currentPlayer.SetTurn(false);
                }
                ReorderCardOnHand();
                txtValueCard.gameObject.SetActive(false);
            }
            else if (playersOnBoard.ContainsKey(data.userId))
            {
                var player = playersOnBoard[data.userId];
                //player.SetRemainCard(0);
                player.SetTurn(false);
                //player.SetStatus("Dằng bài");
                var go = IGUIM.CreateCard(cardPrefab, cardData, playersOnBoard[data.userId].Position);
                go.transform.localScale = cardOnHandScale * Vector3.one;
                card = go.GetComponent<Card>();
                x = player.showCardDirection.x * ((i - 1) * distanceCard + 0.3f) + player.cardView.transform.position.x;
                y = player.cardView.transform.position.y;
                card.SetSortingOrder((i % 5) * (int)player.showCardDirection.x + 150);
            }
            if (card != null)
            {
                //card.SetSortingOrder(cardsOnBoard.Count, boardLayerName);
                card.SetFlip(true, 20);
                card.canTouch = false;

                var xx = x;
                var yy = y - 0.3f;
                TweenCallback action = () =>
                {
                    //var valueTxt = Instantiate(IGUIM.instance.casinoGameUI.valueCardTxt) as Text;
                    //if (IGUIM.instance == null || IGUIM.instance.transform == null)
                    //{
                    //    Destroy(valueTxt);
                    //    return;
                    //}
                    if (OGUIM.me.id != data.userId)
                    {
                        var valueTxt = Instantiate(IGUIM.instance.casinoGameUI.valueCardTxt) as Text;
                        valueTxt.gameObject.transform.SetParent(IGUIM.instance.transform, false);
                        valueTxt.gameObject.transform.position = new Vector3(xx, yy, 0);
                        valueTxt.gameObject.transform.localScale = Vector3.one * 0.4f;
                        valueTxt.text = valueOnCard + "";

                    }

                    //IGUIM.instance.handType.text = ValueCardHand(data.handType, data.point);
                };

                var pos = new Vector3(x, y, 0);
#if !UNITY_WEBGL
                var rotate = Random.Range(-10, 10);
#else
                var rotate = 0;
#endif
                var tween = card.DoAnimate(time, 0, pos, rotate, cardOnBoardScale);


                if (i == 1)
                    tween.OnComplete(action);

                AddCardToBoard(card);
            }
        }

        if (OGUIM.me.id == data.userId)
        {
            
            IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_Dan, Btn_CheckUser }, new bool[] { false, false, false });

            DOVirtual.DelayedCall(0.5f, () =>
            {
                IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_Dan, Btn_CheckUser }, new bool[] { false, false, false });
            });
        }

    }

    public void OnUserCheckCard(TurnData data)
    {
        var playersOnBoard = IGUIM.GetPlayersOnBoard();

        foreach (var user in data.users)
        {
            if (user.extra != null && user.extra.cards != null && user.extra.cards.Any() && user.id != OGUIM.me.id)
                Instance_OnSubmitTurn(new TurnData { userId = user.id, cards = user.extra.cards, handType = user.extra.handType, point = user.extra.point });
            if (playersOnBoard.ContainsKey(user.id))
            {
                user.isPlayer = true;

                playersOnBoard[user.id].SetTurn(false);
              
                playersOnBoard[user.id].FillData(user);
                playersOnBoard[user.id].Reset();
                if (data.specialty == 0 && user.chipChange != 0 && user.isPlayer)
                {
                    var str = Ultility.CoinToString(user.chipChange);// + " " + OGUIM.currentMoney.name;
                    IGUIM.SpawnTextEfx(str, playersOnBoard[user.id].avatarView.imageAvatar.transform.position, user.chipChange > 0);
                }
                else
                {
                    if (user.chipChange == 0 && user.isPlayer)
                    {
                        var str = "Hoà";
                        IGUIM.SpawnTextEfx(str, playersOnBoard[user.id].avatarView.imageAvatar.transform.position, user.chipChange > 0);
                    }
                }
            }
        }
    }

    private void ChangeCardsOnBoard()
    {
        float time = 0.5f;
        float distanceCard = 0.3f;
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        foreach (var key in playersOnBoard.Keys)
        {
            var player = playersOnBoard[key];
            int i = 0;
            if (player.userData.playedCards == null || !player.userData.playedCards.Any())
                continue;
            foreach (var cardData in player.userData.playedCards)
            {
                var cardKey = cardData.ToString();
                Card card;
                if (cardsOnBoard.ContainsKey(cardKey))
                {
                    card = cardsOnBoard[cardKey];
                }
                else
                {
                    var go = IGUIM.CreateCard(cardPrefab, cardData, player.PHOM_submitCardsTransform.position);
                    card = go.GetComponent<Card>();
                    card.SetFlip(true, 20);
                    AddCardToBoard(card);
                }
                if (card != null)
                {
                    float x = i * distanceCard;
                    card.SetSortingOrder(i);

                    var pos = new Vector3(x, 0, 0) + player.PHOM_submitCardsTransform.position;
                    var tween = card.DoAnimate(time, 0, pos, 0, cardOnBoardScale);
                }
                i++;
            }
        }
    }


    public void ShowValueHandCard(TurnData data, bool isStart)
    {
        if (cardsOnHand.Any())
        {
            var y = IGUIM.instance.cardOnHandTransform.position.y;
            var tempCards = cardsOnHand.Select(x => x.Value.cardData).ToList();

            IGUIM.instance.setHandTypeText(ValueCardHand(data.handType, data.point));
            pointCard = data.point;
            handType = data.handType;
            if (!isStart)
            {
                if (OGUIM.me.owner)
                {
                    IGUIM.SetButtonsActive(new string[] { Btn_Dan }, new bool[] { false });

                    if (data.point >= 16 || tempCards.Count >= 5 || data.handType > 2)
                    {
                        IGUIM.SetButtonsActive(new string[] { Btn_CheckUser }, new bool[] { true });
                    }

                    if (data.point >= 21 || tempCards.Count >= 5 || data.handType > 2)
                    {
                        IGUIM.SetButtonsActive(new string[] { Btn_Rut }, new bool[] { false });
                    }
                }
                else
                {
                    if (data.point == 21 || tempCards.Count >= 5)
                    {
                        IGUIM.SetButtonsActive(new string[] { Btn_Rut }, new bool[] { false });

                        IGUIM.SetButtonsActive(new string[] { Btn_Dan }, new bool[] { true });
                    }
                }
            }
        }
    }

    public void ShowValueHandCard(PHOM_TurnData data, bool isStart)
    {
        if (cardsOnHand.Any())
        {
            var y = IGUIM.instance.cardOnHandTransform.position.y;
            var tempCards = cardsOnHand.Select(x => x.Value.cardData).ToList();
            //if (txtValueCard == null)
            //{
            //    txtValueCard = Instantiate(IGUIM.instance.casinoGameUI.valueCardTxt) as Text;
            //    txtValueCard.gameObject.transform.SetParent(IGUIM.instance.transform, false);
            //}
            // txtValueCard.gameObject.SetActive(true);
            // txtValueCard.gameObject.transform.position = new Vector3(0, y, 0);
            //txtValueCard.text = ValueCardHand(data.handType, data.point);
            IGUIM.instance.setHandTypeText(ValueCardHand(data.handType, data.point));
            pointCard = data.point;
            handType = data.handType;
            if (!isStart)
            {
               

                if (OGUIM.me.owner)
                {
                    //IGUIM.SetAllButtons(false, Btn_Rut);
                    //if (data.point >= 16 || tempCards.Count >= 5 || data.handType > 2)
                    //{
                    //    IGUIM.SetButtonsActive(new string[] { Btn_CheckUser }, new bool[] { true });
                    //}

                    //if (data.point >= 21 || tempCards.Count >= 5 || data.handType > 2)
                    //{
                    //    IGUIM.SetButtonsActive(new string[] { Btn_Rut }, new bool[] { false });
                    //}

                    var btnRut = handType <= 2 && handType != (int)XIDACH_HAND_TYPE.QUAC;
                    var btnCheck = handType == 2; //pointCard >= 16 && pointCard <= 21 || handType > 2;
                    IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_CheckUser }, new bool[] { btnRut, btnCheck});
                }
                else
                {
                    //if (data.point >= 21 || tempCards.Count >= 5)
                    //{
                    //    IGUIM.SetButtonsActive(new string[] { Btn_Rut }, new bool[] { false });

                    //}

                    //if (data.point < 16 && tempCards.Count < 5)
                    //{
                    //    IGUIM.SetButtonsActive(new string[] { Btn_Dan }, new bool[] { false });
                    //}
                    //else if (tempCards.Count >= 5)
                    //{
                    //    IGUIM.SetButtonsActive(new string[] { Btn_Dan }, new bool[] { false });
                    //}
                    //else if (data.point >= 16 || tempCards.Count >= 5 || data.handType > 2)
                    //{
                    //    if (data.point <= 21)
                    //        IGUIM.SetButtonsActive(new string[] { Btn_Dan }, new bool[] { true });

                    //    if (data.point > 21)
                    //        IGUIM.SetButtonsActive(new string[] { Btn_Dan }, new bool[] { false });
                    //}

                    var btnRut = handType <= 2 && handType != (int)XIDACH_HAND_TYPE.QUAC;
                    var btnDan = pointCard >= 16 && pointCard <= 21 || handType > 2;
                    IGUIM.SetButtonsActive(new string[] { Btn_Rut, Btn_Dan }, new bool[] { btnRut, btnDan });
                }
            }
        }
    }

    private void _wc_OnNotifyChickenBet(CasinoTurnData turn)
    {
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        foreach (var user in turn.users)
        {
            if (playersOnBoard.ContainsKey(user.id) && user.isPlayer && !user.owner)
            {
                playersOnBoard[user.id].SetBet(turn.room.bet);

                GameObject go = IGUIM.SpawnChipEfx();
                go.transform.SetParent(IGUIM.instance.transform, false);

                go.transform.position = playersOnBoard[user.id].avatarView.imageAvatar.transform.position;

                go.transform.DOMove(playersOnBoard[user.id].betMoneyView.moneyNumber.transform.position, 0.5f)
                    .OnComplete(() => go.Recycle());
            }

            if (user.owner)
            {
                playersOnBoard[user.id].betMoneyView.gameObject.SetActive(false);
            }
        }
    }

    private string ValueCardHand(int type, int point)
    {
        XIDACH_HAND_TYPE hand = (XIDACH_HAND_TYPE)type;
        switch (hand)
        {
            case XIDACH_HAND_TYPE.XIBANG:
                return "XÌ BÀNG";

            case XIDACH_HAND_TYPE.XIDACH:
                return "XÌ DÁCH";

            case XIDACH_HAND_TYPE.NGU_LINH:
                return "NGŨ LINH";

            case XIDACH_HAND_TYPE.QUAC:
                return "QUẮC";

            default:
                return point.ToString() + " điểm";
        }
    }
}

public enum XIDACH_HAND_TYPE
{
    NON = 0,
    QUAC = 1,
    DU = 2,
    NGU_LINH = 3,
    XIDACH = 4,
    XIBANG = 5,
}
