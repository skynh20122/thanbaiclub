﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PokerGameManager : CardGameManager
{
    public Text valueCardTxt;
    List<int> foldUserIds = new List<int>();
    private PokerState StatePoker;
    private int call_chips = 0;
    public override void AddListener()
    {
        WarpClient.wc.OnUserCall += _wc_OnUserCall;
        WarpClient.wc.OnUserFold += _wc_OnUserFold;
        WarpClient.wc.OnUserRaise += _wc_OnUserRaise;
        WarpClient.wc.OnCalculatePot += _wc_OnCalculatePot;
        WarpClient.wc.OnNotifyChickenBet += _wc_OnNotifyChickenBet;
        WarpClient.wc.OnPokerRound += OnCommunityCardTurn;
        base.AddListener();
    }

    private void Start()
    {
        if (OGUIM.instance != null)
        {

            //OGUIM.instance.chatInGameBtn.gameObject.SetActive(true);
        }
    }

    public override void RemoveListener()
    {
        WarpClient.wc.OnUserCall -= _wc_OnUserCall;
        WarpClient.wc.OnUserFold -= _wc_OnUserFold;
        WarpClient.wc.OnUserRaise -= _wc_OnUserRaise;
        WarpClient.wc.OnCalculatePot -= _wc_OnCalculatePot;
        WarpClient.wc.OnNotifyChickenBet -= _wc_OnNotifyChickenBet;

        WarpClient.wc.OnPokerRound -= OnCommunityCardTurn;
        base.RemoveListener();
    }

    private void _wc_OnNotifyChickenBet(CasinoTurnData turn)
    {
        var playersOnBoard = IGUIM.GetPlayersOnBoard();

        //small blind
        if (playersOnBoard.ContainsKey(turn.smallBlind))
        {
            playersOnBoard[turn.smallBlind].SetBet(turn.room.bet);

            var alertStr = "small";
            IGUIM.SpawnTextEfx(alertStr, playersOnBoard[turn.smallBlind].betStart.transform.position);
        }

        //big blind
        if (playersOnBoard.ContainsKey(turn.bigBlind))
        {
            playersOnBoard[turn.bigBlind].SetBet(turn.room.bet * 2);

            var alertStr = "big";
            IGUIM.SpawnTextEfx(alertStr, playersOnBoard[turn.bigBlind].betStart.transform.position);
        }
        IGUIM.instance.readyText.text = "";
        IGUIM.instance.readyText.gameObject.SetActive(false);
        IGUIM.instance.MoveDealerToUser(turn.dealer);
    }


    private void _wc_OnCalculatePot(TurnData turn)
    {
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        if (turn == null || turn.pots == null || turn.pots.Count <= 0)
        {
            UILogView.Log("_wc_OnCalculatePot: no pots");
            return;
        }
        if (valueTxtPot != null)
            valueTxtPot.gameObject.SetActive(false);
        if (objChipPot != null)
            objChipPot.SetActive(false);

        int count = turn.pots.Count;
        var widthPerPot = 1.5f;
        float i = -(Mathf.Min(count, 4) - 1) * 0.5f;
        var y = (count <= 4 ? 0f : 0.6f) + IGUIM.instance.TrsPosPotPoker.position.y;
        foreach (var pot in turn.pots)
        {
            var takenId = pot.takenUserId;
            var totalKoins = pot.totalKoins;
            var x = 0f;
            foreach (var pKey in playersOnBoard.Keys)
            {
                if (!playersOnBoard[pKey].userData.isPlayer)
                    continue;
                GameObject go = IGUIM.SpawnChipEfx();
                go.transform.SetParent(IGUIM.instance.transform, false);
                go.transform.position = playersOnBoard[pKey].avatarView.imageAvatar.transform.position;
                go.transform.localScale = Vector3.one * 0.5f;

                if (count > 4 && i > 1.5f)
                {
                    i = -(Mathf.Min(count - 4, 4) - 1) * 0.5f;
                    //y =  y;
                }
                x = IGUIM.instance.TrsPosPotPoker.position.x + (i * widthPerPot);

                go.transform.DOMove(new Vector3(x, y, 0), 0.5f).OnComplete(() =>
                {
                    if (takenId == OGUIM.me.id)
                        go.transform.DOMove(IGUIM.instance.currentUser.avatarView.moneyView.moneyImage.transform.position, 0.5f)
                                    .SetDelay(1).OnComplete(() => go.Recycle());
                    else
                        go.transform.DOMove(playersOnBoard[takenId].avatarView.imageAvatar.transform.position, 0.5f)
                                    .SetDelay(1).OnComplete(() => go.Recycle());
                });
            }
            var yy = y;
            DOVirtual.DelayedCall(0.5f, () =>
            {
                var valueTxt = Instantiate(IGUIM.instance.casinoGameUI.valueCardTxt) as Text;
                valueTxt.gameObject.transform.SetParent(IGUIM.instance.transform, false);
                valueTxt.gameObject.transform.position = new Vector3(x, yy - 0.5f, 0);
                valueTxt.gameObject.transform.localScale = Vector3.one * 0.5f;
                valueTxt.text = LongConverter.ToK(totalKoins);
                Destroy(valueTxt, 1f);
            });
            i++;
        }
    }

    private void _wc_OnUserRaise(TurnData turn)
    {
        UIManager.PlaySound("pokerrasie");
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        if (playersOnBoard.ContainsKey(turn.userId))
        {
            playersOnBoard[turn.userId].userData.total_chips = turn.total_chips;
            playersOnBoard[turn.userId].userData.properties = turn.properties;
            playersOnBoard[turn.userId].avatarView.moneyView.FillData(MoneyType.Gold, turn.gold);
            if (OGUIM.me.id == turn.userId)
            {
                OGUIM.me.gold = turn.gold;
            }
            playersOnBoard[turn.userId].SetBet(turn.total_chips);

            var alertStr = "";
            if (turn.properties != null && turn.properties.user_allin)
                alertStr = "Xả láng";
            else
            {
                alertStr = "Tố " + Ultility.CoinToString(turn.bet);
                playersOnBoard[turn.userId].SetStatus("Tố thêm " + Ultility.CoinToString(turn.bet));
            }

            if (turn.userId != OGUIM.me.id)
            {
                call_chips = turn.call_chips;
            }
            IGUIM.SpawnTextEfx(alertStr, playersOnBoard[turn.userId].avatarView.imageAvatar.transform.position);

            GameObject go = IGUIM.SpawnChipEfx();
            go.transform.SetParent(IGUIM.instance.transform, false);

            go.transform.position = playersOnBoard[turn.userId].avatarView.imageAvatar.transform.position;

            go.transform.DOMove(playersOnBoard[turn.userId].betMoneyView.moneyNumber.transform.position, 0.5f)
                .OnComplete(() => go.Recycle());
        }
        if (turn.pot != 0)
            currentPot = turn.pot;
        IGUIM.SetAllButtons(false);
    }

    private void _wc_OnUserFold(TurnData turn)
    {
        UIManager.PlaySound("fold");
        foldUserIds.Add(turn.userId);
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        if (playersOnBoard.ContainsKey(turn.userId))
        {
            playersOnBoard[turn.userId].SetStatus("Úp bỏ");
            playersOnBoard[turn.userId].SetReady(false);
            playersOnBoard[turn.userId].userData.isReady = false;

            IGUIM.SpawnTextEfx("Úp bỏ", playersOnBoard[turn.userId].avatarView.imageAvatar.transform.position, false);
        }
        if (turn.userId == OGUIM.me.id)
        {
            foreach (var cKey in cardsOnHand.Keys)
                cardsOnHand[cKey].SetDisable();
        }

        IGUIM.SetAllButtons(false);
    }

    private void _wc_OnUserCall(TurnData turn)
    {
        UIManager.PlaySound("chips");
        Debug.Log("User Call data :" + JsonUtility.ToJson(turn));
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        if (playersOnBoard.ContainsKey(turn.userId))
        {
            playersOnBoard[turn.userId].userData.total_chips = turn.total_chips;
            playersOnBoard[turn.userId].userData.properties = turn.properties;
            playersOnBoard[turn.userId].avatarView.moneyView.FillData(MoneyType.Gold, turn.gold);
            if (OGUIM.me.id == turn.userId)
            {
                OGUIM.me.gold = turn.gold;
            }
            //if(OGUIM.me.id != turn.userId)
            //{
            //    if(turn.call_chips >= OGUIM.me.gold)
            //    {
            //        call_chips = (int)OGUIM.me.gold;
            //    }
            //    else
            //    {
            //        call_chips = turn.call_chips;
            //    }
                
            //}
            if (turn.call_chips != 0)
                playersOnBoard[turn.userId].SetBet(turn.total_chips);

            var alertStr = "";
            if (turn.properties != null && turn.properties.user_allin)
                alertStr = "Xả láng";
            else
            {
                if (turn.call_chips > 0)
                {
                    alertStr = "Theo " + Ultility.CoinToString(turn.call_chips);
                }
                else
                {
                    alertStr = "Theo";
                }
            }


            playersOnBoard[turn.userId].SetStatus(alertStr);
            IGUIM.SpawnTextEfx(alertStr, playersOnBoard[turn.userId].avatarView.imageAvatar.transform.position);
        }
        if (turn.pot != 0)
            currentPot = turn.pot;
        IGUIM.SetAllButtons(false);
    }

    public override void Instance_OnSubmitTurn(TurnData data)
    {
        if (OGUIM.me.id == data.userId)
            IGUIM.SetButtonActive("BACAY_submit_btn", false);

        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        float time = 0.5f;
        float distanceCard = 0.4f;
        Vector2 sizeOfCards = CalculateSizeOfCards(data.cards.Count, distanceCard, Vector3.down, cardOnBoardScale);

        var tempCards = data.cards;


        var valueOnCard = ValueCardHand(data.handType);
        float x = 0;
        float y = 0;
        var cardPos = IGUIM.instance.cardOnHandTransform.position;
        var currentPlayer = IGUIM.instance.currentUser;

        for (var i = 0; i < tempCards.Count; i++)
        {
            CardData cardData = tempCards[i];
            Card card = null;
            if (OGUIM.me.id == data.userId)
            {
                string cardStr = cardData.ToString();
                if (cardsOnHand.ContainsKey(cardStr))
                {
                    card = cardsOnHand[cardStr];
                    cardsOnHand.Remove(cardStr);

                    card.SetSortingOrder(cardsOnBoard.Count + 100);

                    x = i * distanceCard - (sizeOfCards.x * 0.5f - 0.5f);
                    y = cardPos.y;
                    currentPlayer.SetTurn(false);
                }
                ReorderCardOnHand();
            }
            else if (playersOnBoard.ContainsKey(data.userId))
            {
                var player = playersOnBoard[data.userId];
                player.SetRemainCard(0);
                player.SetTurn(false);
                var go = IGUIM.CreateCard(cardPrefab, cardData, playersOnBoard[data.userId].Position);
                go.transform.localScale = cardOnHandScale * Vector3.one;
                card = go.GetComponent<Card>();
                x = player.showCardDirection.x * ((i - 1) * distanceCard + 0.3f) + player.cardView.transform.position.x;
                y = player.cardView.transform.position.y;
                card.SetSortingOrder((i % 5) * (int)player.showCardDirection.x + 150);
            }
            if (card != null)
            {
                //card.SetSortingOrder(cardsOnBoard.Count, boardLayerName);
                card.SetFlip(true, 20);
                card.canTouch = false;


                var xx = x;
                var yy = y - 0.3f;
                TweenCallback action = () =>
                {
                    var valueTxt = Instantiate(IGUIM.instance.casinoGameUI.valueCardTxt) as Text;
                    if (IGUIM.instance == null || IGUIM.instance.transform == null)
                    {
                        Destroy(valueTxt);
                        return;
                    }
                    if (foldUserIds.Contains(data.userId))
                        valueOnCard = "ÚP BỎ";
                    valueTxt.gameObject.transform.SetParent(IGUIM.instance.transform, false);
                    valueTxt.gameObject.transform.position = new Vector3(xx, yy, 0);
                    valueTxt.gameObject.transform.localScale = Vector3.one * 0.7f;
                    valueTxt.text = valueOnCard + "";
                };

                var pos = new Vector3(x, y, 0);
#if !UNITY_WEBGL
                var rotate = Random.Range(-10, 10);
#else
                var rotate = 0;
#endif
                var tween = card.DoAnimate(time, 0, pos, rotate, cardOnBoardScale);


                if (i == 1)
                    tween.OnComplete(action);

                AddCardToBoard(card);
            }
        }
    }
    public override void Instance_OnSetTurn(TurnData data)
    {

        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        foreach (var key in playersOnBoard.Keys)
        {
            playersOnBoard[key].SetTurn(data.userId == key, interval, interval);
        }

        bool isHost = (data.userId == OGUIM.me.id);
        if (isHost)
        {
            //UIManager.PlaySound("turnstart");
        }
        if (IGUIM.instance.callText != null)
        {
            if (call_chips >= OGUIM.me.gold)
            {
                IGUIM.instance.callText.text = "Tất tay";
            }
            else
           if (call_chips > 0)
            {
                IGUIM.instance.callText.text = "Theo" + "\n " + LongConverter.ToFull(call_chips);
            }
            else
            {
                IGUIM.instance.callText.text = "Theo";
            }
        }
            
        IGUIM.SetAllButtons(false);
        //IGUIM.instance.callText.text = Ultility.CoinToString(data.bet);
        IGUIM.SetButtonsActive(new string[] { "LIENG_call_btn", "LIENG_raise_btn", "LIENG_fold_btn" },
                               new bool[] { isHost, isHost && data.maxBet > 0, isHost });
    }
    public override void Instance_OnStartMatch(byte[] payLoadBytes)
    {
        base.Instance_OnStartMatch(payLoadBytes);
        var turn = ZenMessagePack.DeserializeObject<TurnData>(payLoadBytes, WarpContentTypeCode.MESSAGE_PACK);
        foldUserIds.Clear();
        currentPot = 0;
        call_chips = 0;
        if (objChipPot != null)
            objChipPot.gameObject.SetActive(false);
        if (valueTxtPot != null)
            valueTxtPot.gameObject.SetActive(false);
        IGUIM.instance.DisableAllCommunityCard();
        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        IGUIM.instance.InitValueSlide();
        IGUIM.instance.readyText.text = "";
        IGUIM.instance.readyText.gameObject.SetActive(false);
        //IGUIM.instance.callText.text = "";
        if (turn.userId == OGUIM.me.id && turn.cards.Any())
        {
            IGUIM.SetAllButtons(false);
            IGUIM.instance.ShowHideSlidePoker(false);

            DOVirtual.DelayedCall(1, () =>
            {
                SetHandCardValue(turn);
            });
        }
        foreach (var key in playersOnBoard.Keys)
        {
            //playersOnBoard[key].Reset();

            if (playersOnBoard[key].userData.isPlayer)
            {
                playersOnBoard[key].SetTurn(true, interval, 30);
                playersOnBoard[turn.userId].SetReady(true);
                playersOnBoard[turn.userId].userData.isReady = true;
                if (key == OGUIM.me.id)
                {
                    IGUIM.SetAllButtons(false);
                    IGUIM.instance.ShowHideSlidePoker(false);

                    DOVirtual.DelayedCall(1, () =>
                    {
                        SetHandCardValue(turn);
                    });
                }
            }
        }

    }
    public override void Instance_OnEndMatch(byte[] payLoadBytes)
    {
        base.Instance_OnEndMatch(payLoadBytes);

        var data = ZenMessagePack.DeserializeObject<TurnData>(payLoadBytes, WarpContentTypeCode.MESSAGE_PACK);
        if (data == null)
        {
            Debug.Log(this.GetType().Name + " - Instance_OnEndMatch: data is null");
            return;
        }

        var playersOnBoard = IGUIM.GetPlayersOnBoard();



        foreach (var user in data.users)
        {
            //show bài của người chơi ra bàn
            if (user.extra != null && user.extra.cards != null && user.extra.cards.Any() && user.id != OGUIM.me.id && !foldUserIds.Contains(user.id))
            {
                if (data.users.Count > foldUserIds.Count + 1)
                    Instance_OnSubmitTurn(new TurnData { userId = user.id, cards = user.extra.cards, handType = user.extra.handType });
            }


            if (playersOnBoard.ContainsKey(user.id))
            {
                user.isPlayer = true;

                playersOnBoard[user.id].SetTurn(false);
                
                playersOnBoard[user.id].FillData(user);
                playersOnBoard[user.id].Reset();
                if (user.chipChange > 0 && user.isPlayer)
                {
                    DOVirtual.DelayedCall(1f, () =>
                    {
                        var str = Ultility.CoinToString(user.chipChange);// + " " + OGUIM.currentMoney.name;
                        IGUIM.SpawnTextEfxPoker(str, playersOnBoard[user.id].avatarView.imageAvatar.transform.position, user.chipChange > 0);
                    });

                }
                //if (user.chipChange > 0 && user.extra.bestHand.Count == 5)
                //{
                //    ShowHighlightBestCard(user.extra.bestHand);
                //}
            }
        }

        //nổi bật quân bài mạnh nhất
        if (data.bestHand.Count == 5)
            ShowHighlightBestCard(data.bestHand);
        IGUIM.SetAllButtons(false);
    }

    private void ShowHighlightBestCard(List<CardData> lstBest)
    {
        foreach (var key in cardsOnBoard.Keys)
        {
            var card = cardsOnBoard[key];
            if (isInBestCard(key, lstBest))
            {
                card.SetHighLight(true);
            }
            else
            {
                card.SetDisable();
            }
        }
        foreach (var key in IGUIM.instance.dicCommunityCard.Keys)
        {
            var card = IGUIM.instance.dicCommunityCard[key];
            if (isInBestCard(key, lstBest))
            {
                card.SetHighLight(true);
            }
            else
            {
                card.SetDisable();
            }
        }
        foreach (var key in cardsOnHand.Keys)
        {
            var card = cardsOnHand[key];
            if (isInBestCard(key, lstBest))
            {
                card.SetHighLight(true);
            }
            else
            {
                card.SetDisable();
            }
        }
    }
    private bool isInBestCard(string key, List<CardData> lstBest)
    {
        for (int i = 0; i < lstBest.Count; i++)
        {
            var cd = lstBest[i];
            if (key.Equals(cd.ToString()))
            {
                return true;
            }
        }
        return false;
    }


    public override void ReorderCardOnHand()
    {
        base.ReorderCardOnHand();
    }

    private void OnCommunityCardTurn(TurnData turn)
    {
        UIManager.PlaySound("deal2");

        call_chips = 0;
        IGUIM.instance.SetCommunityCard(turn.common_cards);
        SetHandCardValue(turn);
        CalcuPotEachRound(turn);
    }
    Text valueTxtPot;
    GameObject objChipPot;
    int currentPot;
    private void CalcuPotEachRound(TurnData turn)
    {

        Debug.Log("Caculate Pot : " + turn.pot);

        var playersOnBoard = IGUIM.GetPlayersOnBoard();
        if (turn.pot == 0|| turn.pot <= currentPot)
        {
            UILogView.Log("_wc_OnCalculatePot: no pots");
            return;
        }
        if(turn.pot > currentPot)
          currentPot = turn.pot;
        int count = 1;
        var widthPerPot = 1.5f;
        float i = -(Mathf.Min(count, 4) - 1) * 0.5f;
        var y = (count <= 4 ? 0f : 0.6f) + IGUIM.instance.TrsPosPotPoker.position.y;
        // var takenId = -1;
        var totalKoins = currentPot;//turn.pot;
        var x = 0f;
        foreach (var pKey in playersOnBoard.Keys)
        {
            var go = IGUIM.SpawnChipEfx();
            go.transform.SetParent(IGUIM.instance.transform, false);
            go.transform.position = playersOnBoard[pKey].avatarView.imageAvatar.transform.position;
            go.transform.localScale = Vector3.one * 0.5f;
            //go.SetActive(true);
            playersOnBoard[pKey].SetBet(0);

            if (count > 4 && i > 1.5f)
            {
                i = -(Mathf.Min(count - 4, 4) - 1) * 0.5f;
                //y =  y;
            }
            x = IGUIM.instance.TrsPosPotPoker.position.x + (i * widthPerPot);

            go.transform.DOMove(new Vector3(x, y, 0), 0.5f).OnComplete(() =>
            {
                go.Recycle();
            });
        }
        var yy = y;
        DOVirtual.DelayedCall(0.5f, () =>
        {
            if (objChipPot == null)
                objChipPot = IGUIM.SpawnChipEfx();
            objChipPot.transform.SetParent(IGUIM.instance.transform, false);
            objChipPot.transform.position = new Vector3(x, y, 0);
            objChipPot.transform.localScale = Vector3.one * 0.5f;
            objChipPot.SetActive(true);

            if (valueTxtPot == null)
                valueTxtPot = Instantiate(IGUIM.instance.casinoGameUI.valueCardTxt) as Text;
            valueTxtPot.gameObject.transform.SetParent(IGUIM.instance.transform, false);
            valueTxtPot.gameObject.transform.position = new Vector3(x, yy - 0.5f, 0);
            valueTxtPot.gameObject.transform.localScale = Vector3.one * 0.5f;
            valueTxtPot.text = LongConverter.ToK(totalKoins);
            valueTxtPot.gameObject.SetActive(true);
            //Destroy(valueTxt, 2f);
        });
    }


    Text TxtValueHand;
    private void SetHandCardValue(TurnData turn)
    {
        if (turn.handType > 1)
        {

            var valueOnCard = ValueCardHand(turn.handType);
            IGUIM.instance.setHandTypeTextPoker(valueOnCard);
            // if (foldUserIds.Contains(OGUIM.me.id))
            //     valueOnCard = "ÚP BỎ";
            //TxtValueHand.text = valueOnCard + "";            
        }
        else
        {
            IGUIM.instance.setHandTypeTextPoker("");
        }
    }

    private string ValueCardHand(int type)
    {
        POKER_HAND_CARD hand = (POKER_HAND_CARD)type;
        switch (hand)
        {
            case POKER_HAND_CARD.TU_QUY_A:
                return "Tứ quý A";
            case POKER_HAND_CARD.THUNG_PHA_SANH:
                return "Thùng phá sảnh";
            case POKER_HAND_CARD.THUNG_PHA_SANH_HA:
                return "Thùng phá sảnh hạ";
            case POKER_HAND_CARD.THUNG_PHA_SANH_THUONG:
                return "Thùng phá sảnh thượng";
            case POKER_HAND_CARD.TU_QUY:
                return "Tứ quý";
            case POKER_HAND_CARD.CU_LU:
                return "Cù lũ";
            case POKER_HAND_CARD.THUNG:
                return "Thùng";
            case POKER_HAND_CARD.SANH:
                return "Sảnh";
            case POKER_HAND_CARD.SANH_THUONG:
                return "Sảnh thượng";
            case POKER_HAND_CARD.SANH_HA:
                return "Sảnh hạ";
            case POKER_HAND_CARD.SAM_CO:
                return "Sám cô";
            case POKER_HAND_CARD.THU:
                return "Thú";
            case POKER_HAND_CARD.DOI:
                return "Đôi";
            default:
                return "";
        }
    }
}

public enum PokerState
{
    PREPARE = 0,
    START,
    PREPLOP,
    FLOP,
    TURN,
    RIVER,
}

public enum POKER_HAND_CARD
{
    TU_QUY_A = 14,
    THUNG_PHA_SANH_THUONG = 13, //10-J-Q-K-A đồng chất
    THUNG_PHA_SANH_HA = 12, //A-2-3-4-5 đồng chất
    THUNG_PHA_SANH = 11, //5 cây liên tiếp đồng chất  
    TU_QUY = 10,
    CU_LU = 9, //Một bộ ba và 1 bộ đôi
    THUNG = 8, //Dãy 5 lá bài đồng chất
    SANH_THUONG = 7,
    SANH_HA = 6,
    SANH = 5, //Dãy liên tiếp ko đồng chất
    SAM_CO = 4,// Một bộ ba
    THU = 3, // 2 bộ đôi
    DOI = 2,// 1 đôi
    MAU_THAU = 1,
}