﻿using UnityEngine;
using System.Collections;
using System.IO;
using FishPro;

public class LoadResource {

	private static LoadResource _instance;

	public static LoadResource Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new LoadResource();
			}
			return _instance;
		}
	}

	public T Load<T>(string url) where T : Object
	{
		#if UNITY_EDITOR
		//load asset in editor
		return UnityEditor.AssetDatabase.LoadAssetAtPath<T>( url);
		#else
		return ResourceManager.Instance.LoadAsset<T>(url);
		#endif
	}

	public Object[] LoadAll(string url)
	{
		#if UNITY_EDITOR
		//load asset in editor
		return UnityEditor.AssetDatabase.LoadAllAssetsAtPath (url);
		#else
		//load asset from asset bundle
		return ResourceManager.Instance.LoadAllAsset(url);

		#endif
	}
	public AudioClip[] LoadAllAssetsAtPath(string path)
	{
		#if UNITY_EDITOR
		if (path.EndsWith("/"))
		{
			path = path.TrimEnd('/');
		}
		string[] GUIDs = UnityEditor.AssetDatabase.FindAssets("", new string[] { path });
		AudioClip[] objectList = new AudioClip[GUIDs.Length];
		for (int index = 0; index < GUIDs.Length; index++)
		{
			string guid = GUIDs[index];
			string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(guid);
			AudioClip asset = UnityEditor.AssetDatabase.LoadAssetAtPath(assetPath, typeof(AudioClip)) as AudioClip;
			objectList[index] = asset;
		}

		return objectList;
		#else
		//load asset from asset bundle
		return ResourceManager.Instance.LoadAllTypeAsset<AudioClip>(path);
		#endif

	}
}
