﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace FishPro
{
	public class ProgressUI : Singleton<ProgressUI> {

		private Image loadingPrgress;
		private Text text;
		private int totalDownload;
		private float currentprogress;
		private float totalProgress;
		private Text loadingText;
		float countTime;
		private bool isUpdate = false;
		private float timeShockWare;

		private void CreateReference()
		{
			loadingPrgress = Utils.FindChildByName<Image>(this.gameObject, "ProgressLoading");
			text = Utils.FindChildByName<Text>(this.gameObject, "Text");
			loadingText = Utils.FindChildByName<Text>(this.gameObject, "TextLoading");
			//			Image bg = Utils.FindChildByName<Image>(this.gameObject, "background");
			//			bg.sprite = Resources.Load<Sprite> ("SplashLoginBg");

			Image nameGame = Utils.FindChildByName<Image>(this.gameObject, "Name");
			Object[] spriteObjects = Resources.LoadAll ("General");
			for (int i = 0; i < spriteObjects.Length; i++) {
				try{
					if(spriteObjects[i].name.Equals("GameName"))
					{
						Sprite sprite = (Sprite)spriteObjects[i];
						nameGame.sprite = sprite;
						break;
					}
				} catch{
				}
			}
			RandomText ();
			Destroy (GameObject.Find("LoadingPopup"));
//			ShockWave.Get().StartIt(new Vector3(Random.Range(0,CONFIG.WIDTH_SCREEN), Random.Range(0,CONFIG.HEIGHT_SCREEN), 0),true,0.644f,1.014f, 1, 1.151f);
		}

		public void Init()
		{
			#if !UNITY_WEBGL
			CreateReference ();
			#endif
		}

		public void SetTotalAssetDownload(int total)
		{
			this.totalDownload = total;
			isUpdate = true;
		}

		public void SetProgress(float progress)
		{
			this.currentprogress = progress;
		}

		public void LoadAssetSuccess()
		{
			totalProgress += 1;
		}

		public void LoadAllSuccess()
		{
			Destroy (this);
			//ShockWave.DestoryAll ();
		}

		void Update()
		{
//			if (Input.GetMouseButtonDown(0))
//			{
//				ShockWave.Get().StartIt(Input.mousePosition,true,0.644f,1.014f, 1, 1.151f);
//			}
//			timeShockWare += Time.deltaTime;
//			if (timeShockWare >= 6f) {
//				ShockWave.Get().StartIt(new Vector3(Random.Range(0,CONFIG.WIDTH_SCREEN), Random.Range(0,CONFIG.HEIGHT_SCREEN), 0),true,0.644f,1.014f, 1, 1.151f);
//				timeShockWare = 0;
//			}
			if (!isUpdate)
				return;

			#if UNITY_WEBGL
			Application.ExternalCall("UpdateProgress", ((float)((totalProgress + currentprogress) / totalDownload)));
			#else
			loadingPrgress.fillAmount = (float)((totalProgress + currentprogress) / totalDownload);
			loadingText.text = "Loading " + ((int)((float)((totalProgress + currentprogress) / totalDownload) * 100)) + "%";
			countTime += Time.deltaTime;
			if (countTime >= 5) {
				countTime = 0;
				RandomText ();
			}
			#endif
		}

		private void RandomText()
		{
			int i = UnityEngine.Random.Range (0, listText.Length);
			text.text = listText [i];
		}

		private string[] listText = new string[]{
			"Săn Boss là một cách kiếm vàng nhanh nhất",
			"Súng càng lớn, ăn càng dày",
			"Nắm giữ thời điểm, mộng vàng trong tay",
			"Chào mừng bạn đến với game Bắn Cá Boss",
			"Muốn kiếm vàng nhanh, phòng cao thẳng tiến",
		};

	}
}
