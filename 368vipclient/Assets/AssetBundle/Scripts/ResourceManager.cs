﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FishPro
{
	public enum EModeState
	{
	    Lobby = 0,
	    Ingame,
	}

	public enum EAssetState
	{
	    Init,
	    Mode,
	}

	public class ResourceManager : MonoBehaviour
	{
		public string[] _assetName = { "Login", "Common", "InGame", "Lobby", "MiniGame" };
        private int _initDownloadCount;
	    private EAssetState _currentState;
	    private EModeState _gameState;
	    private Dictionary<string, AssetBundle> _resourceHash;
	    private Action _chagemodeCompleteHandler;
	    private Action _initDownloadAssetCompleteHandler;

	    private static ResourceManager _instance;

	    public static ResourceManager Instance
	    {
	        get
	        {
	            return _instance;
	        }
	    }

	    void Awake()
	    {
			if (_instance == null) {
				_instance = this;
				DontDestroyOnLoad (this);
			} else {
				Destroy (this);
			}
	        this.Init();
	    }
	    
		public void Reset(string[] assetName, bool useProgress = false)
		{
			DownloadAssetBundle.Instance.useProgress = useProgress;
			this._assetName = assetName;
			_initDownloadCount = 0;
			_initDownloadAssetCompleteHandler = null;
			_chagemodeCompleteHandler = null;
		}

	    private void Init()
	    {
	        this._currentState = EAssetState.Init;
	        this._gameState = EModeState.Lobby;
	        this._resourceHash = new Dictionary<string, AssetBundle>();
	    }

	    public void InitDownload(Action initDownloadAssetCompleteHandler = null)
	    {
	        if(this._initDownloadAssetCompleteHandler == null)
	        {
	            this._initDownloadAssetCompleteHandler = initDownloadAssetCompleteHandler;
	        }
	        if(this._initDownloadCount < this._assetName.Length)
	        {
	            string assetName = this._assetName[this._initDownloadCount];
	            this._initDownloadCount++;
	            this.LoadAssetBundle(assetName, 0);
	        }
	        else
	        {
	            this._initDownloadAssetCompleteHandler();
	        }
	    }

	    public void ChageState(EModeState currentState, Action chageModeCompleteHandler)
	    {
	        if(this._gameState != currentState)
	        {
	            this._chagemodeCompleteHandler = chageModeCompleteHandler;
	            this._currentState = EAssetState.Mode;
	            this._gameState = currentState;
	            this.ResetResource();
	        }
	    }

	    private void ResetResource()
	    {
	        switch(this._gameState)
	        {
	            case EModeState.Lobby:
	                this.RemoveResource("Ingame");
	                this.LoadAssetBundle("Lobby", 0);
	                break;
	            case EModeState.Ingame:
	                this.RemoveResource("Lobby");
	                this.LoadAssetBundle("Ingame", 0);
	                break;
	        }
	    }

	    private void LoadAssetBundle(string assetName, int assetVersion)
	    {
	        DownloadAssetBundle.Instance.LoadAsset(assetName, assetVersion, LoadCompleteHandler);
	    }

	    private void LoadCompleteHandler(string assetName, AssetBundle asset)
	    {
			Utils.DebugLog(assetName);
	        this._resourceHash.Add(assetName, asset);

	        switch(this._currentState)
	        {
	            case EAssetState.Init:
	                this.InitDownload();
	                break;
	            case EAssetState.Mode:
	                this._chagemodeCompleteHandler();
	                break;
	        }
	    }

	    private void RemoveResource(string assetName)
	    {
	        this._resourceHash[assetName].Unload(true);
	        this._resourceHash.Remove(assetName);
	    }

	    public void ClearResource()
	    {
	        foreach(string key in this._resourceHash.Keys)
	        {
	            this._resourceHash[key].Unload(true);
	            this._resourceHash.Remove(key);
	        }
	    }


	    public T LoadAsset<T>(string name) where T: UnityEngine.Object
	    {
	        string key ="";

	        key = name.Split('/')[3];
			if (this._resourceHash.ContainsKey(key) && this._resourceHash[key] != null)
	        {
	            return this._resourceHash[key].LoadAsset<T>(name);
	        }

	        return null;
	    }

	    public UnityEngine.Object[] LoadAllAsset(string name)
	    {
	        string key = "";

	        key = name.Split('/')[3];

			if (this._resourceHash.ContainsKey(key) && this._resourceHash[key] != null)
	        {
	            return this._resourceHash[key].LoadAssetWithSubAssets(name);
	        }

	        return null;
	    }

		public T[] LoadAllTypeAsset<T>(string name)where T:UnityEngine.Object
		{
			string key = "";

			key = name.Split('/')[3];

			if (this._resourceHash.ContainsKey(key) && this._resourceHash[key] != null)
			{
				return this._resourceHash[key].LoadAllAssets<T>();
			}

			return null;
		}
	}
}