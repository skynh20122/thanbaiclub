﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using Newtonsoft.Json;
using FishPro;

public class DownloadAssetBundle : MonoBehaviour
{
	#if UNITY_STANDALONE_WIN
    private string platform = "pc-";
	#elif UNITY_ANDROID
	private string platform = "android-";
	#elif UNITY_IOS || UNITY_IPHONE
	private string platform = "ios-";
	#elif UNITY_WEBGL
	private string platform = "webgl-";
	#endif
    private Action<string, AssetBundle> _loadCompleteHandler;
    private Dictionary<string, Hash128> _assetVersionList;

    private static DownloadAssetBundle _instance;

	public bool useProgress;

    public static DownloadAssetBundle Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DownloadAssetBundle>();
            }
            return _instance;
        }
    }

    void Awkae()
    {
        if(_instance == null)
        {
            _instance = this;
			
        }
    }

	public void LoadAssetVersion(Action loadAssetAction = null)
    {
        this._assetVersionList = new Dictionary<string, Hash128>();
		StartCoroutine(this.DownssetVersion(loadAssetAction));
    }
    
	private IEnumerator DownssetVersion(Action loadAssetAction)
    {
		string name = string.Empty;
		#if UNITY_ANDROID
		name = "Android.json";
		#elif UNITY_IOS || UNITY_IPHONE
		name = "Ios.json";
		#elif UNITY_WP8 || UNITY_WP_8_1
		name = "Wp.json";
		#elif UNITY_WEBGL
		name = "WebGL.json";
		#elif UNITY_STANDALONE_WIN
		name = "StandaloneWindows.json";
        #elif UNITY_STANDALONE_OSX
		name = "StandaloneWindows.json";
        #endif
		UnityWebRequest www = UnityWebRequest.Get(GameConfig.DOWN_ASSET_URL + name);
        yield return www.Send();
        if (www.error == null)
        {
            string obj = www.downloadHandler.text;
            Dictionary<string, string> json = JsonConvert.DeserializeObject<Dictionary<string, string>>(obj);
            foreach (string key in json.Keys)
            {
                if (!_assetVersionList.ContainsKey(key))
                {
                    _assetVersionList.Add(key, Hash128.Parse(json[key]));
                    UILogView.Log(key + " : " + json[key]);
                }
            }
            if (loadAssetAction != null)
                loadAssetAction.Invoke();
        }
    }    

    public void LoadAsset(string assetName, int version, Action<string, AssetBundle> loadCompleteHandler = null)
    {
        this._loadCompleteHandler = loadCompleteHandler;
        StartCoroutine(this.Download(assetName, version));
    }

    private IEnumerator Download(string assetName, int version)
    {
        string key = platform + assetName.ToLower();
		
		WWW www = WWW.LoadFromCacheOrDownload(GameConfig.DOWN_ASSET_URL + platform + assetName, _assetVersionList[key]);
        if (www == null)
        {
            yield break;
        }
        while(!www.isDone)
        {
			if(useProgress)
				Singleton<ProgressUI>.Instance.SetProgress (www.progress);
            yield return null;
        }
        if (www.error == null)
        {
            var asset = www.assetBundle;
			if(useProgress)
				Singleton<ProgressUI>.Instance.LoadAssetSuccess ();
            this._loadCompleteHandler(assetName, asset);
        }
    }
}
