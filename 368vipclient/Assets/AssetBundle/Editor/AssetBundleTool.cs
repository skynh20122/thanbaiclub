﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class AssetBundleTool : EditorWindow
{
    private readonly string[] _platform = { "Android", "IOS", "PC", "WEBGL" };

    private int _selectPlatform;
    private string _selectPath = "";

    private bool _isLock;
    private Vector2 _scrollPosition;
    private Dictionary<string, List<string>> _bundlePathList;

    private AssetBundleManifest _manifest;

    private static GUIStyle _guiStyle;

    [MenuItem("Banca/Build AssetBundle")]
    public static void Init()
    {
        AssetBundleTool assetbundleTool = (AssetBundleTool)GetWindow(typeof(AssetBundleTool), false, "AssetBundle Tool", true);
        assetbundleTool.minSize = new Vector2(700, 600);
        assetbundleTool.Show();
        Caching.expirationDelay = 3600 * 24 * 10;

        InitOption();
    }

    [MenuItem("Banca/Clean Cache")]
    public static void CleanCache()
    {
        Caching.ClearCache();
    }

    static void InitOption()
    {
        _guiStyle = new GUIStyle
        {
            fontSize = 15,
            alignment = TextAnchor.MiddleLeft
        };
    }

    void OnGUI()
    {
        if (_guiStyle == null)
        {
            return;
        }

        GUILayout.BeginVertical("box");
        {
            GUILayout.BeginHorizontal("box", GUILayout.Height(25));
            {
                GUILayout.Label("Select Platform", _guiStyle, GUILayout.Height(25), GUILayout.Width(200));
                _selectPlatform = GUILayout.SelectionGrid(_selectPlatform, _platform, _platform.Length, GUILayout.Height(25));
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal("box", GUILayout.Height(25));
            {
                GUILayout.Label("Path - ", _guiStyle, GUILayout.Height(25), GUILayout.Width(200));
                _selectPath = GUILayout.TextField(_selectPath, 100, _guiStyle);
            }
            GUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Select Path", GUILayout.Height(30)))
        {
            CreatePath();
        }
        GUILayout.EndVertical();

        if (_bundlePathList != null && _bundlePathList.Count > 0 && !_isLock)
        {
            GUILayout.BeginVertical("box");
            {
                GUILayout.BeginHorizontal("box", GUILayout.Height(25));
                GUILayout.Label("Index", GUILayout.Width(50));
                GUILayout.Label("Name", GUILayout.Width(400));
                GUILayout.Label("Button", GUILayout.Width(150));
                GUILayout.EndHorizontal();

                _scrollPosition = GUILayout.BeginScrollView(_scrollPosition);
                {
                    int i = 0;
                    foreach (string key in _bundlePathList.Keys)
                    {
                        GUILayout.BeginHorizontal("box", GUILayout.Height(25));
                        GUILayout.Label(i.ToString(), GUILayout.Width(50));
                        GUILayout.Label(key, GUILayout.Width(400));
                        if (GUILayout.Button("Delete Asset", GUILayout.Height(25)))
                        {
                            _isLock = true;
                            DeleteAsset(key);
                            break;
                        }
                        GUILayout.EndHorizontal();
                        i++;
                    }
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();

            if (GUILayout.Button("Create AssetBundle", GUILayout.Height(40)))
            {
                CreateAssetBundle();
            }
        }
    }

    private void DeleteAsset(string key)
    {
        _bundlePathList.Remove(key);
        _isLock = false;
    }

    private void CreatePath()
    {
        try
        {
            if (_bundlePathList == null)
            {
                _bundlePathList = new Dictionary<string, List<string>>();
            }

            _bundlePathList.Clear();

            Object[] obj = Selection.GetFiltered(typeof(object), SelectionMode.Assets);
            List<string> filePathList = new List<string>();

            for (int i = 0; i < obj.Length; i++)
            {
                string selectPath;
                _selectPath = selectPath = AssetDatabase.GetAssetPath(obj[i]);
                filePathList.AddRange(
                    Directory.GetFiles(Directory.GetCurrentDirectory() + Path.AltDirectorySeparatorChar + selectPath,
                        "*.*", SearchOption.AllDirectories));
            }

            for (int i = 0; i < filePathList.Count; i++)
            {
                if (!filePathList[i].EndsWith(".meta"))
                {
                    filePathList[i] = filePathList[i].Replace(Path.DirectorySeparatorChar,
                        Path.AltDirectorySeparatorChar);
                    int index = filePathList[i].IndexOf(Path.AltDirectorySeparatorChar,
                        filePathList[i].LastIndexOf("FishResources", StringComparison.Ordinal));
                    string s = _platform[_selectPlatform] + "-" + filePathList[i].Substring(index + 1);
                    var bundleName = s.Replace(Path.AltDirectorySeparatorChar, '-');
                    string[] str = bundleName.Split('-');
                    bundleName = str[0] + "-" + str[1];
                    int pathIndex = filePathList[i].IndexOf(Path.AltDirectorySeparatorChar + "Assets", StringComparison.Ordinal);

                    string path = filePathList[i].Substring(pathIndex + 1);

                    Debug.Log(bundleName + " __ " + path);

                    if (!_bundlePathList.ContainsKey(bundleName))
                    {
                        _bundlePathList.Add(bundleName, new List<string>());
                    }
                    _bundlePathList[bundleName].Add(path);
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    private void CreateAssetBundle()
    {
        int i = 0;
        AssetBundleBuild[] build = new AssetBundleBuild[_bundlePathList.Count];
        var ouputPath = GetOutputPath();

        foreach (string key in _bundlePathList.Keys)
        {
            build[i] = new AssetBundleBuild
            {
                assetBundleName = ouputPath + key,
                assetNames = _bundlePathList[key].ToArray()
            };

            Debug.Log(build[i].assetBundleName + " __ " + ouputPath + "___" + key);
            i++;
        }

        string path = EditorUtility.SaveFolderPanel("Save Bundle to ", "", "") + "/Output";
        if (!Directory.Exists(path)) Directory.CreateDirectory(path);

        if (_selectPlatform != 3)
        {
            this._manifest = BuildPipeline.BuildAssetBundles(path, build, BuildAssetBundleOptions.DeterministicAssetBundle | BuildAssetBundleOptions.ForceRebuildAssetBundle, GetBuildTarget());
        }
        else
        {
            this._manifest = BuildPipeline.BuildAssetBundles(path, build, BuildAssetBundleOptions.UncompressedAssetBundle, GetBuildTarget());
        }


        this.CreateJson(path, build);
    }

    private void CreateJson(string path, AssetBundleBuild[] build)
    {
        string json = "{";
        for(int i =0; i < build.Length; i++)
        {
            string name = build[i].assetBundleName.Split('/')[1].ToLower();
            json += '"' + name + '"' + ":" + '"' + this._manifest.GetAssetBundleHash(build[i].assetBundleName).ToString() + '"';
            if(i <build.Length - 1)
            {
                json += ",";
            }
        }
        json += "}";

        string localPath = path + "/" + GetBuildTarget().ToString() + ".json";
        byte[] buf = System.Text.Encoding.UTF8.GetBytes(json);
        FileStream t = File.Open(localPath, FileMode.OpenOrCreate);
        t.Write(buf, 0, buf.Length);
        t.Close();

        Debug.Log(json);
    }

    private string GetOutputPath()
    {
        string path = "";

        if (_selectPlatform == 0)
        {
            path += "android" + Path.AltDirectorySeparatorChar;
        }
        else if(_selectPlatform == 1)
        {
            path += "ios" + Path.AltDirectorySeparatorChar;
        }
        else if(_selectPlatform == 2)
        {
            path += "window" + Path.AltDirectorySeparatorChar;
        }
        else if (_selectPlatform == 3)
        {
            path += "webgl" + Path.AltDirectorySeparatorChar;
        }

        return path;
    }

    private BuildTarget GetBuildTarget()
    {
        if (_selectPlatform == 0)
        {
            return BuildTarget.Android;
        }
        else if (_selectPlatform == 1)
        {
            return BuildTarget.iOS;
        }
        else if (_selectPlatform == 2)
        {
            return BuildTarget.StandaloneWindows;
        }
        else if (_selectPlatform == 3)
        {
            return BuildTarget.WebGL;
        }
        return BuildTarget.StandaloneWindows;
    }
}
