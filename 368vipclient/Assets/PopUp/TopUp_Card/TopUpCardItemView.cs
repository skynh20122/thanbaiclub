﻿using UnityEngine;
using UnityEngine.UI;

public class TopUpCardItemView : MonoBehaviour
{
    public Text Label_1;
    public Text Label_2;
    public Image iconGold;

    public bool FillData(ValueTopup data)
    {
        try
        {
            Label_1.text = LongConverter.ToFull(long.Parse(data.money)) + "VNĐ";
            Label_1.color = Color.white;
            Label_2.text = LongConverter.ToFull(long.Parse(data.gold));
            if(iconGold != null && !iconGold.IsActive())
            {
                iconGold.gameObject.SetActive(true);
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log("TopUpCardItemView FillData: " + ex.Message);
            return false;
        }
        return true;
    }
}
