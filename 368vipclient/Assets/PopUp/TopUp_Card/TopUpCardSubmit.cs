﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TopUpCardSubmit : MonoBehaviour
{
    public InputField seriInputField, pinInputField;
    public Text statusLabel;
    public Button buttonSubmit;
    public ScrollRect scrollRect;
    public Dropdown dropdown;

    public ToggleGroup toggleCardGroup;
    public List<Toggle> toggleCards;

    private string provider = "VT";
    public Dropdown drProvider;
    private string[] listCardImage = new string[] { "viettel", "mobi", "vina","gate" };
    private string[] listCardName = new string[] { "Viettel", "Mobiphone", "Vinaphone","Gate" };
	private string[] listProvider = new string[] { "VT", "MOBI", "VINA","Gate" };
    private string[] listCardAvaiable = new string[] { "VT", "MOBI", "VINA","GATE" };

    public static TopUpToggleCard currentCard { get; set; }

    private void Awake()
    {
        InitUI();
    }
    public void Start()
    {
        //scrollRect.horizontalNormalizedPosition = 0f;
        FillCardData();

    }
    private void InitUI()
    {
        if (drProvider != null)
            drProvider.onValueChanged.AddListener(OnCardTypeChanged);
    }
    private void OnCardTypeChanged(int index)
    {
        if (index == 1)
        {
            provider = "MOBI";
        }
        else if (index == 2)
        {
            provider = "VINA";
        }
        else
        {
            provider = "VT";
        }

    }

    private void OnEnable()
    {
        WarpClient.wc.OnCardTopupDone += Wc_OnCardTopupDone;
    }

    private void OnDisable()
    {
        WarpClient.wc.OnCardTopupDone -= Wc_OnCardTopupDone;
    }

    public void FillCardData()
    {
        for (int i = 0; i < toggleCards.Count; i++)
        {
            try
            {
                var cardData = toggleCards[i].GetComponent<TopUpToggleCard>();
                cardData.name = listCardName[i];
                cardData.provider = listProvider[i];
				cardData.image.sprite = ImageSheet.Instance.resourcesDics[listCardImage[i]];//"iap-card-" + 
                cardData.isAvaiable = listCardAvaiable.Any(x => x == listProvider[i]);
            }
            catch (System.Exception ex)
            {
                Debug.Log("FillCardData: " + ex.Message);
            }
        }

        //scrollRect.horizontalNormalizedPosition = 0f;
    }

    private void Wc_OnCardTopupDone(WarpResponseResultCode status)
    {

        if (status == WarpResponseResultCode.SUCCESS)
        {
            OGUIM.Toast.Show("Gửi thông tin thẻ thành công. Hệ thống đang xử lý.");
            seriInputField.text = "";
            pinInputField.text = "";
        }
        else
        {
            OGUIM.Toast.Show("Thông tin thẻ sai,Vui lòng kiểm tra lại !",UIToast.ToastType.Loading,10f);
        }
        //buttonSubmit.interactable = true;
    }

    public void GetCurrentCardData()
    {
        var checkToggle = toggleCardGroup.ActiveToggles().FirstOrDefault();
        if (checkToggle != null)
        {
            currentCard = checkToggle.GetComponent<TopUpToggleCard>();
            if (currentCard != null && currentCard.isAvaiable)
            {
                statusLabel.text = "Thẻ " + currentCard.name;
               // buttonSubmit.interactable = true;
            }
            else
            {
                //OGUIM.Toast.ShowLoading("Hệ thống thẻ " + currentCard.name + " đang bảo trì");
                //buttonSubmit.interactable = false;
            }
        }
        int m_DropdownValue = dropdown.value;
        string m_Message = dropdown.options[m_DropdownValue].text;
        Debug.Log("m_DropdownValue: "+ m_DropdownValue);
        Debug.Log("m_Message: " + m_Message);
    }
    public void SetCurrentCardData(CardContent card)
    {
        if (pinInputField != null)
            pinInputField.text = card.pin.Trim();
        if (seriInputField != null)
            seriInputField.text = card.serial.Trim();
        int amount = 10000;
        int len = card.amount.IndexOf(".");
        string cardValue = card.amount.Substring(0, len);
        int.TryParse(cardValue, out amount);
        Debug.Log("Amount : " + amount);
        dropdown.value = GetIndex(amount);
        //set toggle card
       
        switch (card.provider_code)
        {
            case "VT":
                SetToggleCard(0);
                break;

            case "MOBI":
                SetToggleCard(1);
                break;

            case "VINA":
                SetToggleCard(2);
                break;
            default:
                break;
        }
    }
    public void SubmitCard()
    {
        Debug.Log("Onclick submit card");

        seriInputField.text = seriInputField.text.Trim();
        pinInputField.text = pinInputField.text.Trim();
        int value = GetValueCard(dropdown.value);
        if (value > 0)
        {
            if (SubmitFormExtend.ValidateCard(seriInputField, pinInputField, statusLabel))
            {
                //Debug.Log("submit card success :" + currentCard.provider);
                //OGUIM.Toast.ShowLoading("Đang kiểm tra giao dịch...");
                WarpRequest.CardTopup(seriInputField.text, pinInputField.text, provider, value);
            }
        }
        else
        {
            OGUIM.Toast.Show("Vui lòng chọn mệnh giá thẻ!", UIToast.ToastType.Warning);
        }
    }

    public void SetToggleCard(int indexToggle)
    {
        for (int i = 0; i < toggleCards.Count; i++)
        {
            if (i == indexToggle)
                toggleCards[i].isOn = true;
            else
                toggleCards[i].isOn = false;
        }
    }
    public int GetValueCard(int index)
    {
        if (index == 1)
        {
            return 10000;
        }
        else if (index == 2)
        {
            return 20000;
        }
        else if (index == 3)
        {
            return 50000;
        }
        else if (index == 4)
        {
            return 100000;
        }
        else if (index == 5)
        {
            return 200000;
        }
        else if (index == 6)
        {
            return 300000;
        }
        else if (index == 7)
        {
            return 500000;
        }
        return 0;
    }

    public int GetIndex(int value)
    {
        if (value == 10000)
        {
            return 1;
        }
        else if (value == 20000)
        {
            return 2;
        }
        else if (value == 50000)
        {
            return 3;
        }
        else if (value == 100000)
        {
            return 4;
        }
        else if (value == 200000)
        {
            return 5;
        }
        else if (value == 300000)
        {
            return 6;
        }
        else if (value == 500000)
        {
            return 7;
        }
        return 0;
    }
}
