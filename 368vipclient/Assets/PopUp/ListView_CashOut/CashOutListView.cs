﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

[RequireComponent(typeof(UIListView))]
[DisallowMultipleComponent]
public class CashOutListView : MonoBehaviour
{
    public CashOutType cashOutType;
    public PlayerMoneyView playerMoneyViewOnTab_0;
    public PlayerMoneyView playerMoneyViewOnTab_1;
    public UIListView uiListView;
    public int maxItems = 10;

    private List<CashOutItemView> listView = new List<CashOutItemView>();
    public static List<CashoutProduct> listData = new List<CashoutProduct>();

    public static string cashOutVersion;
    public static string cashOutUrlImage;
    public static CashoutProduct curentCashOut;

    public static List<CashoutProduct> listCashOutCardViettel = new List<CashoutProduct>();
    public static List<CashoutProduct> listCashOutCardMobi = new List<CashoutProduct>();
    public static List<CashoutProduct> listCashOutCardVina = new List<CashoutProduct>();
    public static List<CashoutProduct> listCashOutItem = new List<CashoutProduct>();
    public CardType typeCard = CardType.VIETTEL;

    private Dropdown drCardType, drCardValue;
    private Text txtGoldValue;
    private Button btnChange;
    private int cardValue;
    private string cardProvider;
    private void Awake()
    {
        if (uiListView == null)
            uiListView = transform.GetComponent<UIListView>();

        InitUI();
    }

    public void Start()
    {
        WarpClient.wc.OnGetConfigDone += Wc_OnGetConfigDone;
        WarpClient.wc.OnGetCashOutListDone += Wc_OnGetCashOutListDone;
    }

    private void InitUI()
    {
        drCardType = transform.Find("DropdownCardType").GetComponent<Dropdown>();
        drCardType.onValueChanged.AddListener(OnCardTypeChanged);
        drCardValue = transform.Find("DropdownCost").GetComponent<Dropdown>();
        drCardValue.onValueChanged.AddListener(OnCardValueChanged);
        txtGoldValue = transform.Find("InputField/Text").GetComponent<Text>();
        btnChange = transform.Find("Button_Doi").GetComponent<Button>();
        btnChange.onClick.AddListener(CashOutClicked);
        cardProvider = "VT";
        cardValue = 0;
    }

    private void CashOutClicked()
    {
        if (cardValue == 0)
        {
            OGUIM.MessengerBox.Show("Yêu cầu đổi", "Bạn cần nhập mệnh giá thẻ !");
            return;
        }
        CashoutProduct product = null;
        if (cardProvider.Equals("VT"))
        {
            product = listCashOutCardViettel.Where(x => x.money == cardValue).FirstOrDefault();
        }
        else if (cardProvider.Equals("MOBI"))
        {
            product = listCashOutCardMobi.Where(x => x.money == cardValue).FirstOrDefault();
        }
        else if (cardProvider.Equals("VINA"))
        {
            product = listCashOutCardVina.Where(x => x.money == cardValue).FirstOrDefault();
        }

        if (product != null)
        {
            OGUIM.MessengerBox.Show("Yêu cầu đổi", "Yêu cầu đổi "
                + "<color=#FFC800FF>" + product.name + "</color>"
                + " bằng "
                + "\n" + "<color=#FFC800FF>" + LongConverter.ToFull(product.gold) + " " + GameBase.moneyGold.name + "</color>",
                "Xác nhận", () =>
                {
                    //OGUIM.Toast.ShowLoading("Đang gửi yêu cầu đổi " + CashOutListView.curentCashOut.name + "...");
                    WarpRequest.CashOut(product.id);
                },
                "Lần sau", null);
        }
        else
        {
            OGUIM.MessengerBox.Show("Yêu cầu đổi", "Yêu cầu đổi thẻ của bạn không đúng !");
        }
    }
    private void OnCardValueChanged(int index)
    {
        cardValue = convertCardValue(index);
        txtGoldValue.text = convertCardToGold(index);
    }
    private void OnCardTypeChanged(int index)
    {
        cardProvider = convertCardProvider(index);
    }
    private int convertCardValue(int index)
    {
        if (index == 1)
        {
            return 20000;
        }
        else if (index == 2)
        {
            return 50000;
        }
        else if (index == 3)
        {
            return 100000;
        }
        else if (index == 4)
        {
            return 200000;
        }
        else if (index == 5)
        {
            return 300000;
        }
        else if (index == 6)
        {
            return 500000;
        }
        return 0;
    }
    private string convertCardProvider(int index)
    {
        if (index == 0)
        {
            return "VT";
        }
        else if (index == 1)
        {
            return "MOBI";
        }
        else if (index == 2)
        {
            return "VINA";
        }

        return "VT";
    }
    private string convertCardToGold(int index)
    {
        if (index == 1)
        {
            return "80.000";
        }
        else if (index == 2)
        {
            return "250.000";
        }
        else if (index == 3)
        {
            return "450.000";
        }
        else if (index == 4)
        {
            return "900.000";
        }
        else if (index == 5)
        {
            return "1.350.000";
        }
        else if (index == 6)
        {
            return "2.250.000";
        }
        return "";
    }
    public void OnEnable()
    {
        if (playerMoneyViewOnTab_0 != null)
            playerMoneyViewOnTab_0.FillData(MoneyType.Gold, OGUIM.me.gold);
        if (playerMoneyViewOnTab_1 != null)
            playerMoneyViewOnTab_1.FillData(MoneyType.Gold, OGUIM.me.gold);
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnCashOutDone += Wc_OnCashOutDone;
            WarpClient.wc.OnUpdatedMoneyDone += Wc_OnUpdatedMoneyDone;
        }
    }

    public void OnDisable()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnCashOutDone -= Wc_OnCashOutDone;
            WarpClient.wc.OnUpdatedMoneyDone -= Wc_OnUpdatedMoneyDone;
        }
    }

    private void Wc_OnUpdatedMoneyDone(UpdatedMoneyData data)
    {
        if (data.chipType == (int)MoneyType.Gold)
        {
            if (playerMoneyViewOnTab_0 != null)
                playerMoneyViewOnTab_0.FillData((MoneyType)data.chipType, data.total);
            if (playerMoneyViewOnTab_1 != null)
                playerMoneyViewOnTab_1.FillData((MoneyType)data.chipType, data.total);
        }
    }

    private void Wc_OnGetConfigDone(WarpResponseResultCode status, RootConfig data)
    {
        //// Nếu dữ liệu trả về có data.version --> gán data.link(link ảnh cashout), data.verson --> lưu tham số và đối chiếu phiên bản 
		if (status == WarpResponseResultCode.SUCCESS && data != null && data.data.version != null)
        {
            cashOutVersion = data.data.version;
            cashOutUrlImage = data.data.link.Replace(".zip", "/").Replace(".Zip", "/").Replace(".ZIP", "/");
            WarpRequest.GetCashOutList();
        }
    }

    private void Wc_OnGetCashOutListDone(WarpResponseResultCode status, List<CashoutProduct> data)
    {
        Debug.Log("Data card : " + JsonUtility.ToJson(data));
        // List giải thưởng với type = 0 là card, type = 1 là vật phẩm --> Dựa vào link + tên ảnh để lấy ảnh cashout từ web về và vẽ
        if (status == WarpResponseResultCode.SUCCESS && data != null)
        {
            listCashOutCardViettel = data.Where(x => x.type == 0 && x.provider.Equals("VT")).ToList();
            listCashOutCardMobi = data.Where(x => x.type == 0 && x.provider.Equals("MOBI")).ToList();
            listCashOutCardVina = data.Where(x => x.type == 0 && x.provider.Equals("VINA")).ToList();
            listCashOutItem = data.Where(x => x.type == 1).ToList();
            //FillData();
        }
    }

    public void Get_Data(bool reload)
    {
        Debug.Log("Get data viettel");
        if (reload)
        {
            uiListView.ClearList();
            listCashOutCardViettel = new List<CashoutProduct>();
            listCashOutCardMobi = new List<CashoutProduct>();
            listCashOutCardVina = new List<CashoutProduct>();
            listCashOutItem = new List<CashoutProduct>();
            listView = new List<CashOutItemView>();
        }

        if (!listView.Any() || reload)
		{
			
			//OGUIM.Toast.ShowLoading("Đang tải dữ liệu, vui lòng chờ giây lát...");

            //if (string.IsNullOrEmpty(cashOutVersion) || string.IsNullOrEmpty(cashOutUrlImage))
            //    WarpRequest.GetConfig(ConfigType.REWARD_CONFIG);
            //else 
            
            if (!listCashOutCardViettel.Any() || !listCashOutItem.Any())
                WarpRequest.GetCashOutList();
            else
                FillData();

        }
    }

    public void FillData()
    {
        if (cashOutType == CashOutType.CARD)
        {

        
            if(typeCard == CardType.VIETTEL)
            {
                listData = listCashOutCardViettel;
            }else if(typeCard == CardType.MOBI)
            {
                listData = listCashOutCardMobi;
            }
            else
            {
                listData = listCashOutCardVina;
            }
            Debug.Log("data card : " + JsonUtility.ToJson(listData));
        }
        else
            listData = listCashOutItem;

        if (listData != null && listData.Any())
        {
            //int count = 0;
            foreach (var i in listData)
            {
                if (listView.Count > maxItems)
                    return;

                try
                {
                    var ui = uiListView.GetUIView<CashOutItemView>(uiListView.GetDetailView());
                    //if (count % 2 != 0)
                    //    ui.GetComponent<Image>().color = new Color32(0, 0, 0, 0);

                    if (ui.FillData(i))
                    {
                        listView.Add(ui);
                        //count++;
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.Log("FillData: " + ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
        OGUIM.Toast.Hide();
    }

    private void Wc_OnCashOutDone(WarpResponseResultCode status, CashoutData data)
    {
        var title = "Thông báo đổi thưởng";
        var content = "Yêu cầu đổi thưởng";

        if (status == WarpResponseResultCode.SUCCESS && data != null)
        {
            if (curentCashOut != null)
                content += curentCashOut.name;

            OGUIM.me.gold = data.gold;
            OGUIM.instance.meView.moneyView.FillData(GameBase.moneyGold.type, OGUIM.me.gold);
            if (playerMoneyViewOnTab_0 != null)
            playerMoneyViewOnTab_0.FillData(GameBase.moneyGold.type, OGUIM.me.gold);
            if (playerMoneyViewOnTab_1 != null)
                playerMoneyViewOnTab_1.FillData(GameBase.moneyGold.type, OGUIM.me.gold);

            if (data.status == (int)CastOutStatus.NEW)
            {
                OGUIM.MessengerBox.Show(title, "Yêu cầu đổi thành công. Đợi hệ thống xử lý");
            }
            else if (data.status == (int)CastOutStatus.ACCEPT)
            {
                OGUIM.MessengerBox.Show(title, "Yêu cầu đổi thành công. Đợi hệ thống xử lý");
            }
            else if (data.status == (int)CastOutStatus.RECEIVE)
            {
                if (data.card.Any())
                {
                    foreach (var i in data.card)
                    {
                        content += " trị giá " + LongConverter.ToFull(i.amount) + " VNĐ" + "\n";
                        content += "Mã thẻ: " + i.pin + "\n";
                        content += "Serial: " + i.serial + "\n";
                        content += "Hạn sử dụng: " + i.expire + "\n";
                    }
                    OGUIM.MessengerBox.Show(title, content);
                }
            }
            else if (data.status == (int)CastOutStatus.REJECT)
            {
                OGUIM.MessengerBox.Show(title, "Yêu cầu đổi bị từ chối. Liên hệ CSKH để được hỗ trợ.");
            }
            else if (data.status == (int)CastOutStatus.ERROR)
            {
                OGUIM.MessengerBox.Show(title, "Yêu cầu không thành công. Liên hệ CSKH để được hỗ trợ.");
            }
        }
        else if (status != WarpResponseResultCode.SUCCESS)
        {
            switch (status)
            {
                case WarpResponseResultCode.INVALID_GOLD:
                    content = "Bạn cần có " + LongConverter.ToFull(data.min) + " " + GameBase.moneyGold.name + " để thực hiện";
                    break;
                case WarpResponseResultCode.CASHOUT_LOCKED:
                    content = "Hệ thống tạm bảo trì. Vui lòng thử lại sau";
                    break;
                case WarpResponseResultCode.CASHOUT_ITEM_NOT_EXIST:
                    content = "Thẻ cào này tạm hết. Chọn thẻ khác hoặc thử lại sau";
                    break;
                case WarpResponseResultCode.CASHOUT_OVER_AMOUNT_DAY:
                    content = "Hệ thống tạm bảo trì. Vui lòng thử lại sau";
                    break;
                case WarpResponseResultCode.CASHOUT_OVER_USER_AMOUNT_DAY:
                    content = "Rất tiếc, quá số lượng đổi trong ngày.";
                    break;
                case WarpResponseResultCode.CASHOUT_SMALLER_MIN_VALUE:
                    content = "Số " + GameBase.moneyGold.name + " giao dịch nhỏ hơn quy định.";
                    break;
                case WarpResponseResultCode.CASHOUT_OVER_MAX_VALUE:
                    content = "Số " + GameBase.moneyGold.name + " giao dịch lớn hơn quy định.";
                    break;
                case WarpResponseResultCode.CASHOUT_OVER_AMOUNT_LIMIT:
                    content = "Loại thẻ tạm hết. Vui lòng chọn mệnh giá khác hoặc thử lại sau.";
                    break;
                case WarpResponseResultCode.CASHOUT_OVER_TIMES_LIMIT:
                    content = "Loại thẻ tạm hết. Vui lòng chọn mệnh giá khác hoặc thử lại sau.";
                    break;
                case WarpResponseResultCode.CASHOUT_OVER_TIME_DELAY:
                    content = "Thao tác quá nhanh. Thử lại sau " + data.time + " phút";
                    break;
                case WarpResponseResultCode.USER_NOT_VERIFY:
                    content = "Vui lòng xác thực SỐ ĐIỆN THOẠI trước";
                    break;
                case WarpResponseResultCode.CASHOUT_INVALID_BALANCE:
                    content = "Số dư tối thiều là " + LongConverter.ToFull(data.min) + " " + GameBase.moneyGold.name;
					break;
				case WarpResponseResultCode.TRANSFER_IN_PLAY_MOD:
					content = "Không thể đổi khi đang chơi game";
					break;
                default:
                    content = "Đổi thất bại, liên hệ CSKH để biết thêm chi tiết";
                    break;
            }
            OGUIM.MessengerBox.Show(title, content);
        }
        curentCashOut = null;
        OGUIM.Toast.Hide();
    }

    #region Get reward config --> Get list reward --> Get cashout config
    //// Lấy thông tin giải thưởng
    //WarpRequest.GetConfig(ConfigType.REWARD_CONFIG);
    //// Nếu dữ liệu trả về có data.version --> gán data.link(link ảnh cashout), data.verson --> lưu tham số và đối chiếu phiên bản 
    //WarpClient.wc.OnGetConfigDone();

    //// Lấy danh sách giải thưởng
    //WarpRequest.GetListReward();
    //// List giải thưởng với type = 0 là card, type = 1 là vật phẩm --> Dựa vào link + tên ảnh để lấy ảnh cashout từ web về và vẽ
    //WarpClient.wc.OnRewardListDone();

    ////Cashout
    //int id; // Chỉ cần id vì hiện tại server chỉ cho đổi số lượng 1 với tất cả phần thưởng nên gán cứng luôn
    //WarpRequest.CashOut(id);
    //// Dựa vào result code, status để hiển thị thông báo
    //WarpClient.wc.OnCashOutDone();
    #endregion
}

public enum CashOutType
{
    CARD = 0,
    ITEM = 1,
}

public enum CardType
{
    VIETTEL = 0,
    MOBI = 1,
    VINA = 2
}
