﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class LobbyViewListView : MonoBehaviour
{
    //public float scaleFakeData = 3f;
    //public float timeFakeData = 3f;
    public UIListView uiListView;
    public static List<Lobby> listData = new List<Lobby>();
    public static List<Lobby> listSoloData = new List<Lobby>();
    public static List<Lobby> listFavoriteData = new List<Lobby>();
    public static List<LobbyView> listView = new List<LobbyView>();
	public static List<LobbyStatus> listLobbiesStatus = new List<LobbyStatus> ();

    private UIAnimation anim { get; set; }

    void Awake()
    {
        anim = gameObject.GetComponent<UIAnimation>();
    }

    void Start()
    {
        //InvokeRepeating("FakeTablePlayer", 1.5f, timeFakeData);
    }

    private void OnEnable()
    {
        if (WarpClient.wc != null)
        {
			WarpClient.wc.OnListLobbiesDone += Wc_OnListLobbiesDone;
			WarpClient.wc.OnGetLobbyInfo += Wc_OnGetLobbyInfo;
        }
    }

    private void OnDisable()
    {
        if (WarpClient.wc != null)
        {
			WarpClient.wc.OnListLobbiesDone -= Wc_OnListLobbiesDone;
			WarpClient.wc.OnGetLobbyInfo -= Wc_OnGetLobbyInfo;
        }
    }

    public void Get_Data(bool reload)
    {
        if (anim != null)
            anim.Hide();
		if (!listView.Any () || reload) {
			
			//OGUIM.Toast.ShowLoading ("Đang tải dữ liệu, vui lòng chờ giây lát...");
			uiListView.ClearList ();
			listData = new List<Lobby> ();
			listView = new List<LobbyView> ();
			WarpRequest.ListLobbies ();
		} 
		//else 
		//{
		//	// Get list lobbies status
		//	WarpRequest.GetLobbyInfoConfig();
		//}
    }

    private void Wc_OnListLobbiesDone(WarpResponseResultCode resultCode, List<Lobby> data)
    {
        if (resultCode == WarpResponseResultCode.SUCCESS)
        {
            foreach (var i in data)
            {
                //var checkFavorite = listFavoriteData.FirstOrDefault(x => x.id == i.id);
                //if (checkFavorite != null)
                //    i.play = checkFavorite.play;

                switch ((LobbyId)i.id)
                {
                    case LobbyId.TLMNDL:
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "TLMN";
                        i.shotname = "TLMN";
                        i.subname = "";
                        break;
					case LobbyId.TLMNDL_SOLO:						
                        i.lobbymode = LobbyMode.SOLO;
                        i.desc = "TLMN Solo";
                        i.shotname = "TLMN";
                        i.subname = "Solo";
                        break;
                    case LobbyId.PHOM:
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Phỏm";
                        i.shotname = "Phỏm";
                        i.subname = "";
                        break;
					//case LobbyId.PHOM_SOLO:
					//	if (GameBase.isLiteVersion) {
					//		i.status = 0;
					//		break;
					//	}
     //                   i.lobbymode = LobbyMode.SOLO;
     //                   i.desc = "Phỏm";
     //                   i.shotname = "Phỏm Solo";
     //                   i.subname = "Solo";
     //                   break;
                    case LobbyId.SAM:
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Sâm";
                        i.shotname = "Sâm";
                        i.subname = "";
                        break;
					//case LobbyId.SAM_SOLO:
					//	if (GameBase.isLiteVersion) {
					//		i.status = 0;
					//		break;
					//	}
     //                   i.lobbymode = LobbyMode.SOLO;
     //                   i.desc = "Sâm";
     //                   i.shotname = "Sâm";
     //                   i.subname = "Solo";
     //                   break;
					case LobbyId.BACAY:						
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Ba cây";
                        i.shotname = "Ba cây chương";
                        i.subname = "";
                        break;
					case LobbyId.BACAY_GA:						
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Ba cây";
                        i.shotname = "Ba cây Gà";
                        i.subname = "";
                        break;
                    case LobbyId.MAUBINH:
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Mậu Binh";
                        i.shotname = "Mậu Binh";
                        i.subname = "";
                        break;
					case LobbyId.LIENG:						
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Liêng";
                        i.shotname = "Liêng";
                        i.subname = "";
                        break;
                    case LobbyId.POKER:                        
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Poker";
                        i.shotname = "Poker";
                        i.subname = "";
                        break;
                    case LobbyId.XIDACH:                        
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Xì dách";
                        i.shotname = "Xì dách";
                        i.subname = "";
                        break;
                    case LobbyId.BAUCUA:
                       
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Bầu cua mới";
                        i.shotname = "Bầu cua mới";
                        i.subname = "";
                        break;
                    case LobbyId.BAUCUA_OLD:
                       // i.status = 0;
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Bầu cua";
                        i.shotname = "Bầu cua";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.XOCDIA:
                       
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Xóc đĩa mới";
                        i.shotname = "Xóc đĩa mới";
                        i.subname = "";
                        break;
                    case LobbyId.XOCDIA_OLD:
                       // i.status = 0;
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Xóc đĩa";
                        i.shotname = "Xóc đĩa";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.SLOT:
                       
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Đập hũ";
                        i.shotname = "Đập hũ";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.SLOT2:
                       
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Đập hũ";
                        i.shotname = "Đập hũ";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.SLOT3:
                       
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Đập hũ";
                        i.shotname = "Đập hũ";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.XENG_HOAQUA:
                      
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Xèng";
                        i.shotname = "Xèng";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.ROULETTE:
                      
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Roullet";
                        i.shotname = "Roullet";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.TAIXIU2:
                       
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.desc = "Tài xỉu";
                        i.shotname = "Tài xỉu";
                        i.subname = "";
                        i.onlygold = true;
                        break;
                    case LobbyId.MINI_LOTTERY:
                     
                        i.lobbymode = LobbyMode.CLASSIC;
                        i.playmode = (int)PlayMode.QUICK;
                        i.desc = "Xổ số";
                        i.shotname = "Xổ số";
                        i.subname = "";
                        i.onlygold = true;
                        break;

                    default:
						i.status = 0;
						break;
                }
            }
            listData = data.OrderBy(x => x.order).ToList();

            FillData();

			// Get list lobbies status
			//WarpRequest.GetLobbyInfoConfig();
			// On Get Done --> listLobbiesStatus --> FakeTablePlayer
        }
    }

	private void Wc_OnGetLobbyInfo(WarpResponseResultCode resultCode, List<LobbyStatus> data)
	{
		if (resultCode == WarpResponseResultCode.SUCCESS) {
			listLobbiesStatus = data;
			//TrueTablePlayer ();
		} 
	}


    public void FillData()
    {
        if (listData.Any())
        {
            //Fake data game mode
            listSoloData = new List<Lobby>();
            int count = 0;
            foreach (var i in listData)
            {
                if (i.status == 1)
                {
                    //if (i.lobbymode == LobbyMode.SOLO)
                    //{
                    //    listSoloData.Add(i);
                    //}
                    //else
                    //{
                        try
                        {
                            var ui = uiListView.GetUIView<LobbyView>(uiListView.GetDetailView());
                            ui.FillData(i);
                            listView.Add(ui);

                            count++;
                        }
                        catch (System.Exception ex)
                        {
                            UILogView.Log("IconGameListView FillData: " + "\n" + ex.Message + "\n" + ex.StackTrace, true);
                        }
                    //}
                }
            }
        }

        OGUIM.Toast.Hide();
        if (anim != null)
            anim.Show();
    }   
}
	