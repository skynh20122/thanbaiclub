﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class RoomView : MonoBehaviour
{
    public Image moneyImage;
    public Image background;
    public Text valueText;
    public bool canJoin;
    public int roomValue;
    public MoneyType moneyType;

    public void JoinRoom()
    {
        if (canJoin)
        {
			
            WarpRequest.JoinRoom(moneyType, roomValue);
        }
        else
        {
            OGUIM.Toast.Show("Chọn mức cược phù hợp!", UIToast.ToastType.Warning, 3f);          
        }
    }

    public void FillData(int _roomValue, bool _canJoin, MoneyType _moneyType)
    {
        try
        {
            roomValue = _roomValue;
            canJoin = _canJoin;
            moneyType = _moneyType;

            valueText.text = LongConverter.ToK(roomValue);
            if (moneyType == MoneyType.Gold)
                moneyImage.sprite = GameBase.moneyGold.image;
            else
                moneyImage.sprite = GameBase.moneyKoin.image;

            if (!canJoin)
            {
                valueText.color = new Color32(255, 255, 255, 128);
                moneyImage.color = new Color32(255, 255, 255, 128);
            }
            else
            {
                valueText.color = new Color32(255, 200, 0, 255);
                moneyImage.color = new Color32(255, 255, 255, 255);
            }
        }
        catch (System.Exception ex)
        {
            UILogView.Log("RoomView FillData: " + roomValue + "\n" + ex.Message + "\n" + ex.StackTrace, true);
        }
    }
}
