﻿using UnityEngine;
using UnityEngine.UI;

public class GiftcodeView : MonoBehaviour
{
    public InputField giftCode;
   // public int wrongNumberMax = 3;
    private int wrongNumber=0;

    private void OnEnable()
    {
        WarpClient.wc.OnGiftCodeDone += Wc_OnGiftCodeDone;
    }
    
    private void OnDisable()
    {
        WarpClient.wc.OnGiftCodeDone -= Wc_OnGiftCodeDone;
    }

    private void Wc_OnGiftCodeDone(WarpResponseResultCode status)
    {
        if (status != WarpResponseResultCode.SUCCESS)
        {
            var text = "";
            if (status == WarpResponseResultCode.GIFT_CODE_NOT_EXITS)
                text = "Giftcode không tồn tại";
            else if (status == WarpResponseResultCode.GIFT_CODE_USED)
                text = "Giftcode đã được sử dụng";
            else if (status == WarpResponseResultCode.GIFT_CODE_EXPIRED)
                text = "Giftcode đã hết hạn sử dụng";
            else if (status == WarpResponseResultCode.GIFT_CODE_EVENT_MAX_ACTIVE)
                text = "Giftcode đã vượt quá số lượng sử dụng của sự kiện";
            else
                text = "Không thành công.";

            OGUIM.Toast.ShowNotification(text);

        }       
        giftCode.enabled = true;
    }

    private void Update()
    {
        if (gameObject.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Return) && giftCode.isFocused && !string.IsNullOrEmpty(giftCode.text))
            {
                Submit();
            }
        }
    }

    public void Submit()
    {
        if (wrongNumber < 3)
        {

            if (SubmitFormExtend.ValidateString(giftCode, "Mã quà tặng", false))
            {
                //OGUIM.Toast.ShowLoading("Đang tiến hành xác nhận...");
                giftCode.enabled = false;
				WarpRequest.EnterGiftCode(giftCode.text);
				giftCode.text = "";
            }
        }
        else
        {
            OGUIM.Toast.ShowLoading("Bạn đã nhập sai quá 3 lần");
        }
    }
}
