﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using UnityEngine.Networking;

public class HttpRequest
{
    private static HttpRequest instance;
    public static HttpRequest Instance
    {
        get
        {
            if (instance == null)
                instance = new HttpRequest();
            return instance;
        }
    }

    private static string url_initialize = OldWarpChannel.httpRequest + "initialize";
    private static string url_forgot_password = OldWarpChannel.httpRequest + "auth/forgot_password";
    private static string url_register = OldWarpChannel.httpRequest + "auth/register";
    private static string url_version_cfg = OldWarpChannel.httpRequest + "version_cfg";
    //private static string url_payment_initialize = OldWarpChannel.httpRequest + "payment/initialize";
    //private static string url_payment_complete = OldWarpChannel.httpRequest + "payment/complete";
    //private static string url_cfg_by_key = OldWarpChannel.httpRequest + "cfg_by_key";
    private static string url_dealer_cfg = OldWarpChannel.httpRequest + "dealer_cfg";
   
    #region ActionOnResponse
    public delegate void GetAPITokenDelegate(WarpResponseResultCode status, string apiToken);
    public static event GetAPITokenDelegate OnGetAPITokenDone;

    public delegate void ForgotPasswordDelegate(WarpResponseResultCode status, string data);
    public static event ForgotPasswordDelegate OnForgotPasswordDone;

    public delegate void GetVersionConfigureDelegate(WarpResponseResultCode status);
    public static event GetVersionConfigureDelegate OnGetVersionConfigureDone;

    public delegate void RegisterDelegate(WarpResponseResultCode status, UserData data);
    public static event RegisterDelegate OnRegisterDone;

    public delegate void GetDealerConfigDelegate(WarpResponseResultCode status, List<UserData> data);
    public static event GetDealerConfigDelegate OnGetDealerConfigDone;

	public delegate void CheckLocationDelegate(WarpResponseResultCode status, bool result);
	public static event CheckLocationDelegate OnCheckLocationDone;
    #endregion

    public IEnumerator GetAPIToken()
    {
        WWWForm form = new WWWForm();
        form.AddField("api_key", GameBase.api_key);
        form.AddField("api_secret", GameBase.api_secret);
        WWW www = new WWW(url_initialize, form);
        yield return www;

        try
        {
            if (!string.IsNullOrEmpty(www.error))
            {
               // Debug.Log("Error " + www.error);
                OnGetAPITokenDone(WarpResponseResultCode.BAD_REQUEST, GameBase.apiToken);
            }
            else
            {
                var data = JsonUtility.FromJson<InitializeData>(www.text);
                if (data != null && data.status == 0)
                    GameBase.apiToken = data.data;
                OnGetAPITokenDone(WarpResponseResultCode.SUCCESS, GameBase.apiToken);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
			OnGetAPITokenDone(WarpResponseResultCode.BAD_REQUEST, GameBase.apiToken);
        }
    }

    public IEnumerator GetVersionConfigure()
    {
        var form = new WWWForm();
        form.AddField("os_type", GameBase.osType.ToString());
        form.AddField("version", GameBase.clientVersion);
        form.AddField("providerCode", GameBase.providerCode);
        //var headers = form.headers;
        //headers["Content-Type"] = "application/json";
        //var url = url_version_cfg + "?os_type=" +
        //    GameBase.osType.ToString() + "&version=" +
        //    GameBase.clientVersion;
        //Debug.Log("url : " + url);
        //// url = "https://vuahaitac.club/gameserver/api/config/getconfigurl";
         var www = new WWW(url_version_cfg,form);
        //var www = UnityWebRequest.Get(url);
        //www.SetRequestHeader("Content-Type", "application/json");
        yield return www;

        try
        {
            UILogView.Log("GetConfig : " + www.isDone);
            UILogView.Log("GetConfig : " + www.url);
            UILogView.Log("GetConfig : " + www.error);
        
            UILogView.Log("GetConfig : " + www.text);
            HandleGetVersionConfig(www.text);
            if (OnGetVersionConfigureDone != null)
                OnGetVersionConfigureDone(WarpResponseResultCode.SUCCESS);
        }
        catch (Exception ex)
        {
            Debug.Log("config err : " + ex.ToString());

            var _backupData = PlayerPrefs.GetString("versionConfig");
            HandleGetVersionConfig(_backupData);
            if (OnGetVersionConfigureDone != null)
                OnGetVersionConfigureDone(WarpResponseResultCode.BAD_REQUEST);
        }
    }	
    public IEnumerator ForgotPassword(string username, int mobile)
    {
        var form = new WWWForm();
        form.AddField("username", username);
        form.AddField("mobile", mobile);

        var headers = form.headers;
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["token"] = GameBase.apiToken;
        var www = new WWW(url_forgot_password, form.data, headers);
        yield return www;

        try
        {
            var data = JsonUtility.FromJson<InitializeData>(www.text);
            if (data != null)
            {
                OnForgotPasswordDone((WarpResponseResultCode)data.status, data.message);
            }
            else
            {
                OnForgotPasswordDone(WarpResponseResultCode.BAD_REQUEST, "");
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OnForgotPasswordDone(WarpResponseResultCode.BAD_REQUEST, "");
        }
    }

    public IEnumerator Register(string _fullname,string _username, string _password, string _mobile = "")
    {
        var form = new WWWForm();
        form.AddField("fullname", _fullname);
        form.AddField("username", _username);
        form.AddField("password", _password);
        form.AddField("mobile", _mobile);

        form.AddField("provider_code", GameBase.providerCode);
        form.AddField("client_version", GameBase.clientVersion);
        form.AddField("platform", GameBase.platform);
        form.AddField("model", GameBase.model);
        form.AddField("device_uuid", GameBase.device_uuid);
        form.AddField("refCode", GameBase.refCode);

        var headers = form.headers;
        headers["Content-Type"] = "application/x-www-form-urlencoded";
        headers["token"] = GameBase.apiToken;
        var www = new WWW(url_register, form.data, headers);
        yield return www;

        try
        {
            var data = JsonUtility.FromJson<InitializeData>(www.text);
            UILogView.Log("WWW Text : " + www.text);
            UILogView.Log("Device : " + GameBase.device_uuid);
            if (data != null)
            {
                OnRegisterDone((WarpResponseResultCode)data.status, new UserData { username = _username, password = _password, mobile = _mobile });
            }
            else
            {
                OnRegisterDone(WarpResponseResultCode.BAD_REQUEST, null);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OnRegisterDone(WarpResponseResultCode.BAD_REQUEST, null);
        }
    }

    public IEnumerator GetDealerConfig()
    {
        var headers = new Dictionary<string, string>();
        headers["token"] = GameBase.apiToken;
        var www = new WWW(url_dealer_cfg, null, headers);
        yield return www;

        try
        {
            //var temp = JsonUtility.FromJson<RootAgencyData>(www.text.ToString());
            if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text))
            {
                var listData = new List<UserData>();
                var root = new JSONObject(www.text);
                if (root["status"].i == 0)
                {
                    var data = new JSONObject(root["data"].ToString());
                    var locked = new JSONObject(data["locked"].ToString());
                    if (!locked.b)
                    {
                        var agencyList = new JSONObject(data["agency"].ToString());
                        for (int i = 0; i < agencyList.Count; i++)
                        {
                            try
                            {
                                var agency = new JSONObject(agencyList[i].ToString());
                                if (agency != null)
                                {
                                    string add = agency["add"].str;
                                    string name = agency["name"].str;
                                    string tel = agency["tel"].str;
                                    string fb = agency["fb"].str;
                                    string userId = agency["userId"].str;
                                    listData.Add(new UserData { displayName = name, mobile = tel, passport = add, faceBookId = fb, id = int.Parse(userId) });
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.Log("Wc_OnGetSMSConfig Passer: " + ex.Message);
                            }
                        }
                    }

                    if (listData.Any())
                        OnGetDealerConfigDone(WarpResponseResultCode.SUCCESS, listData);
                    else
                        OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
                }
                else
                    OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
            }
            else
            {
                OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
            OnGetDealerConfigDone(WarpResponseResultCode.BAD_REQUEST, null);
        }
    }

	public IEnumerator CheckLocation()
	{	
		string url = "http://ipinfo.io/geo";
		WWW www = new WWW(url);
		yield return www;

		try
		{
			var data = JsonUtility.FromJson<LocationData>(www.text);
			if (data != null)
			{
				var result = (data.country == "VN" ? true : false);
				if (result == false)
					GameBase.underReview = true;
				OnCheckLocationDone(WarpResponseResultCode.SUCCESS, result);
			}
			else
			{
				OnCheckLocationDone(WarpResponseResultCode.BAD_REQUEST, false);
			}
		}
		catch (Exception ex)
		{
			Debug.Log(ex.ToString());
			OnCheckLocationDone(WarpResponseResultCode.BAD_REQUEST, false);
		}
			
	}
    public void HandleGetVersionConfig(string _data)
    {
        if (String.IsNullOrEmpty(_data))
            return;
       Debug.Log(_data);
        var root = new JSONObject(_data);
        var data = new JSONObject(root["data"].ToString());
        if (data != null)
        {
            JSONObject under_review_versions = new JSONObject();
            JSONObject need_update_versions = new JSONObject();
            JSONObject download_urls = new JSONObject();
            JSONObject packageData = data["provider_by_package"];
            if (packageData != null)
            {
                int numOfPack = 0;
                // check number of urls
                while (true)
                {
                    if (packageData[numOfPack] != null)
                    {
                        if (Application.identifier == packageData.keys[numOfPack])
                        {
#if UNITY_ANDROID || UNITY_IOS
                            GameBase.providerCode = packageData[numOfPack].str;
#endif
                        }
                        numOfPack = numOfPack + 1;
                    }
                    else
                        break;
                }

            }

            if (data["hotline"] != null && data["hotline"][0] != null)
                GameBase.hotline = data["hotline"][0].str;
            if (data["mobile_update_urls"] != null)
                GameBase.mobileUpdateUrl = data["mobile_update_urls"].str;

            

            download_urls = data["download_urls"];
            if (download_urls != null)
            {
                int numOfUrls = 0;
                // check number of urls
                while (true)
                {
                    if (download_urls[numOfUrls] != null)
                        numOfUrls = numOfUrls + 1;
                    else
                        break;
                }

                for (int i = 0; i < numOfUrls; i++)
                {
                    if (GameBase.providerCode == download_urls[i]["provider_code"].str)
                    {
                        GameBase.downloadURL = download_urls[i]["link"].str;
                        GameBase.latest_version = download_urls[i]["latest_version"].str;
                        GameBase.fbFanpage = download_urls[i]["fanpage"].str;
                        Debug.Log("http fb : " + GameBase.fbFanpage);
                        under_review_versions = download_urls[i]["under_review_versions"];
                        need_update_versions = download_urls[i]["need_update_versions"];
                        if(download_urls[i]["current_version"] != null && download_urls[i]["current_version"].str.Equals(GameBase.clientVersion))
                        {
                        if (download_urls[i]["cash_out"] != null)
                            GameBase.cash_out = download_urls[i]["cash_out"].str.ToInt();

                        if (download_urls[i]["cash_in"] != null)
                            GameBase.cash_in = download_urls[i]["cash_in"].str.ToInt();
                        if (download_urls[i]["phone_call"] != null)
                            GameBase.phone_call = download_urls[i]["phone_call"].str.ToInt();
                        if (download_urls[i]["facebook"] != null)
                            GameBase.facebook = download_urls[i]["facebook"].str.ToInt();
                        if (download_urls[i]["top"] != null)
                            GameBase.top = download_urls[i]["top"].str.ToInt();
                        if (download_urls[i]["gifcode"] != null)
                            GameBase.gifcode = download_urls[i]["gifcode"].str.ToInt();
                        if (download_urls[i]["quest"] != null)
                            GameBase.quest = download_urls[i]["quest"].str.ToInt();
                        if (download_urls[i]["events"] != null)
                            GameBase.events = download_urls[i]["events"].str.ToInt();
                        if (download_urls[i]["inbox"] != null)
                            GameBase.inbox = download_urls[i]["inbox"].str.ToInt();
                        if (download_urls[i]["isOnEventsOnStart"] != null && download_urls[i]["isOnEventsOnStart"].str.ToInt() == 1)
                            GameBase.isOnEventsOnStart = true;
                        if (download_urls[i]["headlineFeature"] != null)
                            GameBase.headLineFeature = download_urls[i]["headlineFeature"].str.ToInt();

                        }
                    }
                }
            }

#if UNITY_ANDROID || UNITY_IOS
            if (under_review_versions!=null)
            {
               // Debug.Log("under_review_versions:" + under_review_versions);
                int numOfVers = 0;
                // check number of urls
                while (true)
                {
                    if (under_review_versions[numOfVers] != null)
                        numOfVers = numOfVers + 1;
                    else
                        break;
                }

                bool check = false;
                for (int i = 0; i < numOfVers; i++)
                {
                    if (GameBase.clientVersion == under_review_versions[i].str)
                    {
						GameBase.underReview = true;
						break;
                    }
                }
            }

			if (need_update_versions!=null)
            {
                int numOfVers = 0;
                // check number of urls
                while (true)
                {
					if (need_update_versions[numOfVers] != null)
                        numOfVers = numOfVers + 1;
                    else
                        break;
                }
                for (int i = 0; i < numOfVers; i++)
                {
					if (GameBase.clientVersion == need_update_versions[i].str)
					{
						GameBase.needUpdateToPlay = true;
						break;
					}
                }
            }

            // Request download when lastest ver higher than current ver
            int clientVer = int.Parse(GameBase.clientVersion.Replace(".", ""));
            int lastestVer = 0;
            int.TryParse(GameBase.latest_version.Replace(".", ""), out lastestVer);
            if (lastestVer > clientVer)
                GameBase.newVersionAvaiable = true;

#endif
        }
        PlayerPrefs.SetString("versionConfig", _data);
        PlayerPrefs.Save();
    }
}
