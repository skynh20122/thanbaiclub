﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Threading.Tasks;

public class OldWarpChannel : MonoBehaviour
{

    public static OldWarpChannel Channel;
    public WebSocket client_socket = null;
    public static string httpRequest = "https://pokvip.club/api/";
    //public static string httpRequest = "http://192.168.100.7:8181/api/";
    public static string warp_host = "wss://pokvip.club/websocket";
    private static int warp_port = 8888;

    public float recieveDelay = 3;
    public float recieveTimeOut = 5;
    public bool isConnected = false;
    public int tryToConnectCount = 0;
    public int minutesToStopSocketCount = 10;
    private float firstTimeConnect;

    public Action actionOnConnected;

    [HideInInspector]
    public bool getKickedOut = false;
    [HideInInspector]
    public float timeToConnecting = 0;
    // For Reconnecting 
    [HideInInspector]
    public bool isInGame = false;
    [HideInInspector]
    public bool getApiFailed = false;   

    public int countPendingRecovery = 0;
    public float sendLastTime;
    private DateTime recieveLastTime;
    private float receiveLastTime;
    public float lastTimeKeepAlive;
 

    public readonly static Queue<Action> ExecuteOnMainThread = new Queue<Action>();

    void Awake()
    {
        if (Channel != null)
            Destroy(Channel.gameObject);

        Channel = this;
        DontDestroyOnLoad(gameObject);

        InvokeRepeating("DataReceive", 1, recieveDelay);
        //InvokeRepeating("CheckConnectionAlive", 3, 3);
        InvokeRepeating("SentKeepAlive", 30, 30);
        isConnected = false;
    }

    public void DataReceive()
    {
        if (isConnected == true)
        {
			if (Time.time - receiveLastTime > recieveDelay) 
            {
                //Debug.Log ("DIFF: " + (Time.time - receiveLastTime));
                recieveLastTime = DateTime.Now;
                receiveLastTime = Time.time;
                socket_recv();
            }
        }
    }

    public void socket_recovering(bool reset)
    {
        if (WarpClient.currentState == WarpConnectionState.RECOVERING)
        {
            if (reset)
            {
                client_socket.Close();
                client_socket = null;
            }

            if (client_socket == null)
            {
                client_socket = new WebSocket(new Uri(warp_host));
                if (connectCoroutine != null)
                {
                    StopCoroutine(connectCoroutine);
                    connectCoroutine = null;
                }
                connectCoroutine = client_socket.Connect();
                StartCoroutine(connectCoroutine);
                firstTimeConnect = Time.time;
            }


            if (client_socket.isConnected)
            {
                this.isConnected = true;
                //WarpClient.currentState = WarpConnectionState.CONNECTED;
                if (actionOnConnected != null)
                    actionOnConnected();
                countPendingRecovery = 0;
            }
            if (!string.IsNullOrEmpty(client_socket.error))
            {
                string error = client_socket.error;
                Debug.Log("error:" + error);
                if (error.Contains("An exception has occurred while connecting"))
                {
                    onLostConnection(0);
                }
            }
        }
    }

    IEnumerator connectCoroutine;
    public void socket_connect(Action onConnected = null)
    {
        Debug.Log("OnConected : " + onConnected == null);
        if (onConnected != null)
            actionOnConnected = onConnected;
        if (WarpClient.currentState == WarpConnectionState.CONNECTING && !this.isConnected)
        {
            if (client_socket == null)
            {
                if (connectCoroutine != null)
                {
                    Debug.Log("connectCoroutine : " + connectCoroutine == null);
                    StopCoroutine(connectCoroutine);
                    connectCoroutine = null;
                }
                Debug.Log("client_socket : " + warp_host);
                client_socket = new WebSocket(new Uri(warp_host));
                connectCoroutine = client_socket.Connect();
                StartCoroutine(connectCoroutine);
                Debug.Log("connected  : " + warp_host);
                firstTimeConnect = Time.time;
            }
            else
            {
                if (client_socket.isConnected)
                {
                    Debug.Log("CONNECTED TO " + warp_host);
                    this.isConnected = true;
                    WarpClient.currentState = WarpConnectionState.CONNECTED;
                    if (actionOnConnected != null)
                        actionOnConnected();
                    countPendingRecovery = 0;
                }
                if (!string.IsNullOrEmpty(client_socket.error))
                {
                    string error = client_socket.error;
                    Debug.Log("error:" + error);
                    if (error.Contains("An exception has occurred while connecting"))
                    {
                        onLostConnection(0);
                    }
                }
            }
        }
        else if (client_socket.isConnected)
        {
            UILogView.Log("CONNECTED TO " + warp_host + ":" + warp_port);
            if (actionOnConnected != null)
                actionOnConnected();
        }
    }

    public void socket_recv()
    {
        if (client_socket != null && this.isConnected)
        {
            byte[] reply = client_socket.Recv();
            if (reply != null)
            {
                WarpClient.wc.ResponseData(reply);
                lastTimeKeepAlive = Time.time;
            }
        }
            
        if (client_socket == null || !this.isConnected)
        {
            Debug.Log("++++++++++++++++++++ RECEIVE NOT SUCCESS ++++++++++++++++++++++++++");
            Debug.Log("set status to RECOVERING");
            WarpClient.currentState = WarpConnectionState.RECOVERING;          
            isConnected = false;
        }
    }

    public void socket_send(byte[] buffer)
    {
        if (client_socket != null && this.isConnected)
        {
            client_socket.Send(buffer);
            lastTimeKeepAlive = Time.time;
        }
        if (client_socket == null || !this.isConnected)
        {
            Debug.Log("++++++++++++++++++++ SENT NOT SUCCESS ++++++++++++++++++++++++++");
            Debug.Log("set status to RECOVERING");
            WarpClient.currentState = WarpConnectionState.RECOVERING;           
            isConnected = false;
        }
    }
    public void socket_close()
    {
        StopAllCoroutines();
        if (client_socket != null && this.isConnected)
            client_socket.Close();       
        client_socket = null;
        isConnected = false;
        WarpClient.currentState = WarpConnectionState.DISCONNECTED;
    }

    private void serverCloseSocket()
    {
        UILogView.Log(" **************** SERVER CLOSED SOCKET *****************");               
        isConnected = false;
        getKickedOut = true;
        onLostConnection(0);
       socket_close();
    }

    public void onLostConnection(int status)
    {
        UILogView.Log("onLostConnection");
        string text_lost_connection = "Kết nối với máy chủ thất bại! Vui lòng kiểm tra lại kết nối Internet";
        if (status == 2)
            text_lost_connection = "Quá thời gian kết nối. vui lòng đăng nhập lại";
        else if (status == 3)
            text_lost_connection = "Phiên làm việc hết hạn, vui lòng đăng nhập lại";
        else if (status == 4)
            text_lost_connection = "Có lỗi xảy ra, vui lòng đăng nhập lại";
       
        WarpClient.currentState = WarpConnectionState.DISCONNECTED;
        try
        {
            if (WarpClient.wc == null || WarpClient.wc.SessionId == 0)
                OGUIM.UnLoadGameScene(true);
            else                
                OGUIM.MessengerBox.Show("Thông Báo", "Kết nối thất bại, vui lòng kiểm tra mạng");

            OGUIM.Toast.Show(text_lost_connection, UIToast.ToastType.Warning);
        }
        catch (Exception ex)
        {
            UILogView.Log("onLostConnection: " + ex.Message + " " + ex.StackTrace, true);
        }
    }

    void OnApplicationQuit()
    {
        // Call logout instead of socket close cuz need to inform server that you are quit, not for the client
        WarpClient.wc.Logout();
        //socket_close();
        // fix for Unity Hangout after Stop from Play
#if UNITY_EDITOR
        socket_close();
#endif
        UILogView.Log("Application ending after " + Time.time + " seconds");
    }

    private void OnApplicationPause(bool pause)
    {
        UILogView.Log("Pause: " + pause + " " + recieveLastTime.ToString("HH:mm") + " " + (DateTime.Now - recieveLastTime).Minutes + " s ago!!!!!!!!!!!!!!", true);
        //if (!pause && !OGUIM.isTheFirst && recieveLastTime.AddMinutes(minutesToStopSocketCount) < DateTime.Now)
        //{
        //    serverCloseSocket();
        //    OGUIM.UnLoadGameScene(true);
        //}
    }

    void Update()
    {
        if (isConnected == false && WarpClient.currentState == WarpConnectionState.CONNECTING)
        {
            timeToConnecting += Time.deltaTime;
            socket_connect();
            if (timeToConnecting >= 2)
            {
                OGUIM.Toast.Show("Không thể kết nối", UIToast.ToastType.Warning);
                timeToConnecting = 0;
            }
        }
        //KEEP_ALIVE

    }

   
    private void CheckConnectionAlive1S()
    {
        Debug.Log("CheckConnectionAlive: "+ this.isConnected);
        if (!this.isConnected && countPendingRecovery <= 5)
        {
            isConnected = false;           
            Debug.Log("countPendingRecovery:" + countPendingRecovery);
            socket_recovering(countPendingRecovery % 3 == 1);
            countPendingRecovery++;
            if (countPendingRecovery > 5)
            {
                //countPendingRecovery = 0;
                onLostConnection(2);
                serverCloseSocket();
            }
        }   
    }
    private void SentKeepAlive()
    {
        Debug.Log("SentKeepAlive: " + this.isConnected);
        if (this.isConnected)
        {
            float diffLastSend = Time.time - lastTimeKeepAlive;
            if (diffLastSend > 30)
            {
                Debug.Log("diffLastSend++" + diffLastSend);
                WarpRequest.SendKeepAlive();
            }
        }
    }
}

