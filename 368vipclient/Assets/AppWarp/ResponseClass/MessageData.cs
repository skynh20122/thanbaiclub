﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class RootMessage
{
    public List<Message> messages;
    public string desc;
    public Message message;
}

[Serializable]
public class Message
{
    public string content;
    public string createdDate;
    public int id;
    public int senderId;
    public string senderName;
    public string senderAvatar;
    public string senderFacebookId;
    public int status;
    public string title;
    public int type = -1;
    public int value;

    public int typeMessage;
    public string contentData;
    public int contentDisplayType;
    public UserData sender;
}

[Serializable]
public class CardContent
{
    public String provider_code;
    public String amount;
    public String pin;
    public String serial;
    public String expire;

    override
    public string ToString()
    {
        string CardName = "";
        switch (provider_code)
        {
            case "VT":
                CardName = "Viettel";
                break;
            case "MOBI":
                CardName = "Mobiphone";
                break;
            case "VINA":
                CardName = "Vinaphone";
                break;
            default:
                CardName = "Viettel";
                break;
        }
        return  "Mạng : " + CardName + "\r\n"
            + "Thẻ : " + amount + "\r\n"
           + "Mã thẻ : " + pin + "\r\n"
           + "Serial : " + serial + "\r\n"
           + "Hạn : " + expire;
            
    }
}
[Serializable]
public class UserMessageCount
{
    public int countMsg;
    public int countTask;
    public int countCons;
    public int countAchi;
}
[Serializable]
public class HeadLineMessage
{
    public string message;
    public int userId;
}