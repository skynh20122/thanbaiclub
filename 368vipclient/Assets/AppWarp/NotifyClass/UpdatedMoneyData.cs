﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class UpdatedMoneyData
{
	public int total;
	public int change;
	public int type;
	public int chipType;
}
[Serializable]
public class UpdatedMobileData
{
    public string number;
    public int userId;
}

[Serializable]
public class CashOutNotify
{
    public int status;
    public long gold;
}