﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System;

public class UISlot : MonoBehaviour
{
    public static UISlot instance;

    public bool selectAllAtStart = false;
    public static void SpawnTextEfx(string str, Vector3 pos)
    {
        var te = instance.textEfxPrefab.Spawn();
        te.gameObject.transform.SetParent(instance.transform, false);
        te.gameObject.transform.position = pos;
        te.gameObject.transform.localScale = Vector3.one * (pos == Vector3.zero ? 1 : 0.6f);

        te.SetData(str.ToUpper(), 0.5f, 1.5f);
    }

    public List<int> listLineSelected;
    public List<Toggle> listToggleLineOnGame;
    public List<Toggle> listToggleLineOnTab;
    public List<NumberAddEffect> listJackpotLabel;
    public List<GameObject> listJackpotView;
    //public List<LobbyView> listSlotRoomView;

    public string soundOnLineSelect;
    public string soundOffLineSelect;
    public string soundSpin;
    public string soundMegaWin;
    public string soundJackpot;

    public NumberAddEffect moneyLabel;
    public NumberAddEffect currentBetLabel;
    public NumberAddEffect totalBetLabel;
    public NumberAddEffect lastWinLabel;
    public Text totalLineLabelInButton;
    public Text totalLineLabelInButtonSlot2;
    public Text totalLineLabelInButtonSlot3;
    public Text autoSpinLabelSlot, autoSpinLabelSlot2, autoSpinLabelSlot3;
    public Text trialSpinLabelSlot, trialSpinLabelSlot2, trialSpinLabelSlot3;
    public Text totalLineLableInPopup;
    //public Text buttonSpinStatusLabel;
    public int betRoomValue;
    public int freeValue;
    public Dropdown dropdownSlot, dropdownSlot2, dropdownSlot3;
    public Button btnLineSlot, btnLineSlot2, btnLineSlot3;
    public Button buttonSpin,buttonSpin2,buttonSpin3;
    public Button btnAutoSpinSlot, btnAutoSpinSlot2, btnAutoSpinSlot3;
    bool isAuto = false;
   public bool isTrail = false;

    public SlotMachine slotMachine;
    public SlotMachine slotMachine1;
    public SlotMachine slotMachine2;
    public SlotMachine slotMachine3;
    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;
    public UIPopupTabs popUpSlot;

    public UIPopupTabs popUp;
    //public UIAnimation popUpSelectRoom;
    public GameObject coinRainEfxPrefab;
    public TextEffext textEfxPrefab;
    public Effect_Jackpot jackpotEfx;
    public bool isSpining = false;
    public bool isAutoJoinRoom = false;
    public int totalLine;
    public int totalBet;
    public NumberAddEffect txtJackpot;
    public GameObject gmPrefab;

    SlotGameManager gameManager;
    public int nextBetRoomValue;
    public Image Background;
    public Image winBG, betBG;
    bool musicCheck = false;
    public void Awake()
    {
        instance = this;
        musicCheck = UIManager.instance.musicToggle.isOn;
        if (musicCheck)
            UIManager.instance.MuteMusic();
        var gm = Instantiate(gmPrefab) as GameObject;
        gm.gameObject.transform.SetParent(transform);
        gameManager = gm.GetComponent<SlotGameManager>();
        //OGUIM.instance.chatInGameBtn.SetActive(false);
        coinRainEfxPrefab.CreatePool(4);
        textEfxPrefab.CreatePool(10);
        if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
        {
            MenuTopManager.instance.setMenu(MenuType.PURPLE);
            Backgound.instance.setBackground("bg_main");
            foreach (var toggle in listToggleLineOnGame)
            {
                toggle.gameObject.GetComponent<SlotTypeManager>().setType(SlotType.PURPLE);
            }
            slotMachine1.gameObject.SetActive(true);
            slotMachine2.gameObject.SetActive(false);
            slotMachine3.gameObject.SetActive(false);
            slot1.gameObject.SetActive(true);
            slot2.gameObject.SetActive(false);
            slot3.gameObject.SetActive(false);
            if (winBG != null)
                winBG.sprite = ImageSheet.Instance.resourcesDics["cuoc_btn"];
            if (betBG != null)
                betBG.sprite = ImageSheet.Instance.resourcesDics["cuoc_btn"];
            slotMachine = slotMachine1;
            slotMachine.UpdateUIByRoom(793);
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
        {
            MenuTopManager.instance.setMenu(MenuType.RED);
            Backgound.instance.setBackground("CASINO_SLOT2");
            foreach (var toggle in listToggleLineOnGame)
            {
                toggle.gameObject.GetComponent<SlotTypeManager>().setType(SlotType.RED);
            }
            slotMachine1.gameObject.SetActive(false);
            slotMachine2.gameObject.SetActive(true);
            slotMachine3.gameObject.SetActive(false);
            slot1.gameObject.SetActive(false);
            slot2.gameObject.SetActive(true);
            slot3.gameObject.SetActive(false);
            if (winBG != null)
                winBG.gameObject.SetActive(false);
            if (betBG != null)
                betBG.gameObject.SetActive(false);
            slotMachine = slotMachine2;
            slotMachine.UpdateUIByRoom(2339);
        }
        else
        {
            MenuTopManager.instance.setMenu(MenuType.GREEN);
            Backgound.instance.setBackground("slot3_background");
            foreach (var toggle in listToggleLineOnGame)
            {
                toggle.gameObject.GetComponent<SlotTypeManager>().setType(SlotType.GREEN);
            }
            slotMachine1.gameObject.SetActive(false);
            slotMachine2.gameObject.SetActive(false);
            slotMachine3.gameObject.SetActive(true);
            slot1.gameObject.SetActive(false);
            slot2.gameObject.SetActive(false);
            slot3.gameObject.SetActive(true);
            if (winBG != null)
                winBG.sprite = ImageSheet.Instance.resourcesDics["cuoc_button_xanh"];
            if (betBG != null)
                betBG.sprite = ImageSheet.Instance.resourcesDics["cuoc_button_xanh"];
            slotMachine = slotMachine3;
            slotMachine.UpdateUIByRoom(2685);
        }

        Select_Tat();
        updateAutoSpinLabel();
        updateTrialLabel();
    }
    public void OnEnable()
    {
        moneyLabel.FillData(0);
        lastWinLabel.FillData(0);
        if (selectAllAtStart)
            Select_Tat();
        UpdateUI();

        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnGetRoomsDone += Wc_OnGetRoomsDone;
            WarpClient.wc.OnJoinRoomDone += Wc_OnJoinRoomDone;
            WarpClient.wc.OnLeaveRoom += Wc_OnLeaveRoom;
            OGUIM.listRoomSlot = null;
            AutoCheckAndJoinRoom(true);
        }
    }

    public void OnDisable()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnGetRoomsDone -= Wc_OnGetRoomsDone;
            WarpClient.wc.OnJoinRoomDone -= Wc_OnJoinRoomDone;
            WarpClient.wc.OnLeaveRoom -= Wc_OnLeaveRoom;
        }

        if (musicCheck)
        {
            UIManager.instance.musicToggle.isOn = true;
            UIManager.instance.UpdateMusicState();
        }
    }

    public void AutoCheckAndJoinRoom(bool autoJoinRoom)
    {
        isAutoJoinRoom = autoJoinRoom;
        if (OGUIM.listRoomSlot == null || !OGUIM.listRoomSlot.Any())
        {
            OGUIM.isListen = false;
            WarpRequest.GetRooms(OGUIM.currentLobby.id);
            return;
        }
        else
        {

            for (int i = 0; i < OGUIM.listRoomSlot.Count; i++)
            {
                listJackpotLabel[i].FillData(OGUIM.listRoomSlot[i].funds);
                listJackpotLabel[i].name = OGUIM.listRoomSlot[i].id.ToString();
                listJackpotView[i].name = OGUIM.listRoomSlot[i].id.ToString();

                FillDataInPopUp();
            }

            var checkRoom = OGUIM.listRoomSlot.OrderBy(x => x.bet).FirstOrDefault();
            if (checkRoom != null && isAutoJoinRoom)
            {
                OGUIM.isListen = false;
                WarpRequest.JoinRoom(OGUIM.currentLobby.onlygold ? GameBase.moneyGold.type : OGUIM.currentMoney.type, checkRoom.bet);
            }

            
        }
    }

    private void Wc_OnGetRoomsDone(WarpResponseResultCode status, List<RoomSlot> data)
    {
        if (status == WarpResponseResultCode.SUCCESS && data.Any())
        {
            OGUIM.listRoomSlot = data;
            AutoCheckAndJoinRoom(isAutoJoinRoom);
        }
        else
        {
            OGUIM.Toast.Show("Không thành công. Vui lòng thử lại...", UIToast.ToastType.Warning);
        }
    }

    private void Wc_OnJoinRoomDone(WarpResponseResultCode status, Room data)
    {
        
        if (status == WarpResponseResultCode.SUCCESS && data != null && data.room != null)
        {
            Debug.Log("SLOT ROOM: " + data.room.id);

            for (int i = 0; i < listJackpotView.Count; i++)
            {
                if (listJackpotView[i].name != data.room.id.ToString())
                    listJackpotView[i].SetActive(false);
                else
                    listJackpotView[i].SetActive(true);
            }
            MenuTopManager.instance.UpdateJackpot(data.room.funds);
            //UpdateJackpot(data.room.funds);
            OGUIM.instance.currentLobbyId = LobbyId.SLOT;
            OGUIM.currentRoom = data;
            OGUIM.autoLeaveRoom = false;
            if (currentBetLabel != null)
                currentBetLabel.FillData(data.room.bet, "", 50);
            UpdateUI();
            OGUIM.Toast.Hide();

            //if (popUpSelectRoom.state == UIAnimation.State.IsShow)
            //   popUpSelectRoom.Hide();

            OGUIM.instance.lobbyName.text = OGUIM.currentLobby.shotname;  //+" - "+ LongConverter.ToFull(betRoomValue)
            OGUIM.instance.roomBetValue.text = LongConverter.ToFull(betRoomValue) + " " + OGUIM.currentMoney.name;
        }
        else
        {
            OGUIM.Toast.Show("Không thành công. Vui lòng thử lại...", UIToast.ToastType.Warning);
            //OGUIM.UnLoadGameScene();
            OGUIM.Toast.Hide();
        }
        OGUIM.isListen = true;
    }

    private void Wc_OnLeaveRoom(Room room, int status)
    {
        if (status == (int)WarpResponseResultCode.SUCCESS && nextBetRoomValue != 0)
        {

            OGUIM.isListen = false;
            WarpRequest.JoinRoom(OGUIM.currentLobby.onlygold ? GameBase.moneyGold.type : OGUIM.currentMoney.type, nextBetRoomValue);
            nextBetRoomValue = 0;
        }
        else
            OGUIM.Toast.Hide();
    }

    void Update()
    {
        if (isAuto && !isSpining && !slotMachine.IsSpinning)
        {
            Spin();
        }
    }

    public void ChangeBetRoom(int roomIndex = -1)
    {
        if (slotMachine.IsSpinning)
        {
            OGUIM.Toast.ShowNotification("Đợi kết thúc");
            return;
        }


        if (OGUIM.listRoomSlot == null || !OGUIM.listRoomSlot.Any())
        {
            WarpRequest.GetRooms(OGUIM.currentLobby.id);
        }
        else if (OGUIM.listRoomSlot.Count >= 3)
        {
            if (roomIndex == -1)
            {
                nextBetRoomValue = 0;
                if (betRoomValue == OGUIM.listRoomSlot[0].bet)
                {
                    nextBetRoomValue = OGUIM.listRoomSlot[1].bet;
                }
                else if (betRoomValue == OGUIM.listRoomSlot[1].bet)
                {
                    nextBetRoomValue = OGUIM.listRoomSlot[2].bet;
                }
                else if (betRoomValue == OGUIM.listRoomSlot[2].bet)
                {
                    nextBetRoomValue = OGUIM.listRoomSlot[0].bet;
                }
            }
            else
            {
                nextBetRoomValue = OGUIM.listRoomSlot[roomIndex].bet;
            }
            OGUIM.isListen = false;
            BuildWarpHelper.LeaveRoom(null);
        }
    }

    public void FillDataInPopUp()
    {
        var lobbySlotStatus = LobbyViewListView.listLobbiesStatus.FirstOrDefault(x => (int)x.zoneId == (int)LobbyId.SLOT);
        long players = 100;
        if (lobbySlotStatus != null)
            players = lobbySlotStatus.player + UnityEngine.Random.Range(-10, 20);

        for (int i = 0; i < OGUIM.listRoomSlot.Count; i++)
        {
            listJackpotLabel[i].FillData(OGUIM.listRoomSlot[i].funds);
            listJackpotLabel[i].name = OGUIM.listRoomSlot[i].id.ToString();

            //        if (listSlotRoomView[i].lobbyData == null)
            //        {
            //            listSlotRoomView[i].nameText.text = LongConverter.ToFull(OGUIM.listRoomSlot[i].bet);
            //            listSlotRoomView[i].lobbyData = new Lobby
            //            {
            //                tables = OGUIM.listRoomSlot[i].funds,
            //                id = OGUIM.listRoomSlot[i].id
            //            };
            //        }

            //        if (listSlotRoomView[i].lobbyData != null)
            //        {
            //            listSlotRoomView[i].lobbyData.tables = OGUIM.listRoomSlot[i].funds;
            //if (i == 0)
            //	listSlotRoomView [i].lobbyData.players = (long)(players * 0.6);
            //else if (i == 1)
            //	listSlotRoomView [i].lobbyData.players = (long)(players * 0.3);
            //else if (i == 2)
            //	listSlotRoomView [i].lobbyData.players = (long)(players * 0.1);
            //        }
            //listSlotRoomView[i].UpdateStatus();
        }
    }

    public void Spin()
    {
        // isSpining 			  : check by Logic (client/server)
        // slotMachine.IsSpinning : check by Animation (client spin)
        if (isSpining || slotMachine.IsSpinning)
        {
            Debug.LogError("SPINNING");
            return;
        }

        if (totalLine <= 0)
        {
            if (OGUIM.Toast != null)
                OGUIM.Toast.Show("Hãy Chọn dòng cược...!", UIToast.ToastType.Warning);
            popUp.Show(1);
        }
        else
        {
            //Send request
            isSpining = true;

            BuildWarpHelper.SLOT_Spin(isTrail ? 0 : 1, listLineSelected, () =>
            {
                UILogView.Log("Spin is timeout");
                isSpining = false;
                OGUIM.Toast.Hide();
            });
        }
    }

    public void AutoSpinToggle()
    {
        if (isAuto)
        {
            //Spin();
        }
    }
    public void AutoValueChange()
    {
        isAuto = !isAuto;
        if (isAuto)
        {
            //Spin();
        }
    }
    public void TrialValueChange()
    {
        isTrail = !isTrail;
    }
    public void BetValueChange()
    {
        int index = 0;
        if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
        {
            index = dropdownSlot.value;
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
        {
            index = dropdownSlot2.value;
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
        {
            index = dropdownSlot3.value;
        }
        Debug.Log("index : " + index);
        ChangeBetRoom(index);
    }

    public void ShowLine(int line, float delay)
    {
        var buttonLine = listToggleLineOnGame.FirstOrDefault(x => x.transform.parent.name == "Select_Line (" + line + ")");
        if (buttonLine != null)
        {
            if (buttonLine.isOn)
                UIManager.PlaySound(soundOnLineSelect);
            else
                UIManager.PlaySound(soundOffLineSelect);

            DOVirtual.DelayedCall(delay, () =>
            {
                buttonLine.transform.parent.gameObject.GetComponent<Select_Line>().ShowLine();
            }).SetId(slotMachine.doTweenId);
        }
    }

    public void UpdateStatusFromMainGame()
    {
        for (int i = 0; i < listToggleLineOnGame.Count; i++)
        {
            if (i < listToggleLineOnTab.Count)
            {
                listToggleLineOnTab[i].isOn = listToggleLineOnGame[i].isOn;
            }
        }

        // buttonAuto.toggle.isOn = false;
        UpdateUI();
    }

    public void UpdateStatusFromTabGame()
    {
        for (int i = 0; i < listToggleLineOnTab.Count; i++)
        {
            if (i < listToggleLineOnGame.Count)
            {
                listToggleLineOnGame[i].isOn = listToggleLineOnTab[i].isOn;
            }
        }
        //buttonAuto.toggle.isOn = false;
        UpdateUI();
    }

    public void UpdateUI()
    {
        if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null)
            betRoomValue = OGUIM.currentRoom.room.bet;

        moneyLabel.FillData(OGUIM.me.gold);

        totalLine = listToggleLineOnGame.Count(x => x.isOn);
        totalBet = totalLine * betRoomValue;
        if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
        {
            totalLineLabelInButton.text = totalLine + "";
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
        {
            totalLineLabelInButtonSlot2.text = totalLine + "";
        }
        else
        {
            totalLineLabelInButtonSlot3.text = totalLine + "";
        }

        totalBetLabel.FillData(totalBet);

        listLineSelected = new List<int>();
        foreach (var i in listToggleLineOnGame.Where(x => x.isOn))
        {
            var index = listToggleLineOnGame.IndexOf(i) + 1;
            listLineSelected.Add(index);
        }

        if (totalLine > 0)
            totalLineLableInPopup.text = "Chọn " + "<b><color=#FFC800FF>" + totalLine + "</color></b>" + " dòng, tổng cược " + "<b><color=#FFC800FF>" + LongConverter.ToK(totalBet) + "</color></b>" + " Gold";
        else
            totalLineLableInPopup.text = "Chọn dòng cược trước";

    }

    public void Select_Tat()
    {
        for (int i = 0; i < listToggleLineOnTab.Count; i++)
        {
            listToggleLineOnTab[i].isOn = true;
        }
        UpdateStatusFromTabGame();
    }

    public void Select_Le()
    {
        for (int i = 0; i < listToggleLineOnTab.Count; i++)
        {
            if (i % 2 == 0)
                listToggleLineOnTab[i].isOn = true;
            else
                listToggleLineOnTab[i].isOn = false;
        }
        UpdateStatusFromTabGame();
    }

    public void Select_Chan()
    {
        for (int i = 0; i < listToggleLineOnTab.Count; i++)
        {
            if (i % 2 != 0)
                listToggleLineOnTab[i].isOn = true;
            else
                listToggleLineOnTab[i].isOn = false;
        }
        UpdateStatusFromTabGame();
    }

    public void Select_Bo()
    {
        for (int i = 0; i < listToggleLineOnTab.Count; i++)
        {
            listToggleLineOnTab[i].isOn = false;
        }
        UpdateStatusFromTabGame();
    }

    public void ShowTopUp()
    {
        if (GameBase.underReview)
        {
#if UNITY_ANDROID || UNITY_IOS
            OGUIM.instance.popupIAP.Show();
            return;
#endif
        }
        if (GameBase.cash_in == 0)
            return;
        OGUIM.instance.Cash_In_Feature = PlayerPrefs.GetInt(Common.CASH_IN_FEATURE, 0);
        if (OGUIM.instance.Cash_In_Feature == 0)
        {
            OGUIM.instance.authPopup.Show(AuthenType.CASH_IN);
            return;
        }
        OGUIM.instance.popupTopUp.Show();
    }

    public void ShowUserHistory()
    {
        if (OGUIM.instance != null)
        {
            OGUIM.instance.popupHistory.Show(0);
            var currentLobbyId = LobbyId.SLOT;
            if(OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
            {
                currentLobbyId = LobbyId.SLOT2;
            }else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
            {
                currentLobbyId = LobbyId.SLOT3;
            }
            OGUIM.instance.UserHistoryListView.Get_Data(true, currentLobbyId);
        }

    }

    public void ShowJacpotHistory()
    {
        if (OGUIM.instance != null)
            OGUIM.instance.popupHistory.Show(1);

    }

    public void UpdateJackpot(long jackpotValue)
    {
        if (txtJackpot != null)
            txtJackpot.FillData(jackpotValue);
    }

    public void UpdateJackpotValue(int id, long values)
    {
        var checkRoom = OGUIM.listRoomSlot.FirstOrDefault(x => x.id == id);
        if (checkRoom != null)
            checkRoom.funds = values;

        var checkJackpotLabel = listJackpotLabel.FirstOrDefault(x => x.name == id.ToString());
        if (checkJackpotLabel != null)
            checkJackpotLabel.FillData(values);

        // var checkRoomView = listSlotRoomView.FirstOrDefault(x => x.lobbyData != null && x.lobbyData.id == id);
        // if (checkRoomView != null)
        //{
        //    checkRoomView.lobbyData.tables = values;
        //checkRoomView.UpdateStatus();
        //}
    }

    public void UpdateJackpotValue(List<long> values)
    {
        for (int i = 0; i < listJackpotLabel.Count; i++)
        {
            listJackpotLabel[i].FillData(values[i]);
            //listSlotRoomView[i].lobbyData.tables = values[i];
            // listSlotRoomView[i].UpdateStatus();
        }
    }

    public void CreateSpecialEfx(int jackpotType)
    {
        jackpotEfx.Active(jackpotType);
    }

    public void CreateCoinRainEfx()
    {
        //var pos = new Vector3(0, -1.5f, 0);
        ////for (int i = 0; i < Random.Range(30, 50); i++)
        //{
        //    var go = coinRainEfxPrefab.Spawn();
        //    go.transform.SetParent(transform, false);
        //    go.transform.rotation = Quaternion.Euler(Vector3.left * 60);
        //    go.transform.position = pos;
        //    go.transform.DOMoveZ(0, 2.5f).OnComplete(() => go.Recycle());
        //    //go.GetComponent<ChipEffect>().Run(Instance.userCoinTransform.position);
        //}
    }

    public void SetWinCoinAndSpinAndUserCoin(int chip, int freeSpin, long userCoin)
    {
        lastWinLabel.FillData(chip);
        freeValue = freeSpin;
        moneyLabel.FillData(userCoin);
        OGUIM.me.gold = userCoin;
        OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, OGUIM.me.gold);
    }

    public void DoSpin(List<int[]> faces)
    {
        isSpining = true;
        slotMachine.Spin(faces);
        UIManager.PlaySound("reverse");

        for (int i = 0; i < 20; i++)
        {
            int index = i + 1;
            if (index > 12)
                index = 11;
            DOVirtual.DelayedCall(0.15f * (i + 1), () => UIManager.PlaySound("combo_" + index));
        }
    }

    public void ShowLine(List<PayLine> payLines)
    {
        UIManager.PlaySound("slow_down");
        for (int i = 0; i < payLines.Count; i++)
        {
            ShowLine(payLines[i].line, i * 0.7f);
            slotMachine.ShowLine(payLines[i].matched, i * 0.7f);
        }
        slotMachine.ShowLine(null, payLines.Count * 0.7f);
        DOVirtual.DelayedCall(Mathf.Min(payLines.Count + 1, 3), () =>
        {
            UIManager.PlaySound("stage_clear");
            OnSpinCompleted();
        }).SetId(slotMachine.doTweenId);
    }

    public void OnSpinCompleted()
    {
        isSpining = false;

        Debug.Log("OnSpinCompleted autoLeaveRoom: " + OGUIM.autoLeaveRoom);
        if (OGUIM.autoLeaveRoom)
        {
            OGUIM.GoBack();
        }
        else
        {
            //lastWinLabel.FillData(0);
            if (isAuto)
            {
                //Spin();
            }
        }
    }

    public void ResetSpin()
    {
        //buttonSpinStatusLabel.text = "";
        DOTween.Kill(slotMachine.doTweenId);
        slotMachine.ShowLine(null);
        lastWinLabel.FillData(0);
    }

    public void autoSpinClicked()
    {
        isAuto = !isAuto;
        updateAutoSpinLabel();
        DisableButton(!isAuto);
    }
    private void DisableButton(bool isDisable)
    {
        if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
        {
            dropdownSlot.interactable = isDisable;
            btnLineSlot.interactable = isDisable;
            buttonSpin.interactable = isDisable;
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
        {
            dropdownSlot2.interactable = isDisable;
            btnLineSlot2.interactable = isDisable;
            buttonSpin2.interactable = isDisable;
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
        {
            dropdownSlot3.interactable = isDisable;
            btnLineSlot3.interactable = isDisable;
            buttonSpin3.interactable = isDisable;
        }
    }
    public void trialSpinClicked()
    {
        if (slotMachine.IsSpinning)
        {
            OGUIM.Toast.ShowNotification("Đợi kết thúc");
            return;
        }
        isTrail = !isTrail;
        updateTrialLabel();

        if (isTrail)
        {
            if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
            {
                dropdownSlot.value = 2;
            }
            else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
            {
                dropdownSlot2.value = 2;
            }
            else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
            {
                dropdownSlot3.value = 2;
            }
            ChangeBetRoom(2);
            Select_Tat();

            if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
            {
                dropdownSlot.interactable = false;
                btnLineSlot.interactable = false;
                btnAutoSpinSlot.interactable = false;
            }
            else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
            {
                dropdownSlot2.interactable = false;
                btnLineSlot2.interactable = false;
                btnAutoSpinSlot2.interactable = false;
            }
            else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
            {
                dropdownSlot3.interactable = false;
                btnLineSlot3.interactable = false;
                btnAutoSpinSlot3.interactable = false;
            }
        }
        else
        {
            if (OGUIM.currentLobby.id == (int)LobbyId.SLOT)
            {
                dropdownSlot.interactable = true;
                btnLineSlot.interactable = true;
                btnAutoSpinSlot.interactable = true;
            }
            else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT2)
            {
                dropdownSlot2.interactable = true;
                btnLineSlot2.interactable = true;
                btnAutoSpinSlot2.interactable = true;
            }
            else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
            {
                dropdownSlot3.interactable = true;
                btnLineSlot3.interactable = true;
                btnAutoSpinSlot3.interactable = true;
            }
        }
    }
    private void updateAutoSpinLabel()
    {
        if (isAuto)
        {
            if (autoSpinLabelSlot != null)
                autoSpinLabelSlot.text = "Bật";
            if (autoSpinLabelSlot2 != null)
                autoSpinLabelSlot2.text = "Bật";
            if (autoSpinLabelSlot3 != null)
                autoSpinLabelSlot3.text = "Bật";
        }
        else
        {
            if (autoSpinLabelSlot != null)
                autoSpinLabelSlot.text = "Tắt";
            if (autoSpinLabelSlot2 != null)
                autoSpinLabelSlot2.text = "Tắt";
            if (autoSpinLabelSlot3 != null)
                autoSpinLabelSlot3.text = "Tắt";
        }
    }

    private void updateTrialLabel()
    {
        if (isTrail)
        {
            if (trialSpinLabelSlot != null)
                trialSpinLabelSlot.text = "Thử";
            if (trialSpinLabelSlot2 != null)
                trialSpinLabelSlot2.text = "Thử";
            if (trialSpinLabelSlot3 != null)
                trialSpinLabelSlot3.text = "Thử";
        }
        else
        {
            if (trialSpinLabelSlot != null)
                trialSpinLabelSlot.text = "Thật";
            if (trialSpinLabelSlot2 != null)
                trialSpinLabelSlot2.text = "Thật";
            if (trialSpinLabelSlot3 != null)
                trialSpinLabelSlot3.text = "Thật";
        }
    }

    public class LineData
    {
        public int index;
    }

}
