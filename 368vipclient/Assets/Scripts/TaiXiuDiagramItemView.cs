﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaiXiuDiagramItemView : MonoBehaviour {
    public Sprite[] sprites;
    public Image vi;
    public HistoryBase _data;
    public Text txtSum;
    public List<Image> lstVi;
    private void Awake()
    {
        txtSum = transform.Find("Text").GetComponent<Text>();
    }
    public bool FillData(HistoryBase data)
    {
        if (vi == null)
            return false;

        _data = data;

        if (_data.point > 10)
            vi.sprite = sprites[0];
        else
            vi.sprite = sprites[1];

        return true;
    }

    public bool FillDataType2(HistoryBase data)
    {
        if (vi == null)
            return false;

        _data = data;

        txtSum.text = data.point.ToString();

        if (_data.point > 10)
        {
            vi.sprite = sprites[0];
           // txtSum.color = Color.black;
        }
        else
        {
            vi.sprite = sprites[1];
            //txtSum.color = Color.black;
        }

        return true;
    }

    public bool FillDataTypeSum(List<HistoryBase> data)
    {
        if (lstVi == null)
            return false;

        int len = lstVi.Count > data.Count ? data.Count : lstVi.Count;
        for(int i = 0; i < len; i++)
        {
            if (data[i].point > 10)
            {
                lstVi[i].sprite = sprites[0];
                lstVi[i].gameObject.SetActive(true);
            }
            else
            {
                lstVi[i].sprite = sprites[1];
                lstVi[i].gameObject.SetActive(true);
            }
        }

        return true;
    }
}
