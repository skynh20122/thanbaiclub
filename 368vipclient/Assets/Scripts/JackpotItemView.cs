﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JackpotItemView : MonoBehaviour {
    public List<Text> labels;
    public Button buttonDetail;
    public List<string> data;
    public bool FillData(List<string> _data)
    {
       

        if (labels != null && _data != null)
        {
            data = _data;
            for (int i = 0; i < labels.Count; i++)
            {
                try
                {
                    labels[i].text = data[i];
                }
                catch (System.Exception ex)
                {
                    UILogView.Log("HistoryItemView: " + ex.Message, true);
                    return false;
                }
            }
            return true;
        }
        else
            return false;
    }
}
