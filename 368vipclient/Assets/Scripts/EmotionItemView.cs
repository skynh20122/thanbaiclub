﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmotionItemView : MonoBehaviour {

    public Image image;
    [HideInInspector]
    public Sprite data;
    public bool FillData(Sprite _data)
    {
        if (image == null)
            image = GetComponentInChildren<Image>();
        if (image != null)
        {
            data = _data;
            image.sprite = data;
            return true;
        }

        return false;
    }

    public void sendEmotion()
    {
        
        if(data != null)
        {
            if (OGUIM.instance.timeChat == 0)
            {
                if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null && OGUIM.currentRoom.room.id != 0)
                {
                    //inputChat.interactable = false;
                    BuildWarpHelper.SendChat(OGUIM.currentRoom.room.id, data.name, (int)ChatType.Emotion, "1", () => { });
                }
            }
            else
            {
                OGUIM.Toast.Show("Bạn chát quá nhanh ! Hãy chát chậm lại.");
            }

        }
    }

    public void sendEmotionMiniRoom()
    {
        if (data != null)
        {
            if (MiniGames.instance != null && MiniGames.instance.miniRoom != null && MiniGames.instance.miniRoom.id != 0)
            {
                //inputChat.interactable = false;
                BuildWarpHelper.SendChat(MiniGames.instance.miniRoom.id, data.name, (int)ChatType.Emotion, "1", () => { });
            }
        }
    }
}
