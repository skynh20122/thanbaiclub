﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaiXiuChatView : MonoBehaviour {

    public RootChatData _data;
    private Text content;
    private string formatMe = "<color=#4BFF00> {0} :</color> {1}";
    private string formatUser = "<color=#FFFF00> {0} :</color> {1}";

    private void Awake()
    {
        content = transform.Find("content").GetComponent<Text>();
    }

    private void Start()
    {
        
    }

    public bool FillData(RootChatData data)
    {
        if (data == null || string.IsNullOrEmpty(data.message))
            return false;

        _data = data;

        if (data.userId == OGUIM.me.id)
        {
            content.text = string.Format(formatMe, data.userName, data.message);

            UILogView.Log(string.Format(formatMe, data.userName, data.message));
        }
        else
        {
            content.text = string.Format(formatUser, data.userName, data.message);
            UILogView.Log(string.Format(formatUser, data.userName, data.message));
        }

        return true;

    }
}
