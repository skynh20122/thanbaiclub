﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupInbox : MonoBehaviour {

    private UIAnimation anim;
    private void Awake()
    {
        if (anim == null)
            anim = GetComponent<UIAnimation>();
    }

    public void Show()
    {
        anim.Show();
    }
    public void Hide()
    {
        anim.Hide();
    }
}
