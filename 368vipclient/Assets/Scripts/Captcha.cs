﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Captcha : MonoBehaviour {

    private const string chars = "01234567899876543210";
    public static Captcha instance;
    public Text captchaText;
    [SerializeField]
    private int captchaLenth = 3;
    [HideInInspector]
    public string captcha;
    private void Awake()
    {
        if(instance == null)
        instance = this;

    }

    private void Start()
    {
        if(captchaText != null)
        {
            captchaText.text = getCaptcha();
        }
    }

    private string getCaptcha()
    {
        var result = new char[captchaLenth];
       
        for(int i = 0; i < captchaLenth; i++)
        {
            result[i] = chars[Random.Range(0, chars.Length - 1)];
        }
        captcha = new string(result);
        return captcha;
    }

    public void resetCaptcha()
    {
        if(captchaText != null)
        {
            captchaText.text = getCaptcha();
        }
    }
}
