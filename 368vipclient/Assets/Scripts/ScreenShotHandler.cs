﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ScreenShotHandler : MonoBehaviour {

    private static ScreenShotHandler instance;
    public static string screenShotPath = "";
    public static string screenShotpng = "ScreenShot.png";
    public static string screenShotFileName = "";
    public Camera myCamera;
    private bool takeScreenShotOnNextFrame = false;

    private void Awake()
    {
        instance = this;
        if(myCamera == null)
            myCamera = gameObject.GetComponent<Camera>();
        screenShotPath = Application.dataPath + "/ScreenShotCamera.png";
        screenShotFileName = Application.persistentDataPath + "/" + screenShotpng;
    }

    private void OnPostRender()
    {
        if (takeScreenShotOnNextFrame)
        {
            takeScreenShotOnNextFrame = false;

            RenderTexture renderTexture = myCamera.targetTexture;
            Texture2D result = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);

            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
            result.ReadPixels(rect, 0, 0);
            byte[] byteArray = result.EncodeToPNG();
            System.IO.File.WriteAllBytes(screenShotPath, byteArray);

            Debug.Log("Save image to : " + screenShotPath);

            RenderTexture.ReleaseTemporary(renderTexture);
            myCamera.targetTexture = null;
        }
    }

    private void takeScreenShot(int width,int height)
    {
        myCamera.targetTexture = RenderTexture.GetTemporary(width, height, 16);
        takeScreenShotOnNextFrame = true;
    }

    public static void takeScreenShots(int w,int h)
    {
        instance.takeScreenShot(w, h);
    }

    public static void captureScreenShots(Action OnCaptureCompleted)
    {
        instance.captureScreenShot(OnCaptureCompleted);
    }
    public void captureScreenShot(Action OnCaptureCompleted)
    {
        StartCoroutine(captureScreen(OnCaptureCompleted));
        Debug.Log("Capture : "  + ScreenShotHandler.screenShotFileName);
    }

    IEnumerator captureScreen(Action OnCaptureCompleted)
    {

        screenShotpng = "ScreenImage" +
                DateTime.Now.Year +
                DateTime.Now.Month +
                DateTime.Now.Day +
                DateTime.Now.Hour +
                DateTime.Now.Minute +
                DateTime.Now.Millisecond +
                ".png";
        screenShotFileName = Application.persistentDataPath + "/" + screenShotpng;
        ScreenCapture.CaptureScreenshot(screenShotpng);
        yield return new WaitForEndOfFrame();
        if (OnCaptureCompleted != null)
            OnCaptureCompleted();
    }

    protected const string MEDIA_STORE_IMAGE_MEDIA = "android.provider.MediaStore$Images$Media";
    protected static AndroidJavaObject m_Activity;

    public static string SaveImageToGallery(Texture2D a_Texture, string a_Title, string a_Description)
    {
        using (AndroidJavaClass mediaClass = new AndroidJavaClass(MEDIA_STORE_IMAGE_MEDIA))
        {
            using (AndroidJavaObject contentResolver = Activity.Call<AndroidJavaObject>("getContentResolver"))
            {
                AndroidJavaObject image = Texture2DToAndroidBitmap(a_Texture);
                return mediaClass.CallStatic<string>("insertImage", contentResolver, image, a_Title, a_Description);
            }
        }
    }

    protected static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D a_Texture)
    {
        byte[] encodedTexture = a_Texture.EncodeToPNG();
        using (AndroidJavaClass bitmapFactory = new AndroidJavaClass("android.graphics.BitmapFactory"))
        {
            return bitmapFactory.CallStatic<AndroidJavaObject>("decodeByteArray", encodedTexture, 0, encodedTexture.Length);
        }
    }

    protected static AndroidJavaObject Activity
    {
        get
        {
            if (m_Activity == null)
            {
                AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                m_Activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            }
            return m_Activity;
        }
    }

    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            RenderTexture renderTexture = instance.myCamera.targetTexture;
            tex = new Texture2D(renderTexture.width,renderTexture.height);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            RenderTexture.ReleaseTemporary(renderTexture);
            instance.myCamera.targetTexture = null;
        }
        return tex;
    }
}
