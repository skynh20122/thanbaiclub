﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingView : MonoBehaviour
{

    public static LoadingView instance = null;

    private UIAnimation anim;
    private UISlider slider;
    public LoadingState state = LoadingState.NONE;
    
    void Awake()
    {
        if (instance == null)
            instance = this;
        if (anim == null)
            anim = GetComponent<UIAnimation>();
        slider = transform.Find("Loading Bar").GetComponent<UISlider>();
    }


    public void ShowLoading()
    {
        anim.Show(()=> {
            state = LoadingState.SHOW;
            if (slider != null)
                slider.Loading("Đang tải ...", Random.Range(0f, 50f));
        });
        
    }

    public void HideLoading()
    {
        if (state == LoadingState.NONE)
            return;

        if (slider != null)
            slider.SetValue(1);
        anim.Hide(() => {
            state = LoadingState.NONE;
        });
    }


    public static void Show()
    {
        if(instance != null)
            instance.ShowLoading();
    }
    public static void Hide()
    {
        if (instance != null)
            instance.HideLoading();
    }
}
public enum LoadingState : int
{
    NONE = 0,
    SHOW = 1,
}
