﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhomCardHold : MonoBehaviour {

    public Text cardHoldText;
    private int count;
    public static PhomCardHold instance;
    private void Awake()
    {
        if(instance == null)
        instance = this;
    }

    public void setHoldText(int value)
    {
        if(cardHoldText != null)
        {
            if(value > 0)
            {
                if (!gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(true);
                }

                cardHoldText.text = value.ToString();
            }
            else
            {
                cardHoldText.text = value.ToString();
                gameObject.SetActive(false);
            }
        }
    }

    public void subHoldCount()
    {
        if(cardHoldText != null)
        {
            int.TryParse(cardHoldText.text, out count);
            count--;
            gameObject.SetActive(count > 0);
            cardHoldText.text = count == 0 ? "" : count.ToString();
        }
    }
}
