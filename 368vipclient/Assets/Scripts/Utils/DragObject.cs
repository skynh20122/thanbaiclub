﻿using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class DragObject : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IPointerDownHandler
{
    private GameObject targetObject;
    private RectTransform _rect;
    private Canvas _myCanvas;
    private Vector3 _tempPos;

    public Action Finish;

    void Start()
    {
        if (targetObject == null)
            targetObject = gameObject;
        _rect = targetObject.GetComponent<RectTransform>();
        _myCanvas = FindObjectOfType<Canvas>();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_myCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            _rect.position = new Vector3(_tempPos.x + eventData.position.x, _tempPos.y + eventData.position.y, _rect.position.z);
            return;
        }

        Vector3 pos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(_rect, Input.mousePosition, _myCanvas.worldCamera, out pos))
            _rect.position = new Vector3(_tempPos.x + pos.x, _tempPos.y + pos.y, _rect.position.z);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _rect.transform.localPosition = new Vector3(_rect.transform.localPosition.x, _rect.transform.localPosition.y, 0);
        if (Finish != null)
            Finish();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {

        if (_myCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            Vector3 objectPos = _rect.position;
            _tempPos = new Vector3(objectPos.x - eventData.position.x, objectPos.y - eventData.position.y, 0);
            return;
        }

        Vector3 pos;
        if (RectTransformUtility.ScreenPointToWorldPointInRectangle(_rect, Input.mousePosition, _myCanvas.worldCamera, out pos))
        {
            Vector3 objectPos = _rect.position;
            _tempPos = new Vector3(objectPos.x - pos.x, objectPos.y - pos.y, 0);
        }
    }



    public void OnPointerClick(PointerEventData eventData)
    {
        this.transform.SetAsLastSibling();
    }

    public void OnPointerDown(PointerEventData eventData)
    {

        MiniGames.instance.TargetMiniGame(this.transform);
        Debug.Log("on Pointer down");
    }
}
