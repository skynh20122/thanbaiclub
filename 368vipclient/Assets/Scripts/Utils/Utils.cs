﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;

namespace FishPro
{
	public class Utils: MonoBehaviour {

		public static void cleanAllChilds (GameObject parent)
		{
			foreach (Transform childTransform in parent.transform) {
				Destroy (childTransform.gameObject);
			}
		}

		public static T FindChildByName<T>(GameObject parent,string childName)
		{
			Transform chilTransform = parent.gameObject.transform.Find(childName);
			GameObject childObject = chilTransform.gameObject;
			return childObject.GetComponent<T>();

		}

		public static void DebugLog(string log)
		{
			//if (CONFIG.isShowDebug)
			//	Debug.Log(log);
		}

		public static GameObject createGO (
			GameObject prefab, 
			GameObject parent, 
			Vector3 localScale = default(Vector3),
			Vector3 localPos = default(Vector3) 
		)
		{
			if (localScale == default(Vector3))
				localScale = Vector3.one;
			if (localPos == default(Vector3))
				localPos = Vector3.zero;
			if (prefab != null) {
				GameObject go = UnityEngine.Object.Instantiate (prefab) as GameObject;
				go.transform.SetParent (parent.transform);
				go.transform.localPosition = localPos;
				go.transform.localScale = localScale;
				return go;
			} else {
				return null;
			}
		}

		//public static int GetPlatformId()
		//{
		//	#if UNITY_ANDROID
		//	//return CONFIG.SOURCE_ID_ANDROID;
		//	#elif UNITY_IOS || UNITY_IPHONE
		//	return CONFIG.SOURCE_ID_IOS;
		//	#elif UNITY_WEBGL
		//	return CONFIG.SOURCE_ID_WEB;
		//	#elif UNITY_STANDALONE_WIN
		//	return CONFIG.SOURCE_ID_PC;
		//	#else
		//	return CONFIG.SOURCE_ID_OTHERS;
		//	#endif
		//}

		public static string formatNumberRound(float number)
		{
			double value = Math.Round ((double)number, 2);
			return value.ToString ();
		}

		public static string formatMoney(long money, bool isShowPositive = false) {
			bool isPositive = true;
			if (money < 0) {
				money = money * -1;
				isPositive = false;
			}
			string moneyString = money.ToString();
			int count = 0;
			for (int i = moneyString.Length - 1; i > 0; i--) {
				count++;
				if (count % 3 == 0) {
					count = 0;
					moneyString = moneyString.Insert(i, ".");
				}
			}
			if (isPositive) {
				if(isShowPositive)
					return "<color=#ffff1a>+"+moneyString+"</color>";
				else 
					return "<color=#ffff1a>"+moneyString+"</color>";
			}
			else
				return "<color=#ffff1a>-" + moneyString+"</color>";
		}

		public static string formatMoney2(long money) {
			bool isPositive = true;
			if (money < 0) {
				money = money * -1;
				isPositive = false;
			}
			string moneyString = money.ToString();
			int count = 0;
			for (int i = moneyString.Length - 1; i > 0; i--) {
				count++;
				if (count % 3 == 0) {
					count = 0;
					moneyString = moneyString.Insert(i, ".");
				}
			}
			if (isPositive) {
				return moneyString;
			}
			else
				return "-" + moneyString;
		}

		public static string formatUsername(string username, int display_len = 16)
		{
			if (username == null)
				return string.Empty;
			string ret = string.Empty;
			if (username.Length > display_len)
			{
				ret = username.Substring(0, display_len);
				ret += "...";
			}
			else
			{
				ret = username;
			}

			return ret;
		}

		public static string formatMail(string username, int display_len = 60)
		{
			string ret = string.Empty;
			if (username.Length >= display_len)
			{
				ret = username.Substring(0, 60);
				ret += "...";
			}
			else
			{
				ret = username;
			}

			return ret;
		}

		public static string formatTime(float time)
		{
			string ret = string.Empty;
			long timeOnline = (long)System.Math.Round (time);
			long minute = timeOnline / 60;
			long seconds = (timeOnline % 60);

			if (minute < 10) {
				ret = "0" + minute;
			} else{
				ret = minute.ToString ();
			}
			if(seconds >= 10){	
				ret += ":" + seconds;
			}else{
				ret += ":0" + seconds;
			}

			return ret;
		}

		public static string DecryptionData(string text)
		{
			//to-do
			return text;
		}

		public static GameObject CreateGameObjectNonPrefab(GameObject parent,
			Vector3 localScale = default(Vector3),
			Vector3 localPos = default(Vector3))
		{ 
			if (localScale == default(Vector3))
				localScale = Vector3.one;
			if (localPos == default(Vector3))
				localPos = Vector3.zero;

			GameObject go = new GameObject();
			go.transform.SetParent(parent.transform);
			go.transform.localPosition = localPos;
			go.transform.localScale = localScale;
			return go;
		}

		public static Vector2 GetPositionSliceBySittingId(int sittingId, Vector2 position)
		{
			Vector2 direction = GetVectorDirectionBySittingID (sittingId);
			return new Vector2 (position.x * direction.x, position.y * direction.y);
		}

		private static Vector2 GetVectorDirectionBySittingID(int sittingId)
		{
			switch (sittingId) {
			case 2:
				return new Vector2 (1, 1);
			case 3:
				return new Vector2 (-1, 1);
			case 5:
				return new Vector2 (-1, -1);
			case 7:
				return new Vector2 (1, -1);
			default:
				return new Vector2 (1, 1);
			}
		}

		public static Vector3 GetLocalScaleBySittingId(int sittingId)
		{
			Vector3 direction = Vector3.one;

			//if (sittingId == CONFIG.SITTING_ID_1)
			//{
			//	return direction;
			//}
			//else if (sittingId == CONFIG.SITTING_ID_2)
			//{

			//	return new Vector3(-1, 1, 1);
			//}
			//else if (sittingId == CONFIG.SITTING_ID_3)
			//{

			//	return new Vector3(-1, -1, 1);
			//}
			//else if (sittingId == CONFIG.SITTING_ID_4)
			//{

			//	return new Vector3(1, -1, 1);
			//}
			return direction;
		}

		//public static float[] BaseSpeedAnimationFish(int fishType)
		//{
		//	switch (fishType)
		//	{
		//	case CONFIG.FISH_TYPE_1:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_2:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_3:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_4:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_5:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_6:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_7:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_8:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_9:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_10:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_11:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_12:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_13:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_14:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_15:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_16:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_17:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_18:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_19:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_20:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_21:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_22:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_23:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_24:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_25:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.FISH_TYPE_26:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.GIFT_FISH:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.SPECIAL_GUN_FISH:
		//		return new float[3] { 120, 120, 120 };
		//	case CONFIG.JACKPOT_TYPE:
		//		return new float[4] { 120, 120, 120, 120 };
		//	default:
		//		return new float[4] { 120, 120, 120, 120 };
		//	}
		//}

		//public static float[] BaseSpeedAnimationFishDie(int fishType)
		//{
		//	switch (fishType) {
		//	case CONFIG.FISH_TYPE_1:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_2:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_3:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_4:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_5:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_6:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_7:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_8:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_9:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_10:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_11:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_12:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_13:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_14:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_15:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_16:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.FISH_TYPE_17:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.JACKPOT_TYPE:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.MAMON_TYPE:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.BONUS_TYPE:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.GIFT_FISH:
		//		return new float[3] { 10, 10, 10 };
		//	case CONFIG.SPECIAL_GUN_FISH:
		//		return new float[4] { 10, 10, 10, 10 };
		//	default:
		//		return new float[4] { 10, 10, 10, 10 };
		//	}
		//}

		public static string GetFileNameNoExtension(string fileName)
		{
			int fileExtPos = fileName.LastIndexOf(".");
			if (fileExtPos >= 0 )
				fileName= fileName.Substring(0, fileExtPos);

			return fileName;
		}

		//public static bool CheckFunction(EFeatureStatus function)
		//{
		//	if (function == EFeatureStatus.Close) {
		//		MiniGameManager.Instance.ShowCommandPopup(MyLocalization.GetText("LOCK_FUNTION"));
		//	} else if (function == EFeatureStatus.CommingSoon) {
		//		MiniGameManager.Instance.ShowCommandPopup(MyLocalization.GetText("COMMING_SOON"));
		//	} else if (function == EFeatureStatus.None) {
		//		MiniGameManager.Instance.ShowCommandPopup(MyLocalization.GetText("LOCK_FUNTION"));
		//		//MiniGameManager.Instance.ShowCommandPopup (MyLocalization.GetText ("FP_NOT_LOAD_INFO"), () => ScreenManager.Instance.ShowScreen (SCREEN_CODE.LOGIN));
		//		return false;
		//	} else if (function == EFeatureStatus.Open) {
		//		return true;
		//	}
		//	return false;
		//}

		//public static bool ValidateName(string username)
		//{
		//	// Ki?m tra Username.
		//	Regex usernameRegex0 = new Regex (@"^[\S]+$");
		//	Regex usernameRegex1 = new Regex (@"^[\w]+$");
		//	Regex usernameRegex2 = new Regex (@"^(\w{3,15})$");
		//	Regex usernameRegex3 = new Regex (@"^([A-z]+\w{2,14})$");

		//	if (!usernameRegex0.IsMatch (username)) 
		//	{
		//		MiniGameManager.Instance.ShowCommandPopup (MyLocalization.GetText ("NAME_KHOANG_TRANG"));
		//		return false;
		//	}

		//	if (!usernameRegex1.IsMatch (username)) 
		//	{
		//		MiniGameManager.Instance.ShowCommandPopup (MyLocalization.GetText ("NAME_KY_TU_DAC_BIET"));
		//		return false;
		//	}

		//	if (!usernameRegex2.IsMatch (username)) 
		//	{
		//		MiniGameManager.Instance.ShowCommandPopup (MyLocalization.GetText ("NAME_DO_DAI"));
		//		return false;
		//	}

		//	if (!usernameRegex3.IsMatch (username)) 
		//	{
		//		MiniGameManager.Instance.ShowCommandPopup (MyLocalization.GetText ("NAME_BAT_DAU_CHU_SO"));
		//		return false;
		//	}

		//	if (username.Contains("fb_"))
		//	{
		//		return false;
		//	}

		//	return true;
		//}

		//public static void SetupLanguage(ELanguage language)
		//{
		//	MyLocalization.language = language.ToString ();
		//	if (language == ELanguage.Eng)
		//	{
		//		Lean.LeanLocalization.GetInstance().SetLanguage("English");
		//	}
		//	else if (language == ELanguage.Vni)
		//	{
		//		Lean.LeanLocalization.GetInstance().SetLanguage("Vietnamese");
		//	}
		//	else if (language == ELanguage.Cam)
		//	{
		//		Lean.LeanLocalization.GetInstance().SetLanguage("Cambodia");
		//	}
		//	if (ScreenManager.Instance.currentScreen == SCREEN_CODE.LOGIN) {
		//		UIManager.Instance.LoginView.ChangeText ();
		//	}
		//}

		//public static ELanguage GetLanguage(string key)
		//{
		//	ELanguage language = ELanguage.None;
		//	if (key.Equals("English"))
		//	{
		//		language = ELanguage.Eng;
		//	}
		//	else if (key.Equals("Vietnamese"))
		//	{
		//		language = ELanguage.Vni;
		//	}
		//	else if (key.Equals("Cambodia"))
		//	{
		//		language = ELanguage.Cam;
		//	}
		//	return language;
		//}

		//public static Sprite GetAvata()
		//{
		//	long idAvata = MainPlayerInfo.Instance.PlayerID % 6 + 1;
		//	return PrefabControl.CommonSprite (idAvata.ToString());
		//}

		//public static Sprite GetAvata(int id)
		//{
		//	long idAvata = id % 6 + 1;
		//	return PrefabControl.CommonSprite (idAvata.ToString());
		//}


        public static string NumberGroup(string strNumber)
        {
            return NumberGroup(Convert.ToInt64(strNumber.Replace(",", "").Replace(".", "")));
        }

        public static string NumberGroup(int strNumber)
        {
            return (strNumber == 0) ? "0" : string.Format("{0:##,##}", strNumber).Replace(",", ".");
        }

        public static string NumberGroup(long strNumber)
        {
            return (strNumber == 0) ? "0" : string.Format("{0:##,##}", strNumber).Replace(",", ".");
        }
        
        public static void ConvertSlot(string fromListString, List<int> toListInt, int slotCount)
        {
            if (string.IsNullOrEmpty(fromListString)) return;
            toListInt.Clear();
            for (int i = 0; i < slotCount; i++)
            {
                toListInt.Add(int.Parse(fromListString.Split(',')[i]));
            }
        }
        //public static List<PrizeLine> ConvertPrizeLines(string prizeData, string positionData, List<int> listSlot)
        //{
        //    List<PrizeLine> result = new List<PrizeLine>();
        //    if (string.IsNullOrEmpty(prizeData) || string.IsNullOrEmpty(positionData))
        //    {
        //        return result;
        //    }
        //    var lsPrizeData = prizeData.Split(';');
        //    var lsPos = positionData.Split(';');
        //    for (int i = 0; i < lsPrizeData.Length; i++)
        //    {
        //        var tempPrize = lsPrizeData[i].Split(',');
        //        var tempPos = lsPos[i].Split(',');
        //        PrizeLine temp = new PrizeLine
        //        {
        //            LineID = Int32.Parse(tempPrize[0]),
        //            PrizeID = Int32.Parse(tempPrize[1]),
        //            PrizeValue = Int32.Parse(tempPrize[2]),
        //            Items = new List<int>()
        //        };
        //        for (int j = 0; j < tempPos.Length; j++)
        //        {
        //            temp.Items.Add(Int32.Parse(tempPos[j]));
        //        }
        //        result.Add(temp);
        //    }

        //    return result;
        //}

        public static Color HexToColor(string hex)
        {
            hex = hex.Replace("#", "");
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        }

        public static List<int> ConvertLines(string fromListString)
        {
            if (string.IsNullOrEmpty(fromListString))
                return null;
            string[] str = fromListString.Split(new[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
            List<int> result = new List<int>();
            for (int i = 0; i < str.Length; i++)
            {
                result.Add(int.Parse(str[i]));
            }
            return result;
        }

        public static string ConvertToString(List<int> originalListInt)
        {
            int count = originalListInt.Count;
            string targetStr;

            if (count > 0)
                targetStr = originalListInt[0].ToString();
            else
                targetStr = "";

            for (int i = 1; i < count; i++)
            {
                targetStr += "," + originalListInt[i];
            }

            return targetStr;
        }
    }
}
