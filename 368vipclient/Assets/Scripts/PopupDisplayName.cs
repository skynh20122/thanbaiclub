﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupDisplayName : MonoBehaviour
{

    public InputField inputFieldDisplayName;
    public bool showUpdateStatus;
    public UIAnimation anim;
    public UserData userData;

    private void OnEnable()
    {
        if (WarpClient.wc != null)
            WarpClient.wc.OnUpdateUserInfoDone += Wc_OnUpdateUserInfoDone;
    }

    private void OnDisable()
    {
        if (WarpClient.wc != null)
            WarpClient.wc.OnUpdateUserInfoDone -= Wc_OnUpdateUserInfoDone;
    }

    private void Wc_OnUpdateUserInfoDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            inputFieldDisplayName.text = "";
            anim.Hide();
            OGUIM.Toast.ShowNotification("Cập nhật thông tin thành công...");
            WarpRequest.GetUserInfo(OGUIM.me.id);
        }else if(status == WarpResponseResultCode.BAD_REQUEST)
        {
            OGUIM.Toast.ShowNotification("Cập nhật thông tin thất bại, tên đã tồn tại hoặc có chứa ký tự đặc biệt...");
        }
    }

    public void Show(UserData _user)
    {
        userData = _user;
        inputFieldDisplayName.text = userData.displayName;
        //inputFieldStatus.gameObject.SetActive(showUpdateStatus);
        //inputFieldStatus.text = userData.status;
        anim.Show();
    }

    public void UpdateInfo()
    {
        if (SubmitFormExtend.ValidateString(inputFieldDisplayName, "Tên hiển thị", false, 6, 24))
        {
            OGUIM.Toast.ShowLoading("Đang cập nhật thông tin...");
            WarpRequest.UpdateUserInfo(inputFieldDisplayName.text, userData.mobile);

        }
    }
}
