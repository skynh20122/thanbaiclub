﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionSheet : MonoBehaviour {

    public static EmotionSheet instance;
    private Sprite[] resources;
    public Dictionary<string, Sprite> resourcesDics;

    void Awake()
    {
        instance = this;
        resources = Resources.LoadAll<Sprite>("emo");
        resourcesDics = new Dictionary<string, Sprite>();
        foreach (var i in resources)
        {
            try
            {
                resourcesDics.Add(i.name, i);
            }
            catch (System.Exception ex)
            {
                Debug.Log("Duplicate: " + i.name + " " + ex.ToString());
            }
        }
    }
}
