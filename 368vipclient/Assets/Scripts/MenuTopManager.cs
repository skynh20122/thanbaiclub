﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuTopManager : MonoBehaviour
{

    public Image backgroundMenu;
    public Image avataFrame;
    public Image backButton;
    public Image settingIcon;
    public Image notifyIcon;
    public Image inboxIcon;
    public Image goldBackground;
    public GameObject menuTop;
    public GameObject menuTopCard;
    public PlayerMoneyView jackpotView;
    public static MenuTopManager instance;
    private void Awake()
    {
        instance = this;
        jackpotView.gameObject.SetActive(false);
    }

    public void setMenu(MenuType type)
    {
        switch (type)
        {
            case MenuType.NONE:
                menuTop.SetActive(false);
                menuTopCard.SetActive(false);
                break;
            case MenuType.RED:
                setSprite("red");
                break;
            case MenuType.GREEN:
                setSprite("green");
                break;
            case MenuType.PURPLE:
                setSprite("purple");
                break;
            case MenuType.CASINO:
                setCasinoSprite();
                break;
            case MenuType.CARD:
                setCasinoSprite();
                break;
        }
    }

    private void setSprite(string type)
    {
        if (menuTopCard != null)
            menuTopCard.gameObject.SetActive(false);
        if (menuTop != null)
            menuTop.gameObject.SetActive(true);
        if (backgroundMenu != null && ImageSheet.Instance.resourcesDics.ContainsKey("top_bg_" + type))
            backgroundMenu.sprite = ImageSheet.Instance.resourcesDics["top_bg_" + type];
        if (avataFrame != null && ImageSheet.Instance.resourcesDics.ContainsKey("top_avata_" + type))
            avataFrame.sprite = ImageSheet.Instance.resourcesDics["top_avata_" + type];
        if (backButton != null && ImageSheet.Instance.resourcesDics.ContainsKey("btn_back_" + type))
            backButton.sprite = ImageSheet.Instance.resourcesDics["btn_back_" + type];
        if (settingIcon != null && ImageSheet.Instance.resourcesDics.ContainsKey("btn_caidat_" + type))
            settingIcon.sprite = ImageSheet.Instance.resourcesDics["btn_caidat_" + type];
        if (notifyIcon != null && ImageSheet.Instance.resourcesDics.ContainsKey("btn_thongbao_" + type))
            notifyIcon.sprite = ImageSheet.Instance.resourcesDics["btn_thongbao_" + type];
        if (inboxIcon != null && ImageSheet.Instance.resourcesDics.ContainsKey("btn_inbox_" + type))
            inboxIcon.sprite = ImageSheet.Instance.resourcesDics["btn_inbox_" + type];
        if (goldBackground != null && ImageSheet.Instance.resourcesDics.ContainsKey("top_cuoc_bg_" + type))
            goldBackground.sprite = ImageSheet.Instance.resourcesDics["top_cuoc_bg_" + type];


        if (OGUIM.currentLobby == null)
        {
            jackpotView.gameObject.SetActive(false);
        }
        else if (OGUIM.currentLobby.id == (int)LobbyId.SLOT ||
          OGUIM.currentLobby.id == (int)LobbyId.SLOT2 ||
          OGUIM.currentLobby.id == (int)LobbyId.SLOT3)
        {
            jackpotView.gameObject.SetActive(true);
        }
        else
        {
            jackpotView.gameObject.SetActive(false);
        }
    }

    private void setCasinoSprite()
    {
        if (menuTop != null)
            menuTop.gameObject.SetActive(false);
        if (menuTopCard != null)
        {
            menuTopCard.gameObject.SetActive(true);
        }
    }

    public void UpdateJackpot(long value)
    {
        if (jackpotView != null)
            jackpotView.FillData(value);
    }
}

public enum MenuType
{
    NONE = 0,
    RED = 1,
    GREEN = 2,
    PURPLE = 3,
    CASINO = 4,
    CARD = 5,
}
