﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TaiXiuDiagramListView : MonoBehaviour {

    public Text taiLabel, xiuLabel;
    public UIListView uiListview;
    public List<HistoryBase> lstData = new List<HistoryBase>();
    public List<TaiXiuDiagramItemView> listview = new List<TaiXiuDiagramItemView>();
    public TaiXiuDiagramType type = TaiXiuDiagramType.NONE;

    private void Awake()
    {
        Get_Data();
    }

    public void FillAll(bool reset = false)
    {
        if(type == TaiXiuDiagramType.NONE)
        {
            FillData(reset);
        }
        else
        {
            FillDataSum(reset);
        }
    }
    public void FillData(bool reset = false)
    {
        if (reset)
        {
            uiListview.ClearList();
            listview = new List<TaiXiuDiagramItemView>();
        }

        if(lstData != null && lstData.Any())
        {

            foreach (var i in lstData)
            {
                try
                {
                    var ui = uiListview.GetUIView<TaiXiuDiagramItemView>(uiListview.GetDetailView());
                    

                    if (ui.FillData(i))
                    {
                        listview.Add(ui);
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.Log("FillData: " + ex.Message + "\n" + ex.StackTrace);
                }
            }

            int tai = lstData.Where(x => x.point > 10).ToList().Count;
            int xiu = lstData.Where(x => x.point <= 10).ToList().Count;

            taiLabel.text = tai.ToString();
            xiuLabel.text = xiu.ToString();
        }
    }

    public void FillDataSum(bool reset = false)
    {
        if (reset)
        {
            uiListview.ClearList();
            listview = new List<TaiXiuDiagramItemView>();
        }
        int tai = 0;
        int xiu = 0;
        int count = 0;
        if (lstData != null && lstData.Any())
        {
            var temp = new List<HistoryBase>();
            int type = lstData[lstData.Count - 1].point > 10 ? 0 : 1;
            temp.Add(lstData[lstData.Count - 1]);
            for(int i = lstData.Count - 2;i >= 0;i--)
            {
                int temp_type = lstData[i].point > 10 ? 0 : 1;
                if(temp_type == type && temp.Count <= 6)
                {
                    temp.Add(lstData[i]);
                }
                else
                {
                    var ui = uiListview.GetUIView<TaiXiuDiagramItemView>(uiListview.GetDetailView());
                    if (ui.FillDataTypeSum(temp))
                    {
                        listview.Add(ui);
                    }

                    if(count <= 18)
                    {
                        if(type == 1)
                        {
                            tai += temp.Count;
                        }
                        else
                        {
                            xiu += temp.Count;
                        }
                    }

                    count++;

                    temp.Clear();
                    temp.Add(lstData[i]);
                    type = temp_type;
                }
            }

            

            taiLabel.text = tai.ToString();
            xiuLabel.text = xiu.ToString();
        }
    }

    public void Get_Data()
    {
        lstData = MiniTaiXiu.instance.historyTx;
        FillAll(true);
    }
}

public enum TaiXiuDiagramType
{
    NONE = 0,
    SUM = 1
}
