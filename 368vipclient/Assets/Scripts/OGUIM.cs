﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using DG.Tweening;
using System.Runtime.InteropServices;
using Facebook.Unity;

public class OGUIM : MonoBehaviour
{
    public LobbyId currentLobbyId;
    public string currentGameScene = "";
    public static bool isListen = true;
    public static bool isLoadingScence = false;
    public static bool isTheFirst = true;

    public GameObject popupCanvas;
    public List<GameObject> underReviewList;
    public List<GameObject> webglList;
    public List<GameObject> liteList;


    public Text lobbyName;
    public Text roomBetValue;

    #region GAME
    public static Lobby currentLobby { get; set; }
    public static Lobby nextLobby { get; set; }
    public static Room currentRoom { get; set; }
    #endregion

    public AvatarView meView;
    public LobbyView lobbyViewInRooms;
    //public LobbyView lobbyViewInLeader;
    public GameObject lobbyModeView;
    public Toggle autoLoginToggle;
    public Toggle toggleGold;
    public Toggle toggleKoin;
    public Toggle toggleServer;

    public UIToast popupToast;
    public static UIToast Toast;

    public UIMessageBox popupMessenger;
  
    public static UIMessageBox MessengerBox;
    

    public UIAnimation homeCanvas;
    public UIAnimation LoginCanvas;
    public UIAnimation RegisterCanvas;
    public UIAnimation girl;

    public UIAnimation mainMenuCanvas;
    public UIAnimation mainLobiesCanvas;
    public UIAnimation mainRoomsCanvas;
    public UIAnimation mainLeaderCanvas;


    public UIAnimation miniGameGroup;
    public UIAnimation menuTopCanvas;

    public UIPopupTabs popupAchieMissiDaily;
    public UIAnimation popupTopUp;
    public UIAnimation popupGifcode;
    public TopUpCardSubmit topUpCard;
    public UIAnimation popupIAP;
    public UIPopupTabs popupCashOut;
    public TopUpTransfer popupTransfer;
    public PopupUserInfo popupUserInfo;
    public PopupSendMes popupSendMes;
    public PopUpPlayersInRoom popUpPlayersInRoom;
    public PopupVerifyPhone popupVerifyPhone;
    public PopupDisplayName popupDisplayName;
    public ChatView popupChatView;

    public UIPopupTabs popupHistory;
    public HistoryListView UserHistoryListView;
    public HistoryListView JackpotHistoryListView;
    public PopupAllMes popupInbox;
    public PopupAllMes popupEvents;

    public UIAnimation inGameCanvas;

    public Toggle inviteToggleInMesBox;
    public Toggle inviteToggleInSettings;
    public CheckAuthPopup authPopup;
    public Text gameLabel;
    public Text hotlineLabel;
    public GameObject hotlineButton;
    public GameObject hotlineButtonOnSettings;
    public GameObject facebooKButtonOnSettings;
    public GameObject websiteButtonOnSettings;
    public GameObject giftButton;
    public GameObject tienButton;
    public GameObject chatInGameBtn;
    public GameObject backButton;
    public GameObject menuBottom;
    public GameObject objCashOut;
    public GameObject objCashIn;
    [SerializeField]
    private GameObject objInbox, objEvents, objPhoneCall,
        objFacebook, objTopRank, objGifcode, objQuest, headLine,headLineRoom, headLineInGame;
    
    [HideInInspector]
    public float timeChat = 0f;
    public Text textVersion;
    public Text textNeedUpdate;
    public UIAnimation needUpdatePanel;

    public static LoginType loginType { get; set; }
    public static UserData me { get; set; }
    public static int isVerified { get; set; }
    public static Money currentMoney { get; set; }
    private bool autoLoginGame = false;
    public static bool autoLeaveRoom { get; set; }

    public static Dictionary<string, UIAnimation> listNavigation { get; set; }
    public static Dictionary<string, UIAnimation> listPopup { get; set; }
    public static Dictionary<string, UIAnimation> listMenu { get; set; }
    public static Dictionary<string, UIAnimation> listToast { get; set; }

    public static List<RootChatData> listChatRoomData { get; set; }
    public static List<RootChatData> listChatWorldData { get; set; }

    public static Action actionOnFacebookLoginDone { get; set; }
    #region SLOT - XENG
    public static List<RoomSlot> listRoomSlot = new List<RoomSlot>();
    public static List<RoomSlot> listRoomXeng = new List<RoomSlot>();
    #endregion

    public GameNumberController goGameLottery;
    public static GameNumberController gameLottery;
    public static OGUIM instance { get; set; }

    private float timeRelease = 0.5f;
    [HideInInspector]
    public bool isAutoReady = false;
    public bool isShowPopupChangePhone = false;
    public Toggle autoReady;
    [HideInInspector]
    public int Cash_In_Feature = 0;
    [HideInInspector]
    public int Cash_Out_Feature = 0;
    private void Awake()
    {
        Application.targetFrameRate = 60;
#if UNITY_WEBGL
        Shader.EnableKeyword("UNITY_UI_CLIP_RECT");
        Backgound.instance.deactiveLoadingBar();
        if(webglList != null && webglList.Count > 0)
        {
            for(int i = 0; i < webglList.Count; i++)
            {
                webglList[i].gameObject.SetActive(false);
            }
        }
#endif
        Backgound.instance.HideVersion();
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        
        instance = this;

        Toast = popupToast;
        MessengerBox = popupMessenger;
        listNavigation = new Dictionary<string, UIAnimation>();
        listPopup = new Dictionary<string, UIAnimation>();
        listMenu = new Dictionary<string, UIAnimation>();
        listToast = new Dictionary<string, UIAnimation>();

        me = new UserData();

        currentMoney = GameBase.moneyGold;

        AddHttpRequestListener();
        AddWarpRequestListener();
        //gameLabel.text = "<color=#FFC800>" + GameBase.preGameName + "</color> " + GameBase.sufGameName;
        if (autoReady != null)
            autoReady.onValueChanged.AddListener(SetAutoReady);
        getAutoReady();
#if UNITY_WEBGL && !UNITY_EDITOR
       // Debug.unityLogger.logEnabled = false;
#endif

        Cash_In_Feature = PlayerPrefs.GetInt(Common.CASH_IN_FEATURE, 0);
        Cash_Out_Feature = PlayerPrefs.GetInt(Common.CASH_OUT_FEATURE, 0);
    }

    public void getAutoReady()
    {
       if( PlayerPrefs.HasKey("AutoReadySetting"))
        {
            isAutoReady = PlayerPrefs.GetInt("AutoReadySetting") == 1 ? true : false;
        }
        autoReady.isOn = isAutoReady;
    }
    public void SetAutoReady(bool isReady)
    {
        isAutoReady = isReady;
        if(isAutoReady && IGUIM.instance != null && IGUIM.instance.gameManager != null)
        {
            if(!OGUIM.me.owner)
                IGUIM.instance.ReadyBtn_Click();
        }
        PlayerPrefs.SetInt("AutoReadySetting", isAutoReady ? 1 : 0);
        PlayerPrefs.Save();
    }
    public void Start()
    {
        //ShowInviteCheck ();
#if UNITY_WEBGL && !UNITY_EDITOR
		GetProviderCode ();
#endif
        FixedCanvasOrder(popupCanvas);
        if (toggleGold != null && toggleKoin != null)
        {
            toggleGold.GetComponent<UIToggle>().UpdateTextContent(GameBase.moneyGold.name);
            toggleKoin.GetComponent<UIToggle>().UpdateTextContent(GameBase.moneyKoin.name);
        }
        StartCoroutine(HttpRequest.Instance.GetAPIToken());

        listChatRoomData = new List<RootChatData>();
        listChatWorldData = new List<RootChatData>();
        gameLottery = goGameLottery;

    }

    private void Update()
    {
        if (timeRelease > 0)
        {
            timeRelease -= Time.deltaTime;
            if (timeRelease < 0)
            {
                timeRelease = 0f;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape) && popupMessenger.popupContent.state == UIAnimation.State.IsHide) //The listener for the 'Back' button event
        {
            GoBack();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            WarpRequest.GetSystemMessage(SystemMessageType.ADMIN_MESSAGE);
        }
    }

    #region HttpRequest
    private void AddHttpRequestListener()
    {
        HttpRequest.OnGetAPITokenDone += Http_OnGetAPITokenDone;
        HttpRequest.OnRegisterDone += Http_OnRegisterDone;
        HttpRequest.OnGetVersionConfigureDone += Http_OnGetVersionConfigureDone;
        HttpRequest.OnForgotPasswordDone += Http_OnForgotPasswordDone;
        HttpRequest.OnCheckLocationDone += Http_OnCheckLocationDone;
    }

    private void RemoveHttpRequestListener()
    {
        HttpRequest.OnGetAPITokenDone -= Http_OnGetAPITokenDone;
        HttpRequest.OnRegisterDone -= Http_OnRegisterDone;
        HttpRequest.OnGetVersionConfigureDone -= Http_OnGetVersionConfigureDone;
        HttpRequest.OnForgotPasswordDone -= Http_OnForgotPasswordDone;
        HttpRequest.OnCheckLocationDone -= Http_OnCheckLocationDone;
    }
    #endregion

    #region On HttpRequest Done
    private void Http_OnGetAPITokenDone(WarpResponseResultCode status, string apiToken)
    {
        if (status != WarpResponseResultCode.SUCCESS)
        {
            Toast.ShowNotification("Kết nối thất bại, kiểm tra kết nối internet");
            GotoHome();
        }

        //StartCoroutine(HttpRequest.Instance.GetVersionConfigureV2());
        // For Downgrade
        StartCoroutine(HttpRequest.Instance.GetVersionConfigure());
    }

    private void Http_OnGetVersionConfigureDone(WarpResponseResultCode status)
    {
#if UNITY_WEBGL
        GameBase.underReview = false;
        GameBase.needUpdateToPlay = false;
#endif

        //textVersion.text = GameBase.gameName.ToUpper() + " - " + GameBase.clientVersion + " - " + GameBase.providerCode;

        if (!string.IsNullOrEmpty(GameBase.hotline))
        {
            hotlineLabel.text = GameBase.hotline;
            hotlineButton.SetActive(true);
            if (hotlineButtonOnSettings != null)
                hotlineButtonOnSettings.SetActive(true);
        }

        if (!string.IsNullOrEmpty(GameBase.website))
        {
            if (websiteButtonOnSettings != null)
                websiteButtonOnSettings.SetActive(true);
        }

        if (!string.IsNullOrEmpty(GameBase.fbFanpage))
        {
            if (facebooKButtonOnSettings != null)
                facebooKButtonOnSettings.SetActive(true);
        }

        if (GameBase.needUpdateToPlay)
        {
            textNeedUpdate.text = "<size=42>THÔNG BÁO</size>" + "\n\n" + "Đã có phiên bản mới." + "\n" + "Cập nhật để tiếp tục chơi!";
            instance.girl.Show();
            needUpdatePanel.Show();
        }
        else
        {
            if (GameBase.newVersionAvaiable)
            {
                MessengerBox.Show("Thông báo", "Đã có phiên bản mới." + "\n" + "Cập nhật để trải nghiệp tính năng mới!",
                    "Cập nhật", () =>
                    {
                        UpdateApp();
                    },
                    "Lúc khác", null);
                GotoHome();
            }
            else if (instance.mainMenuCanvas.state != UIAnimation.State.IsShow)
                AutoLoginCheck();
            // Dont autologincheck if already
        }

        Toast.Hide();

#if !UNITY_WEBGL
        StartCoroutine(HttpRequest.Instance.CheckLocation());
#endif
    }

    private void Http_OnForgotPasswordDone(WarpResponseResultCode status, string data)
    {
        if (string.IsNullOrEmpty(data))
            data = "Hệ thống đang được nâng cấp, quay lại sau";
        MessengerBox.Show("Quên mật khẩu", data);

        Debug.Log("HTTPRequest_OnGetVersionConfigureDone: " + status);
    }

    private void Http_OnCheckLocationDone(WarpResponseResultCode status, bool result)
    {
#if UNITY_ANDROID || UNITY_IOS
        if (GameBase.underReview)
        {
            int index = 0;
            foreach (var i in underReviewList)
            {
                if (index != 2)
                    i.SetActive(false);
                index++;
            }
        }
#endif

        if (GameBase.isLiteVersion)
        {
            foreach (var i in liteList)
                i.SetActive(false);
        }

    }
    #endregion

    #region WEB_GL
    public void HiddenWebGLPart()
    {
#if UNITY_WEBGL
        foreach (var i in webglList)
            i.SetActive(false);
#endif
    }
    #endregion

    #region GIFT
    public void SetGiftButton()
    {
#if UNITY_WEBGL
        return;
#endif
        if (giftButton != null)
            if ((me.isLikeReward || (string.IsNullOrEmpty(me.faceBookId)) && me.isRateReward))
                giftButton.gameObject.SetActive(false);
            else
                giftButton.gameObject.SetActive(true);

    }
    #endregion


    private void AddWarpRequestListener()
    {
        #region Login Logout
        WarpClient.wc.OnLoginDone += Wc_OnLoginDone;
        WarpClient.wc.OnLogoutDone += Wc_OnLogoutDone;
        #endregion

        #region AddListener SubGame -> JoinLobby -> SubLobby -> (SLOT = GetRooms) -> JoinRoom -> Goto Scene
        WarpClient.wc.OnSubGameDone += OnSubGameDone;
        WarpClient.wc.OnJoinLobbyDone += OnJoinLobbyDone;
        WarpClient.wc.OnSubLobbyDone += OnSubLobbyDone;
        WarpClient.wc.OnJoinRoomDone += OnJoinRoomDone;

        WarpClient.wc.OnGetRoomsDone += OnGetRoomsDone;
        WarpClient.wc.OnActionsInLobbyDone += OnActionsInLobbyDone;
        #endregion

        #region AddListener LeaveRoom -> UnSubLobby -> LeaveLobby -> UnSubGame -> Goto
        WarpClient.wc.OnLeaveRoom += OnLeaveRoom;
        WarpClient.wc.OnUnSubLobbyDone += OnUnSubLobbyDone;
        WarpClient.wc.OnLeaveLobbyDone += OnLeaveLobbyDone;
        WarpClient.wc.OnUnSubGameDone += OnUnsubGameDone;
        #endregion

        #region Chat
        WarpClient.wc.OnChat += Wc_OnChat;
        WarpClient.wc.OnWorldChat += Wc_OnWorldChat;
        #endregion

        #region UserInfo UserStat UpdatedMoney GeneralMessageNotification CashOut OnGetMessages
        WarpClient.wc.OnGetUserInfoDone += Wc_OnGetUserInfoDone;
        WarpClient.wc.OnGetUserStatDone += Wc_OnGetUserStatDone;
        WarpClient.wc.OnUpdatedMoneyDone += Wc_OnUpdatedMoneyDone;
        WarpClient.wc.OnGeneralMessageNotificationDone += Wc_OnGeneralMessageNotificationDone;
        WarpClient.wc.OnGetSystemMessagesDone += Wc_OnGetSystemMessagesDone;
        WarpClient.wc.OnGetMessagesDone += Wc_OnGetMessagesDone;
        WarpClient.wc.OnUpdateMobileDone += Wc_OnUpdateMobileDone;
        WarpClient.wc.OnCashoutNotifyDone += Wc_OnCashoutNotifyDone;
        #endregion

        #region IAP
        WarpClient.wc.OnIAPTopupDone += Wc_OnIAPTopupDone;
        #endregion

        #region PromoConfig
        WarpClient.wc.OnGetPromotionConfigureDone += Wc_OnGetPromoConfigDone;
        #endregion

        #region Gift
        WarpClient.wc.OnGetConfigDone += Wc_OnGetConfigDone;
        WarpClient.wc.OnGetLinkPolicyDone += Wc_OnGetLinkPolicyDone;
        #endregion
    }

    private void Wc_OnCashoutNotifyDone(CashOutNotify data)
    {
        if (data.status == 1)
        {
            instance.popupMessenger.Show("Thông Báo", "Yêu cầu đổi thưởng của bạn đã được duyệt.Xem chi tiết tại hòm thư !");
        }
        else if (data.status == 0)
        {
            instance.popupMessenger.Show("Thông Báo", "Yêu cầu đổi thưởng của bạn không thành công.Hãy thử lại !");
        }

        OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, data.gold);
    }

    private void Wc_OnGetLinkPolicyDone(string msg)
    {
        Application.OpenURL(msg);
    }

    #region ShowInvite Check
    public void ToggleShowInvite()
    {
        if (inviteToggleInMesBox != null && inviteToggleInSettings != null)
        {
            if (MessengerBox.popupContent.gameObject.activeSelf)
            {
                PlayerPrefs.SetInt("showInvite", inviteToggleInMesBox.isOn ? 1 : 0);
            }
            else
            {
                PlayerPrefs.SetInt("showInvite", inviteToggleInSettings.isOn ? 1 : 0);
            }
            PlayerPrefs.Save();

            ShowInviteCheck();
        }
    }

    public static void ShowInviteCheck()
    {
        if (instance.inviteToggleInMesBox != null)
        {
            instance.inviteToggleInMesBox.isOn = PlayerPrefs.GetInt("showInvite", 1) == 1 ? true : false;
            instance.inviteToggleInSettings.isOn = PlayerPrefs.GetInt("showInvite", 1) == 1 ? true : false;
        }
    }
    #endregion

    #region AutoLogin QuickLogin Login Register Facebook Logout MoneyChange Event

    #region AutoLogin
    private void AutoLoginCheck()
    {
        int autoLoginState = PlayerPrefs.GetInt("autoLogin", 1);
        if (autoLoginState == 1)
        {
            autoLoginToggle.isOn = true;

            loginType = (LoginType)PlayerPrefs.GetInt("loginType", 0);

            if (loginType == LoginType.USER_PASS)
            {
                me.username = PlayerPrefs.GetString("username", "");
                me.password = PlayerPrefs.GetString("password", "");
                //Debug.LogError("INFO : name: " + me.username + ", pass: " + me.password);

                if (!string.IsNullOrEmpty(me.username) && !string.IsNullOrEmpty(me.password))
                {
                    autoLoginGame = true;
                    WarpClient.wc.ConnectedToServer(false, () => WarpRequest.Login(LoginType.USER_PASS));
                }
                else
                    GotoHome();
            }
            else if (loginType == LoginType.FACEBOOK)
            {
                me.faceBookId = PlayerPrefs.GetString("faceBookId", "");
                me.facebookAccessToken = PlayerPrefs.GetString("facebookAccessToken", "");
                if (!string.IsNullOrEmpty(me.faceBookId) && !string.IsNullOrEmpty(me.facebookAccessToken))
                {
                    autoLoginGame = true;
                    WarpClient.wc.ConnectedToServer(false, () => WarpRequest.Login(LoginType.FACEBOOK));
                }
                else
                    GotoHome();
            }
            else
            {
                GotoHome();
            }
        }
        else
        {
            autoLoginToggle.isOn = false;
            GotoHome();
        }
    }

    public void ToggleAutoLogin()
    {
        int autoLogin = -1;

        if (autoLoginToggle.isOn)
        {
            autoLogin = 1;
        }
        else
        {
            autoLogin = 0;
        }

        PlayerPrefs.SetInt("autoLogin", autoLogin); //We set the new value in the PlayerPrefs
        PlayerPrefs.Save(); //We save the value
    }
    #endregion

    #region Facebook
    public void LoginWithFB()
    {
        //Toast.ShowLoading ("Đang đăng nhập facebook");
        LoginWithFB(() =>
            {
                PlayerPrefs.SetString("faceBookId", me.faceBookId);
                PlayerPrefs.SetString("facebookAccessToken", me.facebookAccessToken);
                PlayerPrefs.SetInt("loginType", LoginType.FACEBOOK.ToInt());

                PlayerPrefs.Save();
                loginType = LoginType.FACEBOOK;
                WarpClient.wc.ConnectedToServer(false, () => WarpRequest.Login(LoginType.FACEBOOK));
            });
    }

    public void LoginWithFB(Action onFacebookLoginDone)
    {
        actionOnFacebookLoginDone = onFacebookLoginDone;
        InitFB();
    }

    private void InitFB()
    {
        if (!Facebook.Unity.FB.IsInitialized)
        {
            UILogView.Log("Initialize the Facebook SDK");
            Facebook.Unity.FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            UILogView.Log("Already initialized, signal an app activation App Event");
            Facebook.Unity.FB.ActivateApp();
            LoginFB();
        }
    }

    private void InitCallback()
    {
        if (Facebook.Unity.FB.IsInitialized)
        {
            UILogView.Log("Signal an app activation App Event");
            // Signal an app activation App Event
            Facebook.Unity.FB.ActivateApp();
            // Continue with Facebook SDK
            // ...

            LoginFB();
        }
        else
        {
            UILogView.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    public void LoginFB()
    {

        var perms = new List<string>() { "public_profile", "email" };
        Facebook.Unity.FB.LogInWithReadPermissions(perms, LoginCallback);
    }


    private void InitShare()
    {
        if (Facebook.Unity.FB.IsInitialized)
        {
            Facebook.Unity.FB.ActivateApp();

            var perms = new List<string>() { "publish_actions" };
            Facebook.Unity.FB.LogInWithPublishPermissions(perms, ShareLoginCallback);
            Debug.Log("Login to : " + ScreenShotHandler.screenShotFileName);
        }
        else
        {
            UILogView.Log("Failed to init fb share");
        }
    }

    private void ShareLoginCallback(Facebook.Unity.ILoginResult result)
    {
        if (Facebook.Unity.FB.IsLoggedIn)
        {
            byte[] image = System.IO.File.ReadAllBytes(ScreenShotHandler.screenShotFileName);
            WWWForm wWForm = new WWWForm();
            wWForm.AddBinaryData("image", image, "ScreenShot.png");
            Facebook.Unity.FB.API("me/photos", Facebook.Unity.HttpMethod.POST, ShareCallBack, wWForm);

        }
    }

    private void ShareCallBack(IGraphResult result)
    {
        Debug.Log("result callback : " + result.RawResult);
    }


    private void ShareLinkCallback(Facebook.Unity.IShareResult result)
    {
        if (result.Cancelled)
        {

        }
        else if (!string.IsNullOrEmpty(result.Error))
        {

        }
        else
        {
            Toast.Show("Chia sẻ thành công");
        }
    }
    private void LoginCallback(Facebook.Unity.ILoginResult result)
    {
        if (Facebook.Unity.FB.IsLoggedIn)
        {
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;

            // Print current access token's granted permissions

            string permLog = "";
            foreach (string perm in aToken.Permissions)
            {
                permLog += perm + ",";
            }
            UILogView.Log("LoginCallback: " + permLog);
            UILogView.Log("LoginCallback: " + aToken.UserId);

            me.faceBookId = aToken.UserId;
            me.facebookAccessToken = aToken.TokenString;

            if (actionOnFacebookLoginDone != null)
                actionOnFacebookLoginDone();
        }
        else
        {
            Toast.Hide();
            if (result == null)
                UILogView.Log("Facebook LoginCallback: " + result.Error, true);
            if (!string.IsNullOrEmpty(result.Error))
                UILogView.Log("Facebook LoginCallback: " + result.Error, true);
            else if (result.Cancelled)
                UILogView.Log("Facebook LoginCallback: User cancelled login", true);
        }
    }


    #endregion

    #region ChangeMoneyType
    public void ChangeMoneyType(int type)
    {
        if (type == (int)MoneyType.Gold)
        {
            toggleGold.isOn = true;
            toggleKoin.isOn = false;
        }
        //else
        //{
        //    if (currentLobby != null && currentLobby.onlygold)
        //    {
        //        Toast.ShowNotification(currentLobby.desc + " chỉ hỗ trợ chơi " + GameBase.moneyGold.name);
        //    }
        //    else
        //    {
        //        toggleGold.isOn = false;
        //        toggleKoin.isOn = true;
        //    }
        //}
    }

    public void ToggleChangeMoneyType(int type)
    {
        if (type == (int)MoneyType.Gold)
        {
            currentMoney = GameBase.moneyGold;
        }
        else
        {
            currentMoney = GameBase.moneyKoin;
        }

        meView.moneyView.FillData(me);

        PlayerPrefs.SetInt("currentMoney", (int)(currentMoney.type));
        PlayerPrefs.Save();
    }
    #endregion

    public void QuickLogin()
    {
        me = new UserData();
        PlayerPrefs.SetInt("loginType", LoginType.QUICK_LOGIN.ToInt());
        PlayerPrefs.Save();
        loginType = LoginType.QUICK_LOGIN;
        WarpClient.wc.ConnectedToServer(false, () => WarpRequest.Login(LoginType.QUICK_LOGIN));
    }

    public void Login(string _username, string _password)
    {
        me = new UserData { username = _username, password = _password };

        PlayerPrefs.SetString("username", me.username);
        PlayerPrefs.SetString("password", me.password);
        PlayerPrefs.SetInt("loginType", LoginType.USER_PASS.ToInt());
        PlayerPrefs.Save();
        loginType = LoginType.USER_PASS;
        Debug.Log("me Data : " + me.username + me.password);
        WarpClient.wc.ConnectedToServer(false, () => WarpRequest.Login(LoginType.USER_PASS));
    }

    public void Regiter(string _fullname, string _username, string _password, string _phoneNumber)
    {
        StartCoroutine(HttpRequest.Instance.Register(_fullname, _username, _password, _phoneNumber));
    }
    #endregion

    #region OnLoginDone OnGetUserStatDone OnLogoutDone OnRegisterDone
    private void Wc_OnLoginDone(WarpResponseResultCode resultCode, RootUserData data, string desc)
    {
        //loadingFade.destroyLoading();
        string annoucetext = "";
        if (resultCode == WarpResponseResultCode.SUCCESS)
        {
            me = data.desc;
            isVerified = me.verified;
            if(me.mobile.Length > 3)
            {
                isVerified = 1;
            }
            var lastMoney = PlayerPrefs.GetInt("currentMoney", 1);

            meView.FillData(data.desc);

            ChangeMoneyType(lastMoney);
            handleConfigLobby();
            SetGiftButton();
           
            DOVirtual.DelayedCall(0.5f, () =>
            {
                instance.popupEvents.GetAllMes();
            });
        }
        else if (resultCode == WarpResponseResultCode.AUTH_ERROR)
        {
            if (!string.IsNullOrEmpty(desc))
                UILogView.Log(resultCode + "\n" + desc, true);

            annoucetext = "Tài khoản/mật khẩu không chính xác";
        }
        else if (resultCode == WarpResponseResultCode.NOT_INTERNET_CONNECTION)
        {
            annoucetext = "Kết nối thất bại. Vui lòng thử lại hoặc kiểm tra kết nối mạng";
        }
        else if (resultCode == WarpResponseResultCode.USER_IS_LOCK)
        {
            annoucetext = "Tài khoản đang bị khóa. Liên hệ CSKH để được giải đáp";
        }
        else if (resultCode == WarpResponseResultCode.LOGIN_BY_OTHER_DEVICE)
        {
            annoucetext = "Tài khoản của bạn hiện đang đăng nhập trên thiết bị khác,Liên hệ hotline/fanpage nếu cần hỗ trợ.";
        }
        else
        {
            annoucetext = "Kết nối thất bại, vui lòng thử lại.";
        }

        if (resultCode == WarpResponseResultCode.SUCCESS)
        {
            PlayerPrefs.SetInt("autoLogin", instance.autoLoginToggle.isOn ? 1 : 0);
            PlayerPrefs.SetInt("loginType", (int)loginType);
            

            if(loginType == LoginType.USER_PASS)
            {
                if (data.desc.username.Equals(data.desc.displayName) && PlayerPrefs.GetInt(data.desc.username, 0) < 1)
                {
                    PlayerPrefs.SetInt(data.desc.username, 1);
                    popupDisplayName.Show(data.desc);
                    isShowPopupChangePhone = true;
                }

            }else if(loginType == LoginType.FACEBOOK)
            {
                if (data.desc.displayName.Contains(data.desc.username) && PlayerPrefs.GetInt(data.desc.username, 0) < 1)
                {
                    PlayerPrefs.SetInt(data.desc.username, 1);
                    popupDisplayName.Show(data.desc);
                    isShowPopupChangePhone = true;
                }
            }else if(loginType == LoginType.QUICK_LOGIN)
            {
                if (data.desc.displayName.Contains("player_" + data.desc.id) || data.desc.displayName.Contains("Player_" + data.desc.id))
                {
                    
                    popupDisplayName.Show(data.desc);
                    isShowPopupChangePhone = true;
                }
            }

            if (string.IsNullOrEmpty(data.desc.mobile))
            {
                DOVirtual.DelayedCall(2f, () =>
                {
                    if (popupVerifyPhone != null)
                        popupVerifyPhone.Show();
                });
            }

            PlayerPrefs.Save();

            if (WarpClient.wc.SessionId == 0 || homeCanvas.state == UIAnimation.State.IsShow)
            {
                WarpClient.wc.SessionId = data.sessionid;

                //WarpRequest.GetUserStat(me.id);
                //WarpRequest.GetPromotionConfigure();
                //giftButton.gameObject.SetActive (false);
                //WarpRequest.GetConfig (ConfigType.LIKE_RATE_REWARD);

                LoadLastLobby();

                GoToLobies();
            }
            else
            {
                WarpClient.wc.SessionId = data.sessionid;
                //reconnect
                if (OGUIM.currentRoom != null)
                {
                    BuildWarpHelper.GetRoomInfo(OGUIM.currentRoom, () =>
                    {
                        UILogView.Log("GetRoomInfo is time out.");
                    });
                }
            }
        }
        else
        {
            OGUIM.MessengerBox.Show("Thông báo", annoucetext, "Đồng ý");
            ////Toast.Show(annoucetext);
            if (autoLoginGame)
            {
                UnLoadGameScene(true);
                autoLoginGame = false;
            }
        }


    }

    private void handleConfigLobby()
    {
        if (GameBase.cash_in == (int)ConfigStatus.CLOSE)
            objCashIn.SetActive(false);
        else
            objCashIn.SetActive(true);

        if (GameBase.cash_out == (int)ConfigStatus.CLOSE)
            objCashOut.SetActive(false);
        else
            objCashOut.SetActive(true);

        if (GameBase.phone_call == (int)ConfigStatus.CLOSE)
            objPhoneCall.SetActive(false);
        else
            objPhoneCall.SetActive(true);

        if (GameBase.facebook == (int)ConfigStatus.CLOSE)
            objFacebook.SetActive(false);
        else
            objFacebook.SetActive(true);

        if (GameBase.top == (int)ConfigStatus.CLOSE)
            objTopRank.SetActive(false);
        else
            objTopRank.SetActive(true);

        if (GameBase.gifcode == (int)ConfigStatus.CLOSE)
            objGifcode.SetActive(false);
        else
            objGifcode.SetActive(true);

        if (GameBase.quest == (int)ConfigStatus.CLOSE)
            objQuest.SetActive(false);
        else
            objQuest.SetActive(true);

        if (GameBase.events == (int)ConfigStatus.CLOSE)
            objEvents.SetActive(false);
        else
            objEvents.SetActive(true);

        if (GameBase.inbox == (int)ConfigStatus.CLOSE)
            objInbox.SetActive(false);
        else
            objInbox.SetActive(true);

        if (GameBase.headLineFeature == (int)ConfigStatus.CLOSE)
        {
            if(headLine != null)
                headLine.SetActive(false);
            if (headLineRoom != null)
                headLineRoom.SetActive(false);
            if (headLineInGame != null)
                headLineInGame.SetActive(false);
        }
        else
        {
            if (headLine != null)
                headLine.SetActive(true);
            if (headLineRoom != null)
                headLineRoom.SetActive(true);
            if (headLineInGame != null)
                headLineInGame.SetActive(true);
        }
    }

    public void ShowCashOut()
    {
        if(GameBase.underReview)
        {
            return;
        }
        OGUIM.instance.Cash_In_Feature = PlayerPrefs.GetInt(Common.CASH_OUT_FEATURE, 0);
        if (OGUIM.instance.Cash_Out_Feature == 0)
        {
            OGUIM.instance.authPopup.Show(AuthenType.CASH_OUT);
            return;
        }
        popupCashOut.Show(0);
    }

    public void SendReconnectInfomation()
    {
        if (OGUIM.currentRoom != null)
        {
            BuildWarpHelper.GetRoomInfo(OGUIM.currentRoom, () =>
            {
                UILogView.Log("GetRoomInfo is time out.");
            });
        }
    }

    private void Wc_OnGetUserStatDone(WarpResponseResultCode status, RootStat data)
    {
        if (status == WarpResponseResultCode.SUCCESS && data != null && data.data != null)
        {
            if (isListen)
            {
                //Check favorite Lobby
                var temp = new List<Lobby>();
                foreach (var i in data.data)
                {
                    i.play = i.loss + i.win + i.draw;
                    temp.Add(new Lobby { id = i.zoneId, play = i.play });
                }

                if (LobbyViewListView.listFavoriteData != null)
                    LobbyViewListView.listFavoriteData = temp;

                LoadLastLobby();

                GoToLobies();
            }
        }
        Toast.Hide();
    }

    private void Wc_OnLogoutDone(WarpResponseResultCode status)
    {
        if (OldWarpChannel.Channel != null)
        {
            OldWarpChannel.Channel.StopAllCoroutines();
            OldWarpChannel.Channel.socket_close();
        }
        WarpClient.wc.SessionId = 0;
        isTheFirst = true;
        PopupAllMes.listAdmin = new List<Message>();
        PopupAllMes.listPromo = new List<Message>();
        PopupAllMes.listUser = new List<Message>();
        PopupAllMes.listClaim = new List<Message>();

        GotoHome();
    }

    private void Http_OnRegisterDone(WarpResponseResultCode status, UserData data)
    {
        Toast.Hide();
        if (status == WarpResponseResultCode.SUCCESS)
        {
            me = data;
            OGUIM.loginType = LoginType.USER_PASS;
            WarpClient.wc.ConnectedToServer(false, () => WarpRequest.Login(LoginType.USER_PASS));
        }
        else if (status == WarpResponseResultCode.REG_USER_EXIST)
        {
            OGUIM.MessengerBox.Show("Thông báo","Không thành công. Tài khoản này đã tồn tại.", "Đồng ý");
        }
        else if (status == WarpResponseResultCode.INVALID_USERNAME)
        {
            OGUIM.MessengerBox.Show("Thông báo", "Không thành công. Tài khoản không được chứa ký tự đặc biệt", "Đồng ý");
        }
        else if (status == WarpResponseResultCode.INVALID_MAX_USER_PER_DEVICE)
        {
            OGUIM.MessengerBox.Show("Thông báo", "Không thành công. Thiết bị đã đăng kí quá nhiều tài khoản", "Đồng ý");
        }
        else
        {
            OGUIM.MessengerBox.Show("Thông báo", "Đăng ký thất bại. Liên hệ CSKH để được hỗ trợ ", "Đồng ý");
        }
        
    }
    #endregion

    #region OnChat
    private void Wc_OnWorldChat(RootWorldChat data)
    {
        if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null && OGUIM.currentRoom.room.id != 0)
            return;
        var rootChatData = new RootChatData { userName = data.user.displayName, userId = data.user.id, message = data.message };
        listChatWorldData.Add(rootChatData);
        popupChatView.FillData(rootChatData);
    }

    private void Wc_OnChat(RootChatData rootChatData)
    {
        Debug.Log("onRevice chat message :" + JsonUtility.ToJson(rootChatData));
        if (rootChatData != null)
        {
            Debug.Log("size chat :" + listChatRoomData.Count);
            listChatRoomData.Add(rootChatData);
            popupChatView.FillData(rootChatData);
            if (IGUIM.instance != null)
            {
                var players = IGUIM.GetPlayersOnBoard();
                if (players.ContainsKey(rootChatData.userId))
                    players[rootChatData.userId].chatView.FillData(rootChatData);

                Debug.Log("size chat 2 :" + listChatRoomData.Count);
            }
            if (IGUIM_Casino.instance != null)
            {
                var players = IGUIM_Casino.GetPlayersOnBoard();
                if (players.ContainsKey(rootChatData.userId))
                    players[rootChatData.userId].chatView.FillData(rootChatData);
            }

            Debug.Log("onRevice chat emotion name :" + rootChatData.icon);
        }
    }
    #endregion

    #region OnGetUserInfoDone OnGetAllMessDone
    private void Wc_OnGetUserInfoDone(WarpResponseResultCode status, RootUserInfo data)
    {
        if (status == WarpResponseResultCode.SUCCESS && data != null && data.user.id == me.id)
        {
            instance.meView.FillData(data.user);
        }
    }

    public void Wc_OnGetMessagesDone(WarpResponseResultCode status, List<Message> data)
    {
        // Filter Claimable Xu for Lite Version
        if (GameBase.isLiteVersion)
        {
            var _data = new List<Message>();
            foreach (Message item in data)
            {
                string compare = item.content.ToLower();
                if (item.type != (int)MesType.CLAIMABLE || (!compare.Contains("xu")))
                {
                    _data.Add(item);
                }
            }
            data = _data;
        }
        if (popupInbox != null)
            popupInbox.Wc_OnGetMessagesDone(status, data);
        if (popupEvents != null)
            popupEvents.Wc_OnGetMessagesDone(status, data);
    }

    public void Wc_OnGetSystemMessagesDone(WarpResponseResultCode status, List<Message> data)
    {
        if (popupInbox != null)
            popupInbox.Wc_OnGetSystemMessagesDone(status, data);
        if (popupEvents != null)
            popupEvents.Wc_OnGetSystemMessagesDone(status, data);
    }
    #endregion

    #region Goto Play Game
    public void SubLobby(Lobby data)
    {
        //SubGame -> JoinLobby -> SubLobby
        if (currentLobby == null || currentLobby.id == (int)LobbyId.NONE)
        {
            Debug.Log("Lobby game id :" + data.gameId);
            //Toast.ShowLoading("Đang tiến hành vào " + "\"" + data.desc + " " + data.subname + "\"...");
            Toast.ShowLoading("");
            nextLobby = null;
            currentLobby = data;
            WarpRequest.SubGame(data.gameId);
            SaveLastLobby();
        }
        else if (currentLobby.id != data.id)
        {
            UnSubLobby(data);
        }
        else if (currentLobby.id == data.id)
        {
            GoToRooms();
        }
    }

    public void UnSubLobby(Lobby _nextLobby)
    {
        //UnSubLobby -> LeaveLobby -> UnSubGame
        nextLobby = _nextLobby;
        //Toast.ShowLoading("Đang tiến hành rời " + "\"" + currentLobby.desc + " " + currentLobby.subname + "\"...");

        WarpRequest.LeaveLobby(currentLobby.id);
    }

    public void LoadLastLobby()
    {
        int lastLobbyId = PlayerPrefs.GetInt("lastLobbyId", 0);
        if (lastLobbyId != 0 && LobbyViewListView.listFavoriteData != null)
        {
            var lastLobby = LobbyViewListView.listFavoriteData.FirstOrDefault(x => x.id == lastLobbyId);
            if (lastLobby != null)
                lastLobby.play = 999999999;
        }
    }

    public void SaveLastLobby()
    {
        if (currentLobby != null)
            PlayerPrefs.SetInt("lastLobbyId", OGUIM.currentLobby.id);
        PlayerPrefs.Save();
    }
    #endregion

    #region OnSubGameDone -> OnJoinLobbyDone -> OnSubLobbyDone -> (SLOT = OnGetRoomsDone) -> OnJoinRoomDone
    private void OnSubGameDone(WarpResponseResultCode status)
    {

        if (status == WarpResponseResultCode.SUCCESS && currentLobby != null)
        {
            Debug.Log("Join lobby dataa : " + currentLobby.id);
            Toast.ShowLoading("");
            WarpRequest.JoinLobby(currentLobby.id);
        }
    }

    private void OnJoinLobbyDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS && string.IsNullOrEmpty(currentGameScene))
        {
            currentLobbyId = (LobbyId)currentLobby.id;
            if (currentLobby.playmode == (int)PlayMode.QUICK)
            {

                WarpRequest.SubLobby(currentLobby.id);
            }
            else
            {
                if (mainRoomsCanvas.state == UIAnimation.State.IsHide)
                    GoToRooms();
                else
                {

                    WarpRequest.SubLobby(currentLobby.id);
                }
            }
        }
    }

    private void OnSubLobbyDone(WarpResponseResultCode status, RootRoom data)
    {
        if (status == WarpResponseResultCode.SUCCESS && currentLobby != null)
        {

            Debug.Log("OnSubLobbyDone: on OGUIM: " + (LobbyId)currentLobby.id + " " + currentLobby.lobbymode);
            if (currentLobby.playmode == (int)PlayMode.QUICK)
            {
                LobbyView.QuickJoinRoom(data);
            }
        }
    }

    private void OnGetRoomsDone(WarpResponseResultCode status, List<RoomSlot> data)
    {
        if (status == WarpResponseResultCode.SUCCESS && currentLobbyId == LobbyId.NONE && string.IsNullOrEmpty(instance.currentGameScene))
        {
            if (currentLobby.playmode == (int)PlayMode.QUICK)
            {
                if (currentLobby.id == (int)LobbyId.SLOT || currentLobby.id == (int)LobbyId.SLOT2 || currentLobby.id == (int)LobbyId.SLOT3)
                {
                    var checkRoom = data.OrderBy(x => x.bet).FirstOrDefault();
                    if (checkRoom != null)
                    {

                        WarpRequest.JoinRoom(currentLobby.onlygold ? GameBase.moneyGold.type : currentMoney.type, checkRoom.bet);
                        Debug.Log("Current Money : " + currentMoney.type);
                    }
                }
            }
        }
    }

    private void OnJoinRoomDone(WarpResponseResultCode status, Room data)
    {
        
        if (isListen)
        {
            if (status == WarpResponseResultCode.SUCCESS && string.IsNullOrEmpty(instance.currentGameScene))
            {
                //popupChatView.chatMode = ChatMode.Room;
                //popupChatView.chatTitle.text = "CHAT BÀN";
                //popupChatView.FillData(new List<RootChatData>());
                instance.lobbyName.text = "";
                currentRoom = data;
                autoLeaveRoom = false;
                if (OGUIM.currentLobby.id == (int)LobbyId.PHOM || (LobbyId)OGUIM.currentLobby.id == LobbyId.POKER)
                {
                    Backgound.instance.setBackground("game_bg");
                    //instance.setBackground("table1");
                }
                if ((LobbyId)OGUIM.currentLobby.id == LobbyId.ROULETTE)
                {
                    Backgound.instance.setBackground("background_2");
                }
                // Hide invite popup when join room success
                OGUIM.MessengerBox.Hide();
                GoToIngame();


            }
            else if (status == WarpResponseResultCode.INVALID_CHIP)
            {
                // When invalid chip --> dont have room and dont have chipType too               
                Toast.Show("Cần thêm " + OGUIM.currentMoney.name + " để trải nghiệm", UIToast.ToastType.Warning);
            }
            else
            {
                Toast.Show("Vào phòng thất bại", UIToast.ToastType.Warning);
                BackToLobies();
            }
        }
    }
    #endregion

    #region OnLeaveRoom -> OnLeaveLobbyDone -> OnUnSubLobbyDone -> OnUnsubGameDone
    private void OnLeaveRoom(Room room, int status)
    {
        Debug.Log("On Leave Room : " + status);
        if (room != null && room.userId == me.id)
        {
            if (status == (int)WarpResponseResultCode.ROOM_STARTED)
            {
                if (!autoLeaveRoom)
                {
                    Toast.ShowNotification("Đăng ký rời phòng!");
                }
                else
                {
                    Toast.ShowNotification("Hủy rời phòng!");
                }
                autoLeaveRoom = !autoLeaveRoom;
            }
            else if (status == (int)WarpResponseResultCode.SUCCESS || status == (int)WarpResponseResultCode.BAD_REQUEST)
            {
                Debug.Log(status);
                instance.lobbyName.text = "";
                instance.roomBetValue.text = "";
                if (isListen)
                {
                    //popupChatView.chatMode = ChatMode.World;
                    //popupChatView.chatTitle.text = "CHAT CHUNG";
                    Backgound.instance.setBackground("");
                    popupChatView.FillData(listChatRoomData);
                    UnLoadGameScene();
                    Toast.Hide();
                }
            }
        }
    }

    public void CasinoLeaveRoom()
    {
        //Debug.Log(status);
        instance.lobbyName.text = "";
        instance.roomBetValue.text = "";
        if (isListen)
        {
            //popupChatView.chatMode = ChatMode.World;
            //popupChatView.chatTitle.text = "CHAT CHUNG";
            Backgound.instance.setBackground("");
            popupChatView.FillData(listChatRoomData);
            UnLoadGameScene();
            Toast.Hide();
        }
    }

    private void OnLeaveLobbyDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            if (currentLobby.id != (int)LobbyId.NONE)
            {

                WarpRequest.UnSubLobby(currentLobby.id);
            }
        }
        else
        {
            BackToLobies();
        }
    }

    private void OnUnSubLobbyDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            var gameId = currentLobby.gameId;
            currentLobby = null;
            currentLobbyId = LobbyId.NONE;
            WarpRequest.UnSubGame(gameId);
        }
        else
        {
            BackToLobies();
        }
    }

    private void OnUnsubGameDone(WarpResponseResultCode status)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            if (nextLobby == null)
            {
                Toast.Hide();
                BackToLobies();
            }
            else
            {
                SubLobby(nextLobby);
            }
        }
        else
        {
            BackToLobies();
        }
    }

    private void OnActionsInLobbyDone(SlotJacpot data)
    {
        if (data != null)
        {
            if (!listRoomSlot.Any(x => x.id == data.roomId))
                listRoomSlot.Add(new RoomSlot { id = data.roomId, funds = data.jackpot });

            var checkJackpot = listRoomSlot.FirstOrDefault(x => x.id == data.roomId);
            if (checkJackpot != null)
                checkJackpot.funds = data.jackpot;

            if (LobbyViewListView.listView != null && listRoomSlot.Any())
            {
                var checkLobbyView = LobbyViewListView.listView.FirstOrDefault(x => x.lobbyData.id == (int)LobbyId.SLOT);
                if (checkLobbyView != null)
                    checkLobbyView.lobbyData.tables = listRoomSlot.OrderByDescending(x => x.funds).FirstOrDefault().funds;
            }
        }
    }
    #endregion

    #region OnActionsInLobbyDone OnUpdatedMoneyDone OnCashOutDone
    private void Wc_OnUpdateMobileDone(UpdatedMobileData data)
    {
        if (data != null)
        {
            me.mobile = data.number;
            //Close Popup
            if (popupVerifyPhone != null)
                popupVerifyPhone.Hide();
            #if !UNITY_WEBGL
                        //close webview
                        if (webView == null)
                        {
                            UILogView.Log("webview is null");
                            return;
                        }
                        if (webView.gameObject.activeInHierarchy)
                            webView.gameObject.SetActive(false);
                        webView.Hide();
                        webView.Stop();
            #endif

        }
    }

    private void Wc_OnUpdatedMoneyDone(UpdatedMoneyData data)
    {
        if (data != null)
        {

            string chipName = "";
            if (data.chipType == (int)GameBase.moneyGold.type)
            {
                me.gold = data.total;
                chipName = GameBase.moneyGold.name;
            }
            else
            {
                me.koin = data.total;
                chipName = GameBase.moneyKoin.name;
            }

            string textContent = "";
            string changeMoney = LongConverter.ToFull(data.change);

            if (data.change > 0)
            {
                if (data.type == (int)UpdateMoneyTypeCode.VIDEO_ADS)
                    textContent = "Xem video thành công, nhận thưởng " + changeMoney + " " + chipName;
                else if (data.type == (int)UpdateMoneyTypeCode.INVITE_FB)
                    textContent = "Mời bạn thành công, nhận thưởng " + changeMoney + chipName;
                else if (data.type == (int)UpdateMoneyTypeCode.SMS)
                    textContent = "Nạp thành công " + changeMoney + " " + chipName + " từ giao dịch SMS.";
                else if (data.type == (int)UpdateMoneyTypeCode.CARD)
                    textContent = "Nạp thành công " + changeMoney + " " + chipName + " từ giao dịch thẻ cào.";
                else if (data.type == (int)UpdateMoneyTypeCode.IAP)
                    textContent = "Nạp thành công " + changeMoney + " " + chipName + " từ giao dịch IAP.";
                else if (data.type == (int)UpdateMoneyTypeCode.ADMIN)
                    textContent = "Tài khoản nhận " + changeMoney + " " + chipName + " từ hệ thống.";
                else if (data.type == (int)UpdateMoneyTypeCode.TRANSFER_GOLD)
                    textContent = "Tài khoản nhận " + changeMoney + " " + chipName + " từ giao dịch chuyển Gold.";
                else if (data.type == (int)UpdateMoneyTypeCode.IGAME_CHARGING)
                    textContent = "Nạp thành công " + changeMoney + " " + chipName + " từ cổng thanh toán.";
                else if (data.type == (int)UpdateMoneyTypeCode.BY_GIFT_CODE)
                    textContent = "Nhận thưởng thành công " + changeMoney + " " + chipName + " từ Giftcode!";
                else if (data.type == (int)UpdateMoneyTypeCode.BY_LIKE_RATE)
                    textContent = "Nhận thưởng thành công " + changeMoney + " " + chipName;
                else if (data.type == (int)UpdateMoneyTypeCode.BY_UPDATE_MOBILE)
                    textContent = "Nhận thưởng " + changeMoney + " " + chipName +"cho cập nhận sdt thành công";
                else
                    textContent = "Nạp thành công " + changeMoney + " " + chipName;
            }
            else if (data.change < 0)
                textContent = "Tài khoản thay đổi " + changeMoney + " " + chipName;

            Toast.ShowNotification(textContent);
            MessengerBox.Show("Thông báo", textContent);


            if (meView != null)
                meView.moneyView.FillData((MoneyType)data.chipType, data.total);
            if (IGUIM.instance != null && IGUIM.instance.currentUser != null)
                IGUIM.instance.currentUser.avatarView.moneyView.FillData((MoneyType)data.chipType, data.total);
            else if (IGUIM_Casino.instance != null && IGUIM_Casino.instance.currentUser != null)
                IGUIM_Casino.instance.currentUser.avatarView.moneyView.FillData((MoneyType)data.chipType, data.total);
            else if (data.chipType == (int)MoneyType.Gold && UISlot.instance != null && UISlot.instance.moneyLabel != null)
                UISlot.instance.moneyLabel.FillData(data.total);
        }
    }

    private void Wc_OnGeneralMessageNotificationDone(GeneralMessage data)
    {
        if (data != null)
        {
            string title = "Thông báo";
            string message = "";
            if (!string.IsNullOrEmpty(data.title))
                title = data.title;
            if (!string.IsNullOrEmpty(data.message))
                message = data.message;
            MessengerBox.Show(title, message);
        }
    }
    #endregion

    #region OnIAPTopupDone
    private void Wc_OnIAPTopupDone(WarpResponseResultCode status)
    {
        if (status != WarpResponseResultCode.SUCCESS)
            OGUIM.MessengerBox.Show("Thông báo", "Nạp thẻ thất bại. Liên hệ CSKH để được hỗ trợ.");
    }
    #endregion

    #region OnGetPromoConfigDone
    private void Wc_OnGetPromoConfigDone(WarpResponseResultCode status, PromotionConfig _data)
    {
        DailyLogin data = _data.data;
        if (status == WarpResponseResultCode.SUCCESS)
        {
            GameBase.consecutive_days_login = data.consecutive_days_login;
            GameBase.days_login = data.days_login;
        }
    }
    #endregion

    #region OnGetConfigDone
    private void Wc_OnGetConfigDone(WarpResponseResultCode status, RootConfig _data)
    {
        if (status == WarpResponseResultCode.SUCCESS)
        {
            GameBase.likeReward = _data.data.likeReward;
            GameBase.rateReward = _data.data.rateReward;
            SetGiftButton();
        }

    }
    #endregion
    #region Main Event Static
    public static void GoBack()
    {
        //time delay
        if (OGUIM.instance.timeRelease > 0)
        {
            return;
        }
        else
        {
            OGUIM.instance.timeRelease = 0.5f;
        }
        if(instance.webView != null && instance.webView.gameObject.activeInHierarchy)
        {
            instance.webView.Hide();
            instance.webView.gameObject.SetActive(false);
            return;
        }

        if (listNavigation != null && listNavigation.Any())
        {
            try
            {
                //Check current toast stage
                if (Toast.toastType == UIToast.ToastType.Loading && Toast.anim.state != UIAnimation.State.IsHide)
                {
                    UILogView.Log("GoBack: Please wait all toast is hide!!!!!!!!!!");
                    return;
                }
                else if (MessengerBox.popupContent.state == UIAnimation.State.IsAnimation)
                {
                    UILogView.Log("GoBack: Please wait all MessengerBox is hide!!!!!!!!!!");
                    return;
                }

                //Check all popup 
                //is IsShow -> IsHide
                //is IsAnimation -> Return
                var checkPopUp = listPopup.FirstOrDefault(x => x.Value.state != UIAnimation.State.IsHide).Value;
                if (checkPopUp != null)
                {
                    if (checkPopUp.state == UIAnimation.State.IsAnimation)
                    {
                        UILogView.Log("GoBack: Please wait all popup is hide!!!!!!!!!!");
                    }
                    else
                    {

                        checkPopUp.Hide();
                    }
                    return;
                }

                else if (listNavigation.Any(x => x.Value.state == UIAnimation.State.IsAnimation))
                {
                    UILogView.Log("GoBack: Please wait all animations done!!!!!!!!!!");
                    return;
                }
                else if (listMenu.Any(x => x.Value.state == UIAnimation.State.IsAnimation))
                {
                    UILogView.Log("GoBack: Please wait all animations done!!!!!!!!!!");
                    return;
                }

                if (MiniGames.instance != null && MiniGames.instance.miniPoker != null && MiniGames.instance.miniPoker.isShow)
                {

                    MiniGames.instance.miniPoker.Close_Click();

                    return;
                }
                else if (MiniGames.instance != null && MiniGames.instance.miniSpin != null && MiniGames.instance.miniSpin.isShow)
                {

                    MiniGames.instance.miniSpin.Close_Click();
                    return;
                }
                else if (MiniGames.instance != null && MiniGames.instance.miniTaiXiu != null && MiniGames.instance.miniTaiXiu.isShow)
                {

                    MiniGames.instance.miniTaiXiu.Close_Click();
                    return;
                }
                else if (MiniGames.instance != null && MiniGames.instance.miniCaoThap != null && MiniGames.instance.miniCaoThap.isShow)
                {

                    MiniGames.instance.miniCaoThap.Close_Click();
                    return;
                }

                //else if (instance.mainLeaderCanvas.state == UIAnimation.State.IsShow)
                //{
                //    BackFromLeaderToLast();
                //    return;
                //}

                //Check all navigate
                else if (instance.inGameCanvas.state == UIAnimation.State.IsShow)
                {
                    if (currentLobby.id == (int)LobbyId.SLOT || instance.currentLobbyId == LobbyId.SLOT)
                    {
                        if (UISlot.instance != null && UISlot.instance.isSpining)
                        {
                            autoLeaveRoom = true;
                            Toast.ShowNotification("Chờ lượt quay kết thúc");
                            return;
                        }
                    }
                    if (currentLobby.id == (int)LobbyId.SLOT2 || instance.currentLobbyId == LobbyId.SLOT2)
                    {
                        if (UISlot.instance != null && UISlot.instance.isSpining)
                        {
                            autoLeaveRoom = true;
                            Toast.ShowNotification("Chờ lượt quay kết thúc");
                            return;
                        }
                    }
                    if (currentLobby.id == (int)LobbyId.SLOT3 || instance.currentLobbyId == LobbyId.SLOT3)
                    {
                        if (UISlot.instance != null && UISlot.instance.isSpining)
                        {
                            autoLeaveRoom = true;
                            Toast.ShowNotification("Chờ lượt quay kết thúc");
                            return;
                        }
                    }
                    if (currentLobby.id == (int)LobbyId.XENG_HOAQUA || instance.currentLobbyId == LobbyId.XENG_HOAQUA)
                    {
                        if (UIXeng.instance != null)
                        {
                            if (UIXeng.instance.isSpining)
                            {
                                autoLeaveRoom = true;
                                Toast.ShowNotification("Vui lòng chờ lượt quay kết thúc");
                                return;
                            }
                            else if (Convert.ToInt32(UIXeng.instance.txtWinChips.text) != 0)
                            {
                                //Toast.ShowNotification("Hãy nhận tiền trước khi thoát phòng");
                                //return;
                            }
                        }
                    }


                    BuildWarpHelper.LeaveRoom(null);
                    return;
                }

                else if (instance.mainMenuCanvas.state == UIAnimation.State.IsAnimation)
                {
                    return;
                }

                else if (instance.mainRoomsCanvas.state != UIAnimation.State.IsHide)
                {
                    instance.UnSubLobby(null);
                    return;
                }

                else if (instance.mainLobiesCanvas.state != UIAnimation.State.IsHide)
                {
                    LogoutShow();
                    return;
                }

                else if (listNavigation.Count > 1)
                {

                    var current = listNavigation.LastOrDefault();
                    listNavigation.Remove(current.Key);
                    current.Value.Hide();


                    var previous = listNavigation.LastOrDefault();
                    listNavigation.Remove(previous.Key);
                    previous.Value.Show();

                }
                else
                {

                    ApplicationQuitShow();
                }
            }
            catch (Exception ex)
            {
                UILogView.Log("GoBack: " + ex.Message);
            }
        }
        else
        {
            UILogView.Log("GoBack: UIManager.lastShow null or UIManager.isShow null");
            GotoHome();
        }
    }

    public static void LogoutShow()
    {
        MessengerBox.Show("Đăng xuất", "Xác nhận đăng xuất?", "Đồng ý", () =>
        {
            //Toast.ShowLoading("Đang tiến hành đang xuất...");
            WarpClient.wc.Logout();
        },
            "Không", null
            );
    }

    public static void GotoHome()
    {
        UILogView.Log("Go to home 1 :");
        if (instance != null && instance.mainMenuCanvas != null)
            instance.mainMenuCanvas.Hide();
        var current = listNavigation.LastOrDefault();
        if (current.Value != null && current.Value != instance.homeCanvas)
            current.Value.Hide();

        // hide all minigame
        if (MiniGames.instance)
        {
            MiniGames.instance.Reset();
        }
        if (instance.homeCanvas.state != UIAnimation.State.IsShow)
            instance.homeCanvas.Show();
        if (instance.girl.state != UIAnimation.State.IsShow)
            instance.girl.Show();
        MenuTopManager.instance.setMenu(MenuType.NONE);
        instance.miniGameGroup.Hide();

        listNavigation = new Dictionary<string, UIAnimation>();
        listNavigation.Add(instance.homeCanvas.name, instance.homeCanvas);

        if (!instance.autoLoginToggle.isOn)
        {
            if (loginType == LoginType.USER_PASS)
            {
                PlayerPrefs.SetString("username", "");
                PlayerPrefs.SetString("password", "");
            }
            else if (loginType == LoginType.FACEBOOK)
            {
                PlayerPrefs.SetString("faceBookId", "");
                PlayerPrefs.SetString("facebookAccessToken", "");
            }

            PlayerPrefs.SetInt("loginType", 0);
            PlayerPrefs.Save();
        }

        instance.currentLobbyId = LobbyId.NONE;
        currentLobby = null;
        currentRoom = null;

        OldWarpChannel.Channel.socket_close();
        //WarpClient.wc.SessionId = 0;

        Toast.Hide();
    }

    public static void GoToLobies()
    {

        if (listNavigation.Any())
            listNavigation.LastOrDefault().Value.Hide();

        if (instance.girl.state != UIAnimation.State.IsHide)
            instance.girl.Hide();
        if (instance.RegisterCanvas.state != UIAnimation.State.IsHide)
            instance.RegisterCanvas.Hide();
        if (instance.LoginCanvas.state != UIAnimation.State.IsHide)
            instance.LoginCanvas.Hide();

        instance.miniGameGroup.Show();
        instance.mainMenuCanvas.Show();
        instance.mainLobiesCanvas.Show();
        //instance.inGameCanvas.Show();
        instance.menuTopCanvas.Show();
        MenuTopManager.instance.setMenu(MenuType.PURPLE);
        //instance.chatInGameBtn.SetActive(false);

#if UNITY_ANDROID || UNITY_IOS
        if (GameBase.underReview)
            return;
#endif
        
    }

    public static void GoToIngame()
    {

        instance.mainMenuCanvas.Hide();
        listNavigation.LastOrDefault().Value.Hide();
        if (instance.mainLobiesCanvas.state == UIAnimation.State.IsShow)
            instance.mainLobiesCanvas.Hide();
        instance.inGameCanvas.Show();

        LoadGameScene();
    }

    public static void GoToRooms()
    {

        if (currentLobby.lobbymode != LobbyMode.CLASSIC)
            instance.lobbyModeView.SetActive(false);
        else
            instance.lobbyModeView.SetActive(false);

        // Hidden in lite mode
        if (GameBase.isLiteVersion)
            instance.lobbyModeView.SetActive(false);

        instance.mainLobiesCanvas.Hide();
        instance.mainRoomsCanvas.Show();
    }

    public static void BackToLobies()
    {
        var current = listNavigation.LastOrDefault();
        current.Value.Hide();
        listNavigation.Remove(current.Key);
        if (instance.mainLobiesCanvas.state == UIAnimation.State.IsHide)
            instance.mainLobiesCanvas.Show();
        if (instance.mainMenuCanvas.state == UIAnimation.State.IsHide)
            instance.mainMenuCanvas.Show();
        //if (instance.inGameCanvas.state == UIAnimation.State.IsHide)
        //    instance.inGameCanvas.Show();
        MenuTopManager.instance.setMenu(MenuType.PURPLE);
    }

    public static void BackFromLeaveRoomToLast()
    {
        Debug.Log("Go to leave 2");
        //instance.popupChatView.chatMode = ChatMode.World;
        //instance.popupChatView.chatTitle.text = "CHAT CHUNG";
        instance.popupChatView.FillData(listChatWorldData);

        var current = listNavigation.LastOrDefault();
        Debug.Log("Curent 1 : " + current);
        current.Value.Hide();
        listNavigation.Remove(current.Key);
        current = listNavigation.LastOrDefault();
        Debug.Log("Curent 2 : " + current);
        if (current.Value == instance.mainLobiesCanvas)
            currentLobby = null;
        if (listNavigation != null && listNavigation.Count > 0)
            listNavigation.LastOrDefault().Value.Show();
        instance.mainMenuCanvas.Show();
        MenuTopManager.instance.setMenu(MenuType.PURPLE);
    }

    public void GoToLeader()
    {
        instance.mainMenuCanvas.Hide();
        listNavigation.LastOrDefault().Value.Hide();
        instance.mainLeaderCanvas.Show();
    }

    public void SupportCall()
    {
        UIManager.Call(GameBase.hotline);
    }

    [DllImport("__Internal")]
    private static extern void OpenPage(string url);

    public void SupportFacebookPage()
    {
        UIManager.OpenURL(GameBase.fbFanpage);

    }

    public void SupportWebsite()
    {
        UIManager.OpenURL(GameBase.website);
    }
    [SerializeField]
    private RectTransform webviewRect;
    [SerializeField]
    private UniWebView webView;
    public void VerifyPhone(string url)
    {
#if UNITY_WEBGL
        //UIManager.OpenURL(url);
        //OpenPage(url);
        var w = 400;
        var h = 540;
        var left = (Screen.width / 2) - (w / 2);
        var top = (Screen.height / 2) - (h / 2);
        string open = "window.open('" + url +
            "','Thần Bài'," + "'height=" + h + ",width=" + w + ",top=" + top + ",left=" + left + "');";
        Debug.Log(open);
        Application.ExternalEval(open);
#else
        if (webView == null)
        {
            var webViewObj = new GameObject("UniWebView");
            webView = webViewObj.AddComponent<UniWebView>();
            webView.OnShouldClose += WebView_OnShouldClose;
            webView.OnKeyCodeReceived += WebView_OnKeyCodeReceived;
            webView.OnMessageReceived += WebView_OnMessageReceived;
            webView.OnPageFinished += WebView_OnPageFinished;
            webView.ReferenceRectTransform = webviewRect;
        }
        webView.gameObject.SetActive(true);
        webView.Load(url);
        webView.Show();
#endif
    }

    private void WebView_OnPageFinished(UniWebView webView, int statusCode, string url)
    {
        throw new NotImplementedException();
    }

    private void WebView_OnMessageReceived(UniWebView webView, UniWebViewMessage message)
    {
        if (message.Path.Equals("close"))
        {
            this.webView.Hide();
            webView.gameObject.SetActive(false);
        }
    }

    private void WebView_OnKeyCodeReceived(UniWebView webView, int keyCode)
    {
        if(keyCode == 4)
        {
            this.webView.Hide();
            this.webView.Stop();
            webView.gameObject.SetActive(false);
        }
            
    }

    private bool WebView_OnShouldClose(UniWebView webView)
    {
        this.webView.Hide();
        this.webView.Stop();
        webView.gameObject.SetActive(false);
        return true;
    }

   

    public static void BackFromLeaderToLast()
    {
        var current = listNavigation.LastOrDefault();
        current.Value.Hide();
        listNavigation.Remove(current.Key);
        listNavigation.LastOrDefault().Value.Show();
        instance.mainMenuCanvas.Show();
    }

    public static void ApplicationQuitShow()
    {
        //MessengerBox.Show("Thoát khỏi trò chơi?", "",
        //    "Không", null,
        //    "Thoát", () =>
        //    {
        //        UIManager.ApplicationQuit();
        //    });
        UIManager.ApplicationQuit();
    }

    public void UpdateApp()
    {
        Application.OpenURL(GameBase.downloadURL);
    }
#endregion

#region Load Unload Scene
    public static string GetActiveScene()
    {
        var activeScene = SceneManager.GetActiveScene();
        return activeScene.name;
    }

    //IMPORTANT - IMPORTANT - IMPORTANT
    public static void FixedCanvasOrder(GameObject go, float delay = 0.0f)
    {
        instance.StartCoroutine(FixedCanvasOrderAsync(go, delay));
    }

    private static IEnumerator FixedCanvasOrderAsync(GameObject go, float delay = 0.0f)
    {
        while (go.activeSelf)
        {
            if (delay == 0)
                yield return new WaitForEndOfFrame();
            else
                yield return new WaitForSeconds(delay);
            go.SetActive(false);
        }
        while (!go.activeSelf)
        {
            if (delay == 0)
                yield return new WaitForEndOfFrame();
            else
                yield return new WaitForSeconds(delay);
            go.SetActive(true);
        }
    }

    public static void LoadGameScene()
    {
        if (isLoadingScence == false && string.IsNullOrEmpty(instance.currentGameScene))
        {

            isLoadingScence = true;
            var checkScene = GameBase.scenes.FirstOrDefault(x => x.Key == (LobbyId)currentLobby.id);
            if (!string.IsNullOrEmpty(checkScene.Value))
            {
                instance.currentGameScene = checkScene.Value;
#if UNITY_WEBGL
                    //SceneManager.LoadScene(checkScene.Value, LoadSceneMode.Additive);
                    //var scene = SceneManager.GetSceneByName(checkScene.Value);
                    //if (scene != null)
                    //    SceneManager.SetActiveScene(SceneManager.GetSceneByName(checkScene.Value));
                    //FixedCanvasOrder(instance.popupCanvas);
                    //Toast.Hide();
                    //isLoadingScence = false;
                    instance.StartCoroutine(LoadGameScene(checkScene.Value));
#else
                instance.StartCoroutine(LoadGameScene(checkScene.Value));
#endif
            }
            else
            {
                Toast.ShowNotification("LoadGameScene not found " + currentLobby.desc);
            }

        }
        else
        {
            UILogView.Log("Loading Scene, please wait...!");
        }
    }

    public static IEnumerator LoadGameScene(string name)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        async.allowSceneActivation = false;
        while (async.progress < 0.3f)
        {
            yield return null;
            var scaledPerc = 0.3f * async.progress / 0.3f;
            Debug.Log("LoadGameScene " + currentLobby.desc + " : " + (100f * scaledPerc).ToString("F0") + "%");
        }

        async.allowSceneActivation = true;
        float perc = 0.5f;
        while (!async.isDone)
        {
            yield return null;
            perc = Mathf.Lerp(perc, 1f, 0.05f);
            Debug.Log("LoadGameScene " + currentLobby.desc + " : " + (100f * perc).ToString("F0") + "%");
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(name));
        //SceneManager.MoveGameObjectToScene(instance.popupCanvas, SceneManager.GetSceneByName(name));
        FixedCanvasOrder(instance.popupCanvas);
        Toast.Hide();
        isLoadingScence = false;
    }

    public static void UnLoadGameScene(bool goHome = false)
    {
        //UIManager.instance.StartCoroutine(OGUIM.Toast.HideNotification());
        if (currentLobby != null)
        {
            if ((LobbyId)currentLobby.id == LobbyId.MINI_LOTTERY)
            {
                //OGUIM.instance.goGameLottery.HideGame();
                instance.currentGameScene = "";
                //ActionOnUnLoadScene(false);
                //Toast.Hide();
                //BackToLobies();
                return;
            }
            var checkScene = GameBase.scenes.FirstOrDefault(x => x.Key == (LobbyId)currentLobby.id);
            if (!string.IsNullOrEmpty(checkScene.Value))
            {
                Scene targetScene = SceneManager.GetSceneByName(checkScene.Value);
                if (targetScene.isLoaded)
                {
                    //SceneManager.MoveGameObjectToScene(instance.popupCanvas, SceneManager.GetSceneByName("1_Scene_Main"));
                    Backgound.instance.setBackground("");
                    FixedCanvasOrder(instance.popupCanvas);
                    if (targetScene == SceneManager.GetActiveScene())
                    {

                        UILogView.Log("Unload current scene will destroy your GameObjects !");
#if UNITY_WEBGL
                        //var scene = SceneManager.GetSceneByName(checkScene.Value);
                        //if (scene != null)
                        //    SceneManager.UnloadScene(scene);
                        //instance.currentGameScene = "";
                        //ActionOnUnLoadScene(goHome);
                        //// Toast.Hide();
                        ///
                        instance.StartCoroutine(UnLoadGameScene(checkScene.Value, goHome));
#else
                        instance.StartCoroutine(UnLoadGameScene(checkScene.Value, goHome));
#endif
                    }
                    else
                    {

#if UNITY_WEBGL
                        //var scene = SceneManager.GetSceneByName(checkScene.Value);
                        //if (scene != null)
                        //    SceneManager.UnloadScene(scene);
                        //instance.currentGameScene = "";
                        //ActionOnUnLoadScene(goHome);
                        ////Toast.Hide();
                        ///
                        instance.StartCoroutine(UnLoadGameScene(checkScene.Value, goHome));
#else
                        instance.StartCoroutine(UnLoadGameScene(checkScene.Value, goHome));
#endif
                    }
                }
                else
                {
                    UILogView.Log(checkScene.Value + " does not loaded !", true);
                    ActionOnUnLoadScene(goHome);
                }
            }
            else
            {
                UILogView.Log("UnLoadGameScene not found " + currentLobby.desc, true);
                ActionOnUnLoadScene(goHome);
            }
        }
        else
        {
            ActionOnUnLoadScene(goHome);
        }
    }

    public static IEnumerator UnLoadGameScene(string name, bool goHome)
    {
        AsyncOperation async = SceneManager.UnloadSceneAsync(name);
        async.allowSceneActivation = false;
        while (async.progress < 0.3f)
        {
            yield return null;
            var scaledPerc = 0.3f * async.progress / 0.3f;
            Debug.Log("UnLoadGameScene: " + (100f * scaledPerc).ToString("F0") + "%");
        }

        float perc = 0.1f;
        while (!async.isDone)
        {
            yield return null;
            perc = Mathf.Lerp(perc, 0.1f, 0.05f);
            Debug.Log("UnLoadGameScene: " + (100f * perc).ToString("F0") + "%");
        }

        instance.currentGameScene = "";
        ActionOnUnLoadScene(goHome);
        Toast.Hide();
    }

    public static void ActionOnUnLoadScene(bool goHome = false)
    {
        if (WarpClient.wc != null && WarpClient.wc.SessionId != 0)
        {
            foreach (var i in listPopup.Keys)
            {
                if (listPopup[i].state != UIAnimation.State.IsHide)
                    listPopup[i].Hide();
            }
        }

        if (goHome)
            GotoHome();
        else
            BackFromLeaveRoomToLast();
    }
#endregion

    public void ShowButtonChatInGame(bool isShow)
    {
        //if (instance != null)
        //    chatInGameBtn.SetActive(isShow);
    }

    public void ShowBackButton(bool isShow)
    {
        if (instance != null)
            backButton.SetActive(isShow);
    }

    public void ShowUserHistory(int id)
    {
        var _lobbyId = LobbyId.NONE;
        //0: miniPoker , 1: Spin ,2 : TaiXiu , 3 : Cao Thấp , 4 : OanTuTi
        //5 : ba cay , 6 : BauCua
        if(id == 0)
        {
            _lobbyId = LobbyId.MINI_POKER;
        }else if(id== 2)
        {
            _lobbyId = LobbyId.MINI_TAIXIU;
            popupHistory.ShowOnly(0);
            popupHistory.Show(0);
            UserHistoryListView.Get_Data(true, _lobbyId);
        }
        else if(id== 3)
        {
            _lobbyId = LobbyId.MINI_CAOTHAP;
        }

        if (_lobbyId == LobbyId.NONE || _lobbyId == LobbyId.MINI_TAIXIU)
            return;
        popupHistory.Show(0);
        UserHistoryListView.Get_Data(true, _lobbyId);
    }

    public void ShowJackpotHistory(int id)
    {
        var _lobbyId = LobbyId.NONE;
        //0: miniPoker , 1: Spin ,2 : TaiXiu , 3 : Cao Thấp , 4 : OanTuTi
        //5 : ba cay , 6 : BauCua
        if (id == 0)
        {
            _lobbyId = LobbyId.MINI_POKER;
        }
        else if (id == 2)
        {
            _lobbyId = LobbyId.MINI_TAIXIU;
        }
        else if (id == 3)
        {
            _lobbyId = LobbyId.MINI_CAOTHAP;
        }

        if (_lobbyId == LobbyId.NONE)
            return;
        popupHistory.Show(1);
        UserHistoryListView.Get_Data(true, _lobbyId);
    }

#if UNITY_WEBGL && !UNITY_EDITOR
	private void GetProviderCode()
	{
		string name = "";


		name = getParameterByName();

		if (!String.IsNullOrEmpty (name)) {
			string[] str = name.Split('&');
			for (var i = 0; i < str.Length; i++) {
				string[] pair = str[i].Split('=');
				if (pair[0] == "pc") {
					//Debug.LogError (pair [1]);
					GameBase.providerCode = pair [1];
				}

				if (pair[0] == "ch") {
					//Debug.LogError (pair [1]);
					GameBase.refCode = pair [1];
				}

				if (pair[0] == "lt" && !String.IsNullOrEmpty(pair [1])){
					//Debug.LogError (pair [1]);
					PlayerPrefs.SetInt("loginType", int.Parse(pair [1]));
					PlayerPrefs.Save ();
				}

				if (pair[0] == "un") {
					//Debug.LogError (pair [1]);
					int loginType = PlayerPrefs.GetInt ("loginType", 0);
					if (loginType == (int)LoginType.USER_PASS)
						PlayerPrefs.SetString("username", pair [1]);
					else if (loginType == (int)LoginType.FACEBOOK)
						PlayerPrefs.SetString("userFBId", pair [1]);

					PlayerPrefs.Save ();
				}

				if (pair[0] == "pw") {
					//Debug.LogError (pair [1]);
					int loginType = PlayerPrefs.GetInt ("loginType", 0);
					if (loginType == (int)LoginType.USER_PASS)
						PlayerPrefs.SetString("password", pair [1]);
					else if (loginType == (int)LoginType.FACEBOOK)
						PlayerPrefs.SetString("fbAccessToken", pair [1]);

					PlayerPrefs.Save ();
				}

				if (pair [0] == "aufb" && pair [1] == "true")
				{
					LoginWithFB ();
					//PlayerPrefs.DeleteKey("userFBId");
					//PlayerPrefs.DeleteKey("fbAccessToken");
					// Avoid conflict when using lt/un/pw
				}
			} 
		}

	}


	[DllImport("__Internal")]
	private static extern string getParameterByName();
#endif



}
