﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotTypeManager : MonoBehaviour {

    public Image imgBackground, imgForeground;
    public Text txt;

    public void setType(SlotType type)
    {
        switch (type)
        {
            case SlotType.GREEN:
                imgBackground.sprite = ImageSheet.Instance.resourcesDics["number1_slot3"];
                imgForeground.sprite = ImageSheet.Instance.resourcesDics["number2_slot3"];
              
                break;
            case SlotType.PURPLE:
                imgBackground.sprite = ImageSheet.Instance.resourcesDics["slot2_btn_line"];
                imgForeground.sprite = ImageSheet.Instance.resourcesDics["slot2_btn_line2"];
                break;
            case SlotType.RED:
                imgBackground.sprite = ImageSheet.Instance.resourcesDics["number2_slot2"];
                imgForeground.sprite = ImageSheet.Instance.resourcesDics["number1_slot2"];
                break;
        }
    }
}
public enum SlotType
{
    GREEN,
    RED,
    PURPLE
}
