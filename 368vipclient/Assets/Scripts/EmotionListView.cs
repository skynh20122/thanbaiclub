﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionListView : MonoBehaviour {

    [HideInInspector]
    public List<EmotionItemView> listView = new List<EmotionItemView>();
    public UIListView uiListView;

    private void Awake()
    {
        if (uiListView == null)
            uiListView = GetComponent<UIListView>();
    }

    private void Start()
    {
        FillData();
    }

    public void FillData()
    {
        var emotions = EmotionSheet.instance.resourcesDics;
        if (emotions != null && emotions.Count > 0)
        {
            foreach(var s in emotions)
            {
                var ui = uiListView.GetUIView<EmotionItemView>(uiListView.GetDetailView());
                if (ui.FillData(s.Value))
                {
                    listView.Add(ui);
                }
            }
        }
    }
}
