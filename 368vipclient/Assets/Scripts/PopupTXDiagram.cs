﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupTXDiagram : MonoBehaviour {

    private UIAnimation anim;
    private TaiXiuDiagramListView lstNone, lstSum;
    private void Awake()
    {
        anim = GetComponent<UIAnimation>();
        lstNone = transform.Find("Content/Diagram2/History").GetComponent<TaiXiuDiagramListView>();
        lstSum = transform.Find("Content/Diagram1/History").GetComponent<TaiXiuDiagramListView>();
    }

    public void Show()
    {
        anim.Show(()=> {
            lstNone.Get_Data();
            lstSum.Get_Data();
        });
    }

    public void Hide()
    {
        anim.Hide();
    }
}
