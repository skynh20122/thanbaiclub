﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Backgound : MonoBehaviour {

    public Image background;
    public GameObject loadingBar;
    public Text txtVersion;
    public static Backgound instance { get; set; }

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        txtVersion.text = GameBase.clientVersion;
    }
    public void setBackground(String name)
    {
      
        if (name.Equals(""))
        {
            background.sprite = ImageSheet.Instance.resourcesDics["background_2"];
            background.gameObject.SetActive(true);
        }
        else
        {
            background.sprite = ImageSheet.Instance.resourcesDics[name];
            background.gameObject.SetActive(true);
        }
    }

    public void deactiveLoadingBar()
    {
        if(loadingBar != null)
            if (loadingBar.activeInHierarchy)
            {
                loadingBar.SetActive(false);
            }

    }

    public void HideVersion()
    {
        if (txtVersion != null)
            txtVersion.gameObject.SetActive(false);
    }
}
