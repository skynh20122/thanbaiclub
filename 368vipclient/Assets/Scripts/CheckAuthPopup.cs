﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CheckAuthPopup : MonoBehaviour
{
    private InputField inputResult;
    private Text txtQuestion;
    private Button btnAccept, btnClose;
    private static List<QuestionConfirm> lstQuestion = new List<QuestionConfirm>();
    private int answer;
    private UIAnimation anim;
    private AuthenType type;

    private void Awake()
    {
        InitUI();
        if (anim == null)
            anim = GetComponent<UIAnimation>();
    }

    private void InitUI()
    {
        inputResult = transform.Find("Content/Panel/Content/InputField_Result").GetComponent<InputField>();
        txtQuestion = transform.Find("Content/Panel/Content/Text").GetComponent<Text>();
        btnAccept = transform.Find("Content/Panel/Buttons/btnAccept").GetComponent<Button>();
        btnAccept.onClick.AddListener(AcceptClicked);
        btnClose = transform.Find("Content/btnClose").GetComponent<Button>();
        btnClose.onClick.AddListener(CloseClicked);
        LoadQuestionAnswer();
    }

    private void AcceptClicked()
    {
        int result = 0;
        int.TryParse(inputResult.text.Trim().Replace(" ", string.Empty), out result);
        if(result == answer)
        {
            if(type == AuthenType.CASH_IN)
            {
                PlayerPrefs.SetInt(Common.CASH_IN_FEATURE, 1);
                PlayerPrefs.Save();
                OGUIM.instance.popupTopUp.Show();
            }
            else if(type == AuthenType.CASH_OUT)
            {
                PlayerPrefs.SetInt(Common.CASH_OUT_FEATURE, 1);
                PlayerPrefs.Save();
                OGUIM.instance.popupCashOut.Show(0);
            }
        }
      
        Hide();
    }

    private void CloseClicked()
    {
        Hide();
    }
    public void Show(AuthenType authenType)
    {
        inputResult.text = "";
        int rnd = Random.Range(0, lstQuestion.Count);
        Debug.Log("rnd : " + rnd);
        txtQuestion.text = lstQuestion[rnd].question;
        answer = lstQuestion[rnd].answer;
        type = authenType;
        anim.Show();
    }

    public void Hide()
    {
        anim.Hide();
    }

    private void LoadQuestionAnswer()
    {
        QuestionConfirm question;

        question = new QuestionConfirm();
        question.id = 1;
        question.question = "Hai cộng mười hai bằng mấy ?";
        question.answer = 14;
        lstQuestion.Add(question);

        question = new QuestionConfirm();
        question.id = 2;
        question.question = "Ba cộng mười tám bằng mấy ?";
        question.answer = 21;
        lstQuestion.Add(question);

        question = new QuestionConfirm();
        question.id = 3;
        question.question = "Bốn cộng mười bốn bằng mấy ?";
        question.answer = 18;
        lstQuestion.Add(question);

        question = new QuestionConfirm();
        question.id = 4;
        question.question = "Bảy cộng hai bằng mấy ?";
        question.answer = 9;
        lstQuestion.Add(question);

        question = new QuestionConfirm();
        question.id = 5;
        question.question = "Tám cộng năm bằng mấy ?";
        question.answer = 13;
        lstQuestion.Add(question);

        question = new QuestionConfirm();
        question.id = 6;
        question.question = "Chín cộng mười sáu bằng mấy ?";
        question.answer = 25;
        lstQuestion.Add(question);
    }
}

public class QuestionConfirm
{
    public int id;
    public string question;
    public int answer;
}

public enum AuthenType
{
    CASH_IN = 1,
    CASH_OUT = 2
}