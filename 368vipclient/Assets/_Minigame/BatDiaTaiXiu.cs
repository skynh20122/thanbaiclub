﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class BatDiaTaiXiu : MonoBehaviour
{

    public GameObject bat;
    public Image[] vis;
    public GameObject vi;
    public Sprite[] taixiuVisSprite;
    public Text txtCoolDown;
    public Image imgCoolDown;

    Animator batAnim;
    public Vector3 originPos;
    public GameObject endPos;
    public GameObject startPos;
    public Text timeWait;
    public GameObject imageTimerWait;
    bool isOpen;
    long DOTweenId;

    private void Awake()
    {
        originPos = transform.localPosition;
        if (vi == null)
            vi = transform.Find("Vi").gameObject;
        if (timeWait == null)
            timeWait = transform.Find("TimerWait/Text").GetComponent<Text>();
        if (imageTimerWait == null)
            imageTimerWait = transform.Find("TimerWait").gameObject;
    }

    void OnEnable()
    {
        batAnim = GetComponent<Animator>();
        isOpen = false;
        DOTweenId = System.DateTime.Now.Ticks;
        transform.localPosition = originPos;
    }

    public void SetTime(float time, bool flare = false)
    {
        if (flare)
        {
            if (time <= 0)
            {
                imageTimerWait.SetActive(false);
            }
            else
            {
                imageTimerWait.SetActive(true);
                txtCoolDown.gameObject.SetActive(false);
                DOTween.Kill(DOTweenId);
                DOVirtual.Float(time, 0, time, (x) =>
                {
                    int t = Mathf.CeilToInt(x);
                    timeWait.text = t.ToString();
                }).SetEase(Ease.Linear).SetId(DOTweenId).OnComplete(() =>
                {
                    imageTimerWait.SetActive(false);
                });
            }
        }
        else
        {
            if (time <= 0)
            {
                txtCoolDown.gameObject.SetActive(false);

            }
            else
            {
                txtCoolDown.gameObject.SetActive(true);
                imageTimerWait.SetActive(false);
                DOTween.Kill(DOTweenId);
                DOVirtual.Float(time, 0, time, (x) =>
                    {

                        int t = Mathf.CeilToInt(x);
                        txtCoolDown.text = t.ToString();


                    }).SetEase(Ease.Linear)
                    .SetId(DOTweenId)
                    .OnComplete(() =>
                    {
                        txtCoolDown.gameObject.SetActive(false);
                    });
            }
        }
    }
    public void StartShake()
    {
        bat.transform.DOLocalMove(startPos.transform.position, 0.5f).OnComplete(() =>
        {
            transform.localPosition = originPos;
            transform.DOKill();
            transform.DOShakePosition(30, 30, 15, 0, true).SetEase(Ease.Linear).SetDelay(isOpen ? 1 : 0);
            isOpen = true;
            Close();
        });

    }

    public void StopShake()
    {
        transform.DOKill();
        transform.DOLocalMove(originPos, 0.5f);
    }

    public void Open()
    {
        StopShake();
        //if (!isOpen)
        //    batAnim.SetBool("isOpen", true);
        //isOpen = true;
        setBatEndPoint();
    }

    public void Close()
    {
        //if (isOpen)
        //    batAnim.SetBool("isOpen", false);
        //isOpen = false;

    }

    public void SetVis(List<int> visData)
    {
        for (int i = 0; i < vis.Length; i++)
        {
            int index = visData[i];
            vis[i].sprite = taixiuVisSprite[index - 1];
        }
        vi.SetActive(true);
    }

    public void setBatEndPoint()
    {
        bat.transform.DOMove(endPos.transform.position, 0.5f).OnComplete(() =>
        {
            bat.transform.SetPosition(endPos.transform.position);
            bat.gameObject.SetActive(false);
        });
        isOpen = true;
    }

    public void setBatStartPoint(Action onComplete = null)
    {
        bat.transform.SetPosition(endPos.transform.position);
        bat.gameObject.SetActive(true);
        bat.transform.DOLocalMove(startPos.transform.position, 0.5f).OnComplete(() =>
        {
            bat.transform.SetPosition(startPos.transform.position);
            if (onComplete != null)
                onComplete();
        });
        isOpen = false;
    }
}
