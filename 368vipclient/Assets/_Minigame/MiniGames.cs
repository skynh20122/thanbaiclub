﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class MiniGames : MonoBehaviour
{
    public Text timeText;
    public Image timeImage;
    private Animator anim;
    public static void SpawnTextEfx(string str, Vector3 pos,Transform parent, bool active = true)
    {
        var te = instance.textEfxPrefab.Spawn();
        te.gameObject.transform.SetParent(parent.transform);
        te.gameObject.transform.position = pos;
        te.gameObject.transform.SetZ(0, Space.Self);
        te.gameObject.transform.localScale = Vector3.one * (pos == Vector3.zero ? 1 : 1f);

        te.SetData(str.ToUpper(), active ? Color.yellow : Color.yellow, 0, 0);
    }

    public TextEffext textEfxPrefab;
    public Effect_Jackpot jackpotEfx;

    public UIRadialLayout uiRadialLayout;
    public CanvasGroup uiRadialLayoutCanvasGround;
    public bool menuIsShow = false;
    public bool menuIsShowing = false;

    public static readonly string miniGameTweenId = "MiniGameTweenId";
    public MiniSpin miniSpin;
    public MiniPoker miniPoker;
    public MiniTaiXiu miniTaiXiu;
    public MiniCaoThap miniCaoThap;
    public GameObject overlay;
    public LobbyId currentGame;
    public LobbyId unSubGame = LobbyId.NONE;
    public RoomInfo miniRoom;
    [Header("New")]
    [SerializeField]
    private GameObject miniGroup;

    public static MiniGames instance;

    long DOTweenId;
    void Awake()
    {
        instance = this;
        uiRadialLayout.gameObject.SetActive(false);
        AddWarpRequestListener();

        textEfxPrefab.CreatePool(10);
        DOTweenId = System.DateTime.Now.Ticks;
        SetTime(0);
        miniRoom = new RoomInfo();
        anim = uiRadialLayout.gameObject.GetComponent<Animator>();
    }

    private void Start()
    {

    }

    public void TargetMiniGame(Transform mTrans)
    {
        mTrans.localScale = Vector3.one;
        for(int i = 0;i < miniGroup.transform.childCount; i++)
        {
            DragObject drag = miniGroup.transform.GetChild(i).GetComponent<DragObject>();
            if(drag != null && drag.transform != mTrans)
            {
                drag.transform.SetAsFirstSibling();
                drag.transform.localScale = Vector3.one;
            }
        }
    }

    public void ShowMenuGame(bool show)
    {
       
        if (show && !menuIsShow) {
            overlay.SetActive(true);
            StartCoroutine(ShowAsync());
        }
        else {
            overlay.SetActive(false);
            StartCoroutine(HideAsync());
        }
    }

    public void subMoney(int bet)
    {
        if (OGUIM.me.gold > bet)
        {
            OGUIM.me.gold = OGUIM.me.gold - bet;
            OGUIM.instance.meView.FillData(OGUIM.me);

            if (OGUIM.currentLobby != null)
            {
                string scenename = SceneManager.GetActiveScene().name;
                if (scenename.Equals("_Scene_Card_Card"))
                {
                    if (IGUIM.instance != null)
                        IGUIM.instance.currentUser.avatarView.FillData(OGUIM.me);
                }
                else if (scenename.Equals("_Scene_Casino_Casino_old"))
                {
                    if (IGUIM_Casino.instance != null)
                        IGUIM_Casino.instance.meMoneyView.FillData(MoneyType.Gold, OGUIM.me.gold);
                }
                else if (scenename.Equals("_Scene_DapHu"))
                {
                    if (UISlot.instance != null)
                        UISlot.instance.moneyLabel.FillData(OGUIM.me.gold);
                }
                else if (scenename.Equals("_Scene_Casino_Roulette"))
                {
                    if (RoulltetGameManager.instance != null)
                        RoulltetGameManager.instance.updateMoneyView();
                }
                else if (scenename.Equals("_Scene_Xeng"))
                {
                    if (UIXeng.instance != null)
                        UIXeng.instance.meView.moneyView.FillData(OGUIM.me);
                }
            }
        }
    }

    public void updateMoney()
    {
            OGUIM.instance.meView.FillData(OGUIM.me);

            if (OGUIM.currentLobby != null)
            {
                string scenename = SceneManager.GetActiveScene().name;
                if (scenename.Equals("_Scene_Card_Card"))
                {
                    if (IGUIM.instance != null)
                        IGUIM.instance.currentUser.avatarView.FillData(OGUIM.me);
                }
                else if (scenename.Equals("_Scene_Casino_Casino_old"))
                {
                    if (IGUIM_Casino.instance != null)
                        IGUIM_Casino.instance.meMoneyView.FillData(MoneyType.Gold, OGUIM.me.gold);
                }
                else if (scenename.Equals("_Scene_DapHu"))
                {
                    if (UISlot.instance != null)
                        UISlot.instance.moneyLabel.FillData(OGUIM.me.gold);
                }
                else if (scenename.Equals("_Scene_Casino_Roulette"))
                {
                    if (RoulltetGameManager.instance != null)
                        RoulltetGameManager.instance.updateMoneyView();
                }
                else if (scenename.Equals("_Scene_Xeng"))
                {
                    if (UIXeng.instance != null)
                        UIXeng.instance.meView.moneyView.FillData(OGUIM.me);
                }
            }
        
    }

    public void SetTime(int time)
    {
        if (timeText == null || timeImage == null)
        {
            Debug.Log("txtCoolDown == null || imgCoolDown == null");
            return;
        }

        if (time <= 0)
        {
            timeText.gameObject.SetActive(false);
            timeImage.gameObject.SetActive(false);
        }
        else
        {
            timeText.gameObject.SetActive(true);
            timeImage.gameObject.SetActive(true);
            DOTween.Kill(DOTweenId);
            DOVirtual.Float(time, 0, time, (x) =>
                {
                    timeImage.fillAmount = x / time;
                    timeText.text = x.ToString("00");
                    if (x <= 0)
                        timeText.gameObject.SetActive(false);
                }).SetEase(Ease.Linear).SetId(DOTweenId);
        }
    }

    IEnumerator ShowAsync()
    {
        uiRadialLayout.DOKill(true);

        uiRadialLayout.transform.localScale = Vector3.zero;
        uiRadialLayout.gameObject.SetActive(true);
        menuIsShowing = true;

        //anim.Play("RadialEnable");// anim show radial layout
        //yield return new WaitForSeconds(anim.GetTimeAnimation("RadialEnable"));// wait for anim time lenth 
        //menuIsShow = true;
        //menuIsShowing = false;
        uiRadialLayout.transform.DOScale(Vector3.one, 0.1f).OnComplete(() =>
        {
            menuIsShowing = false;
            menuIsShow = true;
            uiRadialLayout.gameObject.SetActive(true);
            uiRadialLayout.transform.localScale = Vector3.one;
            uiRadialLayout.transform.localRotation = Quaternion.identity;
        });
        //uiRadialLayout.transform.DORotate((Vector3.forward * 720), 0.1f, RotateMode.LocalAxisAdd).OnComplete(() =>
        //{
        //    menuIsShowing = false;
        //    menuIsShow = true;
        //    uiRadialLayout.gameObject.SetActive(true);
        //    uiRadialLayout.transform.localScale = Vector3.one;          
        //    uiRadialLayout.transform.localRotation = Quaternion.identity;
        //});
        yield return null;
    }

    IEnumerator HideAsync()
    {
        uiRadialLayout.DOKill(true);
        menuIsShowing = true;
        //anim.Play("RadialDisable");
        //yield return new WaitForSeconds(anim.GetTimeAnimation("RadialDisable"));
        //menuIsShow = false;
        //menuIsShowing = false;
        //uiRadialLayout.gameObject.SetActive(false);
        uiRadialLayout.transform.DOScale(Vector3.zero, 0.1f).OnComplete(() =>
        {
            menuIsShowing = false;
            menuIsShow = false;
            uiRadialLayout.gameObject.SetActive(false);
            //uiRadialLayoutCanvasGround.alpha = 0;
            uiRadialLayout.transform.localRotation = Quaternion.identity;
        });
        //uiRadialLayout.transform.DORotate((Vector3.forward * 720), 0.1f, RotateMode.LocalAxisAdd).OnComplete(() =>
        //{
        //    menuIsShowing = false;
        //    menuIsShow = false;
        //    uiRadialLayout.gameObject.SetActive(false);
        //    //uiRadialLayoutCanvasGround.alpha = 0;
        //    uiRadialLayout.transform.localRotation = Quaternion.identity;
        //});
        yield return null;
    }


    void AddWarpRequestListener()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnMinigameResponse += _wc_OnMinigameResponse;
            WarpClient.wc.OnMiniRoomChanged += _wc_OnMiniRoomChanged;
            Debug.Log("MINIGAME AddWarpRequestListener");
        }
        currentGame = LobbyId.NONE;
    }

    void RemoveWarpRequestListener()
    {
        if (WarpClient.wc != null)
        {
            WarpClient.wc.OnMinigameResponse -= _wc_OnMinigameResponse;
            WarpClient.wc.OnMiniRoomChanged -= _wc_OnMiniRoomChanged;
            Debug.Log("MINIGAME RemoveWarpRequestListener");
        }
    }

    private void _wc_OnMiniRoomChanged(Mini_RootData data, WarpNotifyTypeCode notifyType)
    {
        
        if (notifyType == WarpNotifyTypeCode.TAI_XIU)
        {
            if(miniTaiXiu.isShow)
                miniTaiXiu.OnTaiXiuNotifyDone(data);
        }
        else
        {
            if (miniSpin.isShow)
                miniSpin.SetJackpot(data.jackpot);
            else if (miniPoker.isShow)
                miniPoker.SetJackpot(data.jackpot);
            else if (miniCaoThap.isShow && notifyType == WarpNotifyTypeCode.NOTIFY_MINI_ROOM_CHANGED)
                miniCaoThap.SetJackpot(data.jackpot);

            if (miniCaoThap.isShow && notifyType == WarpNotifyTypeCode.NOTIFY_CAOTHAP_END_MATCH)
            {
                miniCaoThap.EndMatch(data);
            }
            
        }
    }

    private void _wc_OnMinigameResponse(Mini_RootData data, int status)
    {
        if (data == null || (WarpResponseResultCode)status != WarpResponseResultCode.SUCCESS)
        {
            if ((WarpResponseResultCode)status == WarpResponseResultCode.INVALID_CHIP_MIN_BET)
            {
                OGUIM.Toast.Show("Bạn không đủ " + GameBase.moneyGold.name + " để tham gia", UIToast.ToastType.Warning, 3f);

            }
            else if (status == (int)WarpResponseResultCode.SET_BET_IN_READY_MODE && data.type == (int)MINIGAME.ADD_BET)
            {
                if (status != 0 && miniTaiXiu.isShow)
                    OGUIM.Toast.ShowNotification("Đợi ván mới bắt đầu.");
            }

            
        }
        else
        {
            if (data.type == (int)MINIGAME.SUBSCRIBE_ROOM)
            {
                
                if (data.gameID == LOBBY_MINIGAME_ID.MINI_SPIN)
                {
                    Debug.Log(data.gameID);
                    miniSpin.Show();
                    miniSpin.SetJackpot(data.jackpot);
                    //OGUIM.Toast.ShowNotification("Popup Show... MINIGAME_ID.MINI_SPIN");
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_POKER)
                {
                    Debug.Log(data.gameID);
                    miniPoker.Show();
                    miniPoker.SetJackpot(data.jackpot);
                    //OGUIM.Toast.ShowNotification("Popup Show... MINIGAME_ID.MINI_POKER");
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_TAIXIU)
                {
                    Debug.Log(data.gameID);
                    if (!miniTaiXiu.isAutoSub)
                        miniTaiXiu.Show();
                    else
                        currentGame = LobbyId.NONE;

                    miniTaiXiu.OnSubDone(data);
                    //OGUIM.Toast.ShowNotification("Popup Show... MINIGAME_ID.MINI_TAIXIU");
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_CAOTHAP)
                {
                    Debug.Log(data.gameID);
                    miniCaoThap.Show();
                    miniCaoThap.SetJackpot(data.jackpot);
                    //OGUIM.Toast.ShowNotification("Popup Show... MINIGAME_ID.MINI_CAOTHAP");
                }
            }
            else if (data.type == (int)MINIGAME.UNSUBSCRIBE_ROOM)
            {
                if (data.gameID == LOBBY_MINIGAME_ID.MINI_SPIN)
                {
                    if (miniSpin.isShow)
                        miniSpin.Hide();
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_POKER)
                {
                    if (miniPoker.isShow)
                        miniPoker.Hide();
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_TAIXIU)
                {
                    if (miniTaiXiu.isShow)
                        miniTaiXiu.Hide();
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_CAOTHAP)
                {
                    if (miniCaoThap.isShow)
                        miniCaoThap.Hide();
                }
                //ShowMenuGame(true);
            }
            else if (data.type == (int)MINIGAME.START_MATCH)
            {
               
                if (data.gameID == LOBBY_MINIGAME_ID.MINI_SPIN)
                {
                    subMoney(miniSpin.currentBet);
                    if (miniSpin.isShow && data != null)
                    {
                        miniSpin.StartMatch(data);
                    }
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_POKER)
                {
                    subMoney(miniPoker.currentBet);
                    if (miniPoker.isShow && data != null)
                    {
                        miniPoker.StartMatch(data);
                    }
                       
                }
                else if (data.gameID == LOBBY_MINIGAME_ID.MINI_CAOTHAP)
                {
                    //subMoney(miniCaoThap.currentBet);
                    if (miniCaoThap.isShow && data != null)
                        miniCaoThap.StartMatch(data);
                }
            }
            else if (data.type == (int)MINIGAME.ADD_BET)
            {
                miniTaiXiu.OnAddBetDone(data);
            }
            else if (data.type == (int)MINIGAME.CLEAR_BET)
            {
                miniTaiXiu.OnClearBetDone();
            }
        }
    }

    public void Reset()
    {
        if (miniSpin.isShow)
            miniSpin.Hide();
        else if (miniPoker.isShow)
            miniPoker.Hide();
        else if (miniTaiXiu.isShow)
            miniTaiXiu.Hide();
        else if (miniCaoThap.isShow)
            miniCaoThap.Hide();

        currentGame = LobbyId.NONE;
        ShowMenuGame(false);
    }

    public void ShowGame(int gameType)
    {
        //if (currentGame != LobbyId.NONE && currentGame != LobbyId.MINI_TAIXIU && gameType == 0)
        //{
        //    //OGUIM.Toast.Show("Bạn đang tham gia minigame...", UIToast.ToastType.Warning);
        //    return;
        //}

        switch (gameType)
        {
            case 0:
                BuildWarpHelper.MINI_Sub(LobbyId.MINI_POKER, 1000, 1, () =>
                {
                    UILogView.Log("MINI_Sub  MINI_POKER is time out");
                    currentGame = LobbyId.NONE;
                });
                currentGame = LobbyId.MINI_POKER;
                break;
            case 1:
                BuildWarpHelper.MINI_Sub(LobbyId.MINI_SPIN, 0, 1, () =>
                {
                    UILogView.Log("MINI_Sub  MINI_SPIN is time out");
                    currentGame = LobbyId.NONE;
                });
                currentGame = LobbyId.MINI_SPIN;
                break;
            case 2:
                miniTaiXiu.isAutoSub = false;
                BuildWarpHelper.MINI_Sub(LobbyId.MINI_TAIXIU, 0, 1, () =>
                {
                    UILogView.Log("MINI_Sub  MINI_TAIXIU is time out");
                    currentGame = LobbyId.NONE;
                });
                currentGame = LobbyId.MINI_TAIXIU;
                break;
            case 3:
                BuildWarpHelper.MINI_Sub(LobbyId.MINI_CAOTHAP, 1000, 1, () =>
                {
                    UILogView.Log("MINI_Sub  MINI_CAOTHAP is time out");
                    currentGame = LobbyId.NONE;
                });
                currentGame = LobbyId.MINI_CAOTHAP;
                break;
        }
        ShowMenuGame(false);
    }
}
