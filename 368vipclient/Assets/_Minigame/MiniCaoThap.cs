﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class MiniCaoThap : MonoBehaviour
{

    public bool isShow;
    public bool isSpin;
    public bool isStart;
    private bool autoLeave;

    public UIAnimation anim;
    public Transform gameTransform;

    public int currentBet = 1000;
    public UIToggleGroup betToggles;

    public Text txtHigh;
    public Text txtLow;
    public Text txtwin;
    public Text txtTime;
    public Button btnHigh;
    public Button btnLow;
    public Button btnSpin;
    public Button btnNewMatch;

    public NumberAddEffect jackpotValue;

    public List<CardData> allCards;
    public SlotMachine slotMachine;
    public Image[] cardA;

    private List<CardData> resultCards;
    private Image[] resultList;

    public Image resultItem;
    public GameObject historyBkg;
    public GameObject historyBar;

    long DOTweenId;
    void Awake()
    {
        DOTweenId = System.DateTime.Now.Ticks;
        SetTime(0);
    }

    void Start()
    {
        anim = GetComponent<UIAnimation>();
        allCards = new List<CardData>();

        for (int i = 0; i < 52; i++)
        {
            allCards.Add(new CardData(Mathf.FloorToInt(i / 4) + 1, i % 4 + 1));
        }
        slotMachine.UpdateUIByRoom(0);
        currentBet = 1000;
    }

    public void Show()
    {
        if (!isShow)
        {
            Reset();
            anim.Show(() => isShow = true);
        }
    }

    public void Hide()
    {
       
        isShow = false;
        betToggles.IsOn(0);
        anim.Hide(() => isShow = false);
        MiniGames.instance.unSubGame = LobbyId.NONE;
    }

    public void Reset(bool betChange = true)
    {
        isStart = false;
        if (betChange)
            betToggles.IsOn(0);
        autoLeave = false;
        SetTime(0);
        txtHigh.text = "0";
        txtLow.text = "0";
        txtwin.text = "0";
        btnHigh.interactable = false;
        btnLow.interactable = false;
        btnNewMatch.interactable = false;
        for (int i = 0; i < cardA.Length; i++)
        {
            cardA[i].sprite = ImageSheet.Instance.resourcesDics["card_cover"];
        }
        resultCards = new List<CardData>();
        SetHistory();
        btnSpin.interactable = true;
        //slotMachine.gameObject.SetActive(false);
        historyBkg.SetActive(false);
        betToggles.Enable();
    }

    public void SetJackpot(int jackpot)
    {
        if (jackpot != 0)
            jackpotValue.FillData(jackpot);
    }

    public void Close_Click()
    {
        
        if (isSpin)
        {
            OGUIM.Toast.ShowNotification("Rời game khi lượt quay kết thúc.");
            autoLeave = true;
        }
        else
        {
            
            BuildWarpHelper.MINI_UnSub(LobbyId.MINI_CAOTHAP,()=> {
               
                UILogView.Log("MINI_CAOTHAP unsub is timeout!");
            });
        }
    }

    public void Spin_Click()
    {
        if (isSpin)
        {
            OGUIM.Toast.Show("Đợi lượt quay kết thúc", UIToast.ToastType.Warning);
            return;
        }
        else
        {
            MiniGames.instance.subMoney(currentBet);
            MiniGames.instance.currentGame = LobbyId.MINI_CAOTHAP;
            BuildWarpHelper.MINI_StartMatch(LobbyId.MINI_CAOTHAP, null, true);

            //StartMatch(new Mini_RootData {
            //    chips = 200,
            //    winCash = 200,
            //    isWinJackpot = true,
            //    cards = new List<CardData> { new CardData(10,1), new CardData(11, 1) , new CardData(12, 1) , new CardData(13, 1) , new CardData(1, 1) }
            //});
        }
    }

    public void HighLow_Click(bool isHigh)
    {
        Debug.Log("isHigh " + isHigh + " - isSpin "+ isSpin);
        if (isSpin)
            return;
        else
        {
            //
            BuildWarpHelper.MINI_ContinueMatch(LobbyId.MINI_CAOTHAP, isHigh, null);
            
        }
    }

    public void NewMatch_Click()
    {
        if (isSpin)
        {
            //OGUIM.Toast.Show("Đợi vòng quay kết thúc", UIToast.ToastType.Warning);
            return;
        }
        else
        {
            //
            BuildWarpHelper.MINI_EndMatch(LobbyId.MINI_CAOTHAP, null, true);
        }
    }

    public void SetCurrentBet(int bet)
    {
        if (currentBet != bet && isShow)
        {
            currentBet = bet;
            //OGUIM.Toast.Show("");
            BuildWarpHelper.MINI_Sub(LobbyId.MINI_CAOTHAP, currentBet, (int)MoneyType.Gold, null);
        }

        //currentToggle++;

        //if (currentToggle == chipToggles.Length)
        //    currentToggle = 0;
    }

    public void onHighClick()
    {
        if (btnHigh.interactable)
        {
            HighLow_Click(true);
        }
    }

    public void onLowClick()
    {
        if (btnLow.interactable)
        {
            HighLow_Click(false);
        }
    }

    public void StartMatch(Mini_RootData data)
    {
        if (data.winChips > 0)
            btnNewMatch.interactable = true;
        btnSpin.interactable = false;
        slotMachine.gameObject.SetActive(true);

        isStart = true;
        isSpin = true;
        allCards = allCards.OrderBy(x => System.Guid.NewGuid()).ToList();
        allCards[8] = data.cards[0];
        slotMachine.Spin(allCards);
        //var gold = data.chips;
        //var winCash = data.winCash;
        var isWinJackpot = data.isWinJackpot;

        SetTime(data.timeCountDown);

        // Disable Bet Toggle Group
        int except = 0;
        if (currentBet == 1000)
            except = 0;
        else if (currentBet == 10000)
            except = 1;
        else if (currentBet == 50000)
            except = 2;
        else if (currentBet == 100000)
            except = 3;
        else if (currentBet == 500000)
            except = 4;
        betToggles.Disable(except);
        DOVirtual.DelayedCall(1.9f, () =>
        {
            //Set High
            if (data.high > 0)
            {
                txtHigh.text = LongConverter.ToK(data.high);
                btnHigh.interactable = true;
             
            }
            else
            {
                txtHigh.text = "";
                btnHigh.interactable = false;
            }
            //Set Low
            if (data.low > 0)
            {
                txtLow.text = LongConverter.ToK(data.low);
                btnLow.interactable = true;
                
            }
            else
            {
                txtLow.text = "";
                btnLow.interactable = false;
            }
            //Set Normal
            txtwin.text = LongConverter.ToK(data.winChips);
            //Set History
            historyBkg.SetActive(true);
            foreach (var card in data.cards)
            {
                resultCards.Add(card);
            }
            SetHistory();

            var toastStrs = new List<string>();
            if (isWinJackpot)
			{
				UIManager.PlaySound("white_win");
                MiniGames.instance.jackpotEfx.Active();
            }

            //var pos = Vector3.zero;
            var pos = gameObject.transform.position;
            for (int i = 0; i < toastStrs.Count; i++)
            {
                MiniGames.SpawnTextEfx(toastStrs[i], pos,gameObject.transform);
                pos += Vector3.down;
            }

            foreach (var card in data.cards)
            {
                if (card.card == 1)
                    cardA[data.numA - 1].sprite = ImageSheet.Instance.resourcesDics["card_1_1"];
            }
            isSpin = false;
        }).SetId(MiniGames.miniGameTweenId);
    }
    
    public void EndMatch(Mini_RootData data)
    {
        Debug.Log("Mini cao thấp :" + JsonUtility.ToJson(data));
        float delay = 0.1f;
        if (isSpin)
            delay = 2.4f;
        
        DOVirtual.DelayedCall(delay, () =>
        {
            if (data.winGolds > 0)
                btnNewMatch.interactable = false;
            btnSpin.interactable = true;
            isStart = false;
            isSpin = false;

            var cash = data.chips;
            var winCash = data.winChips;
            var isWinJackpot = data.isWinJackpot;
            Reset(false);
            var toastStrs = new List<string>();
            if (isWinJackpot)
            {
                UIManager.PlaySound("white_win");
                MiniGames.instance.jackpotEfx.Active();
            }
            if (winCash > 0)
            {
                UIManager.PlaySound("winchip");
                toastStrs.Add(Ultility.CoinToString(winCash) + " " + GameBase.moneyGold.name);

                DOVirtual.DelayedCall(0.1f, () => { SpinDone(cash); });
            }
            else if (winCash == 0 && cash < OGUIM.me.gold)
            {
                OGUIM.Toast.ShowNotification("Kết thúc lượt chơi");
                SpinDone(cash);
            }

            //var pos = Vector3.zero;
            var pos = gameObject.transform.position;
            for (int i = 0; i < toastStrs.Count; i++)
            {
                MiniGames.SpawnTextEfx(toastStrs[i], pos,gameObject.transform);
                pos += Vector3.down;
            }

            betToggles.Enable();
        });
    }

    private void SpinDone(int gold)
    {
        
        isSpin = false;
		
		OGUIM.me.gold = gold;
        //OGUIM.instance.meView.FillData(OGUIM.me);
        MiniGames.instance.updateMoney();
        if (autoLeave)
        {
            Close_Click();
            autoLeave = false;
        }
    }

    public void SetTime(int time)
    {
        if (time > 0)
        {
            txtTime.gameObject.SetActive(true);
            DOTween.Kill(DOTweenId);
            DOVirtual.Float(time, 0, time, (x) =>
            {
                TimeSpan y = TimeSpan.FromSeconds(x);
                txtTime.text = new DateTime(y.Ticks).ToString("mm:ss");

            }).SetEase(Ease.Linear).SetId(DOTweenId);
        }
        else
        {
            txtTime.text = "";
            txtTime.gameObject.SetActive(false);
        }
    }

    private void SetHistory()
    {
        foreach (Transform child in historyBar.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        resultList = null;

        if (resultCards != null && resultCards.Count > 0)
        { 
            int numberOfResult = resultCards.Count;
            resultList = new Image[numberOfResult];
            int indexOfCell = 0;

            foreach (var _value in resultCards)
            {
                resultList[indexOfCell] = Instantiate(resultItem);
                resultList[indexOfCell].sprite = ImageSheet.Instance.resourcesDics["card_" + _value.ToString()];
                resultList[indexOfCell].transform.SetParent(historyBar.transform, false);
                resultList[indexOfCell].SetAlpha(indexOfCell != numberOfResult - 1 ? 0.5f : 1);
                indexOfCell++;
            }
        }
    }
}
