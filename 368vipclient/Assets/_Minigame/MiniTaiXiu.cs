﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public enum TaiXiuNotifyType
{
    UPDATE_INFO = 1,
    START_MATCH = 2,
    END_MATCH = 3,
    UNSUBSCRIBE = 6,
    PAYBACK = 7,
    CHAT = 8
}

public class MiniTaiXiu : MonoBehaviour
{
    public bool isShow;
    public bool isSub;
    public bool isAutoSub;
    private bool autoLeave;
    public UIAnimation anim;
    public Image nanImage;
    public Transform gameTransform;
    public BatDiaTaiXiu batdia;
    private bool isNan = false;
    public bool isShowMiniChatTaiXiu = false;
    public GameObject iconShow, iconHide;
    public GameObject chatTaiXiuPanel;
    public NumberAddEffect userTaiLabel;
    public NumberAddEffect allBetTaiLabel;
    public NumberAddEffect myBetTaiLabel;
    public NumberAddEffect userXiuLabel;
    public NumberAddEffect allBetXiuLabel;
    public NumberAddEffect myBetXiuLabel;
    
    public Text matchId;

    public CanvasGroup TaiGroup;
    public CanvasGroup XiuGroup;
    public GameObject taiWinEfx, xiuWinEfx;

    public GameObject ThanhVi;
    public List<Sprite> vi;
    public Image ImageVi;

    private Image[] listVi;
    private int[] listChipValue;
    private int chipType = 1;
    private int state;
    private int currentBet;
    private int currentTaiBet;
    private int currentXiuBet;
    private int potBet = 0;

    private int allBetXiu;
    private int allBetTai;
    private int myBetXiu;
    private int myBetTai;
    private int userTai;
    private int userXiu;
    public float timeDelays = 3f;
    public bool canNan = false;
    private List<HistoryBase> history;
    public List<HistoryBase> historyTx = new List<HistoryBase>();
    private HistoryBase result;
    public static MiniTaiXiu instance;

    [Header("New")]
    [SerializeField]
    private PlayerChatListView lstTaiXiuChat;
    [SerializeField]
    private GameObject bets, keybroads;
    private bool isKeybroad = false;
    [SerializeField]
    private Animator animNan;
    [SerializeField]
    private Animator animVi;
    [SerializeField]
    private Text txtCurrentTai,txtCurrentXiu;
    [SerializeField]
    private Button btnInputTai, btnInputXiu;
    [SerializeField]
    private GameObject circleEffect;
    private Text SpinId;
    void Awake()
    {
        currentBet = 5000;
        if (instance == null)
            instance = this;

        bets.SetActive(!isKeybroad);
        keybroads.SetActive(isKeybroad);
        if (animNan == null)
            animNan = transform.Find("GameGroup/GameContent/Button_Nan").GetComponent<Animator>();

        //animNan.SetBool("isNan",false);
        if (animVi == null)
            animVi = transform.Find("GameGroup/GameContent/BatDiaControl/AnimVi").GetComponent<Animator>();

        if (matchId == null)
            matchId = transform.Find("GameGroup/GameContent/GameObject/ID").GetComponent<Text>();
        if (circleEffect == null)
            circleEffect = transform.Find("GameGroup/GameContent/BatDiaControl/Effect_Circle").gameObject;
        btnInputTai = transform.Find("GameGroup/GameContent/GameObject/TaiGroup/BetTai").GetComponent<Button>();
        btnInputXiu = transform.Find("GameGroup/GameContent/GameObject/XiuGroup/BetXiu").GetComponent<Button>();
        SpinId = transform.Find("GameGroup/GameContent/GameObject/SpinId").GetComponent<Text>();
    }

    void Start()
    {
        anim = GetComponent<UIAnimation>();

        ///circleEffect.transform.DORotate(new Vector3(0,0,360f), 5f,RotateMode.Fast).SetLoops(-1,LoopType.Restart);
    }

    private void OnEnable()
    {
        TaiGroup.alpha = 1f;
        XiuGroup.alpha = 1f;

        taiWinEfx.SetActive(false);
        xiuWinEfx.SetActive(false);
    }

    public void Show()
    {

        anim.Show(() => isShow = true);
    }

    public void Hide()
    {
        anim.Hide(() => {
            restart();
        });
    }

    public void changeInput()
    {
        isKeybroad = !isKeybroad;
        bets.SetActive(!isKeybroad);
        keybroads.SetActive(isKeybroad);
    }

    public void Close_Click()
    {
        //if (isShow)
        //    Hide();
        MiniGames.instance.unSubGame = LobbyId.MINI_TAIXIU;
        BuildWarpHelper.MINI_UnSub(LobbyId.MINI_TAIXIU, () =>
        {
            MiniGames.instance.unSubGame = LobbyId.NONE;
            UILogView.Log("MINI_TAIXIU unsub is timeout!");
        });
    }

    public void OnTaiXiuNotifyDone(Mini_RootData data)
    {
        int notifyType = data.type;
        bool changeState = false;

        if (chipType == data.room.chipType)
        {
            if (state != data.room.state)
            {
                setTime(data.room.state, data.room.timeCountDown);
                MiniGames.instance.SetTime(data.room.timeCountDown);
            }
            if (state != data.room.state)
                changeState = true;

            state = data.room.state;
        }


        //state = 0 set_bet
        //state = 1 stop_set_bet
        //state = 2 EndMatch

        if (notifyType == (int)TaiXiuNotifyType.UPDATE_INFO)
        {
            allBetXiu = data.room.minBet;
            allBetTai = data.room.maxBet;

            userTai = data.room.maxCount;
            userXiu = data.room.minCount;

            ChangeText();
            if (state == 1 && changeState == true)
            {
                if (isShow)
                    OGUIM.Toast.ShowNotification("Ngừng nhận cược và cân cửa");
            }
        }
        else if (notifyType == (int)TaiXiuNotifyType.START_MATCH)
        {
            reset();
            TaiGroup.alpha = 1f;
            XiuGroup.alpha = 1f;

            allBetTai = 0;
            allBetXiu = 0;

            ChangeText();
            canNan = false;
            batdia.vi.gameObject.SetActive(false);
            batdia.SetTime(0);
            MiniGames.instance.SetTime(data.room.timeCountDown);
            //batdia.StartShake();
            //DOVirtual.DelayedCall(3f, () =>
            //{
            //    batdia.StopShake();
            //    
            //});
            batdia.SetTime(data.room.timeCountDown);
            taiWinEfx.SetActive(false);
            xiuWinEfx.SetActive(false);
            SpinId.text = "# " + data.spinID.ToString();
        }
        else if (notifyType == (int)TaiXiuNotifyType.END_MATCH)
        {

            if (isNan)
            {
                canNan = true;
                batdia.setBatStartPoint(() =>
                {
                    batdia.SetVis(data.vi.faces);
                });
                DOVirtual.DelayedCall(timeDelays, () =>
                {
                    batdia.setBatEndPoint();
                    OGUIM.me.gold = data.chip;
                    EndMatch(data);
                });

            }
            else
            {
                animVi.gameObject.SetActive(true);
                DOVirtual.DelayedCall(animVi.GetTimeAnimation("ViSpin"), () =>
                {
                    animVi.gameObject.SetActive(false);
                    batdia.SetVis(data.vi.faces);
                    OGUIM.me.gold = data.chip;
                    EndMatch(data);
                });

            }

        }
        else if (notifyType == (int)TaiXiuNotifyType.UNSUBSCRIBE)
        {
            if (isShow)
                Hide();
            MiniGames.instance.SetTime(0);
            MiniGames.instance.unSubGame = LobbyId.NONE;
            isSub = false;
        }
        else if (notifyType == (int)TaiXiuNotifyType.CHAT)
        {
            var mes = new RootChatData();
            mes.userId = data.userId;
            mes.userName = data.userName;
            mes.message = data.message;
            lstTaiXiuChat.FillChatTaiXiu(mes);
        }
        else if (data.type == (int)TaiXiuNotifyType.PAYBACK)
        {
            string _pot = "";
            if (data.pot == 0)
            {
                myBetXiu -= data.payback;
                _pot = " cửa XỈU";
            }
            else
            {
                myBetTai -= data.payback;
                _pot = " cửa TÀI";
            }
            ChangeText();

            if (isShow)
                OGUIM.Toast.ShowNotification("Hoàn trả " + LongConverter.ToFull(data.payback) + " " + GameBase.moneyGold.name + " " + _pot);


        }
    }

    public void OnSubDone(Mini_RootData data)
    {
        
        RoomInfo room = data.room;
        MiniGames.instance.miniRoom = room;
        
        var historyChat = data.historyChat;
        addChat(data.historyChat);
        history = data.history;
        historyTx.Clear();
        foreach(var h in data.history)
        {
            historyTx.Add(h);
        }
        userTai = room.maxCount;
        userXiu = room.minCount;
        MiniGames.instance.SetTime(room.timeCountDown);
        setTime(data.room.state,data.room.timeCountDown);
        SpinId.text = "# " + data.spinID.ToString();
        result = history.LastOrDefault();
        SetHistory();
        ChangeText();
        isSub = true;
    }

    private void addChat(List<RootChatData> historyChat)
    {
        if(historyChat != null && historyChat.Count > 0)
            for(int i = 0; i < historyChat.Count; i++)
            {
                lstTaiXiuChat.FillChatTaiXiu(historyChat[i]);
            }
    }
    private void setTime(int state,int countdown)
    {
        if (state == (int)TaiXiuState.SET_BET)
        {
            batdia.vi.gameObject.SetActive(false);
            batdia.SetTime(countdown);
        }
        else if (state == (int)TaiXiuState.STOP_SET_BET)
        {
            batdia.SetTime(countdown, true);
        }
        else if (state == (int)TaiXiuState.END_MATCH)
        {
            batdia.SetTime(countdown, true);
        }
    }

    public void OnAddBetDone(Mini_RootData data)
    {
        if (data.pot == 1)
        {
            myBetTai = data.total;
            allBetTai = data.totalPot;
            currentTaiBet = 0;
            txtCurrentTai.text = currentTaiBet.ToString();
            btnInputXiu.interactable = false;
            txtCurrentXiu.text = "";
        }
        else if (data.pot == 0)
        {
            myBetXiu = data.total;
            allBetXiu = data.totalPot;
            currentXiuBet = 0;
            txtCurrentXiu.text = currentXiuBet.ToString();
            btnInputTai.interactable = false;
            txtCurrentTai.text = "";
        }

        if (OGUIM.me.gold > data.bet)
        {
            //OGUIM.me.gold = OGUIM.me.gold - data.bet;
            //OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, OGUIM.me.gold);

            MiniGames.instance.subMoney(data.bet);
        }
        ChangeText();
    }

    private void reset()
    {
        btnInputTai.interactable = true;
        btnInputXiu.interactable = true;
        currentTaiBet = 0;
        currentXiuBet = 0;
        myBetTai = 0;
        myBetXiu = 0;
        txtCurrentTai.text = "";
        txtCurrentXiu.text = "";
        myBetTaiLabel.FillData(0);
        myBetXiuLabel.FillData(0);
    }

    public void OnClearBetDone()
    {
        myBetTai = 0;
        myBetXiu = 0;
        ChangeText();
    }

    public void SetCurrentBet(int bet)
    {
        currentBet = bet;
    }
    public void SetBet(int bet)
    {
        int currentGole = OGUIM.me.gold.ToInt();
        //potBet = 1 : tài 0 : xiu
        Debug.Log("Curent set bet : " + potBet);
        if (potBet == 1)
        {
            currentTaiBet += bet;
            if (currentTaiBet > currentGole)
            {
                currentTaiBet = currentGole.ToInt();
            }
            txtCurrentTai.text = currentTaiBet.ToString();
        }
        else
        {
            currentXiuBet += bet;
            if (currentXiuBet > currentGole)
            {
                currentXiuBet = currentGole.ToInt();
            }
            txtCurrentXiu.text = currentXiuBet.ToString();
        }
    }
    public void Pot_Click(int pot)
    {
        Debug.Log("Pot click : " + pot);

        potBet = pot;

        Debug.Log("Current Pot : " + potBet);
    }
    public void Chat_Send(string message)
    {
        taixiuRequest _params = new taixiuRequest();
        _params.type = (int)MINIGAME.CHAT_ROOM;
        _params.message = message;
        WarpRequest.TaiXiuRequest(_params);
    }

    private void set_Bet(int bet)
    {
        int currentGole = bet;
        
        if (potBet == 1)
        {
           
            if (currentGole > OGUIM.me.gold.ToInt())
            {
                currentGole = OGUIM.me.gold.ToInt();
            }
            txtCurrentTai.text = currentGole.ToString();
        }
        else
        {
            
            if (currentGole > OGUIM.me.gold.ToInt())
            {
                currentGole = OGUIM.me.gold.ToInt();
            }
            txtCurrentXiu.text = currentGole.ToString();
        }
    }
    
    public void apendBet(int bet)
    {
        if(potBet == 1)
        {
            if(currentTaiBet == 0)
            {
                if(bet == -1 || bet == 10)
                {

                }
                else
                {
                    currentTaiBet = bet;
                }

                set_Bet(currentTaiBet);
            }
            else
            {
                if (bet == -1)
                {
                    currentTaiBet = 0;
                }
                else if(bet == 10)
                {
                    string tai = currentTaiBet.ToString() + "000";
                    int.TryParse(tai, out currentTaiBet);
                    UILogView.Log(tai);
                }
                else
                {
                    string tai = currentTaiBet.ToString() + bet.ToString();
                    int.TryParse(tai, out currentTaiBet);
                    UILogView.Log(tai);
                }
                set_Bet(currentTaiBet);
            }
        }
        else
        {
            if (currentXiuBet == 0)
            {
                if (bet == -1 || bet == 10)
                {

                }
                else
                {
                    currentXiuBet = bet;
                }

                set_Bet(currentXiuBet);
            }
            else
            {
                if (bet == -1)
                {
                    currentXiuBet = 0;
                }
                else if (bet == 10)
                {
                    string tai = currentXiuBet.ToString() + "000";
                    int.TryParse(tai, out currentXiuBet);
                    UILogView.Log(tai);
                }
                else
                {
                    string tai = currentXiuBet.ToString() + bet.ToString();
                    int.TryParse(tai, out currentXiuBet);
                    UILogView.Log(tai);
                }
                set_Bet(currentXiuBet);
            }
        }

    }
    public void Huy_Click(int pot)
    {
        if (potBet == 1)
        {
            currentTaiBet = 0;
            txtCurrentTai.text = currentTaiBet.ToString();
        }
        else
        {
            currentXiuBet = 0;
            txtCurrentXiu.text = currentXiuBet.ToString();
        }
    }
    public void Dat_Click(int pot)
    {
        taixiuRequest _params = new taixiuRequest();
        _params.type = (int)MINIGAME.ADD_BET;
        if (potBet == 1)
        {
            _params.bet = currentTaiBet;
            _params.pot = 1;
        }
        else
        {
            _params.pot = 0;
            _params.bet = currentXiuBet;
        }


        if (_params.bet > 0)
        {

            WarpRequest.TaiXiuRequest(_params);
        }

    }
    public void AddBet(int pot)
    {
        taixiuRequest _params = new taixiuRequest();
        _params.type = (int)MINIGAME.ADD_BET;
        _params.bet = currentBet;
        _params.pot = pot;
        WarpRequest.TaiXiuRequest(_params);
    }

    public void ClearBet()
    {
        taixiuRequest _params = new taixiuRequest();
        _params.type = (int)MINIGAME.CLEAR_BET;
        WarpRequest.TaiXiuRequest(_params);
    }

    private void ChangeText()
    {
        allBetTaiLabel.FillData(allBetTai);
        myBetTaiLabel.FillData(myBetTai, "", 1, true);
        userTaiLabel.FillData(userTai);

        allBetXiuLabel.FillData(allBetXiu);
        myBetXiuLabel.FillData(myBetXiu, "", 1, true);
        userXiuLabel.FillData(userXiu);
    }

    private void SetHistory()
    {
        UILogView.Log("History tài xỉu  : " + history.Count);
        if (listVi != null && listVi.Length > 0)
        {
            foreach (Transform child in ThanhVi.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
            listVi = null;
        }

        int numberOfVi = history.Count;

        listVi = new Image[numberOfVi];
        int indexOfCell = 0;

        foreach (var _value in history)
        {
            listVi[indexOfCell] = Instantiate(ImageVi);
            if (_value.point > 10)
                listVi[indexOfCell].sprite = vi[0];
            else
                listVi[indexOfCell].sprite = vi[1];

            listVi[indexOfCell].transform.SetParent(ThanhVi.transform, false);
            listVi[indexOfCell].SetAlpha(indexOfCell != numberOfVi - 1 ? 1f : 1);
            NumberAddEffect number = listVi[indexOfCell].GetComponentsInChildren<NumberAddEffect>()[0];
            number.number.color = Color.black;
            number.FillData(_value.point);

            indexOfCell++;
        }
    }
    public void setNan()
    {
        isNan = !isNan;

        if (isNan)
        {
            OGUIM.Toast.Show("Bật chế độ nặn ");
            animNan.SetBool("isNan", true);
        }
        else
        {
            OGUIM.Toast.Show("Tắt chế độ nặn ");
            animNan.SetBool("isNan", false);
        }
    }

    private void EndMatch(Mini_RootData data)
    {
        myBetTai = 0;
        myBetXiu = 0;
        userTai = 0;
        userXiu = 0;

        while (history.Count > 19)
            history.RemoveAt(0);
        result = data.vi;
        history.Add(result);
        while (historyTx.Count >= 100)
            historyTx.RemoveAt(0);
        historyTx.Add(result);
        SetHistory();
        EndMatch(data.chipChange, data.vi.point);
    }

    private void EndMatch(long winChips, int result)
    {
        var toastStrs = new List<string>();
        //string potName;
        if (result > 10)
        {
            TaiGroup.alpha = 1f;
            XiuGroup.alpha = 0.5f;
            //potName = "TÀI";
            taiWinEfx.SetActive(true);
            xiuWinEfx.SetActive(false);
        }
        else
        {
            TaiGroup.alpha = 0.5f;
            XiuGroup.alpha = 1f;
            // potName = "XỈU";
            taiWinEfx.SetActive(false);
            xiuWinEfx.SetActive(true);
        }

        if (isShow)
        {
            //toastStrs.Add(potName);
            if (winChips != 0)
                toastStrs.Add(Ultility.CoinToString(winChips));
        }
        //var pos = Vector3.zero;
        var pos = gameObject.transform.position;
        for (int i = 0; i < toastStrs.Count; i++)
        {
            MiniGames.SpawnTextEfx(toastStrs[i], pos, gameObject.transform, winChips > 0);
            pos += Vector3.down;
        }

        if (winChips > 0)
            UIManager.PlaySound("winchip");
        MiniGames.instance.updateMoney();
    }

    public void showMini()
    {
        isShowMiniChatTaiXiu = !isShowMiniChatTaiXiu;
        Debug.Log("Mini Chat Tai Xiu is : " + isShowMiniChatTaiXiu);
        if (isShowMiniChatTaiXiu)
        {
            chatTaiXiuPanel.SetActive(true);
            iconShow.gameObject.SetActive(false);
            iconHide.gameObject.SetActive(true);
        }
        else
        {
            chatTaiXiuPanel.SetActive(false);
            iconShow.gameObject.SetActive(true);
            iconHide.gameObject.SetActive(false);
        }
    }

    public void restart()
    {
        isShow = false;
        isSub = false;
        isNan = false;
        animVi.gameObject.SetActive(false);
        if(batdia != null)
         batdia.vi.SetActive(false);
    }
}

public enum TaiXiuState
{
    SET_BET = 0,
    STOP_SET_BET = 1,
    END_MATCH = 2
}