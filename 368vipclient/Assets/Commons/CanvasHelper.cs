﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasHelper : MonoBehaviour {

	public GameRotationType gameRotationType = GameRotationType.Landscape;
	private CanvasScaler canvas;
	private int width;
	private int height;

	void Awake()
	{
		if (canvas == null)
			canvas = gameObject.GetComponent<CanvasScaler> ();

		width = Screen.width;
		height = Screen.height;

		SetCanvasScale ();
	}

	private void SetCanvasScale()
	{
		if (gameRotationType == GameRotationType.Landscape) {
			ScaleLandscape ();
		} else if (gameRotationType == GameRotationType.Portrait) {
			ScalePortrait ();
		}
	}

	#if UNITY_EDITOR 
	// Update is called once per frame
	void Update () {
		if (width != Screen.width || height != Screen.height)
		{
			width = Screen.width;
			height = Screen.height;
			SetCanvasScale();
		} 
	}
	#endif

	private void ScaleLandscape()
	{
		float ratio = width / height;
		var size32 = 3 / 2f;
		var isMatchHeight = ratio >= size32;
		canvas.matchWidthOrHeight = isMatchHeight ? 1 : 0;
	}

	private void ScalePortrait()
	{
		float ratio = width / height;
		var size32 = 3 / 2f;
		var isMatchHeight = ratio >= size32;
		canvas.matchWidthOrHeight = isMatchHeight ? 0 : 1;
	}
}

public enum GameRotationType: int
{
	Auto,
	Landscape,
	Portrait,
}
