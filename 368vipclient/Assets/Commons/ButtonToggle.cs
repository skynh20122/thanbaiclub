﻿using System;
using System.Collections;
using System.Collections.Generic;
//using com.vietlabs.DebugX;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggle : MonoBehaviour {

    public Button btn;
    public Image imgButton;
    public Text txtButton;
    public List<Sprite> lstSprite;
    //private ImageUIHelper imgUIHelper;
    private Dictionary<string, Sprite> dict;

    private void Start()
    {
        FindObj();
    }

    private void FindObj()
    {
        if (btn == null)
            btn = gameObject.GetComponent<Button>();

        if (imgButton == null)
            imgButton = gameObject.GetComponent<Image>();

        if (imgButton == null)
            imgButton = gameObject.GetComponentInChildren<Image>();

        //if (imgUIHelper == null && imgButton != null)
        //imgUIHelper = imgButton.GetComponent<ImageUIHelper>();

        if (dict == null || dict.Count == 0)
        {
            dict = new Dictionary<string, Sprite>();
            for (int i = 0; i < lstSprite.Count; i++)
            {
                dict.Add(lstSprite[i].name, lstSprite[i]);
            }
        }
    }

    public void AddListener(Action callBack, bool isClearAll= false)
    {
        FindObj();
        if (btn == null) 
        {
            Debug.Log("Button Toggle is Null");
            return;
        }
        if(isClearAll)
        {
            btn.onClick.RemoveAllListeners();
        }
        btn.onClick.AddListener(() => callBack());
    }

    public void RemoveAllListener()
    {
		if (btn == null)
		{
            Debug.Log("Button is null");
			return;
		}
        btn.onClick.RemoveAllListeners();
    }

    public void SetImage(string imageName)
	{
		FindObj();
        //if (imgUIHelper == null)
        //{
        //          //
        //          imgUIHelper = gameObject.GetComponentInChildren<ImageUIHelper>();
        //          if (imgUIHelper == null)
        //          {
        //              Debug.Log("ImageUIHelper Button is null");
        //              return;
        //          }
        //}
        //imgUIHelper.SetSprite(imageName);
        if(dict == null)
        {
            Debug.Log("ImageUIHelper Button is null");
            return;
        }
        imgButton.sprite = dict[imageName];
    }

    public void SetImage(Sprite sprite)
	{
		FindObj();
		if (imgButton == null)
		{
            Debug.Log("Sprite Button is null");
			return;
		}
        imgButton.sprite = sprite;
    }

    public void SetText(string text)
    {
        if(txtButton == null)
        {
            Debug.Log("Text Button is null");
            return;
        }
    }
}
