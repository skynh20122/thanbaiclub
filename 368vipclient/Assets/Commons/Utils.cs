﻿using System;
using System.Collections;
using System.Collections.Generic;
// using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using UnityEngine;
//using com.vietlabs.DebugX;
//using com.vietlabs;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//using gc;
using System.Linq;

public static class Utils
{
    //yellow - highlight
    public const string BUTTON_1 = "btn2 (4)";
    //blue - normal
    public const string BUTTON_2 = "btn1 (4)";

    public static float GetTimeAnimation(this Animator animator, string animName)
    {
        if (animator == null) return -1f;
        if (animator.runtimeAnimatorController == null) return -1f;

        foreach (var animationClip in animator.runtimeAnimatorController.animationClips)
        {
            if (animationClip.name.Equals(animName))
                return animationClip.length;
        }

        return -1f;
    }

    public static string GetTimeFormat(this int time)
    {
        return ((float)time).GetTimeFormat();
    }

    public static string GetTimeFormat(this float time)
    {
        var timeInt = Mathf.FloorToInt(time);
        if (time <= 0)
            return "00:00";
        var day = timeInt / 3600 / 24;
        var hour = timeInt / 3600 % 24 + day * 24; ;
        var minute = timeInt % 3600 / 60;
        var second = timeInt % 60;
        //return Mathf.Floor(time / 60) + ":" + (Mathf.Floor(time % 60) < 10 ? "0" + Mathf.Floor(time % 60) : "" + Mathf.Floor(time % 60));
        return (hour <= 0 ? "" : (hour < 10 ? "0" + hour + ":" : hour + ":")) +
                (minute <= 0 ? "00:" : (minute < 10 ? "0" + minute + ":" : minute + ":")) +
                (second <= 0 ? "00" : (second < 10 ? "0" + second : "" + second));
    }

    public static string GetShortTimeFormat(this int time)
    {
        return ((float)time).GetShortTimeFormat();
    }

    public static string GetShortTimeFormat(this float time)
    {
        //dùng để đua ra giá trị là ngày con mà ko đủ 1 ngày thì mới đưa ra hh:mm:ss

        var timeInt = Mathf.FloorToInt(time);
        if (time <= 0)
            return "00:00";
        var day = timeInt / 3600 / 24;
        var hour = timeInt / 3600 % 24;
        var minute = timeInt % 3600 / 60;
        var second = timeInt % 60;
        //return Mathf.Floor(time / 60) + ":" + (Mathf.Floor(time % 60) < 10 ? "0" + Mathf.Floor(time % 60) : "" + Mathf.Floor(time % 60));
        if (day > 0)
            return day + " ngày";

        return (hour <= 0 ? "" : (hour < 10 ? "0" + hour + ":" : hour + ":")) +
                (minute <= 0 ? "00:" : (minute < 10 ? "0" + minute + ":" : minute + ":")) +
                (second <= 0 ? "00" : (second < 10 ? "0" + second : "" + second));
    }

    public static int GetSignOfDifference(int minuend, int subtrahend)
    {
        var val = minuend - subtrahend;
        return val > 0 ? 1 : (val < 0 ? -1 : 0);
    }

    public static long GetMillisecond()
    {
        return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
    }

    //public static T Get<T>(this Dictionary<string, object> h, string key, bool useDefaultIfNotExist = true)
    //{
    //    return Get<T, object>(h, key, useDefaultIfNotExist);
    //}

    public static DateTime LongToDateTime(this long stick)
    {
        long beginTicks = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

        DateTime date = new DateTime(beginTicks + stick * 10000, DateTimeKind.Utc).ToLocalTime();
        return date;
    }

//    public static List<int> GetLstInt(this Dictionary<string, object> h, string key, bool useDefaultIfNotExist = true)
//    {
//#if !UNITY_WEBGL
//        List<int> lstData = h.Get<int[]>(key).ToList();
//        return lstData;
//#else
//        var dt = h.Get<List<object>>(key);
//        List<int> lt = new List<int>();
//        foreach (var it in dt)
//        {
//            lt.Add(it.ToInt());
//        }
//        return lt;
//#endif
    //}

//    public static T Get<T, T1>(this Dictionary<string, T1> h, string key, bool useDefaultIfNotExist = true)
//    {
//        if (h == null)
//        {
//            return default(T);
//        }

//        if (!h.ContainsKey(key))
//        {
//            // DebugX.Warn("Can't find key <" + key + ">");
//            return default(T);
//        }


//        object value = h[key];

//        if (value == null) return default(T);

//        Type valueT = value.GetType();

//        if (valueT == typeof(T)) return (T)value;
//        if (gcType.CanCast(valueT, typeof(T)))
//        {
//            try
//            {
//#if UNITY_WP8
//                return (T)Convert.ChangeType(value, typeof(T));
//#else
//                return (T)Convert.ChangeType(value, Type.GetTypeCode(typeof(T)));
//#endif
    //        }
    //        catch (Exception e)
    //        {
    //            DebugX.Warn("cast value error with key <" + key + "> error:: " + e.Message);
    //            return default(T);
    //        }
    //    }

    //    return default(T);
    //}

    public static bool IsDicNullOrEmpty(this Dictionary<string, object> dic, string exceptKey = "")
    {
        return (dic == null || dic.Count == 0 ||
                (!string.IsNullOrEmpty(exceptKey) && dic.ContainsKey(exceptKey) && dic.Count == 1));
    }

    public static bool ToBool(this object value, bool defaultValue)
    {
        bool outValue;
        if (bool.TryParse(value.ToString(), out outValue))
        {
            return outValue;
        }

        return defaultValue;
    }

    public static bool ToBool(this int value)
    {
        if (value == 0) { return false; }

        return true;
    }

    public static int ToInt(this object value, int defaultValue = 0)
    {
        int outValue;
        if (int.TryParse(value.ToString(), out outValue))
        {
            return outValue;
        }

        return defaultValue;
    }

    public static float ToFloat(this object value, float defaultValue = 0.0f)
    {
        float outValue;
        if (float.TryParse(value.ToString(), out outValue))
        {
            return outValue;
        }

        return defaultValue;
    }

    public static string ReplaceSpace(this string text)
    {
        return text.Replace(" ", "");
    }

    public static double ToDouble(this object value, double defaultValue = 0)
    {
        double outValue;
        if (double.TryParse(value.ToString(), out outValue))
        {
            return outValue;
        }

        return defaultValue;
    }

    public static long ToLong(this object value, long defaultValue = 0)
    {
        long outValue;
        if (long.TryParse(value.ToString(), out outValue))
        {
            return outValue;
        }

        return defaultValue;
    }

    public static string GetMoneyFormat(this int value)
    {
        return ((long)value).GetMoneyFormat();
    }

    public static string GetMoneyFormat(this long val)
    {
        long value = val;
        bool isNegative = false;
        if (value < 0)
        {
            isNegative = true;
            value *= -1;
        }

        var result = value.ToString();
        var counter = result.Length;
        while (counter > 3)
        {
            counter -= 3;
            if (value < 0 && counter == 1)
                return result;
            result = result.Insert(counter, ".");
        }
        if (isNegative)
        {
            result = "-" + result;
        }

        return result;
    }

    public static string GetMoneyFormatWithString(this long value, string signDefault = ".")
    {
        var result = value.ToString();

        if (result.Length < 6) { return result; }

        result = result.Substring(0, result.Length - 3);

        var counter = result.Length;
        while (counter > 3)
        {
            counter -= 3;
            result = result.Insert(counter, signDefault);
        }
        return result + "K";
    }

    public static string GetMoneyFormatWithString2(this long value)
    {
        return GetMoneyFormat((long)value);

        //string str = "";
        //long thousand = 10000;
        //long val1 = value / thousand;
        //long val2 = value - (val1 * thousand);

        //if (val1 == 0)
        //{
        //    str = val2.ToString();        
        //}
        //else
        //{
        //    str = val1 + " " + LanguageManager.api.GetKey(Localize.ID_CURRENCY_TEN_THOUSAND) + " " + val2;        
        //}


        //return str;

    }

    public static string GetMoneyFormatWithString2(this int value)
    {
        return GetMoneyFormatWithString2((long)value);
    }

    public static string ToStringWithMark(this long val)
    {
        if (val >= 0) { return "+" + val; }

        return "-" + Mathf.Abs(val);
    }

    public static string ToStringWithMarkAndPercent(this long val)
    {
        if (val >= 0) { return "+" + val + "%"; }

        return "-" + Mathf.Abs(val) + "%";
    }

    public static string ToStringWithMark(this float val)
    {
        val = (float)Math.Round((double)val, 2);

        if (val >= 0) { return "+" + val; }

        return "-" + Mathf.Abs(val);
    }

    public static string ToStringWithMarkAndPercent(this float val)
    {
        val = (float)Math.Round((double)val, 2);

        if (val >= 0) { return "+" + val + "%"; }

        return "-" + Mathf.Abs(val) + "%";
    }

    public static string ToStringWithPercent(this float val)
    {
        val = (float)Math.Round((double)val, 2);
        return val + "%";
    }

    public static string ToStringDecimal(this float val)
    {
        val = (float)Math.Round((double)val, 2);
        return val.ToString().Replace('.', ',');
    }

    public static string ToStringRound1(this float val)
    {
        val = (float)Math.Round((double)val, 1);
        return val.ToString().Replace(',', '.');
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = new System.Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId)).Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static string EncryptMd5(this string strToEncrypt)
    {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }
        return hashString.PadLeft(32, '0');
    }

    //public static string EncryptString(this string mString)
    //{
    //    //todo: encrypt
    //    return mString;
    //}

    public static string Decrypt(this string mString)
    {
        //todo: decrypt
        return mString;
    }

    public static string DeepTrace(this object p_obj, int p_level = 0)
    {
        string baseStr = Duplicate("\t", p_level);

        if (p_obj == null) { return "null"; }

        if (p_obj.GetType().IsPrimitive) { return p_obj.ToString(); }

        var list1 = p_obj as IList;
        if (list1 != null)
        {
            IList list = list1;
            string str = baseStr + "[";
            for (int i = 0; i < list.Count; i++)
            {
                str += (i > 0 ? ",\n" : "\n") + baseStr + "\t" + DeepTrace(list[i], p_level + 1);
            }
            return str + "]";
        }

        var dictionary = p_obj as IDictionary;
        if (dictionary != null)
        {
            IDictionary dict = dictionary;
            string str = baseStr + "{";
            bool first = true;

            foreach (DictionaryEntry item in dict)
            {
                str += (first ? "\n" : ",\n") + baseStr + "\t"
                       + (item.Key + ":" + DeepTrace(item.Value, p_level + 1));
                first = false;
            }

            return str + "}";
        }

        return baseStr + p_obj;
    }

    public static string Duplicate(string p_str, int p_nTimes)
    {
        string result = "";
        for (int i = 0; i < p_nTimes; i++) { result += p_str; }
        return result;
    }

    //public static string DictionaryToJson(this Dictionary<string, object> data)
    //{
    //    return MiniJSON.Json.Serialize(data);
    //}

    //public static Dictionary<string, object> JsonToDictionary(this string json)
    //{
    //    return MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
    //}

    //public static T GetComponent<T>(Transform parent, string childName)
    //{
    //    string[] str = childName.Split('/');
    //    Transform tf = null;
    //    for (int i = 0; i < str.Length; i++)
    //    {
    //        tf = parent.Find(str[i]);
    //        if (tf == null)
    //        {
    //            DebugX.Log("can not find " + str[i] + " in " + childName);
    //            return default(T);
    //        }
    //    }

    //    if (tf == null)
    //    {
    //        DebugX.Log("can not GetComponent " + typeof(T).ToString() + " in " + childName);
    //        return default(T);
    //    }

    //    return tf.GetComponent<T>();
    //}

    public static int RandomX(int min = 0, int max = 100)
    {
        return UnityEngine.Random.Range(min, max);
    }

    public static bool RandomChecker(this int value, int min = 0, int max = 100)
    {
        var vRandom = RandomX(min, max);
        return (value - 1) >= vRandom;
    }

    public static bool RandomPercentChecker(this float value, int min = 0, int max = 100)
    {
        var v = UnityEngine.Random.Range(min, max);
        var va = value * max - 1;

        return va >= v;
    }

    public static void SetLayer(this GameObject go, string layer)
    {
        if (go == null) return;
        SetLayer(go.transform, layer);
    }

    public static void SetLayer(this Transform tran, string layer)
    {
        if (tran == null) return;

        tran.gameObject.layer = LayerMask.NameToLayer(layer);

        var childCount = tran.childCount;
        if (childCount == 0) return;

        for (int i = 0; i < childCount; i++)
        {
            var child = tran.GetChild(i);
            child.gameObject.layer = LayerMask.NameToLayer(layer);
            SetLayer(child, layer);
        }
    }

    public static Color GetHpColor(this int percent)
    {
        var str = "";
        if (percent <= 15f) str = "#F50909FF"; //Color.TryParseHexString("F50909FF", out c); //red
        else if (percent <= 50f) str = "#FDEC12FF"; //Color.TryParseHexString("FDEC12FF", out c); //yellow
        else str = "#25DB2DFF"; //Color.TryParseHexString("25DB2DFF", out c); //creen

        Color c;
        if (!ColorUtility.TryParseHtmlString(str, out c))
        {
            c = Color.white;
        }

        return c;
    }

    public static void ClearAllChild(RectTransform rtParent)
    {
        int childs = rtParent.childCount;
        List<GameObject> lstGo = new List<GameObject>();
        for (int i = 0; i < childs; i++)
        {
            lstGo.Add(rtParent.GetChild(i).gameObject);
        }
        for (int i = 0; i < lstGo.Count; i++)
        {
            lstGo[i].SetActive(false);
            GameObject.Destroy(lstGo[i]);
        }

        rtParent.DetachChildren();

        //int childs = rtParent.childCount;
        ////for (int i = childs - 1; i >= 0; i--)
        //for (int i = 0; i < childs; i++)
        //{
        //    //int x = rtParent.childCount;
        //    GameObject go = rtParent.GetChild(i).gameObject;
        //    go.Destroy();
        //    // GameObject.Destroy(go); // rtParent.GetChild(i).gameObject);
        //}
    }

    public static void ClearAllChild(this Transform rtParent)
    {
        if(rtParent == null) return;
        var childs = rtParent.childCount;
        for (var i = childs - 1; i >= 0; i--)
        {
            var childTran = rtParent.GetChild(i);
            if(childTran != null) GameObject.Destroy(childTran.gameObject);
        }
    }

    public static void CreateEventSystem()
    {
        if (GameObject.Find("EventSystem") == null)
        {
            var eventSystem = new GameObject("EventSystem");
            eventSystem.AddComponent<EventSystem>();
            eventSystem.AddComponent<StandaloneInputModule>().forceModuleActive = true;
        }
    }

    public static void DisableInteractiveButtonType1(GameObject gobj) //Button have a text
    {
        gobj.GetComponent<Button>().interactable = false;
        Color col = gobj.transform.GetChild(0).GetComponent<Text>().color;
        col.a = 0.2f;
        gobj.transform.GetChild(0).GetComponent<Text>().color = col;
    }

    public static void EnableInteractiveButtonType1(GameObject gobj)
    {
        gobj.GetComponent<Button>().interactable = true;
        Color col = gobj.transform.GetChild(0).GetComponent<Text>().color;
        col.a = 1.0f;
        gobj.transform.GetChild(0).GetComponent<Text>().color = col;
    }

    public static void SetGroupSizeVertical(RectTransform rt)
    {
        GridLayoutGroup gridLayoutGroup = rt.GetComponent<GridLayoutGroup>();

        int totalData = rt.childCount;
        int totalRow = totalData / gridLayoutGroup.constraintCount;
        if (totalData % gridLayoutGroup.constraintCount != 0)
        {
            totalRow++;
        }

        float totalHeight = gridLayoutGroup.cellSize.y * totalRow + gridLayoutGroup.spacing.y * totalRow + 16f;

        rt.sizeDelta = new Vector2(rt.sizeDelta.x, totalHeight);

        rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, 0);

    }

    public static void SetGroupSizeHorizal(RectTransform rt)
    {
        GridLayoutGroup gridLayoutGroup = rt.GetComponent<GridLayoutGroup>();

        int totalData = rt.childCount;
        int totalCol = totalData / gridLayoutGroup.constraintCount;
        if (gridLayoutGroup.constraintCount > 1)
        {
            if (totalData % gridLayoutGroup.constraintCount != 0)
            {
                // totalCol++;
            }
        }


        float totalWidth = gridLayoutGroup.cellSize.x * totalCol + gridLayoutGroup.spacing.x * totalCol;

        rt.sizeDelta = new Vector2(totalWidth, rt.sizeDelta.y);

        rt.anchoredPosition = new Vector2(0, rt.anchoredPosition.y);

    }

    public static Color ColorFromHex(string hex)
    {
        if (string.IsNullOrEmpty(hex)) return Color.white;
        Color color;

        if (!ColorUtility.TryParseHtmlString(hex, out color))
        {
            color = Color.white;
        }

        return color;
    }

    public static Color32 DisableColorForImage
    {
        get
        {
            return new Color32(120, 120, 120, 255);
        }
    }

    public static Color DisableColorForRequireConsumable
    {
        get
        {
            return ColorFromHex("#FFDF79FF");
        }
    }

    public static Color ColorDefault
    {
        get
        {
            return ColorFromHex("#FFDF79FF");
        }
    }
    public static Color ColorDefault2
    {
        get
        {
            return ColorFromHex("#FFED25FF");
        }
    }
    public static Color ColorRequire
    {
        get
        {
            return ColorFromHex("#F84949FF");
        }
    }

    public static string ToHexStringRGBA(this Color c)
    {
        return ColorUtility.ToHtmlStringRGBA(c);
    }

    public static float RoundFloat2Decimal(float val)
    {
        return val;
    }

    public static void ResetShaderAllChild(this GameObject go)
    {
        if (go == null) return;
        ResetShaderAllChild(go.transform);
    }

    public static void ResetTransform(this UnityEngine.Object obj)
    {
        //UnityX.ResetTransform(obj);
        var go = obj as GameObject;
        go.GetComponent<RectTransform>();
        go.transform.localScale = Vector3.one;
        go.transform.localEulerAngles = Vector3.zero;
        go.transform.localPosition = Vector3.zero;
    }

    public static void ResetShaderAllChild(this Transform tran)
    {
        if (tran == null) return;

        var rd = tran.GetComponent<Renderer>();
        if (rd != null && rd.sharedMaterials != null)
        {
            foreach (var sharedMaterial in rd.sharedMaterials)
            {
                if (sharedMaterial != null) sharedMaterial.shader = Shader.Find(sharedMaterial.shader.name);
            }
        }

        ResetShader(tran);
    }

    public static void ResetShader(this Transform tran)
    {
        var tranCount = tran.childCount;
        if(tranCount == 0) return;

        for (var i = 0; i < tranCount; i++)
        {
            var child = tran.GetChild(i);
            if(child == null) continue;

            var rd = child.GetComponent<Renderer>();
            if (rd != null && rd.sharedMaterials != null)
            {
                foreach (var sharedMaterial in rd.sharedMaterials)
                {
                    if (sharedMaterial != null) sharedMaterial.shader = Shader.Find(sharedMaterial.shader.name);
                }
            }

            ResetShader(child);
        }
    }

    public static void SetRendererAllChild(this GameObject go, bool isRender)
    {
        if (go == null) return;

        var rd = go.GetComponent<Renderer>();
        if (rd != null) rd.enabled = isRender;

        SetRenderer(go.transform, isRender);
    }

    public static void SetRenderer(this Transform tran, bool isRender)
    {
        var tranCount = tran.childCount;
        if (tranCount == 0) return;

        for (var i = 0; i < tranCount; i++)
        {
            var child = tran.GetChild(i);
            if (child == null) continue;

            var rd = child.GetComponent<Renderer>();
            if (rd != null) rd.enabled = isRender;

            SetRenderer(child, isRender);
        }
    }

    public static string GenerateFakeVersionForWeb()
    {
        return "rnd=" + DateTime.Now.ToString("yyyyMMddHHmmsstt");
    }

    public static bool IsPointerOverGameObject()
    {
        var isPointer = false;

        if (Input.touchSupported && Input.touchCount > 0)
        {
            isPointer = UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId);
        }
        else
        {
            isPointer = UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();
        }

        return isPointer;
    }

    public static string Truncate(this string yourStr, int maxChars)
    {
        //Suppose you have a string yourStr, toView and a constant value 

        string toView;

        if (yourStr.Length > maxChars)
            toView = yourStr.Substring(0, maxChars) + " ..."; // all you have is to use Substring(int, int) .net method
        else
            toView = yourStr;
        return toView;
    }

    public static string GetGameOS()
    {
        #if UNITY_ANDROID
                return "android";
        #endif

        #if UNITY_IOS
                return "ios";
        #endif

        #if UNITY_WEBPLAYER
                return "webplayer";
        #endif

        #if UNITY_WEBGL
                return "webgl";
        #endif

        #if UNITY_WP8 || UNITY_WP8_1
                return "wp";
        #endif
        #if UNITY_STANDALONE
                return "execute"; // Mac, Windows or Linux
        #endif
    }

    public static string[] ConvertToStringArray(this string str, char splitString = ':')
    {
        if (string.IsNullOrEmpty(str)) return new string[0];
        return str.Split(splitString);
    }

    public static string ConvertToString(this List<string> strArray, char splitString = ':')
    {
        if (strArray == null) return "";

        var str = "";
        var count = 0;
        foreach (var s in strArray)
        {
            if (count > 0)
            {
                str += splitString;
            }

            str += s;
            count += 1;
        }

        return str;
    }

    public static CanvasGroup EnableCanvas(this CanvasGroup canvas, bool value)
    {
        canvas.alpha = value ? 1 : 0;

        canvas.interactable = value;

        canvas.blocksRaycasts = value;

        return canvas;
    }

    public static string GetCachePath()
    {
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_IOS
        return Application.temporaryCachePath;
#else
        return Application.persistentDataPath;
#endif
    }

    public class SimpleEncryptDecrypt
    {
        static int tempTextLength = 0;
        static string tempTextList = "0123456789abcdefghijklmnopqABCDEFGHIJKLMNOPQRSTUVWXYZ";
        static string tempTextBegin = "poklhbmnqweopk";
        static int deep = 0;

        public static string Encrypt(string text)
        {
            byte[] bytesToEncode = Encoding.UTF8.GetBytes(text);
            string encodedText = Convert.ToBase64String(bytesToEncode);
            string tempText = "";
            for (int i = 0; i < tempTextLength; i++)
            {
                // tempText += tempTextList[PKUtils.randomInstance.Next(0, tempTextList.Length - 1)];
            }

            // encodedText = encodedText.Substring(0, encodedText.Length - 1);

            encodedText = encodedText + tempText;

            encodedText = SimpleEncryptDecrypt.Encrypt2(encodedText);

            encodedText = tempTextBegin + encodedText;

            return encodedText;

        }

        static string Encrypt2(string encodedText)
        {
            for (int i = 0; i < deep; i++)
            {
                byte[] bytesToEncode = Encoding.UTF8.GetBytes(encodedText);
                encodedText = Convert.ToBase64String(bytesToEncode);
            }


            return encodedText;

        }

        public static string Decrypt(string encodedText)
        {
            if (encodedText.StartsWith(tempTextBegin) == false)
            {
                return encodedText;
            }

            encodedText = encodedText.Substring(tempTextBegin.Length, encodedText.Length - tempTextBegin.Length);

            encodedText = SimpleEncryptDecrypt.Decrypt2(encodedText);

            // encodedText = encodedText.Substring(tempTextBegin.Length, encodedText.Length - tempTextBegin.Length);
            encodedText = encodedText.Substring(0, encodedText.Length - tempTextLength);
            // encodedText += "=";

            byte[] decodedBytes = Convert.FromBase64String(encodedText);
            string decodedText = Encoding.UTF8.GetString(decodedBytes);

            return decodedText;
        }

        static string Decrypt2(string decodedText)
        {
            for (int i = 0; i < deep; i++)
            {
                byte[] decodedBytes = Convert.FromBase64String(decodedText);
                decodedText = Encoding.UTF8.GetString(decodedBytes);
            }

            return decodedText;

        }

        public static bool HasEncrypt(string encodedText)
        {
            return encodedText.StartsWith(tempTextBegin);
        }
    }

    public static string ToCamelString(this string text)
    {
        string[] words = text.ToLower().Split(' ');
        string[] result = new string[words.Length];
        int idx = 0;
        foreach (var w in words)
        {
            if (w.Length > 0)
            {
                result[idx] = w[0].ToString().ToUpper() + w.Substring(1);
                idx++;
            }
        }
        text = string.Join(" ", result);
        return text;
    }

    public static bool HasString(this string text, string toCheck)
    {
        return text.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
    }

    //public static Dictionary<string,object> GetFileData(string fileName)
    //{
    //    var data = new Dictionary<string, object>();
    //    var file = Resources.Load(fileName) as TextAsset;
    //    if (file == null){
    //        DebugX.Log("file data null " + fileName);
    //    }
    //    data = file.text.JsonToDictionary();
    //    return data;
    //}

    public static string ToStringToken(this string text, params string[] tokens)
    {
        if (tokens.Length == 0) return text;
        for (int i = 0; i < tokens.Length; i++)
        {
            //text.("{N}", tokens[i]);
            //text.LastIndexOf();
        }

        return text;
    }
	public static string ToStringToken(this string text, string tokens)
	{
        return text.Replace("{N}", tokens);
	}

    //public static string ConvertNameChip(int valBet)
    //{
    //    switch (valBet)
    //    {
    //        case 100:
    //            return LocalKey.Chip_100;

    //        case 1000:
    //            return LocalKey.Chip_1k;

    //        case 10000:
    //            return LocalKey.Chip_10k;

    //        case 100000:
    //            return LocalKey.Chip_100k;

    //        case 1000000:
    //            return LocalKey.Chip_1m;

    //        case 10000000:
    //            return LocalKey.Chip_10m;
    //    }
    //    return LocalKey.Chip_100;
    //}

#region Set Anchor + Pivot 

    public static void SetAnchor(this RectTransform source, AnchorPresets allign, int offsetX = 0, int offsetY = 0)
    {
        source.anchoredPosition = new Vector3(offsetX, offsetY, 0);

        switch (allign)
        {
            case (AnchorPresets.TopLeft):
                {
                    source.anchorMin = new Vector2(0, 1);
                    source.anchorMax = new Vector2(0, 1);
                    break;
                }
            case (AnchorPresets.TopCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 1);
                    source.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
            case (AnchorPresets.TopRight):
                {
                    source.anchorMin = new Vector2(1, 1);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }

            case (AnchorPresets.MiddleLeft):
                {
                    source.anchorMin = new Vector2(0, 0.5f);
                    source.anchorMax = new Vector2(0, 0.5f);
                    break;
                }
            case (AnchorPresets.MiddleCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0.5f);
                    source.anchorMax = new Vector2(0.5f, 0.5f);
                    break;
                }
            case (AnchorPresets.MiddleRight):
                {
                    source.anchorMin = new Vector2(1, 0.5f);
                    source.anchorMax = new Vector2(1, 0.5f);
                    break;
                }

            case (AnchorPresets.BottomLeft):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(0, 0);
                    break;
                }
            case (AnchorPresets.BottonCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0);
                    source.anchorMax = new Vector2(0.5f, 0);
                    break;
                }
            case (AnchorPresets.BottomRight):
                {
                    source.anchorMin = new Vector2(1, 0);
                    source.anchorMax = new Vector2(1, 0);
                    break;
                }

            case (AnchorPresets.HorStretchTop):
                {
                    source.anchorMin = new Vector2(0, 1);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }
            case (AnchorPresets.HorStretchMiddle):
                {
                    source.anchorMin = new Vector2(0, 0.5f);
                    source.anchorMax = new Vector2(1, 0.5f);
                    break;
                }
            case (AnchorPresets.HorStretchBottom):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(1, 0);
                    break;
                }

            case (AnchorPresets.VertStretchLeft):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(0, 1);
                    break;
                }
            case (AnchorPresets.VertStretchCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0);
                    source.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
            case (AnchorPresets.VertStretchRight):
                {
                    source.anchorMin = new Vector2(1, 0);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }

            case (AnchorPresets.StretchAll):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }
        }
    }

    public static void SetPivot(this RectTransform source, PivotPresets preset)
    {

        switch (preset)
        {
            case (PivotPresets.TopLeft):
                {
                    source.pivot = new Vector2(0, 1);
                    break;
                }
            case (PivotPresets.TopCenter):
                {
                    source.pivot = new Vector2(0.5f, 1);
                    break;
                }
            case (PivotPresets.TopRight):
                {
                    source.pivot = new Vector2(1, 1);
                    break;
                }

            case (PivotPresets.MiddleLeft):
                {
                    source.pivot = new Vector2(0, 0.5f);
                    break;
                }
            case (PivotPresets.MiddleCenter):
                {
                    source.pivot = new Vector2(0.5f, 0.5f);
                    break;
                }
            case (PivotPresets.MiddleRight):
                {
                    source.pivot = new Vector2(1, 0.5f);
                    break;
                }

            case (PivotPresets.BottomLeft):
                {
                    source.pivot = new Vector2(0, 0);
                    break;
                }
            case (PivotPresets.BottomCenter):
                {
                    source.pivot = new Vector2(0.5f, 0);
                    break;
                }
            case (PivotPresets.BottomRight):
                {
                    source.pivot = new Vector2(1, 0);
                    break;
                }
        }
    }
#endregion
}

public enum AnchorPresets
{
    TopLeft,
    TopCenter,
    TopRight,

    MiddleLeft,
    MiddleCenter,
    MiddleRight,

    BottomLeft,
    BottonCenter,
    BottomRight,
    BottomStretch,

    VertStretchLeft,
    VertStretchRight,
    VertStretchCenter,

    HorStretchTop,
    HorStretchMiddle,
    HorStretchBottom,

    StretchAll
}

public enum PivotPresets
{
    TopLeft,
    TopCenter,
    TopRight,

    MiddleLeft,
    MiddleCenter,
    MiddleRight,

    BottomLeft,
    BottomCenter,
    BottomRight,
}

public class Tag
{
    public const string UICamera                                 = "UICamera";
    public const string UI                                       = "UI";
    public const string TagCanvas                                = "canvas";
    public const string Tag3D                                    = "3d";
    public const string Environment                              = "Environment";
    public const string TagTransport                             = "transport";
    public const string TagBar                                   = "bar";
    public const string TagBlacksmith                            = "blacksmith";
    public const string TagMe                                    = "me";

    public const string TagBack                                  = "Back";
    public const string TagMain                                  = "Main";
    public const string TagMenu                                  = "Menu";
    public const string TagMenuFloating                          = "MenuFloating";
    public const string TagPopup                                 = "Popup";
    public const string TagPopupAward                            = "PopupAward";
    public const string TagPopupNotify                           = "PopupNotify";
    public const string TagSystemPopup                           = "SystemPopup";
    public const string TagLoadingSceneCanvas                    = "LoadingSceneCanvas";

    public const string TutCreateNPC                             = "TutCreateNPC";
    public const string Guild                                    = "Guild";
    public const string BattleCamera                             = "BattleCamera";
    public const string CharRender                               = "CharRender";

    public const string NpcName                                  = "NpcName";
    public const string CharacterName                            = "CharacterName";

    public const string BattleManager                            = "BattleManager";
}
