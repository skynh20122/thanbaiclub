﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;


public class XengHoaQuaResult
{

    public static XengHoaQuaResult GenarateFruitResult(params int[] bets)
    {
        if (instance == null)
            instance = new XengHoaQuaResult();

        instance.Data = instance.Spin(bets);
        return instance;
    }
    public static int GenarateTaiXiuResult(bool tai)
    {
        if (instance == null)
            instance = new XengHoaQuaResult();

        return instance.spinTaiXiu(tai);
    }

    private static XengHoaQuaResult instance;
    
	public resultFruitSlot Data;

	List<int[]> normalPos = ratioConfig.normalPos (); // Bảng vị trí
	//void Start () {

	//	//Spin (new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
	//	//spinTaiXiu (true);
	//}

	public resultFruitSlot Spin(int[] bet)
	{
		// reset win chip
		var winChips = 0;

		Data = new resultFruitSlot ();

		// 1. Quay lượt đầu
		int[] winPos = spinCircle(false);

		// 2 Kiếm tra luck
		bool isLuck = false;
		bool isLuckFalse = true;

		// 2.1 Nếu vào ô luck
		if (winPos[1] == -1)
		{
			isLuck = true;
			float randomIsLuck = Random.Range (1, 100) / 100f;

			if (randomIsLuck > ratioConfig.luckFalse)
				isLuckFalse = false;			
		}
		// 2.2 Nếu vào ô thường
		else
		{
			isLuck = false;
			float randomIsLuck = Random.Range (1, 100) / 100f;
			if (randomIsLuck < ratioConfig.luckMore)
				isLuckFalse = false;		
		}

		// 3. Quay luck
		int randomLuck = 0;
		int numberOfLuck = 0;
		bool isBigLuck = false; // Vị trí 6 là small luck
		int[][] luckPosition = null;

		if (!isLuckFalse) // Nếu không xịt thì xét tiếp
		{
			randomLuck = Random.Range(1, 100);
			if (winPos[0] == normalPos [0] [1]) // Vi tri 18 là big luck
				isBigLuck = true;

			if (!isBigLuck)
			{
				// Thưởng tối thiểu 4
				numberOfLuck = 4;
				for (int i = 0; i < ratioConfig.ratioBigLuck.Length; i++) {
					//Debug.LogError (firstRandom + " " + ratioConfig.normalRatio [i]); 
					if (randomLuck > ratioConfig.ratioBigLuck [i])
						numberOfLuck = ratioConfig.numberBigLuck[i];
				}
			}
			else
			{
				// Thưởng tối thiểu 1
				numberOfLuck = 1;
				for (int i = 0; i < ratioConfig.ratioSmallLuck.Length; i++) {
					//Debug.LogError (firstRandom + " " + ratioConfig.normalRatio [i]); 
					if (randomLuck > ratioConfig.ratioSmallLuck [i])
						numberOfLuck = ratioConfig.numberSmallLuck[i];
				}
			}
		}

		// Số luck > 0 thì bắn
		if (numberOfLuck > 0)
		{
			luckPosition = new int[numberOfLuck][];
			for (int i = 0; i < numberOfLuck; i++)
			{
				luckPosition[i] = spinCircle (true);
			}
		}

        // TÍNH TIỀN
        int betChips = bet.Sum();

		// TIỀN THẮNG THƯỜNG
		winChips += getWinChip(bet, winPos);
		if (numberOfLuck > 0)
		{
			for (int i = 0; i < numberOfLuck; i++)
			{
				winChips += getWinChip(bet, luckPosition[i]);
			}
		}

        // TỔNG HỢP KẾT QUẢ
        Data.bet = betChips;
		Data.winPos = winPos[0]; // KẾT QUẢ - VỊ TRÍ
		Data.isLuck = isLuck;
		Data.isLuckFalse = isLuckFalse;
		Data.numberOfLuck = numberOfLuck;
        if(luckPosition != null)
            Data.luckPosition = luckPosition.Select(x => x[0]).ToArray();
		Data.winChips = winChips;

		// IN KẾT QUẢ
		Debug.LogError ("VỊ TRÍ: " + Data.winPos);
		Debug.LogError ("TỈ LỆ NHÂN: " + winPos[1]);
		Debug.LogError ("QUAY VÀO LUCK: " + Data.isLuck);
		Debug.LogError ("LUCK XỊT: " + Data.isLuckFalse);
		Debug.LogError ("SỐ BẮN LUCK: " + Data.numberOfLuck);
		if (Data.numberOfLuck > 0) {
			for (int i = 0; i < numberOfLuck; i++) {
				Debug.Log("LUCK " + (i+1) + " LÀ Ô " +  luckPosition[i][0].ToString() + " TRÚNG X" + luckPosition[i][1].ToString() );  
			}
		}
		Debug.LogError ("SỐ TIỀN TIÊU TỐN: "+ betChips);
		Debug.LogError ("SỐ TIỀN THẮNG: "+ winChips);
		return Data;
	}

	public int[] spinCircle (bool luck)
	{
		// 1. Quay lượt đầu
		int firstRandom;

		if (luck == false) 
			firstRandom = Random.Range(1, 1000); // Số random từ 1 - 1000
		else
			firstRandom = Random.Range(ratioConfig.normalRatio[0] + 1, 1000); // Số random từ 1 - 1000
		int firstMultiple = 0; //  Tỉ lệ nhân (kết quả) mỗi lần spin thì reset


		// 1.1 Kiểm tra tỉ lệ nhân
		for (int i = 0; i < ratioConfig.normalRatio.Length; i++) {
			//Debug.LogError (firstRandom + " " + ratioConfig.normalRatio [i]); 
			if (firstRandom > ratioConfig.normalRatio [i])
				firstMultiple = i+1;
		}

		// 1.2 Tìm vị trí
		int[] listPos = normalPos [firstMultiple]; // Vị trí theo tỉ lệ nhân
		int randomPos = Random.Range (1, listPos.Length); // Vị trí ngẫu nhiên có tỉ lệ nhân giống nhau

		int[] winPos = new int[2];
		winPos[0] =	normalPos [firstMultiple] [randomPos - 1];
		winPos[1] =	ratioConfig.normalMultiple[firstMultiple];

		return winPos;
	}

	public int getWinChip(int[] bet, int[] winPos)
	{
		int winChips = 0;
		//Ktra xem có đặt cửa ko
		string namePos=  fruitSlotSetting.posDic[winPos[0]];
		Debug.LogError (namePos);
		if (bet [0] > 0 && namePos == "APPLE")
			winChips += bet [0] * winPos [1];
		if (bet [1] > 0 && namePos == "ORANGE")
			winChips += bet [1] * winPos [1];
		if (bet [2] > 0 && namePos == "MANGO")
			winChips += bet [2] * winPos [1];
		if (bet [3] > 0 && namePos == "BELL")
			winChips += bet [3] * winPos [1];
		if (bet [4] > 0 && namePos == "WATERMELON")
			winChips += bet [4] * winPos [1];
		if (bet [5] > 0 && namePos == "STAR")
			winChips += bet [5] * winPos [1];
		if (bet [6] > 0 && namePos == "77")
			winChips += bet [6] * winPos [1];
		if (bet [7] > 0 && namePos == "BAR")
			winChips += bet [7] * winPos [1];

		return winChips;
	}

	public int spinTaiXiu(bool Tai)
	{		
		if (Data.winChips == 0)
			return 0;
		int result;
		bool win;
		float randomTaiXiu = Random.Range (1, 100) / 100f;
		if (randomTaiXiu < ratioConfig.winSpin) 
		{
			win = true; // cùng cửa
			if (Tai == true)
				result = Random.Range (8, 14);
			else
				result = Random.Range (1, 7);
		} else
		{
			win = false; // lệch cửa
			if (Tai == true)
				result = Random.Range (1, 7);
			else
				result = Random.Range (8, 14);
		} 	

		if (win)
            Data.winChips += Data.winChips;
		else
            Data.winChips = 0;
		
		Debug.LogError ("TÀI XỈU: " + result);
		Debug.LogError ("SỐ TIỀN THẮNG: "+ Data.winChips);
		return result;


		// result = 0 : KHÔNG ĐỦ TIỀN
	}

	public bool convertToBalance(int chips)
	{
		if (chips > Data.winChips)
		{
			return false;
		}
		else
		{
            Data.winChips -= chips;

			return true;
		}

	}
}
