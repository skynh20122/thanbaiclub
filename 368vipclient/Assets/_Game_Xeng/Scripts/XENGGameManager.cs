﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System;

public class XENGGameManager : MonoBehaviour {

	public NumberAddEffect moneyLabel;
    public NumberAddEffect currentBetLabel;
    public readonly string[] itemName = { "apple", "apple", "mango", "watermelon", "watermelon", "lucky", "apple", "orange", "orange", "bell", "seven",
                                            "seven", "apple", "mango", "mango", "star", "star", "lucky", "apple", "bell", "orange", "bell", "bar", "bar"};
    public readonly string[] itemButtonName = { "apple", "orange", "mango", "bell", "watermelon", "star", "seven", "bar" };
    public XENGItem[] items;
    public AudioClip[] sounds;
    public bool canSpin, isNewGame;

    public bool inWinEvent;
    public bool inSpinWinEvent;
    public bool inLuckWinEvent;
    public int[] bets;
    public int[] prevBets;
    public XENGFSM fsm;
    public XENGState idleState, startSpin, fastSpin, endSpin, luckWaitState, luckStartState, luckEndState, luckMoveType1, luckMoveType2, winState, startNumberSpin, endNumberSpin, toIdleState;
    public bool goToWinState;
	public XengResponse data;
    public int luckType;
    public int numberLightVisible;
    public int currentNumberRandom;
    public int originWinChip;
    public AudioSource[] audios;
    public AvatarView meView;
	public int betRoomValue;
	public bool isAutoJoinRoom = false;
    int currentIndex;
    Dictionary<string, AudioClip> soundDics;

    void Awake()
    {
        bets = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        prevBets = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        isNewGame = true;
    }
    void Start () {
        soundDics = new Dictionary<string, AudioClip>();
        foreach (var s in sounds)
        {
            if (s != null)
                soundDics.Add(s.name, s);
        }

        currentIndex = 0;
        luckType = 1;
        numberLightVisible = 0;
        currentNumberRandom = 0;
        inWinEvent = false;
        inLuckWinEvent = false;
        inSpinWinEvent = false;

        items[0].SetLight(true);

        idleState = new IdleState { name = "IdleState" };
        startSpin = new StartSpin { name = "StartSpin" };
        fastSpin = new FastSpin { name = "FastSpin" };
        endSpin = new EndSpin { name = "EndSpin" };
        luckWaitState = new LuckyWaitState { name = "LuckyWaitState" };
        luckStartState = new LuckyStartState { name = "LuckyStartState" };
        luckEndState = new LuckyEndState { name = "LuckyEndState" };
        luckMoveType1 = new LuckMoveType1State { name = "LuckMoveType1State" };
        luckMoveType2 = new LuckMoveType2State { name = "LuckMoveType2State" };
        winState = new WinState { name = "WinState" };
        startNumberSpin = new StartNumberSpin { name = "StartNumberSpin" };
        endNumberSpin = new EndNumberSpin { name = "EndNumberSpin" };
        toIdleState = new ToIdleState { name = "toIdleState" };

        fsm = new XENGFSM
        {
            entity = this,
            current_state = idleState
        };
        fsm.ChangeState(idleState);
    }

	public void OnEnable()
	{
        meView.FillData(OGUIM.me);
		moneyLabel.FillData(0);
		UpdateUI();
       
        OGUIM.instance.lobbyName.text = "";
		OGUIM.instance.roomBetValue.text = "";
		if (WarpClient.wc != null)
		{
			WarpClient.wc.OnGetRoomsDone += Wc_OnGetRoomsDone;
			WarpClient.wc.OnJoinRoomDone += Wc_OnJoinRoomDone;
			WarpClient.wc.OnLeaveRoom += Wc_OnLeaveRoom;
			WarpClient.wc.OnXengStart += Wc_OnXengStart;
			WarpClient.wc.OnXengReceiveMoney += Wc_OnXengReceiveMoney;
			WarpClient.wc.OnXengBetTaiXiu += WarpClient_wc_OnXengBetTaiXiu;
			OGUIM.listRoomXeng = null;
			AutoCheckAndJoinRoom(true);
		}
	}

	public void OnDisable()
	{
		if (WarpClient.wc != null)
		{
			WarpClient.wc.OnGetRoomsDone -= Wc_OnGetRoomsDone;
			WarpClient.wc.OnJoinRoomDone -= Wc_OnJoinRoomDone;
			WarpClient.wc.OnLeaveRoom -= Wc_OnLeaveRoom;
			WarpClient.wc.OnXengStart -= Wc_OnXengStart;
			WarpClient.wc.OnXengReceiveMoney -= Wc_OnXengReceiveMoney;
			WarpClient.wc.OnXengBetTaiXiu -= WarpClient_wc_OnXengBetTaiXiu;

            WarpRequest.GetUserInfo(OGUIM.me.id);
        }
    }

	public void AutoCheckAndJoinRoom(bool autoJoinRoom)
	{
		isAutoJoinRoom = autoJoinRoom;
		if (OGUIM.listRoomXeng == null || !OGUIM.listRoomXeng.Any())
		{
			
			//OGUIM.Toast.ShowLoading("Đang tải dữ liệu, vui lòng chờ giây lát...");
			OGUIM.isListen = false;
			WarpRequest.GetRooms(OGUIM.currentLobby.id);
			return;
		}
		else
		{
			
            //			for (int i = 0; i < OGUIM.listRoomSlot.Count; i++)
            //			{
            //				listJackpotLabel[i].FillData(OGUIM.listRoomSlot[i].funds);
            //				listJackpotLabel[i].name = OGUIM.listRoomSlot[i].id.ToString();
            //				listJackpotView[i].name = OGUIM.listRoomSlot[i].id.ToString();
            //
            //				FillDataInPopUp();
            //			}
            //UIXeng.instance.FillDataInPopUp();
			var checkRoom = OGUIM.listRoomXeng.OrderBy(x => x.bet).FirstOrDefault();
			if (checkRoom != null && isAutoJoinRoom)
			{
				OGUIM.isListen = false;
				WarpRequest.JoinRoom(OGUIM.currentLobby.onlygold ? GameBase.moneyGold.type : OGUIM.currentMoney.type, checkRoom.bet);
			}
		}
	}

	private void Wc_OnGetRoomsDone(WarpResponseResultCode status, List<RoomSlot> data)
	{
		if (status == WarpResponseResultCode.SUCCESS && data.Any())
		{
			OGUIM.listRoomXeng = data;
			AutoCheckAndJoinRoom(isAutoJoinRoom);
		}
		else
		{
			OGUIM.Toast.Show("Không thành công. Vui lòng thử lại...", UIToast.ToastType.Warning);
		}
	}

	private void Wc_OnJoinRoomDone(WarpResponseResultCode status, Room data)
	{
		if (status == WarpResponseResultCode.SUCCESS && data != null && data.room != null)
		{
            Debug.Log("XENG ROOM: " + data.room.id + "      "+ data.room.bet);

//			for (int i = 0; i < listJackpotView.Count; i++)
//			{
//				if (listJackpotView[i].name != data.room.id.ToString())
//					listJackpotView[i].SetActive(false);
//				else
//					listJackpotView[i].SetActive(true);
//			}

			OGUIM.instance.currentLobbyId = LobbyId.SLOT;
			OGUIM.currentRoom = data;
			OGUIM.autoLeaveRoom = false;
			currentBetLabel.FillData(data.room.bet, "", 50);
			UpdateUI();

            // reset after change room
            UIXeng.ResetChips(false, true);

            OGUIM.Toast.Hide();

			if (UIXeng.instance.popUpSelectRoom != null &&
                UIXeng.instance.popUpSelectRoom.state == UIAnimation.State.IsShow)
                UIXeng.instance.popUpSelectRoom.Hide();

			OGUIM.instance.lobbyName.text = OGUIM.currentLobby.desc.ToUpper();// + " - " + LongConverter.ToK(betRoomValue)
			OGUIM.instance.roomBetValue.text = LongConverter.ToFull(betRoomValue) + " " + OGUIM.currentMoney.name;
		}
		else
		{
			OGUIM.Toast.Show("Không thành công. Vui lòng thử lại...", UIToast.ToastType.Warning);
			//OGUIM.UnLoadGameScene();
			OGUIM.Toast.Hide();
		}
		OGUIM.isListen = true;
	}

	private void Wc_OnLeaveRoom(Room room, int status)
	{
		if (status == (int)WarpResponseResultCode.SUCCESS &&  UIXeng.instance.nextBetRoomValue != 0)
		{
			for (int i = 0; i < bets.Length; i++) 
			{
				OGUIM.me.gold += Convert.ToInt32(UIXeng.instance.txtBets[i].text);
			}
			UIXeng.ResetChips (false, true);
			
			OGUIM.isListen = false;
			WarpRequest.JoinRoom(OGUIM.currentLobby.onlygold ? GameBase.moneyGold.type : OGUIM.currentMoney.type, UIXeng.instance.nextBetRoomValue);
            UIXeng.instance.nextBetRoomValue = 0;


        }
		else
        {
            Debug.Log("UIXeng.instance.musicToggle " + UIXeng.instance.musicToggle);
            if (UIXeng.instance.musicToggle)
            {
                UIManager.instance.musicToggle.isOn = true;
                UIManager.instance.UpdateMusicState();
                UIXeng.instance.musicToggle = false;
            }
			OGUIM.Toast.Hide();
        }
	}
		
	private void Wc_OnXengStart (XengResponse _data)
	{
		data = _data;
        //Debug.Log("win chip " + data.winChips);
		UIXeng.instance.isSpining = true;
		OGUIM.me.gold = data.chips;
		originWinChip = data.result.winChips;
		goToWinState = data.result.winChips > 0;
		luckType = UnityEngine.Random.Range(1, 3);
		UIXeng.ResetChips ();
		ResetLight();
		fsm.ChangeState(startSpin);
        OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold,OGUIM.me.gold);
        OGUIM.me.gold = data.chips + data.result.winChips;
        //OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, data.chips + data.result.winChips);
	}

	private void Wc_OnXengReceiveMoney (XengResponse _data)
	{
		data = _data;
		OGUIM.me.gold = data.chips;
		ExitWinEvent();
		if(data.winChips > 0)
        {
            fsm.ChangeState(toIdleState);
        }
		else
		{
			fsm.ChangeState(idleState);
			UIXeng.ClearAllBet ();
		}
		OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, data.chips);
	}

	private void WarpClient_wc_OnXengBetTaiXiu (XengResponse _data)
	{
		data = _data;
		OGUIM.me.gold = data.chips;
		currentNumberRandom = data.result.number;
        if (data.result.winChips > 0)
            goToWinState = true;
		fsm.ChangeState(startNumberSpin);
		OGUIM.instance.meView.moneyView.FillData(MoneyType.Gold, data.chips + data.result.winChips);
	}

    void Update()
    {
        fsm.Update(Time.deltaTime);
    }

	public void UpdateUI()
	{
		if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null)
			betRoomValue = OGUIM.currentRoom.room.bet;
        if(moneyLabel != null)
		moneyLabel.FillData(OGUIM.me.gold);
        OGUIM.instance.meView.moneyView.FillData(OGUIM.me);
        //totalLine = listToggleLineOnGame.Count(x => x.isOn);
        //totalBet = totalLine * betRoomValue;

        //totalLineLabelInButton.text = totalLine + "";
        //totalBetLabel.FillData(totalBet);

        //listLineSelected = new List<int>();
        //		foreach (var i in listToggleLineOnGame.Where(x => x.isOn))
        //		{
        //			var index = listToggleLineOnGame.IndexOf(i) + 1;
        //			listLineSelected.Add(index);
        //		}
        //
        //		if (totalLine > 0)
        //			totalLineLableInPopup.text = "Bạn đã chọn " + "<b><color=#FFC800FF>" + totalLine + "</color></b>" + " dòng, tổng cược là " + "<b><color=#FFC800FF>" + LongConverter.ToK(totalBet) + "</color></b>" + " iCash";
        //		else
        //			totalLineLableInPopup.text = "Vui lòng chọn dòng cược! Chúc bạn may mắn!";
        //
        //		if (buttonTrial.toggle.isOn)
        //		{
        //			buttonTrial.UpdateTextContent("THỬ");
        //			buttonSpinStatusLabel.text = "- THỬ -";
        //		}
        //		else
        //		{
        //			buttonTrial.UpdateTextContent("THẬT");
        //			buttonSpinStatusLabel.text = "";
        //		}
    }
    public void ResetGame(bool all = false)
    {

        for (int i = 0; i < bets.Length; i++)
        {
            if (all)
                prevBets[i] = 0;
            else
                prevBets[i] = bets[i];
            bets[i] = 0;
        }

        numberLightVisible = 0;
        luckType = 1;
        currentNumberRandom = 0;
    }
    public void ResetLight()
    {
        for (int i = 0; i < items.Length; i++)
        {
            TurnLight(i, false);
            HighLight(i, false);
        }
        items[GetCurrentIndex()].SetLight(true);
    }
    public void SetBet(int door)
    {
        bets[door]++;
    }
    public string GetBet(int door)
    {
        return bets[door].ToString("00");
    }
    public int GetCurrentIndex()
    {
        return currentIndex % items.Length;
    }
    public int GetRangePosition(int index1, int index2)
    {
        int range = 0;

        while(index1 != index2)
        {
            index1++;
            index1 = index1 % items.Length;

            range++;
        }

        return range;
    }
    public void TurnLight(float x, bool isOn = true)
    {
        int index = Mathf.FloorToInt(x) % items.Length;
        if (index < 0) index = 0;
        if (isOn)
        {
            int prev = index == 0 ? 23 : (index - 1);
            items[index].SetLight(true);
            items[prev].SetLight(false);
        }
        else
        {
            items[index].SetLight(false);
        }
    }
    public void HighLight(float x, bool isOn)
    {
        int index = Mathf.FloorToInt(x) % items.Length;
        if (index < 0) index = 0;
        Debug.Log("??? " + x);
        items[index].SetHighlight(isOn);
    }

    public void MoveLightPosition()
    {
        currentIndex++;
        TurnLight(currentIndex);
    }
    public void PlaySound(string name, int channel, bool loop = false)
    {
        if (!soundDics.ContainsKey(name))
            return;
        var clip = soundDics[name];
        audios[channel].Stop();
        audios[channel].clip = clip;
        audios[channel].loop = loop;
        audios[channel].Play();
    }
    public void ExitWinEvent()
    {
        if (inWinEvent)
        {
            inWinEvent = false;
            PlaySound("win", 0);
        }
    }
    public bool Equals(int indexItem, int indexButton)
    {
        var length = items.Length;
        return itemName[indexItem % length] == itemButtonName[indexButton % length];
    }
}
