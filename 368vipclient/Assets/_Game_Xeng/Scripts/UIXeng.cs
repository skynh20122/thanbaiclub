﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class UIXeng : MonoBehaviour {

    public static void ClearAllBet()
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/ClearAllBet: instance is not exist!");
            return;
        }
        for (int i = 0; i < instance.txtBets.Length; i++)
        {
            ResetChips(true);
            instance.txtBets[i].text = "00";
            //instance.animBets[i].SetBool("isOn", false);
        }
    }
    public static void ClearBet(int index)
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/ClearAllBet: instance is not exist!");
            return;
        }
        instance.txtBets[index].text = "0";
    }
    public static void SpawnTextEfx(XENGGameManager entity, bool active = true)
    {
        var str = Ultility.CoinToString(entity.data.result.winChips);
        var te = instance.textEfxPrefab.Spawn();
        te.gameObject.transform.SetParent(instance.transform, false);
        te.gameObject.transform.position = Vector3.zero;
        te.gameObject.transform.localScale = Vector3.one ;

        te.SetData(str.ToUpper(), active ? Color.yellow : Color.black, 0, 0);
    }

    public static void fillChip()
    {
        BuildWarpHelper.XENG_ReceiveMoney(() =>
        {
            UILogView.Log("ReceiveMoney is timeout");
            OGUIM.Toast.Hide();
        });
    }
    public static void ResetChips(bool ignoreWinChips = false, bool changeChipInMain = false)
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/SetChips: instance is not exist!");
            return;
        }

        if (!ignoreWinChips)
        {
            instance.txtWinChips.text = "0";
        }
		string moneyDisplay;

        if (OGUIM.currentRoom != null && OGUIM.currentRoom.room != null)
        {
            var betMoney = OGUIM.me.gold / OGUIM.currentRoom.room.bet;

            if (betMoney > 10000000)
                moneyDisplay = LongConverter.ToK(betMoney);
            else
                moneyDisplay = betMoney.ToString();

            instance.txtUserMoney.text = moneyDisplay;
            instance.gameManager.moneyLabel.FillData(OGUIM.me.gold);
            //Debug.Log("CHIPS: " + OGUIM.me.gold);
        }
    }
    public static void SetWinChips(int chip)
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/SetWinChips: instance is not exist!");
            return;
        }
        instance.txtWinChips.text = chip + "";
    }

    public static void SetTaiXiu(int taixiu)
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/SetTaiXiu: instance is not exist!");
            return;
        }
        instance.txtTaiXiu.text = taixiu.ToString();
    }

    public static Text[] GetTxtBets()
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/GetTxtBets: instance is not exist!");
            return null;
        }
        return instance.txtBets;
    }

    public static Animator[] GetAnimBets()
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/GetAnimBets: instance is not exist!");
            return null;
        }
        return instance.animBets;

    }

    public static void MoveWinCreditToCredits(int step)
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/MoveWinCreditToCredits: instance is not exist!");
            return;
        }
		if (instance.gameManager.data.result.winChips > 0) {
			instance.StackChip (true, step);
		} else 
		{
			// Reset lai gold cho dung data
			OGUIM.me.gold = instance.gameManager.data.chips;
			ResetChips(false, true);
			instance.gameManager.fsm.ChangeState (instance.gameManager.idleState);
			UIXeng.ClearAllBet ();
		}
    }

    public static void DoJackPot()
    {
        if (instance == null)
        {
            Debug.Log("XENGGameManager/DoJackPot: instance is not exist!");
            return;
        }
        instance.efxJackpot.Active();
    }

    public List<LobbyView> listXengRoomView;
    public XENGGameManager gameManager;
    public int nextBetRoomValue;
    public Effect_Jackpot efxJackpot;
    public Text[] txtBets;
    public Animator[] animBets; //=========================================
    public Text txtUserMoney, txtWinChips, txtTaiXiu;
	public static UIXeng instance;
	public bool isSpining = false;
    public UIAnimation popUpSelectRoom;
    public bool musicToggle;
    public TextEffext textEfxPrefab;
    public AvatarView meView;
    public Dropdown dropdownBet;

    bool musicCheck = false;

    void Awake()
    {
        instance = this;
        meView.moneyView.FillData(OGUIM.me);
        DOTween.Init();
        musicCheck = UIManager.instance.musicToggle.isOn;
        if (musicCheck)
            UIManager.instance.MuteMusic();
        ResetChips();
        if (UIManager.instance.musicToggle.isOn)
        {
            UIManager.instance.musicToggle.isOn = false;
            UIManager.instance.UpdateMusicState();
            musicToggle = true;
        }
        Backgound.instance.setBackground("bg_xeng");
        MenuTopManager.instance.setMenu(MenuType.RED);
        //OGUIM.instance.chatInGameBtn.SetActive(false);
    }

    public void betValueChange()
    {
        int roomBet = dropdownBet.value;
        
        ChangeBetRoom(roomBet);
    }

    public void ChangeBetRoom(int roomIndex = -1)
    {
        if (!gameManager.fsm.IsInState("IdleState"))
        {
            OGUIM.Toast.ShowNotification("Vui lòng đợi lượt quay kết thúc...!");
            if (popUpSelectRoom != null && popUpSelectRoom.state == UIAnimation.State.IsShow)
                popUpSelectRoom.Hide();
            return;
        }

        
        if (OGUIM.listRoomXeng == null || !OGUIM.listRoomXeng.Any())
        {
            WarpRequest.GetRooms(OGUIM.currentLobby.id);
        }
        else if (OGUIM.listRoomXeng.Count >= 3)
        {
            if (roomIndex == -1)
            {
                nextBetRoomValue = 0;
                if (OGUIM.currentRoom.room.bet == OGUIM.listRoomXeng[0].bet)
                {
                    nextBetRoomValue = OGUIM.listRoomXeng[1].bet;
                }
                else if (OGUIM.currentRoom.room.bet == OGUIM.listRoomXeng[1].bet)
                {
                    nextBetRoomValue = OGUIM.listRoomXeng[2].bet;
                }
                else if (OGUIM.currentRoom.room.bet == OGUIM.listRoomXeng[2].bet)
                {
                    nextBetRoomValue = OGUIM.listRoomXeng[0].bet;
                }
            }
            else
            {
                nextBetRoomValue = OGUIM.listRoomXeng[roomIndex].bet;
                if (nextBetRoomValue == OGUIM.currentRoom.room.bet)
                {
                    popUpSelectRoom.Hide();
                    return;
                }
            }

            // reset before change room
            UIXeng.instance.btnCancel_clicked();

            OGUIM.isListen = false;
            BuildWarpHelper.LeaveRoom(null);
        }
    }

    public void FillDataInPopUp()
    {
        var lobbySlotStatus = LobbyViewListView.listLobbiesStatus.FirstOrDefault(x => (int)x.zoneId == (int)LobbyId.XENG_HOAQUA);
        long players = 100;
        if (lobbySlotStatus != null)
            players = lobbySlotStatus.player + UnityEngine.Random.Range(-10, 20);

        for (int i = 0; i < OGUIM.listRoomXeng.Count; i++)
        {

            if (listXengRoomView[i].lobbyData == null)
            {
                listXengRoomView[i].nameText.text = LongConverter.ToFull(OGUIM.listRoomXeng[i].bet);
                listXengRoomView[i].lobbyData = new Lobby
                {
                    tables = OGUIM.listRoomXeng[i].funds,
                    id = OGUIM.listRoomXeng[i].id
                };
            }

            if (listXengRoomView[i].lobbyData != null)
            {
                listXengRoomView[i].lobbyData.tables = OGUIM.listRoomXeng[i].funds;
                if (i == 0)
                    listXengRoomView[i].lobbyData.players = (long)(players * 0.6);
                else if (i == 1)
                    listXengRoomView[i].lobbyData.players = (long)(players * 0.3);
                else if (i == 2)
                    listXengRoomView[i].lobbyData.players = (long)(players * 0.1);
            }
            //listXengRoomView[i].UpdateStatus();
        }
    }

    public void btnQuay_Clicked()
    {
        if (gameManager.fsm.IsInState("IdleState"))
        {
            if (!gameManager.canSpin)
            {
				OGUIM.Toast.ShowNotification("Vui lòng đặt cửa..!");
			}
			else if (isSpining)
			{
				OGUIM.Toast.ShowNotification("Vui lòng chờ lượt quay kết thúc..!");
			}
            else
			{
                for (int i = 0; i < 8; i++)
                {
                    txtBets[i].text = gameManager.GetBet(i);
                }
                SetTaiXiu(0);
                ResetChips();			
				
				BuildWarpHelper.XENG_Spin(gameManager.bets, () =>
					{
						UILogView.Log("Spin is timeout");
						isSpining = false;
						OGUIM.Toast.Hide();
					});
            }
        }
        else if (gameManager.fsm.IsInState("WinState"))
        {
			BuildWarpHelper.XENG_ReceiveMoney(() =>
				{
					UILogView.Log("ReceiveMoney is timeout");
					OGUIM.Toast.Hide();
				});
        }
        else if (gameManager.fsm.IsInState("ToIdleState"))
		{
			UILogView.Log("CHUYỂN SANG TRẠNG THÁI CHỜ");
            gameManager.fsm.ChangeState(gameManager.idleState);
        }
        else
        {
            Debug.Log("Vui lòng đợi..!");
        }
    }

    public void btnBet_Clicked(int door)
    {
        if (gameManager.fsm.IsInState("IdleState"))
        {
            if (!gameManager.isNewGame)
            {
                gameManager.isNewGame = true;
                ResetChips();
                gameManager.ResetGame(true);
                ClearAllBet();
            }
            if (door == -1)
            {
				if (OGUIM.me.gold < 8 * OGUIM.currentRoom.room.bet)
                {
					OGUIM.Toast.ShowNotification("Cần thêm tiền để đặt cược..!");
                    return;
                }
                for (int i = 0; i < 8; i++)
                {
                    gameManager.SetBet(i);
                    txtBets[i].text = gameManager.GetBet(i);
                }
				OGUIM.me.gold -= 8 * OGUIM.currentRoom.room.bet;
                ResetChips();
            }
            else
			{
				if (OGUIM.me.gold < OGUIM.currentRoom.room.bet)
				{
					OGUIM.Toast.ShowNotification("Cần thêm tiền để đặt cược..!");
					return;
				}
                gameManager.SetBet(door);
				txtBets[door].text = gameManager.GetBet(door);
				OGUIM.me.gold -= OGUIM.currentRoom.room.bet;
                ResetChips();
            }
        }
        else if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
        }
        else
        {
            Debug.Log("Vui lòng đợi..!");
        }
    }

    public void btnCancel_clicked()
    {
        if (gameManager.fsm.IsInState("IdleState"))
        {
			gameManager.isNewGame = true;
			for (int i = 0; i < instance.gameManager.bets.Length; i++) 
			{
				OGUIM.me.gold += instance.gameManager.bets [i] * OGUIM.currentRoom.room.bet;
			}
			ClearAllBet();
			ResetChips();
			gameManager.ResetGame(true);
        }
        else if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
        }
        else
        {
            Debug.Log("Vui lòng đợi..!");
        }
    }

    public void btnTaiXiu_clicked(bool tai)
    {
        if (gameManager.fsm.IsInState("WinState"))
        {
            gameManager.ExitWinEvent();
			int bet = System.Convert.ToInt32 (txtWinChips.text);
			BuildWarpHelper.XENG_BetTaiXiu(bet, tai, () =>
				{
					UILogView.Log("Tai Xiu is timeout");
					OGUIM.Toast.Hide();
				});
        }
    }

    void StackChip(bool down, int step = 1)
    {
        
		var offset = Mathf.Min(gameManager.data.result.winChips, step);
		instance.gameManager.data.result.winChips -= (down ? 1 : -1) * offset * OGUIM.currentRoom.room.bet;
		OGUIM.me.gold -= (down ? -1*OGUIM.currentRoom.room.bet : OGUIM.currentRoom.room.bet) * offset;;
		SetWinChips(instance.gameManager.data.result.winChips / OGUIM.currentRoom.room.bet);
		ResetChips(true);

        if (instance.gameManager.data.result.winChips == 0)
        {
            
            BuildWarpHelper.XENG_ReceiveMoney(() =>
            {
                UILogView.Log("ReceiveMoney is timeout");
                OGUIM.Toast.Hide();
            });
        }
    }

    public void btnStack_clicked(bool down)
    {
        Debug.Log("Stack chip info");
        if (down)
        {
			if(gameManager.data.result.winChips > 0)
                StackChip(true);
        }
        else
        {
			if(gameManager.data.result.winChips < gameManager.originWinChip)
                StackChip(false);
        }
    }

    public void ShowTopUp()
    {
        if (GameBase.underReview)
        {
            #if UNITY_ANDROID || UNITY_IOS
            OGUIM.instance.popupIAP.Show();
            return;
            #endif
        }
        if (GameBase.cash_in == 0)
            return;
        OGUIM.instance.Cash_In_Feature = PlayerPrefs.GetInt(Common.CASH_IN_FEATURE, 0);
        if (OGUIM.instance.Cash_In_Feature == 0)
        {
            OGUIM.instance.authPopup.Show(AuthenType.CASH_IN);
            return;
        }
        OGUIM.instance.popupTopUp.Show();
    }

    public void ShowUserHistory()
    {

        if (OGUIM.instance != null)
        {
            //OGUIM.instance.popupHistory.ShowOnly(0);
            OGUIM.instance.popupHistory.Show(0);
        }
    }

    private void OnDestroy()
    {
        if (musicCheck)
        {
            UIManager.instance.musicToggle.isOn = true;
            UIManager.instance.UpdateMusicState();
        }
    }
}
