﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LuckMoveType1State : XENGState
{
    int currentIndex;
    int targetIndex;
    bool isPlaySound;
    int dir = 1;
    public override void Enter(XENGGameManager entity)
    {
        //Debug.Log("LuckMoveType1State ");
		if (entity.numberLightVisible < entity.data.result.luckyPosition.Count)
        {
            entity.numberLightVisible++;
        }
        else
        {
            entity.TurnLight(currentIndex, false);
            entity.fsm.ChangeState(entity.luckEndState);
            return;
        }

        currentIndex = entity.GetCurrentIndex();
        isPlaySound = false;
        dir = -dir;

        if (entity.numberLightVisible == 1)
        {
            targetIndex = entity.data.result.luckyPosition[0] - 1;
        }
        else if (entity.numberLightVisible == 2)
        {
			currentIndex = entity.data.result.luckyPosition[0] - 1;
			targetIndex = entity.data.result.luckyPosition[1] - 1;
        }
        else if (entity.numberLightVisible == 3)
        {
			currentIndex = entity.data.result.luckyPosition[1] - 1;
			targetIndex = entity.data.result.luckyPosition[2] - 1;
        }
        else if (entity.numberLightVisible == 4)
        {
			currentIndex = entity.data.result.luckyPosition[2] - 1;
			targetIndex = entity.data.result.luckyPosition[3] - 1;
        }
        else if (entity.numberLightVisible == 5)
        {
			currentIndex = entity.data.result.luckyPosition[3] - 1;
			targetIndex = entity.data.result.luckyPosition[4] - 1;
        }

        timeDelay1 = 0.02f;
        timeDelay2 = 0.01f;
        cooldownTime = 0.5f;
        //Debug.Log("LuckMoveType1State sound ?? ");
        entity.PlaySound("luckMoveType1", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        //Debug.Log("LuckMoveType1State Execute ?? " + currentIndex + "     " + targetIndex);
        if(targetIndex == -1)
        {
            targetIndex = 0;
        }
        if(currentIndex != targetIndex)
        {
            timeDelay2 -= deltaTime;
            if(timeDelay2 <= 0)
            {
                entity.TurnLight(currentIndex, false);

                currentIndex += dir;
                if (currentIndex < 0)
                    currentIndex = 23;
                if (currentIndex > 23)
                    currentIndex = 0;

                entity.TurnLight(currentIndex);
                timeDelay2 = timeDelay1;
            }
        }
        else
        {
            if (!isPlaySound)
            {
                entity.PlaySound("itemLuckChoose", 1);
                isPlaySound = true;
            }
            entity.HighLight(currentIndex, true);
            cooldownTime -= deltaTime;
            if(cooldownTime <= 0)
            {
                //Debug.Log("LuckMoveType1State cooldownTime ?? ");
                entity.fsm.ChangeState(entity.luckStartState);
                return;
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
