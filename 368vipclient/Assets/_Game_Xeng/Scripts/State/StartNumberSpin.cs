﻿using System;
using System.Collections.Generic;
using System.Linq;
class StartNumberSpin : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        cooldownTime = 1;
        timeDelay1 = 0.02f;
        timeDelay2 = 0.012f;
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        cooldownTime -= deltaTime;
        timeDelay2 -= deltaTime;
        if(timeDelay2 <= 0)
        {
            var number = UnityEngine.Random.Range(1, 15);
			UIXeng.SetTaiXiu(number);
            entity.PlaySound("spinNumber", 0);
            timeDelay2 = timeDelay1;
        }
        if(cooldownTime <= 0)
        {
            entity.fsm.ChangeState(entity.endNumberSpin);
            return;
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
