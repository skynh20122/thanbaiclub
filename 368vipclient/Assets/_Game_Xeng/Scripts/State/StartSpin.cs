﻿using System;
using System.Collections.Generic;
using System.Linq;

public class StartSpin : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        //disable button Cancel

        //disable button Spin

        //disable 2 button Change chip

        timeDelay1 = 0.34f;
        timeDelay2 = 0.34f;

        entity.PlaySound("startSpin", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        timeDelay2 -= deltaTime;
        if(timeDelay2 <= 0)
        {
            if (timeDelay1 > 0.01)
                timeDelay1 -= 0.04f;

            if(timeDelay1 < 0.05)
            {
                //Change state to fast spin
                entity.fsm.ChangeState(entity.fastSpin);
                return;
            }
            timeDelay2 = timeDelay1;
            entity.MoveLightPosition();
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
