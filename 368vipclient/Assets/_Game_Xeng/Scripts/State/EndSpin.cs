﻿using System;
using System.Collections.Generic;
using System.Linq;
public class EndSpin : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        timeDelay1 = 0.02f;
        timeDelay2 = 0.012f;
        entity.PlaySound("endSpin", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        timeDelay2 -= deltaTime;
		if(entity.GetCurrentIndex() != entity.data.result.pos - 1 && timeDelay2 <= 0)
        {
            timeDelay1 += 0.08f;

            entity.MoveLightPosition();

            timeDelay2 = timeDelay1;
        }
		else if(entity.GetCurrentIndex() == entity.data.result.pos - 1)
        {
            entity.isNewGame = false;
            entity.PlaySound("choose", 1);
            entity.HighLight(entity.GetCurrentIndex(), true);
			if ((entity.data.result.pos - 1) % 12 == 5)
            {
                entity.fsm.ChangeState(entity.luckWaitState);
                return;
            }
			else if(entity.data.result.luckyPosition.Count > 0)
            {
                entity.fsm.ChangeState(entity.luckStartState);
                return;
            }
            else if (entity.goToWinState)
            {
                entity.fsm.ChangeState(entity.winState);
                return;
            }
            else
            {
                entity.fsm.ChangeState(entity.idleState);
                return;
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
