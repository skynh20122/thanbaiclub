﻿
using System.Linq;
using DG.Tweening;

public class WinState : XENGState
{
    int number;
    public override void Enter(XENGGameManager entity)
    {
        UnityEngine.Debug.Log("Go to " + name);
        entity.goToWinState = false;
        if (entity.data.result.winChips > 0)
        {
            UIXeng.SpawnTextEfx(entity);
            UIXeng.fillChip();
        }
        
        UIXeng.SetWinChips(entity.data.result.winChips / OGUIM.currentRoom.room.bet);
        timeDelay1 = 0.02f;
        number = 0;
		UIXeng.instance.isSpining = false;
        if (entity.inSpinWinEvent)
        {
            entity.inSpinWinEvent = false;
            startWinEvent(entity);
			UIXeng.ClearAllBet();
        }
        else if (entity.inLuckWinEvent)
        {
            entity.inLuckWinEvent = false;
            var currIndex = entity.GetCurrentIndex();
            if (currIndex % 12 != 5)
            {
                entity.PlaySound(entity.itemName[currIndex], 1);
				for(int i = 0; i < entity.data.result.luckyPosition.Count; i++)
                {
                    int luckyIndex = (entity.data.result.luckyPosition[i] - 1) % entity.itemName.Length;
                    if (luckyIndex < 0) luckyIndex = 0;
                    DOVirtual.DelayedCall(1f * (i + 1), () => {
                        entity.PlaySound(entity.itemName[luckyIndex], 1);
                    });
                }
				var timeDelay = (entity.data.result.luckyPosition.Count + 1) * 1f;
                DOVirtual.DelayedCall(timeDelay, () => startWinEvent(entity));
            }
            else
            {
				var luckyIndex = entity.data.result.luckyPosition[0] - 1;
                if (luckyIndex < 0) luckyIndex = 0;
                entity.PlaySound(entity.itemName[luckyIndex], 1);
				if (entity.data.result.luckyPosition.Count > 1)
                {
					for (int i = 1; i < entity.data.result.luckyPosition.Count; i++)
                    {
						int luck = entity.data.result.luckyPosition[i] - 1;
                        DOVirtual.DelayedCall(1f * i, () => {
                            entity.PlaySound(entity.itemName[luck], 1);
                        });
                    }
                }
				var timeDelay = 1f + (entity.data.result.luckyPosition.Count - 1) * 1f;
                DOVirtual.DelayedCall(timeDelay, () => startWinEvent(entity));
            }

			//var txtBets = UIXeng.GetTxtBets();
     //       var animBets = UIXeng.GetAnimBets();
     //       if (animBets != null)
     //       {
     //           for (int i = 0; i < animBets.Length; i++)
     //           {
					//if (entity.data.result.luckyPosition.Any(x=> entity.Equals(x-1, i)))
            //            animBets[i].SetBool("isOn", true);
            //        //txtBets[i].text = "00";
            //    }


            //    for (int i = 0; i < animBets.Length; i++)
            //    {
            //        if (entity.Equals(entity.GetCurrentIndex(), i))
            //            animBets[i].SetBool("isOn", true);
            //        //txtBets[i].text = "00";


            //    }
            //}
        }
        else
        {
			//var txtBets = UIXeng.GetTxtBets();
            //var animBets = UIXeng.GetAnimBets();
            //if(animBets != null)
            //{
            //    for (int i = 0; i < animBets.Length; i++)
            //    {
            //        if (entity.Equals(entity.GetCurrentIndex(), i))
            //            animBets[i].SetBool("isOn", true);
            //        //txtBets[i].text = "00";


            //    }
            //}
            entity.PlaySound(entity.itemName[entity.GetCurrentIndex()], 1);
            DOVirtual.DelayedCall(1f, () => startWinEvent(entity));
        }
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        
    }

    public override void Exit(XENGGameManager entity)
    {
        
    }

    void startWinEvent(XENGGameManager entity)
    {
        entity.inWinEvent = true;
        entity.PlaySound("C" + UnityEngine.Random.Range(1, 3).ToString("D2"), 0);
        if(entity.data.result.winChips > entity.data.result.betChips * 5)
			UIXeng.DoJackPot();

        DOVirtual.DelayedCall(4f, () => entity.fsm.ChangeState(entity.idleState));
    }
}
