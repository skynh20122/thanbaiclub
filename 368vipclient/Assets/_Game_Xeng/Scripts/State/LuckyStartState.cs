﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class LuckyStartState : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        //entity.result.Data.isLuck = false;

        cooldownTime = 0.5f;
        timeDelay1 = 0.02f;
        timeDelay2 = 0.012f;
        Debug.Log("Go to LuckyStartState " + cooldownTime);

        entity.PlaySound("waitToMove", 0);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        if(cooldownTime > 0)
        {
            cooldownTime -= deltaTime;
            timeDelay2 -= deltaTime;

            if(timeDelay2 <= 0)
            {
                entity.PlaySound("waitToMove", 0);
                timeDelay2 = timeDelay1;
            }
        }
        else
        {
            if(entity.luckType == 1)
            {
                Debug.Log("Go to luckType1 ");
                entity.fsm.ChangeState(entity.luckMoveType1);
                return;
            }
            else
            {
                Debug.Log("Go to luckType2 ");
                entity.fsm.ChangeState(entity.luckMoveType2);
                return;
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
