﻿using System;
using System.Collections.Generic;
using System.Linq;
public class FastSpin : XENGState
{
    public override void Enter(XENGGameManager entity)
    {
        cooldownTime = 1.45f;
        timeDelay1 = 0.01f;
        timeDelay2 = 0.012f;

        entity.PlaySound("fastSpin", 0, true);
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        cooldownTime -= deltaTime;
        timeDelay2 -= deltaTime;

        if(timeDelay2 <= 0)
        {
            entity.MoveLightPosition();
            timeDelay2 = timeDelay1;
        }
        if(cooldownTime <= 0)
        {
			if(entity.GetRangePosition(entity.GetCurrentIndex(), entity.data.result.pos - 1) == 7)
            {
                entity.fsm.ChangeState(entity.endSpin);
                return;
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
