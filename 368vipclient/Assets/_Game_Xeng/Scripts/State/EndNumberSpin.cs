﻿using System;
using System.Collections.Generic;
using System.Linq;
public class EndNumberSpin : XENGState
{
    int number;
    bool playedSound;
    public override void Enter(XENGGameManager entity)
    {
        cooldownTime = 0.2f;
        timeDelay1 = 0.1f;
        timeDelay2 = 0.1f;
        number = 0;
        playedSound = false;
    }

    public override void Execute(XENGGameManager entity, float deltaTime)
    {
        timeDelay2 -= deltaTime;
        if(timeDelay2 <= 0)
        {
            if(timeDelay1 < 0.3f)
            {
                var n = UnityEngine.Random.Range(1, 15);
				UIXeng.SetTaiXiu(n);
                entity.PlaySound("spinNumber", 0);
                timeDelay1 += 0.05f;
                timeDelay2 = timeDelay1;
            }
            else
            {
				UIXeng.SetTaiXiu(entity.currentNumberRandom);
                timeDelay2 = 0.1f;
                cooldownTime -= deltaTime;

                if (!playedSound)
                {
                    playedSound = true;
                    if (entity.goToWinState)
                        entity.PlaySound("win", 0);
                    else
                        entity.PlaySound("lose", 0);
                }
                if(cooldownTime <= 0)
                {
                    //XENGUIController.SetTaiXiu(0);
					UIXeng.SetWinChips(entity.data.result.winChips/OGUIM.currentRoom.room.bet);
					entity.originWinChip = entity.data.result.winChips;
                    if (entity.goToWinState)
                    {
                        entity.inSpinWinEvent = true;
                        entity.fsm.ChangeState(entity.winState);
                        return;
                    }
                    else
                    {
                        entity.fsm.ChangeState(entity.idleState);
                        return;
                    }
                }
            }
        }
    }

    public override void Exit(XENGGameManager entity)
    {
    }
}
